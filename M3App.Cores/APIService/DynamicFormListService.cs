﻿using M3App.Cores.DataModel;
using M3App.Cores.Interface;
using M3App.Cores.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using M3App.Cores;
using System.Net;

namespace M3App.Cores.APIService
{
    public class DynamicFormListService : IDynamicJoblist
    {
        public CoreApp coreapp = new CoreApp();
        DynamicJobListModel JobListmodel = new DynamicJobListModel();
        JobListAccess jobListAccess = new JobListAccess();

        public async Task<DynamicJobListModel> DynamicJobList()
        {
            try
            {
                jobListAccess.access_key = Settings.AccessKey;
                jobListAccess.job_id = Settings.JobIdKey;
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(jobListAccess, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("FormTypelist", content);
                var result = await response.Content.ReadAsStringAsync();
                DynamicJobListModel jobListModel = JsonConvert.DeserializeObject<DynamicJobListModel>(result);
                return jobListModel;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }
        }
        public FormTypeListDownLoadModel Dynamicformdatas(FormTypeList postdata)
        {
            try
            {
                var columns = new Dictionary<string, string>
            {
                { "form_id", postdata.id.ToString() },
                { "job_id", Settings.JobIdKey},
                { "access_key", Settings.AccessKey},
                { "year", Settings.YearKey}
            
            };
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(columns, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                // HttpResponseMessage response = client.PostAsync("FormTypelist/download", content).Result;
                HttpResponseMessage response = client.PostAsync("FormTypelist/download", content).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                FormTypeListDownLoadModel Formlistdata = JsonConvert.DeserializeObject<FormTypeListDownLoadModel>(result);
                return Formlistdata;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }
        }


        //public async Task<FormTypeListDownLoadModel> Dynamicformdatas(FormTypeList postdata)
        //{
        //    try
        //    {
        //        var columns = new Dictionary<string, string>
        //    {
        //        { "form_id", postdata.id.ToString() },
        //        { "job_id", Settings.JobIdKey},
        //        { "access_key", Settings.AccessKey}
        //    };
        //        var client = new HttpClient();
        //        client.BaseAddress = new Uri(coreapp.ApiUri);
        //        string json = JsonConvert.SerializeObject(columns, Formatting.Indented);
        //        var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
        //        client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
        //        var content = new StringContent(json, Encoding.UTF8, "application/json");
        //        // HttpResponseMessage response = client.PostAsync("FormTypelist/download", content).Result;
        //        HttpResponseMessage response = await client.PostAsync("FormTypelist/download", content);
        //        var result = response.Content.ReadAsStringAsync().Result;
        //        FormTypeListDownLoadModel Formlistdata = JsonConvert.DeserializeObject<FormTypeListDownLoadModel>(result);
        //        return Formlistdata;
        //    }
        //    catch (Exception ex)
        //    {
        //        HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
        //        httpResponseMessage.Content = new StringContent(ex.Message);
        //        return null;
        //    }
        //}
        //public async  Task<UploadResponseModel> FormTypeUpload(FormTypeUpload formTypeUpload)
        //{
        //    try
        //    {
        //        var client = new HttpClient();
        //        client.BaseAddress = new Uri(coreapp.ApiUri);
        //        string json = JsonConvert.SerializeObject(formTypeUpload, Formatting.Indented);
        //        var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
        //        client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
        //        var content = new StringContent(json, Encoding.UTF8, "application/json");
        //        HttpResponseMessage response =await  client.PostAsync("FormTypelist/upload", content);
        //        var result =await  response.Content.ReadAsStringAsync();
        //        UploadResponseModel postdata = JsonConvert.DeserializeObject<UploadResponseModel>(result);
        //        return postdata;
        //    }
        //    catch (Exception ex)
        //    {
        //        HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
        //        httpResponseMessage.Content = new StringContent(ex.Message);
        //        return null;
        //    }
        //}


        public async Task<UploadResponseModel> FormTypeUpload(FormTypeUpload formTypeUpload)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(formTypeUpload, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                //HttpResponseMessage response = await client.PostAsync("FormTypelist/upload", content);
                //var result = await response.Content.ReadAsStringAsync();
                //UploadResponseModel postdata = JsonConvert.DeserializeObject<UploadResponseModel>(result);

                HttpResponseMessage response =  client.PostAsync("FormTypelist/upload", content).Result;
                var result = await response.Content.ReadAsStringAsync();
                UploadResponseModel postdata = JsonConvert.DeserializeObject<UploadResponseModel>(result);
                return postdata;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }
        }

        public async Task<ComposemailResponse> Mailresponse(ComposeMail composeMail)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(composeMail, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response =await client.PostAsync("SendEmailReport", content);
                var result =await  response.Content.ReadAsStringAsync();
                ComposemailResponse mail = JsonConvert.DeserializeObject<ComposemailResponse>(result);
                return mail;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }
        }

        public FormStatusModel FormStatus()
        {
            try
            {
                jobListAccess.access_key = Settings.AccessKey;
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(jobListAccess, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync("getM2Status", content).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                FormStatusModel jobStatus = JsonConvert.DeserializeObject<FormStatusModel>(result);
                return jobStatus;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }
        }


    }
}
