﻿using M3App.Cores.DataModel;
using M3App.Cores.Interface;
using M3App.Cores.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace M3App.Cores.APIService
{
    public class JobListService : IJobList
    {
        public CoreApp coreapp = new CoreApp();
        JobListAccess jobListAccess = new JobListAccess();

        public async Task<JobListModel> JobList()
        {
            try
            {
                jobListAccess.access_key = Settings.AccessKey;
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(jobListAccess, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("Joblist", content);
                var result = await response.Content.ReadAsStringAsync();
                JobListModel jobListModel = JsonConvert.DeserializeObject<JobListModel>(result);
                return jobListModel;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }

        }

        public JobStatusModel JobStatus()
        {
            try
            {
                jobListAccess.access_key = Settings.AccessKey;
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(jobListAccess, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync("getJobStatus", content).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                JobStatusModel jobStatus = JsonConvert.DeserializeObject<JobStatusModel>(result);
                return jobStatus;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }
        }

        public JobSaveStatus JobsaveStatus(JobStatus jobStatus)
        {
            try
            {
                var columns = new Dictionary<string, string>
            {
                { "job_id", Settings.JobIdKey },
                { "job_status", jobStatus.name.ToString()},
                { "access_key", Settings.AccessKey}
            };
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(columns, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync("saveJobStatus", content).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                JobSaveStatus jobsaveStatus = JsonConvert.DeserializeObject<JobSaveStatus>(result);
                return jobsaveStatus;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }
        }
    }
}
