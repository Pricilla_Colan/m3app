﻿using M3App.Cores;
using M3App.Cores.DataModel;
using M3App.Cores.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace M3App.Cores.APIService
{
   public class LoginService : ILoginServices
    {
        public CoreApp coreapp = new CoreApp();
        public async Task<LoginModel> login(User user)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(user, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await client.PostAsync("login", content);
                var result = await response.Content.ReadAsStringAsync();
                LoginModel loginModel = JsonConvert.DeserializeObject<LoginModel>(result);
                return loginModel;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }
        }
        public async Task<SignUpModel> Register(SignUp signUp)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(signUp, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync("signup", content);
                var result = await response.Content.ReadAsStringAsync();
                SignUpModel signupmodel = JsonConvert.DeserializeObject<SignUpModel>(result);
                return signupmodel;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }           
        }
        public CompanyListModel CompanyList()
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                HttpResponseMessage response = client.GetAsync("companyList").Result;
                var result = response.Content.ReadAsStringAsync().Result;
                CompanyListModel companylist = JsonConvert.DeserializeObject<CompanyListModel>(result);
                return companylist;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }
            
        }

        public ForgotPasswordModel  forgotpassword(ForgotPasswordata password)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(password, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync("forgotPassword", content).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                ForgotPasswordModel passwordmodel = JsonConvert.DeserializeObject<ForgotPasswordModel>(result);
                return passwordmodel;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }
            
        }

        public RecoverPasswordModel recoverPassword(PasswordRecoverdata recoverpassword)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(coreapp.ApiUri);
                string json = JsonConvert.SerializeObject(recoverpassword, Formatting.Indented);
                var byteArray = Encoding.UTF8.GetBytes("stromTrackerM3Webservice" + ":" + "stromTrackerM3Webservice");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync("passwordRecover", content).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                RecoverPasswordModel recovepwd = JsonConvert.DeserializeObject<RecoverPasswordModel>(result);
                return recovepwd;
            }
            catch (Exception ex)
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
                httpResponseMessage.Content = new StringContent(ex.Message);
                return null;
            }            
        }
    }
}
