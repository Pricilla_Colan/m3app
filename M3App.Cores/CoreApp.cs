﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M3App.Cores
{
    interface ICoreApp
    {

        string ApiUri { get; }

        string AppVersion { get; }
    }

    public class CoreApp : ICoreApp
    {
        public string ApiUri
        {
            get
            {
                string URL = "https://stormtracker-dev.rrtechllc.com/m3-service/api/";
                return URL;

            }
        }
        public string AppVersion
        {
            get
            {
                string Version = "V 1.0";
                return Version;
            }
        }
    }
}
