﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M3App.Cores.DataModel
{
    public class CategoryDetails
    {
        public string CategoryName { get; set; }
        public List<CategoryDetails> llstCategory { get; set; }
    }
}
