﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace M3App.Cores.DataModel
{
    public class CompanyListModel
    {
        public string result { get; set; }

        public string message { get; set; }

        public List<Company> response { get; set; }
    }

    public class Company
    {       

        public int id { get; set; }
        public string name { get; set; }
    }
}
