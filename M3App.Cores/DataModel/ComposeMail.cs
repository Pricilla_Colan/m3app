﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M3App.Cores.DataModel
{
    public class ComposeMail
    {
        public string email_from { get; set; }
        public string email_to { get; set; }
        public int[] attachment { get; set; }
        public string email_description { get; set; }
        public string email_subject { get; set; }
        public string access_key { get; set; }
        public string job_id { get; set; }
    }
}
