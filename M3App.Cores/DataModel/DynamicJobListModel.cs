﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M3App.Cores.DataModel
{
   public class DynamicJobListModel
    {
        public string result { get; set; }

        public string message { get; set; }

        public List<FormTypeList> response { get; set; }
    }

    public class FormTypeList
    {
        public int id { get; set; }
        public string form_name { get; set; }
        public string form_type { get; set; }
        public string form_json { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string form_status { get; set; } 
        public string form_short_name { get; set; }
    }
}
