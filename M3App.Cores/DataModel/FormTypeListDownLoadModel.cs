﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace M3App.Cores.DataModel
{
    public class FormTypeListDownLoadModel
    {
        public string result { get; set; }
        public string message { get; set; }
        public FormTypeResponse response { get; set; }
    }
    public class FormTypeResponse
    {
        public int id { get; set; }
        public string job_id { get; set; }
        public string staff_id { get; set; }
        public string site_id { get; set; }
        public string form_name { get; set; }
        public string form_type { get; set; }
        public FormJson form_json { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string category_id { get; set; }
        public string form_id { get; set; }
        public string access_key { get; set; }
        public string form_status { get; set; }
    }
    public class FormJson
    {
        public List<Form_basic_Info> basic_Info { get; set; }
        public List<Form_category_Info> category_Info { get; set; }
        public List<DownloadComments> comments { get; set; }
    }
    public class Form_basic_Info
    {
        public string label_name { get; set; }
        public string type { get; set; }
        public object value { get; set; }
        public object options { get; set; }
        public string visible { get; set; }
        public string editable { get; set; }
        
    }
    public class Form_category_Info
    {
        public int id { get; set; }
        public int parent_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        //public Boolean sublevel { get; set; }
        public Boolean? sublevel { get; set; }
        public List<category_Info_fields> fields { get; set; }
        public List<Sub_category_Info> sub_category { get; set; }
    }
    public class category_Info_fields
    {
        public string label_name { get; set; }
        public string type { get; set; }
        public object value { get; set; }
        public object options { get; set; }
        public string visible { get; set; }
        public string editable { get; set; }
    }
    public class Sub_category_Info
    {
        public int id { get; set; }
        public int parent_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        //public Boolean sublevel { get; set; }
        public Boolean? sublevel { get; set; }
        public List<Subcategory_Info_fields> fields { get; set; }
    }
    public class Subcategory_Info_fields
    {
        public string label_name { get; set; }
        public string type { get; set; }
        public object options { get; set; }
        public string value { get; set; }
        public string visible { get; set; }
        public string editable { get; set; }
    }
    public class DownloadComments
    {
        public string date { get; set; }
        public string action_taken { get; set; }
        public string initials { get; set; }
        public byte[] MyImage { get; set; }
        public int Index { get; set; }
    }


    //Checking
    //public class BasicInfo
    //{
    //    public string label_name { get; set; }
    //    public string type { get; set; }
    //    public string value { get; set; }
    //    public string visible { get; set; }
    //    public string editable { get; set; }
    //}

    //public class Field
    //{
    //    public string label_name { get; set; }
    //    public string type { get; set; }
    //    public string value { get; set; }
    //    public string visible { get; set; }
    //    public string editable { get; set; }
    //}

    //public class SubCategory
    //{
    //    public string id { get; set; }
    //    public string parent_id { get; set; }
    //    public string name { get; set; }
    //    public string description { get; set; }
    //    public string sublevel { get; set; }
    //    public List<Field> fields { get; set; }
    //}

    //public class CategoryInfo
    //{
    //    public string id { get; set; }
    //    public string parent_id { get; set; }
    //    public string name { get; set; }
    //    public string description { get; set; }
    //    public string sublevel { get; set; }
    //    public List<SubCategory> sub_category { get; set; }
    //}

    //public class FormJsons
    //{
    //    public List<BasicInfo> basic_Info { get; set; }
    //    public List<CategoryInfo> category_Info { get; set; }
    //    public object comments { get; set; }
    //}

    //public class Response
    //{
    //    public string id { get; set; }
    //    public string job_id { get; set; }
    //    public string staff_id { get; set; }
    //    public string site_id { get; set; }
    //    public FormJsons form_json { get; set; }
    //    public string form_id { get; set; }
    //    public string created_at { get; set; }
    //    public string updated_at { get; set; }
    //    public string form_status { get; set; }
    //    public string form_type { get; set; }
    //    public string category_id { get; set; }
    //    public string form_name { get; set; }
    //    public string access_key { get; set; }
    //}

    //public class Form_TypeList_DownLoadModel
    //{
    //    public string result { get; set; }
    //    public string message { get; set; }
    //    public Response response { get; set; }
    //}
}
