﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M3App.Cores.DataModel
{
   public class FormTypeUpload
    {
        public string form_id { get; set; }
        public string job_id { get; set; }
        public string access_key { get; set; }
        public string year { get; set; }
        public string form_status { get; set; }
        public FormJsonUpload form_json { get; set; }
    }

    public class FormJsonUpload
    {
        public List<Form_basic_InfoUpload> basic_Info { get; set; }
        public List<Form_category_Info> category_Info { get; set; }
        public List<UploadComments> comments { get; set; }
    }

    public class Form_basic_InfoUpload
    {        
        public string label_name { get; set;}
        public string type { get; set; }
        public object value { get; set; }
        public object options { get; set; }
        public string visible { get; set; }
        public string editable { get; set; }
    }

    public class Form_category_InfoUpload
    {
        public int id { get; set; }
        public int parent_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Boolean sublevel { get; set; }
        public List<category_Info_fieldsUpload> fields { get; set; }
        public List<Sub_category_InfoUpload> sub_category { get; set; }
    }

    public class UploadComments
    {
        public string date { get; set; }
        public string action_taken { get; set; }
        public string initials { get; set; }

    }

    public  class category_Info_fieldsUpload
    {
        public string label_name { get; set; }
        public string type { get; set; }
        public string value { get; set; }
        public object options { get; set; }
        public string visible { get; set; }
        public string editable { get; set; }
    }

    public class Sub_category_InfoUpload
    {
        public int id { get; set; }
        public int parent_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Boolean sublevel { get; set; }
        public List<Subcategory_Info_fieldsUpload> fields { get; set; }
    }

    public class Subcategory_Info_fieldsUpload
    {
        public string label_name { get; set; }
        public string type { get; set; }
        public string value { get; set; }
        public object options { get; set; }
        public Boolean visible { get; set; }
        public Boolean editable { get; set; }
    }
}
