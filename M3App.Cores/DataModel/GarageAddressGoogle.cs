﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M3App.Cores.DataModel
{
    public class GarageAddressGoogle
    {
        public class LocationGarageInfo
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class NortheastGarageInfo
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class SouthwestGarageInfo
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class ViewportGarageInfo
        {
            public NortheastGarageInfo northeast { get; set; }
            public SouthwestGarageInfo southwest { get; set; }
        }

        public class GeometryGarageInfo
        {
            public LocationGarageInfo location { get; set; }
            public ViewportGarageInfo viewport { get; set; }
        }

        public class OpeningHoursGarageInfo
        {
            public bool open_now { get; set; }
        }

        public class PhotoGarageInfo
        {
            public int height { get; set; }
            public List<string> html_attributions { get; set; }
            public string photo_reference { get; set; }
            public int width { get; set; }
        }

        public class PlusCodeGarageInfo
        {
            public string compound_code { get; set; }
            public string global_code { get; set; }
        }

        public class ResultGarageInfo
        {
            public GeometryGarageInfo geometry { get; set; }
            public string icon { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public OpeningHoursGarageInfo opening_hours { get; set; }
            public List<PhotoGarageInfo> photos { get; set; }
            public string place_id { get; set; }
            public double rating { get; set; }
            public string reference { get; set; }
            public string scope { get; set; }
            public List<string> types { get; set; }
            public string vicinity { get; set; }
            public PlusCodeGarageInfo plus_code { get; set; }
        }

        public class GarageAddressGoogleInfo
        {
            public List<object> html_attributions { get; set; }
            public string next_page_token { get; set; }
            public List<ResultGarageInfo> results { get; set; }
            public string status { get; set; }
        }
    }

    public class LatitudeLongitudeApi
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }
    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Geometry
    {
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> types { get; set; }
    }
}
