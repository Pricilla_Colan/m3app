﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M3App.Cores.DataModel
{
   public class JobListModel
    {
        public string result { get; set;}
        public string message { get; set;}
        public List<Messageresponse> response {get; set;}
    }

    public class Messageresponse
    {
        public string job_id { get; set; }
        public int job_siteid { get; set; }
        public string job_type { get; set; }
        public string job_status { get; set; }
        public string job_scheduled { get; set;}
        public string site_name { get; set;}
        public string assigned_form_id { get; set; }
    }
}

