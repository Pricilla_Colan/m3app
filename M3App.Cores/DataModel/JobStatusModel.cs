﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M3App.Cores.DataModel
{
   public class JobStatusModel
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<JobStatus> response { get; set; }

    }
    public class JobStatus
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class JobSaveStatus
    {
        public string result { get; set; }
        public string message { get; set; }
    }
    public class SampleModel
    {
        public Int32 Id { get; set; }
        public string Status { get; set; }

    }
}
