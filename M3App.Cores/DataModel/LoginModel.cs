﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M3App.Cores.DataModel
{
   public class LoginModel
    {
        public string result { get; set; }
        public string message { get; set; }
        public response response { get; set; }

    }

    public class response
    {
        public int id { get; set; }
        public int companyid { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public string role { get; set; }
        public string profileimage { get; set; }
        public string signature { get; set; }
        public string access_key { get; set; }
    }
}
