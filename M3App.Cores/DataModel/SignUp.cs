﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M3App.Cores.DataModel
{
  public class SignUp
    {
        public string f_name { get; set; }
        public string l_name { get; set; }
        public int company { get; set; }
        public string email { get; set; }
        public string password { get; set;}
        public string confirm_password { get; set;}

    }
}
