﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M3App.Cores.DataModel
{
   public class SignUpModel
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<SignUpresponse> response { get; set; }
    }
    public class SignUpresponse
    {
        public int id { get; set; }
        public int companyid { get; set; }
        public string fname { get; set; } 
        public string lname { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string lastsession { get; set; }
        public string totalsessions { get; set; }
        public string role { get; set; }
        public string profileimage { get; set; }
        public string signature { get; set; }
        public string status { get; set; }
    }
    
}
