// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System.Collections.Generic;

namespace M3App.Cores.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

        #region Setting Constants
        private static List<string> _dskey = new List<string>();
        private static readonly List<string> _dskeydefault = new List<string>();


        private const string UserNameKey = "username_key";
        private static readonly string UserNameDefault = "";

        private const string LoginStatusKey = "LoginStatus_Key";
        private static readonly string LoginStatusDefault = "";

        private const string PasswordKey = "password_key";
        private static readonly string PasswordDefault = "";

        private const string checkTrueKey = "False";
        private static readonly string SessioncheckTrue = string.Empty;

        private const string unCheckTrueKey = "True";
        private static readonly string SessionunCheckTrue = string.Empty;

        private const string SettingsKey = "settings_key";
		private static readonly string SettingsDefault = string.Empty;

        private const string AccessKeyName = "access_key";
        private static readonly string AccessKeyNameDefault = "";

        private const string JobId = "job_key";
        private static readonly string JobIdDefault = "";

        private const string FormId = "form_key";
        private static readonly string FormIdDefault = "";

        private const string YearStatus = "year_key";
        private static readonly string yearDefault = "";

        private const string JobStatus = "jobstatus_key";
        private static readonly string JobStatusDefault = "";

        private const string SiteName = "Site_Name";
        private static readonly string SiteNameDefault = "";

        private const string LoggedEmail = "Logged_Email";
        private static readonly string LoggedNameDefault = "";

        private const string LoggedKey = "Logged_Key";
        private static readonly string LoggedKeyDefault = "";

        #endregion


        public static string GeneralSettings
		{
			get
			{
				return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(SettingsKey, value);
			}
		}
        public static string AccessKey
        {
            get
            { return AppSettings.GetValueOrDefault(AccessKeyName, AccessKeyNameDefault); }
            set
            { AppSettings.AddOrUpdateValue(AccessKeyName, value);}
        }


        public static string checkTrue
        {
            get { return AppSettings.GetValueOrDefault(checkTrueKey, SessioncheckTrue); }
            set { AppSettings.AddOrUpdateValue(checkTrueKey, value); }
        }

        public static List<string> idslist
        {
            get { return _dskey; }
            set
            {
                _dskey=(value == null ? new List<string>() : value);
            }

        }
        public static string LogEmail
        {
            get { return AppSettings.GetValueOrDefault(LoggedEmail, LoggedNameDefault); }
            set { AppSettings.AddOrUpdateValue(LoggedEmail, value); }
        }

        public static string LogKey
        {
            get { return AppSettings.GetValueOrDefault(LoggedKey, LoggedKeyDefault); }
            set { AppSettings.AddOrUpdateValue(LoggedKey, value); }
        }

        public static string unCheckTrue
        {
            get { return AppSettings.GetValueOrDefault(unCheckTrueKey, SessionunCheckTrue); }
            set { AppSettings.AddOrUpdateValue(unCheckTrueKey, value); }
        }

        public static string JobIdKey
        {
            get
            { return AppSettings.GetValueOrDefault(JobId, JobIdDefault); }
            set
            { AppSettings.AddOrUpdateValue(JobId, value); }
        }

        public static string FormIdKey
        {
            get
            { return AppSettings.GetValueOrDefault(FormId, FormIdDefault); }
            set
            { AppSettings.AddOrUpdateValue(FormId, value); }
        }

        public static string JobStatusKey
        {
            get
            { return AppSettings.GetValueOrDefault(JobStatus, JobStatusDefault); }
            set
            { AppSettings.AddOrUpdateValue(JobStatus, value); }
        }
        public static string YearKey
        {
            get
            { return AppSettings.GetValueOrDefault(YearStatus, yearDefault); }
            set
            { AppSettings.AddOrUpdateValue(YearStatus, value); }
        }

        public static string UserName
        {
            get { return AppSettings.GetValueOrDefault(UserNameKey, UserNameDefault); }
            set { AppSettings.AddOrUpdateValue(UserNameKey, value); }
        }
        public static string LoginStatus
        {
            get { return AppSettings.GetValueOrDefault(LoginStatusKey, LoginStatusDefault); }
            set { AppSettings.AddOrUpdateValue(LoginStatusKey,value); }
        }
        public static string Password
        {
            get { return AppSettings.GetValueOrDefault(PasswordKey, PasswordDefault); }
            set { AppSettings.AddOrUpdateValue(PasswordKey, value); }
        }

        public static string SiteNameKey
        {
            get
            {
                return AppSettings.GetValueOrDefault(SiteName, SiteNameDefault);
            
            }
            set
            {
                AppSettings.AddOrUpdateValue(SiteName, value);
            }
        }

    }
}