﻿using M3App.Cores.DataModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace M3App.Cores.Interface
{
  public  interface IDynamicJoblist
    {
        Task<DynamicJobListModel> DynamicJobList();
    }
}
