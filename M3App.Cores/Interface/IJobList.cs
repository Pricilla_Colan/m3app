﻿using M3App.Cores.DataModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace M3App.Cores.Interface
{
    interface IJobList
    {
        Task<JobListModel> JobList();
        JobStatusModel JobStatus();
    }
}
