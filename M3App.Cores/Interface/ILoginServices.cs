﻿using M3App.Cores.DataModel;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace M3App.Cores.Interface
{
    interface ILoginServices
    {
        Task<LoginModel> login(User user);
        Task<SignUpModel> Register(SignUp signUp);
        CompanyListModel CompanyList();
        ForgotPasswordModel forgotpassword(ForgotPasswordata password);
    }
}
