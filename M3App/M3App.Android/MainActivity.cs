﻿using System;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms;
using Acr.UserDialogs;
using M3App.ViewModels;

namespace M3App.Droid
{
    [Activity(Label = "M3App", Icon = "@drawable/Logo", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            Rg.Plugins.Popup.Popup.Init(this, bundle);
            Xamarin.FormsMaps.Init(this, bundle);
            base.OnCreate(bundle);           
            UserDialogs.Init(this);            
            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());

        }


        BackPressModel _press = new BackPressModel();
        public override void OnBackPressed()
        {
            string PageName = Xamarin.Forms.Application.Current.Properties["PageName"] as string;
            if (PageName == "JobList")
            {
                bool? result = DoBack;
                if (result == true)
                {
                    base.OnBackPressed();
                }
                else if (result == null)
                {
                    App.Current.MainPage = new M3App.Views.LoginForm();
                }
            }
            else
            {
                _press.RedirectGridPage(PageName);
                return;
            }
            
        }
        public bool DoBack
        {
            get
            {
                MasterDetailPage mainPage = App.Current.MainPage as MasterDetailPage;  
                if (mainPage != null)
                {
                    bool doBack = mainPage.Detail.Navigation.NavigationStack.Count > 1 || mainPage.IsPresented;
                    if (!doBack)
                    {
                        mainPage.IsPresented = true;
                        return false;
                    }
                    else
                    {
                        //this.doubleBackToExitPressedOnce = true;
                        Toast.MakeText(this, "Please click Back again to exit", ToastLength.Long).Show();
                        return true;
                    }
                }              
                return true;
            }
        }
    }
}

