﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace M3App.Droid
{
    //[Activity(Label = "SplashActvity")]
    [Activity(Label = "M3App", Icon = "@drawable/Logo", Theme = "@style/MainTheme", MainLauncher = true, NoHistory =true)]    /* Theme = "@style/MainTheme"*/
    public class SplashActvity : Activity
    {
        ValueAnimator animator;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SplashLayout);
            System.Threading.ThreadPool.QueueUserWorkItem(o => LoadActivity());
            //animator = ValueAnimator.OfInt(18, 44);
            //animator.Update += (object sender, ValueAnimator.AnimatorUpdateEventArgs e) =>
            //{
            //    int newValue = (int)e.Animation.AnimatedValue;
            //};
        }



        private void LoadActivity()
        {
            // Simulate a long pause
            System.Threading.Thread.Sleep(500);
            RunOnUiThread(() => StartActivity(typeof(MainActivity)));
        }
    }
}