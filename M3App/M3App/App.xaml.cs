﻿using M3App.Cores.Helpers;
using M3App.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace M3App
{
	public partial class App : Application
	{
       

		public  App ()
		{
			InitializeComponent();
            

            if (Settings.LoginStatus ==string.Empty) 
            {
               // Settings.LoginStatus = "Login";
                MainPage = new M3App.Views.LoginForm();
            }
            else if(Settings.LoginStatus== "Logout")
            {
                MainPage = new M3App.Views.LoginForm();
            }
            else
            {
                Navigatepage();
            }
        }
        public async void Navigatepage()
        { 
            var navPage3 = new NavigationPage();
            navPage3.ToolbarItems.Clear();
            navPage3.BarBackgroundColor = Color.FromHex("#1A257F");
            await navPage3.PushAsync(new M3App.Views.JobList() { Title = "Job List" });
            App.Current.MainPage = new MasterDetailPage
            {
                Master = new MasterPage()
                {
                    Title = "Menu",
                },
                Detail = navPage3
            };
        }
       

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
