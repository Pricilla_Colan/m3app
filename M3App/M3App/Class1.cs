﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Linq;
using System.Threading.Tasks;

namespace M3App
{
    public class Class1 : ViewCell
    {
        static int i = 0;
        public  Class1()
        {            
            var image = new Image
            {
                Source = ImageSource.FromFile("file.png"),
                HeightRequest = 50,
            };
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (object sender, EventArgs e) =>
            {
                if (image.HeightRequest < 250)
                {
                    image.HeightRequest = image.Height + 100;
                    ForceUpdateSize();
                }
            };
            image.GestureRecognizers.Add(tapGestureRecognizer);
            var stackLayout = new StackLayout
            {
                
                Padding = new Thickness(20, 5, 5, 5),
                Orientation = StackOrientation.Horizontal,
                Children = {
                    image,
                    new Label { Text = "Form"+" "+i++, VerticalOptions = LayoutOptions.Center }
                }
            };
            View = stackLayout;
        }
    }
}