﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace M3App.CustomControl
{
    public class CustomMap : Map, INotifyPropertyChanged
    {
        new public event PropertyChangedEventHandler PropertyChanged;

        public static readonly BindableProperty CustomPinsProperty =
            BindableProperty.Create(
                "CustomPins",
                typeof(List<CustomPin>),
                typeof(CustomMap),
                new List<CustomPin>(),
                BindingMode.TwoWay);


        public List<CustomPin> CustomPins
        {
            get { return (List<CustomPin>)GetValue(CustomPinsProperty); }
            set { SetValue(CustomPinsProperty, value); }
        }


        //public List<Custompin> _CustomPins;
        //public List<Custompin> CustomPins
        //{
        //    get
        //    {
        //        return _CustomPins;
        //    }
        //    set
        //    {
        //        _CustomPins = value;
        //        NotifyPropertyChanged("CustomPins");
        //    }
        //}

        // public List<Position> RouteCoordinates { get; set; }


        public static readonly BindableProperty RouteCoordinatesProperty =
            BindableProperty.Create(nameof(RouteCoordinates), typeof(List<Position>), typeof(CustomMap), new List<Position>(), BindingMode.TwoWay);

        public List<Position> RouteCoordinates
        {
            get { return (List<Position>)GetValue(RouteCoordinatesProperty); }
            set { SetValue(RouteCoordinatesProperty, value); }
        }


        public CustomMap()
        {
            RouteCoordinates = new List<Position>();
            CustomPins = new List<CustomPin>();
        }


        #region NotifyProperty
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
