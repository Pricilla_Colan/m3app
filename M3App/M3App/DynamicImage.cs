﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace M3App
{
    public class DynamicImage: INotifyPropertyChanged
    {
        public int x { get; set; }
        public int y { get; set; }
        public int num { get; set; }
        public int near { get; set; }
        public int status { get; set; }
        public int yy { get; set; }
        public int xx { get; set; }

        // xx
        private string _imagex;
        public string imagex
        {
            get { return _imagex; }
            set
            {
                _imagex = value;
                OnPropertyChanged(nameof(imagex));
            }
        }

        private string _imageActivex;
        public string imageActivex
        {
            get { return _imageActivex; }
            set
            {
                _imageActivex = value;
                OnPropertyChanged(nameof(imageActivex));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        //[NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
