﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M3App
{
    public interface IDatabaseConnection
    {
        SQLite.SQLiteConnection DbConnection();
    }
}
