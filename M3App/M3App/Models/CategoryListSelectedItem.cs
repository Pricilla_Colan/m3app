﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace M3App.Models
{
    public class CategoryListSelectedItem : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var tt = Application.Current.Properties["ItemList"];
                if (tt != null && tt != "")
                {
                    int src = System.Convert.ToInt32(tt);
                    int des = System.Convert.ToInt32(value);
                    if (src == des)
                    {
                        return value != null ? Color.Gray : Color.Green;
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
