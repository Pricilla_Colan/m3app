﻿using M3App.Cores.DataModel;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace M3App.Models
{
    [Table("FormData")]
    public class FormDataTempStorage : INotifyPropertyChanged
    {
        private string _formid;
        [PrimaryKey, NotNull]
        public string Form_id
        {
            get
            {
                return _formid;
            }
            set
            {
                this._formid = value;
                OnPropertyChanged(nameof(Form_id));
            }
        }

        private string _job_id;
        [NotNull]
        public string Job_id
        {
            get
            {
                return _job_id;
            }
            set
            {
                this._job_id = value;
                OnPropertyChanged(nameof(Job_id));
            }
        }


        private string _access_key;
        [NotNull]
        public string Access_key
        {
            get
            {
                return _access_key;
            }
            set
            {
                this._access_key = value;
                OnPropertyChanged(nameof(Access_key));
            }
        }

        private string _form_status;
        [NotNull]
        public string Form_status
        {
            get
            {
                return _form_status;
            }
            set
            {
                this._form_status = value;
                OnPropertyChanged(nameof(Form_status));
            }
        }

        private string _allformdata;
        [NotNull]
        public string AllFormData
        {
            get
            {
                return _allformdata;
            }
            set
            {
                this._allformdata = value;
                OnPropertyChanged(nameof(AllFormData));
            }
        }
        //
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }
        
    }
    [Table("JobList")]
    public class JobListDataStorage : INotifyPropertyChanged
    {
        private Int32 _tempid;
        [PrimaryKey,NotNull,AutoIncrement]
        public Int32 tempid
        {
            get
            {
                return _tempid;
            }
            set
            {
                this._tempid = value;
                OnPropertyChanged(nameof(tempid));
            }
        }


        private string _jobid;
        //[PrimaryKey, NotNull]
        [NotNull]
        public string job_id
        {
            get
            {
                return _jobid;
            }
            set
            {
                this._jobid = value;
                OnPropertyChanged(nameof(job_id));
            }
        }

        private Int32 _jobsiteid;
        [NotNull]
        public Int32 job_siteid
        {
            get
            {
                return _jobsiteid;
            }
            set
            {
                this._jobsiteid = value;
                OnPropertyChanged(nameof(job_siteid));
            }
        }


        private string _jobtype;
        //[NotNull]
        public string job_type
        {
            get
            {
                return _jobtype;
            }
            set
            {
                this._jobtype = value;
                OnPropertyChanged(nameof(job_type));
            }
        }

        private string _jobstatus;
        //[NotNull]
        public string job_status
        {
            get
            {
                return _jobstatus;
            }
            set
            {
                this._jobstatus = value;
                OnPropertyChanged(nameof(job_status));
            }
        }

        private string _jobscheduled;
        //[NotNull]
        public string job_scheduled
        {
            get
            {
                return _jobscheduled;
            }
            set
            {
                this._jobscheduled = value;
                OnPropertyChanged(nameof(job_scheduled));
            }
        }

        private string _sitename;
        //[NotNull]
        public string site_name
        {
            get
            {
                return _sitename;
            }
            set
            {
                this._sitename = value;
                OnPropertyChanged(nameof(site_name));
            }
        }

        private string _assignedformid;
        //[NotNull]
        public string assigned_form_id
        {
            get
            {
                return _assignedformid;
            }
            set
            {
                this._assignedformid = value;
                OnPropertyChanged(nameof(assigned_form_id));
            }
        }
        //
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }
       
    }
    [Table("FormTypeList")]
    public class FormTypeListDataStorage : INotifyPropertyChanged
    {
        //NotNull,
        private Int32 _tempid;
        [PrimaryKey, AutoIncrement]
        public Int32 tempid
        {
            get
            {
                return _tempid;
            }
            set
            {
                this._tempid = value;
                OnPropertyChanged(nameof(tempid));
            }
        }

        private Int32 _Jobidref;
        //[NotNull]
        public Int32 Jobidref
        {
            get
            {
                return _Jobidref;
            }
            set
            {
                this._Jobidref = value;
                OnPropertyChanged(nameof(Jobidref));
            }
        }
        private Int32 _id;
        //[PrimaryKey, NotNull]
        [NotNull]
        public Int32 id
        {
            get
            {
                return _id;
            }
            set
            {
                this._id = value;
                OnPropertyChanged(nameof(id));
            }
        }

        private string _form_name;
        [NotNull]
        public string form_name
        {
            get
            {
                return _form_name;
            }
            set
            {
                this._form_name = value;
                OnPropertyChanged(nameof(form_name));
            }
        }


        private string _form_type;
        //[NotNull]
        public string form_type
        {
            get
            {
                return _form_type;
            }
            set
            {
                this._form_type = value;
                OnPropertyChanged(nameof(form_type));
            }
        }

        private string _form_json;
        //[NotNull]
        public string form_json
        {
            get
            {
                return _form_json;
            }
            set
            {
                this._form_json = value;
                OnPropertyChanged(nameof(form_json));
            }
        }

        private string _created_at;
        //[NotNull]
        public string created_at
        {
            get
            {
                return _created_at;
            }
            set
            {
                this._created_at = value;
                OnPropertyChanged(nameof(created_at));
            }
        }

        private string _updated_at;
        //[NotNull]
        public string updated_at
        {
            get
            {
                return _updated_at;
            }
            set
            {
                this._updated_at = value;
                OnPropertyChanged(nameof(updated_at));
            }
        }

        private string _form_status;
        //[NotNull]
        public string form_status
        {
            get
            {
                return _form_status;
            }
            set
            {
                this._form_status = value;
                OnPropertyChanged(nameof(form_status));
            }
        }

        private string _form_short_name;
        //[NotNull]
        public string form_short_name
        {
            get
            {
                return _form_short_name;
            }
            set
            {
                this._form_short_name = value;
                OnPropertyChanged(nameof(form_short_name));
            }
        }
        //
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }

    }
    [Table("JobStatus")]
    public class JobStatusStorage : INotifyPropertyChanged
    {

        private Int32 _tempid;
        [PrimaryKey, NotNull, AutoIncrement]
        public Int32 tempid
        {
            get
            {
                return _tempid;
            }
            set
            {
                this._tempid = value;
                OnPropertyChanged(nameof(tempid));
            }
        }

        private Int32 _id;
        [NotNull]
        public Int32 id
        {
            get
            {
                return _id;
            }
            set
            {
                this._id = value;
                OnPropertyChanged(nameof(id));
            }
        }

        private string _name;
        [NotNull]
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                this._name = value;
                OnPropertyChanged(nameof(name));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }
    }
    [Table("FormDataEntireDisplay")]
    public class FormDataDisplay: INotifyPropertyChanged
    {
        private Int32 _tempid;
        [PrimaryKey, AutoIncrement]
        public Int32 tempid
        {
            get
            {
                return _tempid;
            }
            set
            {
                this._tempid = value;
                OnPropertyChanged(nameof(tempid));
            }
        }
        private Int32 _id;
        //[NotNull]
        public Int32 id
        {
            get
            {
                return _id;
            }
            set
            {
                this._id = value;
                OnPropertyChanged(nameof(id));
            }
        }
        private string _job_id;
        //[NotNull]
        public string job_id
        {
            get
            {
                return _job_id;
            }
            set
            {
                this._job_id = value;
                OnPropertyChanged(nameof(job_id));
            }
        }
        private string _staff_id;
        //[NotNull]
        public string staff_id
        {
            get
            {
                return _staff_id;
            }
            set
            {
                this._staff_id = value;
                OnPropertyChanged(nameof(staff_id));
            }
        }
        private string _site_id;
        //[NotNull]
        public string site_id
        {
            get
            {
                return _site_id;
            }
            set
            {
                this._site_id = value;
                OnPropertyChanged(nameof(site_id));
            }
        }
        private string _form_name;
        //[NotNull]
        public string form_name
        {
            get
            {
                return _form_name;
            }
            set
            {
                this._form_name = value;
                OnPropertyChanged(nameof(form_name));
            }
        }
        private string _form_type;
        //[NotNull]
        public string form_type
        {
            get
            {
                return _form_type;
            }
            set
            {
                this._form_type = value;
                OnPropertyChanged(nameof(form_type));
            }
        }
        private string _form_json;
        //[NotNull]
        public string form_json
        {
            get
            {
                return _form_json;
            }
            set
            {
                this._form_json = value;
                OnPropertyChanged(nameof(form_json));
            }
        }
        private string _created_at;
        //[NotNull]
        public string created_at
        {
            get
            {
                return _created_at;
            }
            set
            {
                this._created_at = value;
                OnPropertyChanged(nameof(created_at));
            }
        }
        private string _updated_at;
        //[NotNull]
        public string updated_at
        {
            get
            {
                return _updated_at;
            }
            set
            {
                this._updated_at = value;
                OnPropertyChanged(nameof(updated_at));
            }
        }
        private string _category_id;
        //[NotNull]
        public string category_id
        {
            get
            {
                return _category_id;
            }
            set
            {
                this._category_id = value;
                OnPropertyChanged(nameof(category_id));
            }
        }
        private string _form_id;
       // [NotNull]
        public string form_id
        {
            get
            {
                return _form_id;
            }
            set
            {
                this._form_id = value;
                OnPropertyChanged(nameof(form_id));
            }
        }
        private string _access_key;
        //[NotNull]
        public string access_key
        {
            get
            {
                return _access_key;
            }
            set
            {
                this._access_key = value;
                OnPropertyChanged(nameof(access_key));
            }
        }
        private string _form_status;
        //[NotNull]
        public string form_status
        {
            get
            {
                return _form_status;
            }
            set
            {
                this._form_status = value;
                OnPropertyChanged(nameof(form_status));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }
    }
    [Table("FormStatusList")]
    public class Form_Status : INotifyPropertyChanged
    {
        private Int32 _tempid;
        [PrimaryKey, NotNull, AutoIncrement]
        public Int32 tempid
        {
            get
            {
                return _tempid;
            }
            set
            {
                this._tempid = value;
                OnPropertyChanged(nameof(tempid));
            }
        }

        private string _form_status;
        [NotNull]
        public string form_status
        {
            get
            {
                return _form_status;
            }
            set
            {
                this._form_status = value;
                OnPropertyChanged(nameof(form_status));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }
    }
    [Table("JobStatusSave")]
    public class JobStatus_Save
    {
        private Int32 _id;
        [PrimaryKey, NotNull]
        public Int32 id
        {
            get
            {
                return _id;
            }
            set
            {
                this._id = value;
                OnPropertyChanged(nameof(id));
            }
        }

        private string _name;
        [NotNull]
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                this._name = value;
                OnPropertyChanged(nameof(name));
            }
        }

        private string _jobid;
        [NotNull]
        public string jobid
        {
            get
            {
                return _jobid;
            }
            set
            {
                this._jobid = value;
                OnPropertyChanged(nameof(jobid));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this,
              new PropertyChangedEventArgs(propertyName));
        }
    }
}

