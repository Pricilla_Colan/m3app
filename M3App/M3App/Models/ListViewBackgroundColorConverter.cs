﻿using M3App.Cores.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace M3App.Models
{
   public class ListViewBackgroundColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {

                var ttm = Settings.idslist;


                if (ttm != null && ttm.Count > 0)
                {
                    foreach (var item in ttm)
                    {
                        int src = System.Convert.ToInt32(item);
                        int des = System.Convert.ToInt32(value);
                        if (src == des)
                        {
                            return value != null ? Color.Gray : Color.Green;
                        }
                    }
                }
            }
            catch(Exception e)
            {

            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // throw new NotImplementedException();
            return null;
        }

    }
}
