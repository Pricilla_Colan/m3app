﻿using SQLite;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using M3App.Cores.DataModel;
using System;
using Newtonsoft.Json;
using M3App.Cores.Helpers;
using Plugin.Connectivity;

namespace M3App.Models
{
    public class LocalStorageAllFields
    {
        private SQLiteConnection database;
        private static object collisionLock = new object();
        public ObservableCollection<FormDataTempStorage> localformdata { get; set; }
        FormTypeUpload _frmtypedatas;

        private SQLiteConnection jssave;
        public ObservableCollection<JobStatus_Save> jobstatussave { get; set; }

        #region
        public async void FormDataAccess(FormTypeUpload _datas)
        {
            try
            {

                database =
                  DependencyService.Get<IDatabaseConnection>().
                  DbConnection();
                database.CreateTable<FormDataTempStorage>();
                this.localformdata =
                  new ObservableCollection<FormDataTempStorage>(database.Table<FormDataTempStorage>());
                // If the table is empty, initialize the collection
                //_frmtypedatas = _datas;
                if (!database.Table<FormDataTempStorage>().Any())
                {
                    AddNewFormDatas(_datas);
                }
                FormDataTempStorage _form = new FormDataTempStorage();
                _form.Form_id = _datas.form_id;
                _form.Job_id = _datas.job_id;

                _form.Access_key = _datas.access_key;
                _form.Form_status = _datas.form_status;
                var jsonformatealldatas = JsonConvert.SerializeObject(_datas.form_json);
                var jsonj1 = JsonConvert.DeserializeObject(jsonformatealldatas);
                _form.AllFormData = jsonformatealldatas;
                SaveFormData(_form);
            }
            catch (Exception ex)
            {

            }
        }
        public void AddNewFormDatas(FormTypeUpload _datas)
        {
            try
            {
                this.localformdata.
                  Add(new FormDataTempStorage
                  {
                      Form_id = "Form_id",
                      Job_id = "Job_id",
                      Access_key = "Access_key",
                      Form_status = "Form_status",
                      AllFormData = "AllFormData",
                  });
            }
            catch(Exception ex)
            {

            }
        }

        public void SelectAllDatas()
        {
            //var query = (from cust in database.Table<FormDataTempStorage>()
            //             select cust).ToList();
            try
            {
                var query = (from cust in database.Table<FormDataTempStorage>()
                             select cust).ToList();
            }
            catch (Exception ex)
            {

            }
        }
        public void SaveFormData(FormDataTempStorage customerInstance)
        {
            try
            {
                var tempjoblisto = (from cust in database.Table<FormDataTempStorage>()
                                    select cust).ToList();

                var tempjoblist = (from cust in database.Table<FormDataTempStorage>()
                                   where cust.Form_id == customerInstance.Form_id
                                   select cust).ToList();
                if (tempjoblist.Count > 0)
                {
                    foreach (var item in tempjoblist)
                    {
                        database.Delete<FormDataTempStorage>(item.Form_id);
                    }
                    lock (collisionLock)
                    {
                        database.Insert(customerInstance);
                    }
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        XFToast.LongMessage("Datas saved successfully on offline");
                    }
                }
                else
                {

                    lock (collisionLock)
                    {
                        database.Insert(customerInstance);
                    }
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        XFToast.LongMessage("Datas saved successfully on offline");
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        //JobStatusSave

        public async void JobStatusLocalSave(JobStatus_Save _datas)
        {
            try
            {
                jssave =
                  DependencyService.Get<IDatabaseConnection>().
                  DbConnection();
                jssave.CreateTable<JobStatus_Save>();
                this.jobstatussave =
                  new ObservableCollection<JobStatus_Save>(jssave.Table<JobStatus_Save>());
               
                if (!database.Table<JobStatus_Save>().Any())
                {
                    AddJobStatus(_datas);
                }
                JobStatus_Save _form = new JobStatus_Save();
                _form.id = _datas.id;
                _form.name = _datas.name;
                _form.jobid = Settings.JobIdKey;
                SaveFormData(_form);
            }
            catch (Exception ex)
            {

            }
        }
        public void AddJobStatus(JobStatus_Save _datas)
        {
            try
            {
                this.jobstatussave.
                  Add(new JobStatus_Save
                  {
                      id = 0,
                      name = "name",
                      jobid = "jobid"
                  });
            }
            catch (Exception ex)
            {

            }
        }

        public void SelectJobStatus()
        {
            try
            {
                var query = (from cust in jssave.Table<JobStatus_Save>()
                             select cust).ToList();
            }
            catch (Exception ex)
            {

            }
        }
        public void SaveFormData(JobStatus_Save customerInstance)
        {
            try {
                var tempjoblisto = (from cust in jssave.Table<JobStatus_Save>()
                                    select cust).ToList();

                var tempjoblist = (from cust in jssave.Table<JobStatus_Save>()

                                   select cust).ToList();
                if (tempjoblist.Count > 0)
                {
                    foreach (var item in tempjoblist)
                    {
                        // database.Delete<JobStatus_Save>(item.Form_id);
                    }
                    lock (collisionLock)
                    {
                        jssave.Insert(customerInstance);
                    }
                    XFToast.LongMessage("Datas saved successfully on offline");
                }
                else
                {

                    lock (collisionLock)
                    {
                        jssave.Insert(customerInstance);
                    }
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        XFToast.LongMessage("Datas saved successfully on offline");
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
