﻿using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace M3App.Models
{
    public class RetrieveOfflineDatas
    {
        private SQLiteConnection jobl;
        private static object collisionLock = new object();
        public ObservableCollection<JobListDataStorage> localformdata { get; set; }

        private SQLiteConnection forml;
        public ObservableCollection<FormTypeListDataStorage> formlistdata { get; set; }

        private SQLiteConnection jbsts;
        public ObservableCollection<JobStatusStorage> jobstsdata { get; set; }

        private SQLiteConnection formdis;
        public ObservableCollection<FormDataDisplay> formdisplaystorage { get; set; }

        private SQLiteConnection formsts;
        public ObservableCollection<Form_Status> formstslst { get; set; }

        private SQLiteConnection database;
        public ObservableCollection<FormDataTempStorage> localformdata_sav { get; set; }

        private SQLiteConnection jobstsave;
        public ObservableCollection<JobStatus_Save> localjobstsave { get; set; }

        #region
        //jobList
        public async void JobListDataAccess(List<JobListDataStorage> _datas)
        {
            try
            {
                jobl =
                  DependencyService.Get<IDatabaseConnection>().
                  DbConnection();
                jobl.CreateTable<JobListDataStorage>();
                this.localformdata =
                  new ObservableCollection<JobListDataStorage>(jobl.Table<JobListDataStorage>());
               
                if (!jobl.Table<JobListDataStorage>().Any())
                {
                    AddNewFormDatas();
                }
                
                DataSaving(_datas);
                SelectAllDatas();
            }
            catch (Exception ex)
            {

            }

        }

        public void DataSaving(List<JobListDataStorage> _datas)
        {
            try
            {
                var tempjoblist = (from cust in jobl.Table<JobListDataStorage>()
                                   select cust).ToList();
                if (tempjoblist.Count > 0)
                {
                    foreach (var item in tempjoblist)
                    {
                        jobl.Delete<JobListDataStorage>(item.tempid);
                    }
                    JobListDataStorage _form = new JobListDataStorage();
                    foreach (var item in _datas)
                    {
                        _form.job_id = item.job_id;
                        _form.job_siteid = item.job_siteid;
                        _form.job_type = item.job_type;
                        _form.job_status = item.job_status;
                        _form.job_scheduled = item.job_scheduled;
                        _form.site_name = item.site_name;
                        _form.assigned_form_id = item.assigned_form_id;
                        SaveFormData(_form);
                    }
                }
                else
                {
                    JobListDataStorage _form = new JobListDataStorage();
                    foreach (var item in _datas)
                    {
                        _form.job_id = item.job_id;
                        _form.job_siteid = item.job_siteid;
                        _form.job_type = item.job_type;
                        _form.job_status = item.job_status;
                        _form.job_scheduled = item.job_scheduled;
                        _form.site_name = item.site_name;
                        _form.assigned_form_id = item.assigned_form_id;
                        SaveFormData(_form);
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }

        public void AddNewFormDatas()
        {
            try
            {
                this.localformdata.
                  Add(new JobListDataStorage
                  {
                      job_id = "Jobid",
                      job_siteid = 0,
                      job_type = "Jobtype",
                      job_status = "Jobstatus",
                      job_scheduled = "Jobscheduled",
                      site_name = "Sitename",
                      assigned_form_id = "Assignedformid"
                  });
            }
            catch (Exception ex)
            {

            }
        }

        public List<JobListDataStorage> SelectAllDatas()
        {
            List<JobListDataStorage> tempjoblist = null;
            try
            {
                jobl =
             DependencyService.Get<IDatabaseConnection>().
             DbConnection();
                jobl.CreateTable<JobListDataStorage>();
                this.localformdata =
                  new ObservableCollection<JobListDataStorage>(jobl.Table<JobListDataStorage>());
                if (jobl.Table<JobListDataStorage>().Any())
                {
                    tempjoblist = (from cust in jobl.Table<JobListDataStorage>()
                                   select cust).ToList();
                }
                return tempjoblist;
            }
            catch(Exception ex)
            {

            }
            return tempjoblist;
        }
        public void SaveFormData(JobListDataStorage _storagedataInstance)
        {
            try
            {
                lock (collisionLock)
                {
                    jobl.Insert(_storagedataInstance);
                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion
        #region 
        //FormTypeList


        public async void FormTypeListDataAccess(List<FormTypeListDataStorage> _datas,int jobid)
        {
            try
            {
                forml =
                  DependencyService.Get<IDatabaseConnection>().
                  DbConnection();

                forml.CreateTable<FormTypeListDataStorage>();
                this.formlistdata =
                  new ObservableCollection<FormTypeListDataStorage>(forml.Table<FormTypeListDataStorage>());

                if (!forml.Table<FormTypeListDataStorage>().Any())
                {
                    AddNewFormDatas_List();
                }
                DataSaving_FormList(_datas,jobid);
                SelectAllDatas_FormList(jobid);
            }
            catch (Exception ex)
            {

            }

        }

        public void DataSaving_FormList(List<FormTypeListDataStorage> _datas,int jobid)
        {
            try
            {

                var tempjoblist = (from cust in forml.Table<FormTypeListDataStorage>()
                                   where cust.Jobidref == jobid
                                   select cust).ToList();
                if (tempjoblist.Count > 0)
                {
                    foreach (var item in tempjoblist)
                    {
                        forml.Delete<FormTypeListDataStorage>(item.tempid);
                    }
                    FormTypeListDataStorage _form = new FormTypeListDataStorage();
                    foreach (var item in _datas)
                    {
                        _form.id = item.id;
                        _form.Jobidref = jobid;
                        _form.form_name = item.form_name;
                        _form.form_type = item.form_name;
                        _form.form_json = item.form_status;
                        _form.created_at = item.created_at;
                        _form.updated_at = item.updated_at;
                        _form.form_status = item.form_status;
                        _form.form_short_name = item.form_short_name;
                        SaveFormData_FormList(_form);
                    }
                }
                else
                {
                    FormTypeListDataStorage _form = new FormTypeListDataStorage();
                    foreach (var item in _datas)
                    {
                        _form.id = item.id;
                        _form.Jobidref = jobid;
                        _form.form_name = item.form_name;
                        _form.form_type = item.form_name;
                        _form.form_json = item.form_status;
                        _form.created_at = item.created_at;
                        _form.updated_at = item.updated_at;
                        _form.form_status = item.form_status;
                        _form.form_short_name = item.form_short_name;
                        SaveFormData_FormList(_form);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void AddNewFormDatas_List()
        {
            try
            {
                this.formlistdata.
                  Add(new FormTypeListDataStorage
                  {
                      id = 0,
                      form_name = "form_name",
                      form_type = "form_type",
                      form_json = "form_json",
                      created_at = "created_at",
                      updated_at = "updated_at",
                      form_status = "form_status",
                      form_short_name = "form_short_name"
                  });
            }
            catch (Exception ex)
            {

            }
        }

        public List<FormTypeListDataStorage> SelectAllDatas_FormList(int jobid)
        {
            List<FormTypeListDataStorage> tempjoblist = null;
            try
            {
                forml =
             DependencyService.Get<IDatabaseConnection>().
             DbConnection();
                forml.CreateTable<FormTypeListDataStorage>();
                this.formlistdata =
                  new ObservableCollection<FormTypeListDataStorage>(forml.Table<FormTypeListDataStorage>());
                if (forml.Table<FormTypeListDataStorage>().Any())
                {
                  var  tempjoblist1 = (from cust in forml.Table<FormTypeListDataStorage>()
                                   select cust).ToList();
                    tempjoblist = (from cust in forml.Table<FormTypeListDataStorage>()
                                   where cust.Jobidref == jobid
                                   select cust).ToList();
                }
            }
            catch(Exception ex)
            {

            }

            //var query = (from cust in jobl.Table<JobListDataStorage>()
            //             select cust).ToList();
            return tempjoblist;
        }
        public void SaveFormData_FormList(FormTypeListDataStorage _storagedataInstance)
        {
            try
            {
                lock (collisionLock)
                {
                    forml.Insert(_storagedataInstance);
                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion
        #region
        //JobStatusList


        public async void JobStatusList(List<JobStatusStorage> _datas)
        {
            try
            {
                jbsts =
                  DependencyService.Get<IDatabaseConnection>().
                  DbConnection();

                jbsts.CreateTable<JobStatusStorage>();
                this.jobstsdata =
                  new ObservableCollection<JobStatusStorage>(jbsts.Table<JobStatusStorage>());

                if (!jbsts.Table<JobStatusStorage>().Any())
                {
                    AddNewJobStatus_List();
                }
                DataSaving_JobStatus(_datas);
                SelectAllDatas_JobStatus();
            }
            catch (Exception ex)
            {

            }

        }

        public void DataSaving_JobStatus(List<JobStatusStorage> _datas)
        {
            try
            {

                var tempjoblist = (from cust in jbsts.Table<JobStatusStorage>()
                                   select cust).ToList();
                if (tempjoblist.Count > 0)
                {
                    foreach (var item in tempjoblist)
                    {
                        forml.Delete<FormTypeListDataStorage>(item.tempid);
                    }
                    JobStatusStorage _form = new JobStatusStorage();
                    foreach (var item in _datas)
                    {
                        _form.id = item.id;
                        _form.name = item.name;

                        SaveFormData_JobStatus(_form);
                    }
                }
                else
                {
                    JobStatusStorage _form = new JobStatusStorage();
                    foreach (var item in _datas)
                    {
                        _form.id = item.id;
                        _form.name = item.name;

                        SaveFormData_JobStatus(_form);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void AddNewJobStatus_List()
        {
            try
            {
                this.jobstsdata.
                  Add(new JobStatusStorage
                  {
                      id = 0,
                      name = "name",
                  });
            }
            catch (Exception ex)
            {

            }
        }

        public List<JobStatusStorage> SelectAllDatas_JobStatus()
        {
            List<JobStatusStorage> tempjoblist = null;
            try
            {
                jbsts =
             DependencyService.Get<IDatabaseConnection>().
             DbConnection();
                jbsts.CreateTable<JobStatusStorage>();
                this.jobstsdata =
                  new ObservableCollection<JobStatusStorage>(jbsts.Table<JobStatusStorage>());
                if (jbsts.Table<JobStatusStorage>().Any())
                {
                    tempjoblist = (from cust in jbsts.Table<JobStatusStorage>()
                                   select cust).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return tempjoblist;
        }
        public void SaveFormData_JobStatus(JobStatusStorage _storagedataInstance)
        {
            try
            {
                lock (collisionLock)
                {
                    jbsts.Insert(_storagedataInstance);
                }
            }
            catch (Exception ex)
            {

            }
        }


        #endregion
        #region
        //FormDataDisplay

        public async void FormDatasDisplay(List<FormDataDisplay> _datas, int formid)
        {
            try
            {
                formdis =
                  DependencyService.Get<IDatabaseConnection>().
                  DbConnection();

                formdis.CreateTable<FormDataDisplay>();
                this.formdisplaystorage =
                  new ObservableCollection<FormDataDisplay>(formdis.Table<FormDataDisplay>());

                if (!formdis.Table<FormDataDisplay>().Any())
                {
                    AddNewForm_Display();
                }
                DataSaving_FormDisplay(_datas,formid);
                //SelectAllDatas_FormDisplay();
            }
            catch (Exception ex)
            {

            }

        }

        public void DataSaving_FormDisplay(List<FormDataDisplay> _datas,int formid)
        {
            try
            {
                var _formid = Convert.ToString(formid);
                var tempjoblist = (from cust in formdis.Table<FormDataDisplay>()
                                   where cust.form_id == _formid && cust.job_id == Settings.JobIdKey
                                   select cust).ToList();
                if (tempjoblist.Count > 0)
                {
                    foreach (var item in tempjoblist)
                    {
                        forml.Delete<FormTypeListDataStorage>(item.tempid);
                    }
                    FormDataDisplay _form = new FormDataDisplay();
                    foreach (var item in _datas)
                    {
                        _form.id = item.id;
                        _form.job_id = item.job_id;
                        _form.staff_id = item.staff_id;
                        _form.site_id = item.site_id;
                        _form.form_name = item.form_name;
                        _form.form_type = item.form_type;
                        _form.form_json = item.form_json;
                        _form.created_at = item.created_at;
                        _form.updated_at = item.updated_at;
                        _form.category_id = item.category_id;
                        _form.form_id = item.form_id;
                        _form.access_key = item.access_key;
                        _form.form_status = item.form_status;

                        SaveFormData_Display(_form);
                    }
                }
                else
                {
                    FormDataDisplay _form = new FormDataDisplay();
                    foreach (var item in _datas)
                    {
                        _form.id = item.id;
                        _form.job_id = item.job_id;
                        _form.staff_id = item.staff_id;
                        _form.site_id = item.site_id;
                        _form.form_name = item.form_name;
                        _form.form_type = item.form_type;
                        _form.form_json = item.form_json;
                        _form.created_at = item.created_at;
                        _form.updated_at = item.updated_at;
                        _form.category_id = item.category_id;
                        _form.form_id = item.form_id;
                        _form.access_key = item.access_key;
                        _form.form_status = item.form_status;

                        SaveFormData_Display(_form);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void AddNewForm_Display()
        {
            try
            {
                this.formdisplaystorage.
                  Add(new FormDataDisplay
                  {
                      id = 0,
                      job_id = "job_id",
                      staff_id = "staff_id",
                      site_id = "site_id",
                      form_name = "form_name",
                      form_type = "form_type",
                      form_json = "form_json",
                      created_at = "created_at",
                      updated_at = "updated_at",
                      category_id = "category_id",
                      form_id = "form_id",
                      access_key = "access_key",
                      form_status = "form_status"
                  });
            }
            catch (Exception ex)
            {

            }
        }

        public List<FormDataDisplay> SelectAllDatas_FormDisplay(FormTypeList formTypeListdata)
        {
            List<FormDataDisplay> tempjoblist = null;
            try
            {
               
                formdis =
             DependencyService.Get<IDatabaseConnection>().
             DbConnection();
                formdis.CreateTable<FormDataDisplay>();
                this.formdisplaystorage =
                  new ObservableCollection<FormDataDisplay>(formdis.Table<FormDataDisplay>());
                if (formdis.Table<FormDataDisplay>().Any())
                {
                    tempjoblist = (from cust in formdis.Table<FormDataDisplay>()
                                   select cust).ToList();

                    var formid = Convert.ToString(formTypeListdata.id);
                    var chktblecnt = chkcnt();
                    var chktble = chktblecnt;
                    if (chktble.Count > 0)
                    {
                        //var cjkjoblist = (from cust in database.Table<FormDataTempStorage>()
                        //                  where cust.Form_id == formid && cust.Job_id == Settings.JobIdKey
                        //                  select cust).ToList();

                        var cjkjoblist = (from cust in database.Table<FormDataTempStorage>()
                                          where cust.Form_id == formid && cust.Job_id == Settings.JobIdKey
                                          select cust).LastOrDefault();

                        tempjoblist = (from cust in formdis.Table<FormDataDisplay>()
                                       where cust.form_id == formid && cust.job_id == Settings.JobIdKey
                                       select cust).ToList();
                        if (tempjoblist.Count > 0)
                        {
                            //tempjoblist[0].form_json = cjkjoblist[0].AllFormData;
                            tempjoblist[0].form_json = cjkjoblist.AllFormData;
                        }
                    }
                    else
                    {

                        tempjoblist = (from cust in formdis.Table<FormDataDisplay>()
                                       where cust.form_id == formid && cust.job_id == Settings.JobIdKey
                                       select cust).ToList();
                    }
                }
            }
            catch(Exception ex)
            {

            }

            return tempjoblist;
        }
        public List<FormDataTempStorage> chkcnt()
        {
            List<FormDataTempStorage> chktble = new List<FormDataTempStorage>();
            try
            {
                database =
             DependencyService.Get<IDatabaseConnection>().
             DbConnection();
                database.CreateTable<FormDataTempStorage>();
                this.localformdata_sav =
                  new ObservableCollection<FormDataTempStorage>(database.Table<FormDataTempStorage>());

                 chktble = (from cust in database.Table<FormDataTempStorage>()
                               select cust).ToList();
            }
            catch (Exception ex)
            {

            }
            return chktble;
        }
        public void SaveFormData_Display(FormDataDisplay _storagedataInstance)
        {
            try
            {
                lock (collisionLock)
                {
                    formdis.Insert(_storagedataInstance);
                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region
        //FormDataDisplay

        public async void FormStatusList(List<string> _datas)
        {
            try
            {
                formsts =
                  DependencyService.Get<IDatabaseConnection>().
                  DbConnection();

                formsts.CreateTable<Form_Status>();
                this.formstslst =
                  new ObservableCollection<Form_Status>(formsts.Table<Form_Status>());

                if (!formsts.Table<Form_Status>().Any())
                {
                    AddNewForm_Status();
                }
                DataSavingFormStatus(_datas);
                
            }
            catch (Exception ex)
            {

            }

        }

        public void DataSavingFormStatus(List<string> _datas)
        {
            try
            {
                var tempjoblist = (from cust in formsts.Table<Form_Status>()
                                   select cust).ToList();
                if (tempjoblist.Count > 0)
                {
                    foreach (var item in tempjoblist)
                    {
                        formsts.Delete<Form_Status>(item.tempid);
                    }
                    Form_Status _form = new Form_Status();
                    foreach (var item in _datas)
                    {
                        _form.form_status = item;
                        SaveFormStatus(_form);
                    }
                }
                else
                {
                    Form_Status _form = new Form_Status();
                    foreach (var item in _datas)
                    {
                        _form.form_status = item;
                        SaveFormStatus(_form);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void AddNewForm_Status()
        {
            try
            {
                this.formstslst.
                  Add(new Form_Status
                  {
                      form_status = "form_status"
                  });
            }
            catch (Exception ex)
            {

            }
        }

        public List<Form_Status> SelectAllDatasFormStatus()
        {
            List<Form_Status> tempjoblist = null;
            try
            {
                formsts =
             DependencyService.Get<IDatabaseConnection>().
             DbConnection();
                formsts.CreateTable<Form_Status>();
                this.formstslst =
                  new ObservableCollection<Form_Status>(formsts.Table<Form_Status>());
                if (formsts.Table<Form_Status>().Any())
                {
                    tempjoblist = (from cust in formsts.Table<Form_Status>()
                                   select cust).ToList();

                }
            }
            catch (Exception ex)
            {

            }

            return tempjoblist;
        }
        public void SaveFormStatus(Form_Status _storagedataInstance)
        {
            try
            {
                lock (collisionLock)
                {
                    formsts.Insert(_storagedataInstance);
                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region
        //FormDataDisplay

        public async void JobStatusSave(List<JobStatus_Save> _datas)
        {
            try
            {
                jobstsave =
                  DependencyService.Get<IDatabaseConnection>().
                  DbConnection();

                jobstsave.CreateTable<JobStatus_Save>();
                this.localjobstsave =
                  new ObservableCollection<JobStatus_Save>(jobstsave.Table<JobStatus_Save>());

                if (!jobstsave.Table<JobStatus_Save>().Any())
                {
                    AddNewJob_Status();
                }
                DataSavingJobStatus(_datas);

            }
            catch (Exception ex)
            {

            }

        }

        public void DataSavingJobStatus(List<JobStatus_Save> _datas)
        {
            try
            {

                var tempjoblist = (from cust in jobstsave.Table<JobStatus_Save>()
                                   select cust).ToList();
                if (tempjoblist.Count > 0)
                {
                    foreach (var item in tempjoblist)
                    {
                        jobstsave.Delete<JobStatus_Save>(item.id);
                    }
                    JobStatus_Save _job = new JobStatus_Save();
                    foreach (var item in _datas)
                    {
                        _job.id = item.id;
                        _job.jobid = item.jobid;
                        _job.name = item.name;
                        SaveJobStatus(_job);
                    }
                }
                else
                {
                    JobStatus_Save _job = new JobStatus_Save();
                    foreach (var item in _datas)
                    {
                        _job.id = item.id;
                        _job.jobid = item.jobid;
                        _job.name = item.name;
                        SaveJobStatus(_job);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void AddNewJob_Status()
        {
            try
            {
                this.localjobstsave.
                  Add(new JobStatus_Save
                  {
                      id = 0,
                      jobid = "jobid",
                      name = "name"

                  });
            }
            catch (Exception ex)
            {

            }
        }

        public List<JobStatus_Save> SelectJobStatus()
        {
            List<JobStatus_Save> tempjoblist = null;
            try
            {
                jobstsave =
             DependencyService.Get<IDatabaseConnection>().
             DbConnection();
                jobstsave.CreateTable<JobStatus_Save>();
                this.localjobstsave =
                  new ObservableCollection<JobStatus_Save>(jobstsave.Table<JobStatus_Save>());
                if (jobstsave.Table<JobStatus_Save>().Any())
                {
                    tempjoblist = (from cust in jobstsave.Table<JobStatus_Save>()
                                   select cust).ToList();

                }
            }
            catch (Exception ex)
            {

            }

            return tempjoblist;
        }
        public void SaveJobStatus(JobStatus_Save _storagedataInstance)
        {
            try
            {
                lock (collisionLock)
                {
                    jobstsave.Insert(_storagedataInstance);
                }
            }
            catch (Exception ex)
            {

            }

        }

        #endregion

    }
}






