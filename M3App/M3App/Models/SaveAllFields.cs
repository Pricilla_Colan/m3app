﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using Rg.Plugins.Popup.Services;
using SignaturePad.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;
using System.Linq;
using Plugin.Connectivity;
using System.Threading.Tasks;
using SQLite;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace M3App.Models
{
    public class SaveAllFields
    {
        FormTypeUpload upload = new FormTypeUpload();
        FormJsonUpload formJson = new FormJsonUpload();
        string FormStatus = "";
        string signimage;
        string description = "";
        List<Form_basic_Info> Basic_Infos = new List<Form_basic_Info>();
        List<DownloadComments> dts;
        RetrieveOfflineDatas _jobliststorage = new RetrieveOfflineDatas();
        List<Form_category_Info> form_Category_Infos = new List<Form_category_Info>();
        List<Sub_category_Info> sub_category = new List<Sub_category_Info>();
        List<category_Info_fields> category_Info_Fields = new List<category_Info_fields>();
        List<Subcategory_Info_fields> sub_category_info_Add = new List<Subcategory_Info_fields>();
        List<DownloadComments> downloadComments = new List<DownloadComments>();
        DynamicFormListService dynamicFormListService = new DynamicFormListService();
        int categoryId = 0;
        UploadComments uploadComments;
        int SubCategoryID = 0;
        int signcount = 0;
        //Offline storage

        private SQLiteConnection database;
        public ObservableCollection<FormDataTempStorage> localformdata { get; set; }
        Form_category_Info catselecteddata;
        public void SaveApproval(FormTypeListDownLoadModel _formmodelfields=null,string type=null,dynamic selectedcatdata=null,dynamic selectedsubcatdata=null,List<DownloadComments> cmts=null)
        {
            try
            {
                Sub_category_Info selectedsub_catdata;
                catselecteddata = selectedcatdata as Form_category_Info;
                selectedsub_catdata = selectedsubcatdata as Sub_category_Info;
                SaveAllFormDatas(_formmodelfields, type, catselecteddata, selectedsub_catdata, cmts);
            }
            catch (Exception ex)
            {

            }
        }

        public async void SaveAllFormDatas(FormTypeListDownLoadModel _formmodelfields = null, string type = null, Form_category_Info catselecteddata = null, Sub_category_Info selectedsubcatdata = null, List<DownloadComments> cmts = null)
        {
            List<Form_basic_InfoUpload> form_Basic_InfoUploads = new List<Form_basic_InfoUpload>();
            try
            {
                var ButtonType = type;
                var FormId = Settings.FormIdKey;
                var JobId = Settings.JobIdKey;
                var Access_key = Settings.AccessKey;
                upload.form_id = FormId;
                upload.job_id = JobId;
                FormStatus= Application.Current.Properties["FormStatus"].ToString();
                upload.form_status = FormStatus;
                upload.access_key = Access_key;
                upload.year = Settings.YearKey;
                Console.WriteLine("YEAR in Save :", upload.year);
                Label label = new Label();
                Entry entry = new Entry();
                CustomPicker customPicker = new CustomPicker();
                Image imagecheck = new Image();
                Image imageuncheck = new Image();
                Image CheckImge = new Image();
                DatePicker datePicker = new DatePicker();
                Editor editor = new Editor();
                List<string> BaseCheckvalues = new List<string>();
                List<string> CatCheckvalues = new List<string>();

                int catcheckedcount = 0;
                int catuncheckedcount = 0;
                int catradiocount = 0;
                int catradiocheckedcount = 0;
                int catcheckboxcount = 0;

                int subcatcheckedcount = 0;
                int subcatuncheckedcount = 0;
                int subcatradiocount = 0;
                int subcatradiocheckedcount = 0;
                int subcatcheckboxcount = 0;


                int basiccheckedcount = 0;
                int basicuncheckedcount = 0;
                int basicradiocheckedcount = 0;
                int basicradiocount = 0;
                int basiccheckboxcount = 0;

                ArrayList Bdata = new ArrayList();
                ArrayList Cdata = new ArrayList();

                Basic_Infos = _formmodelfields.response.form_json.basic_Info;
                form_Category_Infos = _formmodelfields.response.form_json.category_Info;
                downloadComments = _formmodelfields.response.form_json.comments;
                    if (catselecteddata != null)
                    {
                        category_Info_Fields = catselecteddata.fields;
                        categoryId = catselecteddata.id;
                    }
                var subcatselecteddata = selectedsubcatdata as Sub_category_Info;
                if (subcatselecteddata != null)
                {
                    sub_category_info_Add = subcatselecteddata.fields;
                    SubCategoryID = subcatselecteddata.id;
                    categoryId = subcatselecteddata.parent_id;
                }

                List<Xamarin.Forms.View> Basicdata = Application.Current.Properties["BasicDetails"] as List<Xamarin.Forms.View>;
                List<Xamarin.Forms.View> categorydata = Application.Current.Properties["CategoryDetails"] as List<Xamarin.Forms.View>;
                List<Xamarin.Forms.View> subcategorydata = Application.Current.Properties["CategorySubDetails"] as List<Xamarin.Forms.View>;
                List<Xamarin.Forms.View> TankData = Application.Current.Properties["TankDetails"] as List<Xamarin.Forms.View>;
                List<Xamarin.Forms.View> CommentsData = Application.Current.Properties["CommentsDetails"] as List<Xamarin.Forms.View>;

                if (Basicdata != null)
                {
                    for (int i = 0; i < Basicdata.Count; i++)
                    {
                        if (Basicdata[i].StyleId == "Check")
                        {
                            basiccheckedcount++;
                        }

                        if (Basicdata[i].StyleId == "Radio" || Basicdata[i].StyleId == "UnRadio")
                        {
                            basicradiocount++;
                            if (Basicdata[i].StyleId == "Radio")
                            {
                                basicradiocheckedcount++;
                            }
                        }
                        if (Basicdata[i].StyleId == "Check" || Basicdata[i].StyleId == "UnCheck")
                        {
                            basiccheckboxcount++;
                        }
                    }
                }
                var chkcnt = 0;
                var catchkcnt = 0;
                var subcatchkcnt = 0;
                if (Basicdata != null)
                {
                    for (int i = 0; i < Basicdata.Count; i++)
                    {
                        if (Basicdata[i].StyleId != "Label")
                        {

                            if (Basicdata[i].StyleId == "Entry")
                            {
                                entry = (Entry)Basicdata[i];
                                if (entry.Text != "" && entry.Text != null)
                                {
                                    Bdata.Add(entry.Text);
                                }
                                else
                                {
                                    Bdata.Add("");
                                }
                            }
                            else if (Basicdata[i].StyleId == "Textarea")
                            {
                                editor = (Editor)Basicdata[i];
                                if (editor.Text != "" && editor.Text != null)
                                {
                                    Bdata.Add(editor.Text);
                                }
                                else
                                {
                                    Bdata.Add("");
                                }
                            }
                            else if (Basicdata[i].StyleId == "CustomPicker")
                            {
                                customPicker = (CustomPicker)Basicdata[i];
                                var pickerval = customPicker.SelectedItem;
                                if (pickerval != "" && pickerval != null)
                                {
                                    Bdata.Add(pickerval);
                                }
                                else
                                {
                                    Bdata.Add("");
                                }
                            }
                            else if (Basicdata[i].StyleId == "DatePicker")
                            {
                                datePicker = (DatePicker)Basicdata[i];
                                //datePicker.Format = "dd-MM-yyyy";
                                datePicker.Format = "MM-dd-yyyy";
                                var dateonly = datePicker.Date.ToShortDateString();

                                if (dateonly != "" && dateonly != null)
                                {
                                    Bdata.Add(dateonly);
                                }
                                else
                                {
                                    Bdata.Add("");
                                }
                            }
                            else if (Basicdata[i].StyleId == "Radio" || Basicdata[i].StyleId == "UnRadio")
                            {
                                if (Basicdata[i].StyleId == "Radio")
                                {
                                    CheckImge = (Image)Basicdata[i];
                                    var RadioData = CheckImge.AutomationId;
                                    if (RadioData != "" && RadioData != null)
                                    {
                                        Bdata.Add(RadioData);
                                    }
                                }
                                else if (basicradiocheckedcount == 0)
                                {
                                    if (chkcnt == 0)
                                    {
                                        Bdata.Add("");
                                        chkcnt++;
                                    }
                                }
                            }
                            else if (Basicdata[i].StyleId == "Check" || Basicdata[i].StyleId == "UnCheck")
                            {
                                imagecheck = (Image)Basicdata[i];
                                Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)imagecheck.Source;
                                string strFileName = objFileImageSource.File;

                                if (strFileName == "UnChecked.png")
                                {
                                    basicuncheckedcount++;
                                    if (basiccheckedcount == 0)
                                    {
                                        if (basiccheckboxcount == basicuncheckedcount)
                                        {
                                            Bdata.Add("");
                                        }
                                    }
                                }
                                else
                                {
                                    var CheckData = imagecheck.AutomationId;
                                    if (CheckData != "" && CheckData != null)
                                    {
                                        BaseCheckvalues.Add(CheckData);
                                    }
                                    if (basiccheckedcount == BaseCheckvalues.Count)
                                    {
                                        Bdata.Add(BaseCheckvalues);
                                    }
                                }
                            }
                            else if (Basicdata[i].StyleId == "BasicSignature")
                            {
                                var bytearr = "";
                                var sign = "";
                                byte[] Bytes = new byte[0];
                                //signimage = Application.Current.Properties["SignImage"].ToString();
                                //var signaturepadviewobj = categorydata[i] as SignaturePadView;
                                var signaturepadviewobj = Basicdata[i] as SignaturePadView;
                                if (signaturepadviewobj.Points.Count() > 0)
                                {
                                    var stream = await signaturepadviewobj.GetImageStreamAsync(SignatureImageFormat.Png);
                                    if (stream != null)
                                    {
                                        using (MemoryStream ms = new MemoryStream())
                                        {
                                            Bytes = new byte[stream.Length];
                                            stream.CopyTo(ms);
                                            Bytes = ms.ToArray();

                                        }
                                    }
                                    sign = Convert.ToBase64String(Bytes);
                                }
                                // else if (signimage != null && signimage != "")
                                // {
                                //  sign = Application.Current.Properties["SignImage"].ToString();
                                // }

                                if (!string.IsNullOrEmpty(sign))
                                {
                                    Bdata.Add(sign);
                                }
                                else if (signimage != null)
                                {
                                    Bdata.Add(bytearr);
                                }
                                else
                                {
                                    Bdata.Add("");
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (var item in Basic_Infos)
                    {
                        Bdata.Add(item.value);
                    }

                }
                for (int i = 0; i < Bdata.Count; i++)
                {
                    form_Basic_InfoUploads.Add(new Form_basic_InfoUpload { label_name = Basic_Infos[i].label_name, type = Basic_Infos[i].type, value = Bdata[i], visible = Basic_Infos[i].visible, editable = Basic_Infos[i].editable, options = Basic_Infos[i].options });
                }
                formJson.basic_Info = form_Basic_InfoUploads;
                upload.form_json = formJson;
                Console.WriteLine("FORMJSON :", upload.form_json);
                List<category_Info_fields> UpdateFields = new List<category_Info_fields>();
                List<Subcategory_Info_fields> UpdateSubCategoryFields = new List<Subcategory_Info_fields>();
                if (ButtonType == "FormBasicSave")
                {
                    upload.form_json.category_Info = form_Category_Infos;
                }
                else if (ButtonType == "CategoryFields_Save")
                {
                    var idlists = Settings.idslist;
                    idlists.Add(Convert.ToString(categoryId));
                    if (_formmodelfields.response.form_name == "SAMPLE FORM FOR MONTHLY UNDERGROUND STORAGE SYSTEM INSPECTION CHECKLIST")
                    {
                        if (categorydata != null)
                        {
                            if (categorydata.Count > 0)
                            {
                                for (int i = 0; i < categorydata.Count; i++)
                                {
                                    if (categorydata[i].StyleId == "Entry")
                                    {
                                        entry = (Entry)categorydata[i];
                                        Cdata.Add(entry.Text);
                                    }
                                    if (categorydata[i].StyleId == "UnCheck")
                                    {
                                        imageuncheck = (Image)categorydata[i];
                                        if (imageuncheck.IsVisible == true)
                                        {
                                            Cdata.Add("false");
                                        }
                                    }
                                    if (categorydata[i].StyleId == "Check")
                                    {
                                        imagecheck = (Image)categorydata[i];
                                        if (imagecheck.IsVisible == true)
                                        {
                                            Cdata.Add("true");
                                        }
                                    }
                                }
                                for (int i = 0; i < TankData.Count; i++)
                                {
                                    if (TankData[i].StyleId == "Entry")
                                    {
                                        entry = (Entry)TankData[i];
                                        Cdata.Add(entry.Text);
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (var item in category_Info_Fields)
                            {
                                Cdata.Add(item.value);
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < categorydata.Count; i++)
                        {
                            if (categorydata[i].StyleId == "Check")
                            {
                                catcheckedcount++;
                            }
                            if (categorydata[i].StyleId == "Radio" || categorydata[i].StyleId == "UnRadio")
                            {
                                catradiocount++;
                                if (categorydata[i].StyleId == "Radio")
                                {
                                    catradiocheckedcount++;
                                }
                            }
                            if (categorydata[i].StyleId == "Check" || categorydata[i].StyleId == "UnCheck")
                            {
                                catcheckboxcount++;
                            }
                        }
                        if (categorydata != null)
                        {
                            if (categorydata.Count > 0)
                            {
                                for (int jk = 0; jk < categorydata.Count; jk++)
                                {
                                    if (categorydata[jk].StyleId != "Label")
                                    {

                                        if (categorydata[jk].StyleId == "Entry")
                                        {
                                            entry = (Entry)categorydata[jk];
                                            Cdata.Add(entry.Text);
                                        }
                                        else if (categorydata[jk].StyleId == "Textarea")
                                        {
                                            editor = (Editor)categorydata[jk];
                                            Cdata.Add(editor.Text);

                                        }
                                        else if (categorydata[jk].StyleId == "CustomPicker")
                                        {
                                            customPicker = (CustomPicker)categorydata[jk];
                                            Cdata.Add(customPicker.SelectedItem);
                                        }
                                        else if (categorydata[jk].StyleId == "DatePicker")
                                        {
                                            datePicker = (DatePicker)categorydata[jk];
                                            //datePicker.Format = "dd-MM-yyyy";
                                            datePicker.Format = "MM-dd-yyyy";
                                            var dateonly = datePicker.Date.ToShortDateString();
                                            Cdata.Add(dateonly);
                                        }
                                        else if (categorydata[jk].StyleId == "Radio" || categorydata[jk].StyleId == "UnRadio")
                                        {
                                            if (categorydata[jk].StyleId == "Radio")
                                            {
                                                CheckImge = (Image)categorydata[jk];
                                                var RadioData = CheckImge.AutomationId;
                                                Cdata.Add(RadioData);
                                            }
                                            else if (catradiocheckedcount == 0)
                                            {
                                                if (catchkcnt == 0)
                                                {
                                                    Cdata.Add("");
                                                    catchkcnt++;
                                                }
                                            }
                                        }

                                        else if (categorydata[jk].StyleId == "CategorySignature")
                                        {
                                            var bytearr = "";
                                            var sign = "";
                                            byte[] Bytes = new byte[0];
                                            signimage = Application.Current.Properties["SignImageCat"].ToString();
                                            var imagelist = Application.Current.Properties["SignImageCat"];

                                            var signaturepadviewobj = categorydata[jk] as SignaturePadView;
                                            if (signaturepadviewobj.Points.Count() > 0)
                                            {
                                                var stream = await signaturepadviewobj.GetImageStreamAsync(SignatureImageFormat.Png);
                                                if (stream != null)
                                                {
                                                    using (MemoryStream ms = new MemoryStream())
                                                    {
                                                        Bytes = new byte[stream.Length];
                                                        stream.CopyTo(ms);
                                                        Bytes = ms.ToArray();
                                                    }
                                                }
                                                sign = Convert.ToBase64String(Bytes);
                                            }
                                            else if (signimage != null && signimage != "")
                                            {
                                                Image imgchk = null;
                                                if (jk < categorydata.Count - 1)
                                                {
                                                    imgchk = categorydata[jk + 1] as Image;
                                                }
                                                if (imgchk != null)
                                                {
                                                    if (imgchk.IsVisible)
                                                    {

                                                        var signimg = imagelist;
                                                        IList collection = (IList)imagelist;
                                                        var t = collection[signcount] as M3App.Cores.DataModel.category_Info_fields;
                                                        var signimgval = t.value;
                                                        //sign = Application.Current.Properties["SignImageCat"].ToString();
                                                        sign = signimgval.ToString();
                                                    }
                                                    else
                                                    {
                                                        sign = "";
                                                    }
                                                }
                                                else
                                                {
                                                    sign = "";
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(sign))
                                            {
                                                Cdata.Add(sign);
                                            }
                                            else
                                            {
                                                Cdata.Add("");
                                            }
                                            signcount = signcount + 1;

                                        }
                                        else if (categorydata[jk].StyleId == "Check" || categorydata[jk].StyleId == "UnCheck")
                                        {
                                            imagecheck = (Image)categorydata[jk];
                                            Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)imagecheck.Source;
                                            string strFileName = objFileImageSource.File;

                                            if (strFileName == "UnChecked.png")
                                            {
                                                catuncheckedcount++;
                                                if (catcheckedcount == 0)
                                                {
                                                    if (catcheckboxcount == catuncheckedcount)
                                                    {
                                                        Cdata.Add("");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var CheckData = imagecheck.AutomationId;
                                                if (CheckData != "" && CheckData != null)
                                                {
                                                    CatCheckvalues.Add(CheckData);
                                                }
                                                if (catcheckedcount == CatCheckvalues.Count)
                                                {
                                                    Cdata.Add(CatCheckvalues);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        foreach (var item in category_Info_Fields)
                        {
                            Cdata.Add(item.value);
                        }
                    }

                    //Need Save 
                    var a = Cdata;

                    for (int i = 0; i < a.Count; i++)
                    {

                        UpdateFields.Add(new category_Info_fields { label_name = category_Info_Fields[i].label_name, type = category_Info_Fields[i].type, value = Cdata[i], visible = category_Info_Fields[i].visible, editable = category_Info_Fields[i].editable, options = category_Info_Fields[i].options });
                        // UpdateFields.Add(new category_Info_fields { label_name = category_Info_Fields[i].label_name, type = category_Info_Fields[i].type, value = Cdata[i], visible = category_Info_Fields[i].visible, editable = category_Info_Fields[i].editable, options = category_Info_Fields[i].options });

                    }
                }

                else if (ButtonType == "SubCategoryFields_Save")
                {
                    var idlists = Settings.idslist;
                    idlists.Add(Convert.ToString(categoryId));
                    idlists.Add(Convert.ToString(SubCategoryID));
                    for (int ij = 0; ij < subcategorydata.Count; ij++)
                    {
                        if (subcategorydata[ij].StyleId == "Check")
                        {
                            catcheckedcount++;
                        }

                        if (subcategorydata[ij].StyleId == "Radio" || subcategorydata[ij].StyleId == "UnRadio")
                        {
                            subcatradiocount++;
                            if (subcategorydata[ij].StyleId == "Radio")
                            {
                                subcatradiocheckedcount++;
                            }
                        }
                        if (subcategorydata[ij].StyleId == "Check" || subcategorydata[ij].StyleId == "UnCheck")
                        {
                            subcatcheckboxcount++;
                        }
                    }

                    if (subcategorydata.Count > 0)
                    {
                        for (int ik = 0; ik < subcategorydata.Count; ik++)
                        {
                            if (subcategorydata[ik].ClassId == "SubCategoryEntry")
                            {
                                entry = (Entry)subcategorydata[ik];
                                Cdata.Add(entry.Text);
                            }
                            else if (subcategorydata[ik].StyleId == "Check" || subcategorydata[ik].StyleId == "UnCheck")
                            {
                                imagecheck = (Image)subcategorydata[ik];
                                Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)imagecheck.Source;
                                string strFileName = objFileImageSource.File;

                                if (strFileName == "UnChecked.png")
                                {
                                    subcatuncheckedcount++;
                                    if (subcatcheckedcount == 0)
                                    {
                                        if (subcatcheckboxcount == subcatuncheckedcount)
                                        {
                                            Cdata.Add("");
                                        }
                                    }
                                }
                                else
                                {
                                    var CheckData = imagecheck.AutomationId;
                                    if (CheckData != "" && CheckData != null)
                                    {
                                        CatCheckvalues.Add(CheckData);
                                    }
                                    if (subcatcheckedcount == CatCheckvalues.Count)
                                    {
                                        Cdata.Add(CatCheckvalues);
                                    }
                                }

                            }
                            else if (subcategorydata[ik].StyleId == "Radio" || subcategorydata[ik].StyleId == "UnRadio")
                            {
                                if (subcategorydata[ik].StyleId == "Radio")
                                {
                                    CheckImge = (Image)subcategorydata[ik];
                                    var RadioData = CheckImge.AutomationId;
                                    if (RadioData != "" && RadioData != null)
                                    {
                                        Cdata.Add(RadioData);
                                    }
                                }
                                else if (subcatradiocheckedcount == 0)
                                {
                                    if (subcatchkcnt == 0)
                                    {
                                        Cdata.Add("");
                                        subcatchkcnt++;
                                    }
                                }
                            }
                            else if (subcategorydata[ik].ClassId == "Picker")
                            {
                                customPicker = (CustomPicker)subcategorydata[ik];
                                Cdata.Add(customPicker.SelectedItem);
                            }
                            else if (subcategorydata[ik].ClassId == "Areatext")
                            {
                                editor = (Editor)subcategorydata[ik];
                                Cdata.Add(editor.Text);
                            }
                            else if (subcategorydata[ik].ClassId == "PickerDate")
                            {

                                var dateonly = (Label)subcategorydata[ik];
                                var txtval = dateonly.Text;
                                string tt = null;
                                if (txtval == "" || txtval == null || txtval == "Select Date")
                                {
                                    tt = null;
                                }
                                else
                                {
                                    tt = txtval;
                                }


                                Cdata.Add(tt);
                            }
                            else if (subcategorydata[ik].StyleId == "CategorySignature")
                            {
                                var bytearr = "";
                                var sign = "";
                                byte[] Bytes = new byte[0];
                                signimage = Application.Current.Properties["SignImageSubCatList"].ToString();
                                var imagelist = Application.Current.Properties["SignImageSubCatList"];

                                var signaturepadviewobj = subcategorydata[ik] as SignaturePadView;
                                if (signaturepadviewobj.Points.Count() > 0)
                                {
                                    var stream = await signaturepadviewobj.GetImageStreamAsync(SignatureImageFormat.Png);
                                    if (stream != null)
                                    {
                                        using (MemoryStream ms = new MemoryStream())
                                        {
                                            Bytes = new byte[stream.Length];
                                            stream.CopyTo(ms);
                                            Bytes = ms.ToArray();
                                        }
                                    }
                                    sign = Convert.ToBase64String(Bytes);
                                }
                                else if (signimage != null && signimage != "")
                                {
                                    Image imgchk = null;
                                    if (ik < subcategorydata.Count - 1)
                                    {
                                        imgchk = subcategorydata[ik + 1] as Image;
                                    }
                                    if (imgchk != null)
                                    {
                                        if (imgchk.IsVisible)
                                        {

                                            var signimg = imagelist;
                                            IList collection = (IList)imagelist;
                                            var t = collection[signcount] as M3App.Cores.DataModel.Subcategory_Info_fields;
                                            var signimgval = t.value;
                                            sign = signimgval.ToString();
                                        }
                                        else
                                        {
                                            sign = "";
                                        }
                                    }
                                    else
                                    {
                                        sign = "";
                                    }
                                }

                                if (!string.IsNullOrEmpty(sign))
                                {
                                    Cdata.Add(sign);
                                }

                                else
                                {
                                    Cdata.Add("");
                                }
                                signcount = signcount + 1;

                            }
                        }
                    }


                    var b = Cdata;
                    for (int i = 0; i < b.Count; i++)
                    {
                        if (Cdata[i] == null)
                        {
                            UpdateSubCategoryFields.Add(new Subcategory_Info_fields { label_name = sub_category_info_Add[i].label_name, options = sub_category_info_Add[i].options, type = sub_category_info_Add[i].type, value = "", visible = sub_category_info_Add[i].visible, editable = sub_category_info_Add[i].editable });
                        }
                        else
                        {
                            UpdateSubCategoryFields.Add(new Subcategory_Info_fields { label_name = sub_category_info_Add[i].label_name, options = sub_category_info_Add[i].options, type = sub_category_info_Add[i].type, value = Cdata[i].ToString(), visible = sub_category_info_Add[i].visible, editable = sub_category_info_Add[i].editable });
                        }
                    }
                }

                if (categoryId != 0)
                {
                    var Category = UpdateFields;
                    var SubCategory = UpdateSubCategoryFields;
                    if (Category.Count > 0)
                    {
                        for (int i = 0; i < form_Category_Infos.Count; i++)
                        {
                            if (categoryId == form_Category_Infos[i].id)
                            {
                                var StoringData = form_Category_Infos[i];
                                form_Category_Infos.RemoveAt(i);
                                form_Category_Infos.Add(new Form_category_Info { id = StoringData.id, parent_id = StoringData.parent_id, name = StoringData.name, description = description, fields = UpdateFields, sublevel = StoringData.sublevel });
                            }
                        }
                    }
                    if (SubCategory.Count > 0)
                    {
                        for (int i = 0; i < form_Category_Infos.Count; i++)
                        {
                            if (categoryId == form_Category_Infos[i].id)
                            {
                                foreach (var item in form_Category_Infos)
                                {
                                    sub_category = item.sub_category;
                                    if (sub_category != null)
                                    {
                                        for (int j = 0; j < sub_category.Count; j++)
                                        {
                                            if (SubCategoryID == sub_category[j].id)
                                            {
                                                var StoringData = sub_category[j];
                                                sub_category.RemoveAt(j);
                                                sub_category.Add(new Sub_category_Info { id = StoringData.id, parent_id = StoringData.parent_id, fields = SubCategory, description = StoringData.description, sublevel = StoringData.sublevel, name = StoringData.name });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                byte[] imageBytes = new byte[0];
                //if (Signature.Points.Count() > 0)
                //{
                //    var stream = await Signature.GetImageStreamAsync(SignatureImageFormat.Png);
                //    using (MemoryStream ms = new MemoryStream())
                //    {
                //        imageBytes = new byte[stream.Length];
                //        stream.CopyTo(ms);
                //        imageBytes = ms.ToArray();
                //    }
                //}
                formJson.comments = new List<UploadComments>();
                uploadComments = new UploadComments();
                string signdata = Convert.ToBase64String(imageBytes);

                if (type == "AddComments" || type == "EditComments" || type == "DeleteComments")
                {
                    if (type == "EditComments" || type == "DeleteComments")
                    {
                        dts = cmts as List<DownloadComments>;
                    }
                }

                if (type == "AddComments")
                {
                    
                    if (downloadComments != null)
                    {
                        foreach (var itm in downloadComments)
                        {
                            UploadComments ucc = new UploadComments();
                            ucc.action_taken = itm.action_taken;
                            ucc.date = itm.date;
                            ucc.initials = itm.initials;
                            formJson.comments.Add(ucc);
                        }
                    }
                    UploadComments uc = new UploadComments();
                    foreach (var item in CommentsData)
                    {
                        var condichkd = item.GetType();
                        var _typesrc = condichkd.FullName;
                        if (_typesrc == "Xamarin.Forms.Editor")
                        {
                            var t = item as Editor;
                            uc.action_taken = t.Text;
                        }
                        if (_typesrc == "Xamarin.Forms.DatePicker")
                        {
                            var t = item as DatePicker;
                            uc.date = t.Date.ToString("MM-dd-yyyy");
                        }
                        if (_typesrc == "SignaturePad.Forms.SignaturePadView")
                        {
                            var signaturepadviewobj = item as SignaturePadView;
                            byte[] Bytes = new byte[0];
                            if (signaturepadviewobj.Points.Count() > 0)
                            {
                                var stream = await signaturepadviewobj.GetImageStreamAsync(SignatureImageFormat.Png);
                                if (stream != null)
                                {
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        Bytes = new byte[stream.Length];
                                        stream.CopyTo(ms);
                                        Bytes = ms.ToArray();
                                    }
                                }
                                var sign = Convert.ToBase64String(Bytes);
                                uc.initials = sign;
                            }
                            else
                            {
                                uc.initials = "";
                            }
                        }
                    }
                    formJson.comments.Add(uc);
                }
                else if (type == "EditComments")
                {
                    var mn = downloadComments;
                    if (dts != null)
                    {
                        int i = 0;
                        if (downloadComments != null)
                        {
                            downloadComments.ForEach(x =>
                            {
                                x.Index = i;
                                i++;
                            });
                        }
                        var t = (from m in downloadComments
                                 where m.Index == dts[0].Index
                                 select m).FirstOrDefault();
                        t.action_taken = dts[0].action_taken;
                        t.date = dts[0].date;
                        t.initials = dts[0].initials;
                        List<UploadComments> upc = new List<UploadComments>();

                        foreach (var itm in downloadComments)
                        {
                            UploadComments uc = new UploadComments();
                            uc.action_taken = itm.action_taken;
                            uc.date = itm.date;
                            //uc.date = Convert.ToDateTime(itm.date).ToString("MM-dd-yyyy");
                            uc.initials = itm.initials;
                            upc.Add(uc);
                        }
                        formJson.comments = upc;
                    }
                }
                else if (type == "DeleteComments")
                {
                    List<UploadComments> upc = new List<UploadComments>();
                    foreach (var itm in dts)
                    {
                        UploadComments uc = new UploadComments();
                        uc.action_taken = itm.action_taken;
                        uc.date = itm.date;
                        //uc.date= Convert.ToDateTime(itm.date).ToString("MM-dd-yyyy");
                        uc.initials = itm.initials;
                        upc.Add(uc);

                    }
                    formJson.comments = upc;
                }
                else
                {
                    //formJson.comments = null;
                    if (downloadComments != null)
                    {
                        foreach (var itm in downloadComments)
                        {
                            UploadComments ucc = new UploadComments();
                            ucc.action_taken = itm.action_taken;
                            ucc.date = itm.date;
                            ucc.initials = itm.initials;
                            formJson.comments.Add(ucc);
                        }
                    }
                }
                if (type == "AddComments" || type == "EditComments" || type == "DeleteComments")
                {
                    formJson.category_Info = form_Category_Infos;
                }

                if (categoryId != 0)
                {
                    formJson.category_Info = form_Category_Infos.OrderBy(x => x.id).ToList();
                }
                upload.form_json = formJson;
                
                if (CrossConnectivity.Current.IsConnected)
                {
                    PostformDatas(upload);
                    LocalStorageAllFields _localstorage = new LocalStorageAllFields();
                    _localstorage.FormDataAccess(upload);
                }
                else
                {
                    LocalStorageAllFields _localstorage = new LocalStorageAllFields();
                    _localstorage.FormDataAccess(upload);
                    NavigationPage();


                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        public async void PostformDatas_SendMail(FormTypeUpload formTypeUpload, string status = null)
        {
            try
            {
                var postdata = await dynamicFormListService.FormTypeUpload(formTypeUpload);
                if (postdata.result == "Success")
                {
                    UserDialogs.Instance.HideLoading();
                    //XFToast.LongMessage(postdata.message);
                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                    //XFToast.LongMessage(postdata.message);
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                XFToast.LongMessage("Something went wrong");
            }
        }

        public async void PostformDatas_Initial(FormTypeUpload formTypeUpload, string status = null)
        {
            try
            {
                var postdata = await dynamicFormListService.FormTypeUpload(formTypeUpload);
                if (postdata.result == "Success")
                {
                    UserDialogs.Instance.HideLoading();
                    XFToast.LongMessage(postdata.message);
                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                    XFToast.LongMessage(postdata.message);
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                XFToast.LongMessage("Something went wrong");
            }
        }
        
        public async void PostformDatas(FormTypeUpload formTypeUpload, string status = null)
        {
            try
            {
                Console.WriteLine("Form Upload :", formTypeUpload);
                var postdata = await dynamicFormListService.FormTypeUpload(formTypeUpload);
                if (postdata.result == "Success")
                {
                    UserDialogs.Instance.HideLoading();
                    XFToast.LongMessage(postdata.message);
                    NavigationPage();
                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                    XFToast.LongMessage(postdata.message);
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                XFToast.LongMessage("Something went wrong");
            }
        }
        public FormTypeListDownLoadModel GetFormTypeList(FormTypeList Formlistmodel)
        {
            var Formslistdata = dynamicFormListService.Dynamicformdatas(Formlistmodel);
            return Formslistdata;
        }
        public bool OfflineStorage(string Mail=null)
        {
            bool status = false;
            try
            {
                database =
                          DependencyService.Get<IDatabaseConnection>().
                                      DbConnection();
                database.CreateTable<FormDataTempStorage>();
                this.localformdata =
                  new ObservableCollection<FormDataTempStorage>(database.Table<FormDataTempStorage>());

                if (database.Table<FormDataTempStorage>().Any())
                {

                    //var query = (from cust in database.Table<FormDataTempStorage>()
                    //             select cust).ToList();

                    var query = (from cust in database.Table<FormDataTempStorage>()
                                 select cust).LastOrDefault();
                    List<FormDataTempStorage> _query = new List<FormDataTempStorage>();
                    _query.Add(query);
                    if (_query.Count > 0)
                    {
                        foreach (var item in _query)
                        {

                            upload.form_id = item.Form_id;
                            upload.job_id = item.Job_id;
                            upload.form_status = item.Form_status;
                            upload.access_key = Settings.LogKey;
                            var formdatajson = JsonConvert.DeserializeObject<FormJsonUpload>(item.AllFormData);
                            upload.form_json = formdatajson;
                            if (Mail == "SendMail")
                            {
                                PostformDatas_SendMail(upload);
                            }
                            else
                            {
                                PostformDatas_Initial(upload);
                            }
                        }
                        //foreach (var item in query)
                        //{
                        //    database.Delete<FormDataTempStorage>(item.Form_id);
                        //}
                        status = true;
                    }
                    else
                    {
                        return status;
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return status;
        }
        


        public async void NavigationPage()
        {
            try
            {
                FormTypeListDownLoadModel formtypelistmodel_ = new FormTypeListDownLoadModel();
                var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                string PageName = Xamarin.Forms.Application.Current.Properties["PageName"] as string;
                if (PageName == "SubCategoryInfoDetails")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    await Task.Delay(100);
                    formtypelistmodel_ = DefaultvalueAssign(formtypelistmodel_);
                   // var formUpload = Application.Current.Properties["FormTypeUpload"] as FormTypeUpload;
                    App.Current.MainPage = new M3App.Views.CategoryInfoDetails(formtypelistmodel_, FormListtype);
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "SubCategoryDetailsDescription")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    await Task.Delay(100);
                    formtypelistmodel_ = DefaultvalueAssign(formtypelistmodel_);
                    var Form_Category = Application.Current.Properties["SelectedData"] as Form_category_Info;
                    App.Current.MainPage = new M3App.Views.SubCategoryInfoDetails(FormListtype, Form_Category, formtypelistmodel_);
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "EditSingleComment")
                {
                    PopupNavigation.Instance.PopAsync();
                    formtypelistmodel_ = DefaultvalueAssign(formtypelistmodel_);
                    downloadComments = formtypelistmodel_.response.form_json.comments;
                    int i = 0;
                    if (downloadComments != null)
                    {
                        downloadComments.ForEach(x =>
                        {
                            x.Index = i;
                            i++;
                        });
                    }
                    App.Current.MainPage = new M3App.Views.ViewComments(downloadComments, FormListtype);
                }
                else if (PageName == "DisplayAlertMessage")
                {
                    XFToast.LongMessage("Deleted Successfully");
                    formtypelistmodel_ = DefaultvalueAssign(formtypelistmodel_);
                    int i = 0;
                    downloadComments = formtypelistmodel_.response.form_json.comments;
                    if (downloadComments != null)
                    {
                        downloadComments.ForEach(x =>
                        {
                            x.Index = i;
                            i++;
                        });
                    }
                    App.Current.MainPage = new M3App.Views.ViewComments(downloadComments, FormListtype);
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "CommentsPage")
                {
                    var GetPageName = Application.Current.Properties["NavPageNameSave"].ToString();
                    if (GetPageName == "BasicInfoDetails")
                    {
                        // var FormListtype_ = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                        App.Current.MainPage = new M3App.Views.BasicInfoDetails(FormListtype);
                    }
                    else if (GetPageName == "CategoryInfoDetails")
                    {
                        formtypelistmodel_ = DefaultvalueAssign(formtypelistmodel_);
                        // var FormListtype_ = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                        //var formUpload = Application.Current.Properties["FormTypeUpload"] as FormTypeUpload;
                        App.Current.MainPage = new M3App.Views.CategoryInfoDetails(formtypelistmodel_, FormListtype);
                    }
                    else if (GetPageName == "SubCategoryInfoDetails")
                    {
                        var selecteddata = Application.Current.Properties["SelectedData"] as Form_category_Info;
                        formtypelistmodel_ = DefaultvalueAssign(formtypelistmodel_);
                        App.Current.MainPage = new M3App.Views.SubCategoryInfoDetails(FormListtype, selecteddata, formtypelistmodel_);
                    }
                    else if (GetPageName == "SubCategoryDetailsDescription")
                    {
                        var selecteddata = Application.Current.Properties["SelectedSubData"] as Sub_category_Info;
                        var selectedCategory = Application.Current.Properties["SelectedData"] as Form_category_Info;
                        formtypelistmodel_ = DefaultvalueAssign(formtypelistmodel_);
                        App.Current.MainPage = new M3App.Views.SubCategoryDetailsDescription(FormListtype, selecteddata, selectedCategory, formtypelistmodel_);
                    }

                }
            }
            catch (Exception ex)
            {

            }

        }
        public FormTypeListDownLoadModel DefaultvalueAssign(FormTypeListDownLoadModel formtypelistmodel)
        {
            try
            {
                formtypelistmodel = null;
                var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                if (formtypelistmodel == null)
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        formtypelistmodel = new FormTypeListDownLoadModel();
                        formtypelistmodel = GetFormTypeList(FormListtype);
                    }
                    else
                    {
                        formtypelistmodel = new FormTypeListDownLoadModel();
                        var tempjoblist = _jobliststorage.SelectAllDatas_FormDisplay(FormListtype);
                        FormTypeResponse rs = new FormTypeResponse();
                        if (tempjoblist.Count > 0)
                        {
                            foreach (var itn in tempjoblist)
                            {
                                rs.id = itn.id;
                                rs.job_id = itn.job_id;
                                rs.staff_id = itn.staff_id;
                                rs.site_id = itn.site_id;
                                rs.form_name = itn.form_name;
                                rs.form_type = itn.form_type;
                                rs.form_json = JsonConvert.DeserializeObject<FormJson>(itn.form_json);
                                rs.created_at = itn.created_at;
                                rs.updated_at = itn.updated_at;
                                rs.category_id = itn.category_id;
                                rs.form_id = itn.form_id;
                                rs.access_key = itn.access_key;
                                rs.form_status = itn.form_status;

                            }
                            formtypelistmodel.response = rs;
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return formtypelistmodel;
        }
    }
}






////Install-Package SQLitePCL.raw -Version 0.9.2
////Install-Package sqlite-net-pcl -Version 1.1.1
////https://www.youtube.com/watch?v=b0JPpVgAv9w
////https://msdn.microsoft.com/magazine/mt736454