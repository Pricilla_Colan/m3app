﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace M3App.ViewModels
{
    public class BackPressModel
    {
        private object dynamicFormListService;
        DynamicFormListService dynamicFormListService_ = new DynamicFormListService();
        public BackPressModel()
        {

        }
        public async void RedirectGridPage(string PageName)
        {
            //
            //string PageName = Application.Current.Properties["PageName"].ToString();
            try
            {
                if (PageName == "CategoryInfoDetails")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    await Task.Delay(100);
                    var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                    App.Current.MainPage = new M3App.Views.BasicInfoDetails(FormListtype);
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "BasicInfoDetails")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    await Task.Delay(100);
                    App.Current.MainPage = new Views.DynamicJobList();
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "SubCategoryInfoDetails")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    await Task.Delay(100);
                    var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                    //var formUpload = Application.Current.Properties["FormTypeUpload"] as FormTypeUpload;
                    var formtypelistmodel = GetFormTypeList(FormListtype);
                    App.Current.MainPage = new M3App.Views.CategoryInfoDetails(formtypelistmodel, FormListtype);
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "SubCategoryDetailsDescription")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    await Task.Delay(100);
                    var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                    var formtypelistmodel = GetFormTypeList(FormListtype);
                    var Form_Category = Application.Current.Properties["SelectedData"] as Form_category_Info;
                    App.Current.MainPage = new M3App.Views.SubCategoryInfoDetails(FormListtype, Form_Category, formtypelistmodel);
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "EmailForms")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    await Task.Delay(100);
                    App.Current.MainPage = new Views.DynamicJobList();
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "ComposeMailForm")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    App.Current.MainPage = new Views.EmailForms();
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "DynamicJobList")
                {
                    var navPage3 = new NavigationPage();
                    navPage3.ToolbarItems.Clear();
                    navPage3.BarBackgroundColor = Color.FromHex("#1A257F");
                    await navPage3.PushAsync(new M3App.Views.JobList() { Title = "Job List" });
                    App.Current.MainPage = new MasterDetailPage
                    {
                        Master = new MasterPage()
                        {
                            Title = "Menu",
                        },
                        Detail = navPage3
                    };
                }
                else if (PageName == "CommentsPage")
                {
                    var GetPageName = Application.Current.Properties["NavPageNameSave"].ToString();
                    if (GetPageName == "ViewComments")
                    {
                        GetPageName = Application.Current.Properties["NavPageNameSave"].ToString();
                    }
                    if (GetPageName == "BasicInfoDetails")
                    {
                        var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                        App.Current.MainPage = new M3App.Views.BasicInfoDetails(FormListtype);
                    }
                    else if (GetPageName == "CategoryInfoDetails")
                    {
                        var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                      //  var formUpload = Application.Current.Properties["FormTypeUpload"] as FormTypeUpload;
                        var formtypelistmodel = GetFormTypeList(FormListtype);
                        App.Current.MainPage = new M3App.Views.CategoryInfoDetails(formtypelistmodel, FormListtype);
                    }
                    else if (GetPageName == "SubCategoryInfoDetails")
                    {
                        var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                        var formtypelistmodel = GetFormTypeList(FormListtype);
                        var selecteddata = Application.Current.Properties["SelectedData"] as Form_category_Info;
                        App.Current.MainPage = new M3App.Views.SubCategoryInfoDetails(FormListtype, selecteddata, formtypelistmodel);
                    }
                    else if (GetPageName == "SubCategoryDetailsDescription")
                    {
                        var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                        var formtypelistmodel = GetFormTypeList(FormListtype);
                        var selecteddata = Application.Current.Properties["SelectedSubData"] as Sub_category_Info;
                        var selectedCategory = Application.Current.Properties["SelectedData"] as Form_category_Info;
                        App.Current.MainPage = new M3App.Views.SubCategoryDetailsDescription(FormListtype, selecteddata, selectedCategory, formtypelistmodel);
                    }
                }
                else if (PageName == "JobList")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    App.Current.MainPage = new Views.LoginForm();
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "MasterPage")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    App.Current.MainPage = new Views.LoginForm();
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "ViewComments")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    await Task.Delay(100);
                    var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                    var formtypelistmodel = GetFormTypeList(FormListtype);
                    App.Current.MainPage = new M3App.Views.CommentsPage(formtypelistmodel, FormListtype);
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "Register")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    App.Current.MainPage = new Views.LoginForm();
                    UserDialogs.Instance.HideLoading();
                }
                else if (PageName == "RecoverPassword")
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    App.Current.MainPage = new Views.LoginForm();
                    UserDialogs.Instance.HideLoading();
                }
            }
            catch (Exception ex)
            {

            }

        }
        public FormTypeListDownLoadModel GetFormTypeList(FormTypeList Formlistmodel)
        {
            //Application.Current.Properties.Remove("FormTypeListObject");
            //Application.Current.Properties.Add("FormTypeListObject", Formlistmodel);

            var Formslistdata = dynamicFormListService_.Dynamicformdatas(Formlistmodel);
            return Formslistdata;
        }
    }
}
