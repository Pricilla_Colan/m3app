﻿using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using M3App.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Xamarin.Forms;
using Plugin.Settings;
using M3App.Views;
using Acr.UserDialogs;
using System.Threading.Tasks;
using Plugin.Connectivity;

namespace M3App.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        User user = new User();
        LoginModel login = new LoginModel();
        JobListAccess JobListAccess = new JobListAccess();
        Match match;


        public event PropertyChangedEventHandler PropertyChanged;

        #region properties
        private string email;
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
                NotifyPropertyChanged("Email");
            }
        }

        private string password;

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                NotifyPropertyChanged("Password");
            }
        }

        #endregion

        #region ICommand

        public ICommand SubmitCommand
        {
            set;
            get;
        }
        public ICommand SignUpButtonCommand
        {
            set;
            get;
        }
        #endregion

        #region constructor
        public LoginViewModel()
        {
         //  Email = "sheik@colanonline.com";
        //   Password = "Sheik@123";

          //  Email = " gopalsamy.s@colanonline.com";
           //Password = "2ywmaIne";

           // Email = "vijay.ramu@colanonline.com";
           // Password = "123456";
            SubmitCommand = new Command(OnSubmit);
            //SignUpButtonCommand = new Command(onSignup);
        }
        #endregion
        


        public  void OnSubmit()
        {
            
            try
            {
               
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                if (Email != null)
                {
                    match = regex.Match(Email.Trim());
                }
                if (string.IsNullOrWhiteSpace(Email))
                {
                    XFToast.LongMessage("Please Enter UserName");
                }
                else if (!match.Success)
                {
                    XFToast.LongMessage("Please Enter Valid Email ID");
                }
                else if (string.IsNullOrWhiteSpace(Password))
                {
                    XFToast.LongMessage("Please Enter Password");
                }
                else
                {
                    user = new User();
                    user.email = Email.Trim();
                    user.password = Password.Trim();
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    UserLogin();
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                ex.Message.ToString();
            }
        }

        public async void UserLogin()
        {
            try
            {
                LoginService loginService = new LoginService();
                if (CrossConnectivity.Current.IsConnected)
                {
                    var Islogin = await loginService.login(user);
                    if (Islogin.result != "Error")
                    {
                        var accesskey = Islogin.response.access_key;
                        if (accesskey != null)
                        {
                            Settings.AccessKey = accesskey;
                        }
                        if (Islogin.result == "success")
                        {
                            Settings.UserName = Email;
                            Settings.LogEmail = Islogin.response.email;
                            Settings.Password = Password;
                            Settings.LoginStatus = "Login";
                            Settings.LogKey = Islogin.response.access_key;
                            var navPage3 = new NavigationPage();
                            navPage3.BarBackgroundColor = Color.FromHex("#1A257F");
                            await navPage3.PushAsync(new M3App.Views.JobList() { Title = "Job List" });
                            App.Current.MainPage = new MasterDetailPage
                            {
                                Master = new MasterPage()
                                {
                                    Title = "Menu",
                                },
                                Detail = navPage3
                            };

                            UserDialogs.Instance.HideLoading();
                        }
                        else
                        {
                            UserDialogs.Instance.HideLoading();
                            XFToast.LongMessage(Islogin.message);
                        }
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        XFToast.LongMessage(Islogin.message);
                    }
                }
                else if(!CrossConnectivity.Current.IsConnected && (Settings.UserName == ""|| Settings.UserName == null))
                {
                    var Islogin = await loginService.login(user);
                    if(Islogin == null)
                    {
                        UserDialogs.Instance.HideLoading();
                        XFToast.LongMessage("You did'nt Logged in already...");
                    }
                }
                else
                {
                    var Islogin = await loginService.login(user);
                    if (Settings.checkTrue == "True" && Islogin==null && (Settings.UserName!=null|| Settings.UserName != ""))
                    {
                        Settings.UserName = Email;
                        Settings.Password = Password;
                        Settings.LoginStatus = "Login";
                        var navPage3 = new NavigationPage();
                        navPage3.BarBackgroundColor = Color.FromHex("#1A257F");
                        await navPage3.PushAsync(new M3App.Views.JobList() { Title = "Job List" });
                        App.Current.MainPage = new MasterDetailPage
                        {
                            Master = new MasterPage()
                            {
                                Title = "Menu",
                            },
                            Detail = navPage3
                        };

                        UserDialogs.Instance.HideLoading();
                    }

                }

            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                ex.Message.ToString();
            }
        }
       

        #region NotifyProperty

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
