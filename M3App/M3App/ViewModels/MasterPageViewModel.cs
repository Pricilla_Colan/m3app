﻿using Acr.UserDialogs;
using M3App.Cores.Helpers;
using M3App.Models;
using M3App.Views;
using Plugin.Connectivity;
using SQLite;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using M3App.Cores.DataModel;
using M3App.Cores.APIService;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace M3App.ViewModels
{
   public class MasterPageViewModel
    {
        SaveAllFields _offsave = new SaveAllFields();
        public ICommand NavigationCommand
        {
            get
            {
                return new Command(async (value) =>
                {
                    Application.Current.Properties.Remove("Check");
                    Application.Current.Properties.Add("Check", "");
                    switch (value)
                    {
                        case "1":
                            var navPage3 = new NavigationPage();
                            navPage3.BarBackgroundColor = Color.FromHex("#1A257F");
                            await navPage3.PushAsync(new M3App.Views.EmailForms() { Title = "Email Forms" });
                            //Settings.LoginStatus = "Login";
                            var pagename = "EmailForms";
                            Application.Current.Properties.Remove("PageName");
                            Application.Current.Properties.Add("PageName", pagename);
                            App.Current.MainPage = new MasterDetailPage
                            {
                                Master = new MasterPage()
                                {
                                    Title = "Menu",
                                },
                                Detail = navPage3
                            };
                            
                            break;

                        case "2":

                            Settings.LoginStatus = "Logout";
                            string status = Application.Current.Properties["Check"].ToString();
                            if (status == "Yes")
                            {
                                Settings.UserName = "";
                                Settings.Password = "";
                            }
                            
                            App.Current.MainPage = new M3App.Views.LoginForm();
                            break;

                        case "3":
                            if (CrossConnectivity.Current.IsConnected)
                            {
                                _offsave.OfflineStorage();
                            }
                            break;
                    }

                });
            }
        }
        public async void MovingDatasApi()
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    await Task.Delay(500);
                    bool status = _offsave.OfflineStorage();
                    if (status)
                    {
                        UserDialogs.Instance.HideLoading();
                        XFToast.LongMessage("Datas saved on forms successfully...");
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
