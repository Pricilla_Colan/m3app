﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace M3App.ViewModels
{
    public class RegisterViewModel : INotifyPropertyChanged
    {
        SignUp signup = new SignUp();
        LoginService Loginservice = new LoginService();
        CompanyListModel companylist = new CompanyListModel();

        int _companyid;
        string _companyname;
        public event PropertyChangedEventHandler PropertyChanged;

        public void setcompanydata(string compname, int compid)
        {
            _companyname = compname;
            _companyid = compid;
        }

        #region properties

        private string _fname;

        public string FirstName
        {
            get
            {
                return _fname;
            }
            set
            {
                _fname = value;
                NotifyPropertyChanged("FirstName");

            }
        }

        private string _lname;

        public string LastName
        {
            get
            {
                return _lname;
            }
            set
            {
                _lname = value;
                NotifyPropertyChanged("LastName");

            }
        }
        private string _email;

        public string EmailId
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                NotifyPropertyChanged("EmailId");

            }
        }
        private string _passWord;
        public string PassWord
        {
            get
            {
                return _passWord;
            }
            set
            {
                _passWord = value;
                NotifyPropertyChanged("PassWord");
            }
        }
        private string _confirmpassWord;
        public string ConfirmPassWord
        {
            get
            {
                return _confirmpassWord;
            }
            set
            {
                _confirmpassWord = value;
                NotifyPropertyChanged("ConfirmPassWord");
            }
        }
        #endregion

        #region ICommand

        public ICommand RegisterCommand
        {
            get;
            set;
        }

        public ICommand LoginButtonCommand
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        public RegisterViewModel()
        {
            RegisterCommand = new Command(OnRegister);
            LoginButtonCommand = new Command(onSignIN);
        }
        #endregion

        public void OnRegister()
        {
            /* try
             {
                 Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                 Regex passwordRegex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,30}");
                 Match match1 = regex.Match(EmailId ?? "");
                 Match match2 = passwordRegex.Match(PassWord ?? "");
                 if (string.IsNullOrWhiteSpace(FirstName))
                     XFToast.LongMessage("Please Enter First Name ");
                 else if (string.IsNullOrWhiteSpace(LastName))
                     XFToast.LongMessage("Please Enter Last Name");
                 else if (string.IsNullOrWhiteSpace(EmailId))
                     XFToast.LongMessage("Please Enter Email ID");
                 else if (!match1.Success)
                     XFToast.LongMessage("Please Enter Valid Email ID");
                 else if (string.IsNullOrWhiteSpace(PassWord))
                     XFToast.LongMessage("Please Enter PassWord");
                 else if (string.IsNullOrWhiteSpace(ConfirmPassWord))
                     XFToast.LongMessage("Please Enter Confirm PassWord");
                 else if (PassWord != ConfirmPassWord)
                     XFToast.LongMessage("Password not Matched");
                 else
                 {
                     signup = new SignUp();
                     signup.f_name = FirstName;
                     signup.l_name = LastName;
                     signup.company = _companyid;
                     signup.email = EmailId;
                     signup.password = PassWord;
                     signup.confirm_password = ConfirmPassWord;
                     onRegisterUser();
                 }
             }
             catch (Exception ex)
             {
                 ex.Message.ToString();
             }*/

            //


            try
            {
               
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Regex passwordRegex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,30}");
                Match match1 = regex.Match(EmailId ?? "");
                Match match2 = passwordRegex.Match(PassWord ?? "");
                if (string.IsNullOrWhiteSpace(FirstName))
                {
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("Please Enter First Name");
                    }
                    else
                    {
                        XFToast.LongMessage("Please Enter First Name ");
                    }
                }
                else if (string.IsNullOrWhiteSpace(LastName)) 
                {
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("Please Enter Last Name");
                    }
                    else
                    {
                        XFToast.LongMessage("Please Enter Last Name");
                    }
                }
                //XFToast.LongMessage("Please Enter Last Name");
                else if (string.IsNullOrWhiteSpace(EmailId))
                {
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("Please Enter Email ID");
                    }
                    else
                    {
                        XFToast.LongMessage("Please Enter Email ID");
                    }
                }
                //XFToast.LongMessage("Please Enter Email ID");
                else if (!match1.Success)
                {
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("Please Enter Valid Email ID");
                    }
                    else
                    {
                        XFToast.LongMessage("Please Enter Valid Email ID");
                    }
                }
                //XFToast.LongMessage("Please Enter Valid Email ID");
                else if (string.IsNullOrWhiteSpace(PassWord))
                {
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("Please Enter PassWord");
                    }
                    else
                    {
                        XFToast.LongMessage("Please Enter PassWord");
                    }
                }
                //XFToast.LongMessage("Please Enter PassWord");
                else if (string.IsNullOrWhiteSpace(ConfirmPassWord))
                {
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("Please Enter Confirm PassWord");
                    }
                    else
                    {
                        XFToast.LongMessage("Please Enter Confirm PassWord");
                    }
                }
                //XFToast.LongMessage("Please Enter Confirm PassWord");
                else if (PassWord != ConfirmPassWord)
                {
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("Password not Matched");
                    }
                    else
                    {
                        XFToast.LongMessage("Password not Matched");
                    }
                }
                //XFToast.LongMessage("Password not Matched");
                else
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    signup = new SignUp();
                    signup.f_name = FirstName;
                    signup.l_name = LastName;
                    signup.company = _companyid;
                    signup.email = EmailId;
                    signup.password = PassWord;
                    signup.confirm_password = ConfirmPassWord;
                    onRegisterUser();
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                ex.Message.ToString();
            }



            //
        }

        public void onSignIN()
        {
            App.Current.MainPage = new Views.LoginForm();
        }
        public async void onRegisterUser()
        {
            try
            {
                var signupdata = await Loginservice.Register(signup);
                if (signupdata.result == "Success")
                {
                    //XFToast.LongMessage(signupdata.message);
                    //App.Current.MainPage = new Views.LoginForm();

                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert(signupdata.message);
                        App.Current.MainPage = new Views.LoginForm();
                    }
                    else
                    {
                        XFToast.LongMessage(signupdata.message);
                        App.Current.MainPage = new Views.LoginForm();
                    }
                    UserDialogs.Instance.HideLoading();
                }
                else
                {
                    //XFToast.LongMessage(signupdata.message);
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert(signupdata.message);

                    }
                    else
                    {
                        XFToast.LongMessage(signupdata.message);
                    }
                    UserDialogs.Instance.HideLoading();
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                UserDialogs.Instance.HideLoading();
            }
        }

        #region NotifyProperty

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
