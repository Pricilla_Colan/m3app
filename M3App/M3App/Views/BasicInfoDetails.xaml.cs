﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using M3App.Models;
using M3App.ViewModels;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Services;
using SignaturePad.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BasicInfoDetails : ContentPage
    {
        DynamicJobListModel Formlistmodel = new DynamicJobListModel();
        FormTypeList FormTypeListcurrentdata = new FormTypeList();
        DynamicFormListService dynamicFormListService = new DynamicFormListService();
        FormTypeListDownLoadModel formtypelistmodel = new FormTypeListDownLoadModel();
        FormTypeUpload upload = new FormTypeUpload();
        FormJsonUpload formJson = new FormJsonUpload();
        RetrieveOfflineDatas _jobliststorage = new RetrieveOfflineDatas();
        public DateTime? MyDate { get; set; }
		string extoast = null;
        List<Form_category_InfoUpload> form_Category_InfoUploads = new List<Form_category_InfoUpload>();
        List<category_Info_fieldsUpload> category_Info_FieldsUploads = new List<category_Info_fieldsUpload>();
        List<Form_basic_Info> Basic_Infos = new List<Form_basic_Info>();
        List<Form_category_Info> form_Category_Infos = new List<Form_category_Info>();
        List<Sub_category_Info> sub_category = new List<Sub_category_Info>();
        List<category_Info_fields> category_Info_Fields = new List<category_Info_fields>();
        List<Subcategory_Info_fields> sub_category_info_Add = new List<Subcategory_Info_fields>();
        List<DownloadComments> downloadComments = new List<DownloadComments>();
        List<string> infoitem = new List<string>();
        SignaturePadView signaturePadView;
        
        FormTypeList formType_Listdata;
        string FormStatus = "";

        public BasicInfoDetails(FormTypeList formTypeListdata = null)
        {
            try
            {
                InitializeComponent();
                lblJobId.Text = Settings.JobIdKey;
                sitename.Text = Settings.SiteNameKey;
                TitleBasicinfo.Text = formTypeListdata.form_short_name;
                var pagename = base.GetType().Name;
                externalVariables(pagename);
                Application.Current.Properties.Remove("NavPageNameSave");
                Application.Current.Properties.Add("NavPageNameSave", pagename);
                Application.Current.Properties.Remove("FormStatus");
                Application.Current.Properties.Add("FormStatus", "");
                Formstatus.WidthRequest = 200;
                formType_Listdata = formTypeListdata;
                btnApprovalSave.StyleId = "FormBasicSave";
                FormTypeListcurrentdata = formTypeListdata;
                if (CrossConnectivity.Current.IsConnected)
                {
                    formtypelistmodel = GetFormTypeList(FormTypeListcurrentdata);
                    FormOfflineStorage(formtypelistmodel, formTypeListdata.id);
                }
                else
                {
                    FormOfflineRetrieve(formTypeListdata);
					extoast = "val";
                }
                btnBasicInfo.Clicked += BtnBasicInfo_Clicked;
                btnCategoryDetails.Clicked += BtnCategoryDetails_Clicked;
                backPageTap.Tapped += BackPageTap_Tapped;
                cmd_btn.Clicked += Cmd_btn_Clicked;
                btnApprovalSave.Clicked += BtnApprovalSave_Clicked;
                //Formstatus.IsVisible = true;
                // LblFormstatus.IsVisible = true;
                //btnApprovalSave.IsEnabled = true;
                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    bakicon.Margin = new Thickness(0, 15, 0, 0);
                    bakicon.Scale = 0.5;
                    TitleBasicinfo.Margin = new Thickness(0, 15, 0, 0);
                    btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 7);
                    saveapp.Margin = new Thickness(3, 3, 3, 10);
                }
                Formstatus.SelectedIndexChanged += Formstatus_SelectedIndexChanged;
                GetFormstatusList();
                GridBasicInfo.Children.Clear();
                //gridRowCounter = 0;

                if (formtypelistmodel != null)
                {
                    if (formtypelistmodel.response != null)
                    {
                        if (formtypelistmodel.response.form_json != null)
                        {
                            var formid = formtypelistmodel.response.form_id;
                            Settings.FormIdKey = formid;
                            Basic_Infos = formtypelistmodel.response.form_json.basic_Info;
                            form_Category_Infos = formtypelistmodel.response.form_json.category_Info;
                            downloadComments = formtypelistmodel.response.form_json.comments;
                            if (Basic_Infos != null)
                            {
                                foreach (var item in Basic_Infos)
                                {
                                    BasicInfoDetails_Info(GridBasicInfo, item);
                                }
                            }
                            else
                            {
                                BasicInfoFrame.IsVisible = false;
                                btnApprovalSave.IsVisible = false;
                                //XFToast.LongMessage("No data Available");
                                if (Device.RuntimePlatform.ToLower() == "ios")
                                {
                                    UserDialogs.Instance.Alert("No data Available");
                                }
                                else
                                {
                                    XFToast.LongMessage("No data Available");
                                }
                            }
                        }
                        else
                        {
                            BasicInfoFrame.IsVisible = false;
                            btnApprovalSave.IsVisible = false;
                            //XFToast.LongMessage("No data Available");
                            if (Device.RuntimePlatform.ToLower() == "ios")
                            {
                                UserDialogs.Instance.Alert("No data Available");
                            }
                            else
                            {
                                XFToast.LongMessage("No data Available");
                            }
                        }
                    }
                    else
                    {
                        BasicInfoFrame.IsVisible = false;
                        btnApprovalSave.IsVisible = false;
                        //XFToast.LongMessage(formtypelistmodel.message);
                        if (Device.RuntimePlatform.ToLower() == "ios")
                        {
							if (extoast != "val")
							{
								UserDialogs.Instance.Alert(formtypelistmodel.message);
							}
                        }
                        else
                        {
                            XFToast.LongMessage(formtypelistmodel.message);
                        }
                    }
                }
                else
                {
                    BasicInfoFrame.IsVisible = false;
                    btnApprovalSave.IsVisible = false;
                    //XFToast.LongMessage("No data Available");
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("No data Available");
                    }
                    else
                    {
                        XFToast.LongMessage("No data Available");
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        public void FormOfflineStorage(FormTypeListDownLoadModel _data,int formid)
        {
            try
            {
                var jsonformatealldatas = JsonConvert.SerializeObject(_data.response.form_json);
                FormDataDisplay _display = new FormDataDisplay();
                List<FormDataDisplay> _ldisplay = new List<FormDataDisplay>();
                _display.id = _data.response.id;
                _display.job_id = _data.response.job_id != null ? _data.response.job_id : Settings.JobIdKey;
                _display.staff_id = _data.response.staff_id;
                _display.site_id = _data.response.site_id;
                _display.form_name = _data.response.form_name;
                _display.form_type = _data.response.form_type;
                _display.form_json = jsonformatealldatas;
                _display.created_at = _data.response.created_at;
                _display.updated_at = _data.response.updated_at;
                _display.category_id = _data.response.category_id;
                _display.form_id = _data.response.form_id;
                _display.access_key = _data.response.access_key;
                _display.form_status = _data.response.form_status;
                _ldisplay.Add(_display);
                _jobliststorage.FormDatasDisplay(_ldisplay, formid);
            }
            catch (Exception ex)
            {

            }

        }
        public void FormOfflineRetrieve(FormTypeList formTypeListdata)
        {
            try
            {
                var tempjoblist = _jobliststorage.SelectAllDatas_FormDisplay(formTypeListdata);
                FormTypeResponse rs = new FormTypeResponse();
                if (tempjoblist.Count > 0)
                {
                    foreach (var itn in tempjoblist)
                    {
                        rs.id = itn.id;
                        rs.job_id = itn.job_id;
                        rs.staff_id = itn.staff_id;
                        rs.site_id = itn.site_id;
                        rs.form_name = itn.form_name;
                        rs.form_type = itn.form_type;
                        rs.form_json = JsonConvert.DeserializeObject<FormJson>(itn.form_json);
                        rs.created_at = itn.created_at;
                        rs.updated_at = itn.updated_at;
                        rs.category_id = itn.category_id;
                        rs.form_id = itn.form_id;
                        rs.access_key = itn.access_key;
                        rs.form_status = itn.form_status;

                    }
                    formtypelistmodel.response = rs;
                }
                else
                {
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("This form does not synchronized last time");
                        
                    }
                    else
                    {
                        XFToast.LongMessage("This form does not synchronized last time");
                    }
                }
				return;
            }
            catch(Exception ex)
            {

            }
        }


        private async void Cmd_btn_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new Views.CommentsPage(formtypelistmodel, formType_Listdata);
        }
        public FormStatusModel GetFormstatusList()
        {
            FormStatusModel formstatus = new FormStatusModel();
            try
            { 
                if (CrossConnectivity.Current.IsConnected)
                {
                    formstatus = dynamicFormListService.FormStatus();
                    _jobliststorage.FormStatusList(formstatus.response);
                }
                else
                {
                    List<string> _sts = new List<string>();
                    var _stslst = _jobliststorage.SelectAllDatasFormStatus();
                    foreach (var item in _stslst)
                    {
                        _sts.Add(item.form_status);
                    }
                    formstatus.response = _sts;
                }
                if (formstatus != null)
                {
                    if (!string.IsNullOrEmpty(FormTypeListcurrentdata.form_status))
                    {
                        Formstatus.ItemsSource = formstatus.response;
                        Formstatus.SelectedItem = FormTypeListcurrentdata.form_status;
                    }
                    else
                    {
                        Formstatus.ItemsSource = formstatus.response;
                        Formstatus.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {

            }


            return formstatus;
        }
        private void Formstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Picker data = sender as Picker;
                var item = data.SelectedItem;
                FormStatus = item.ToString();
                Application.Current.Properties.Remove("FormStatus");
                Application.Current.Properties.Add("FormStatus", FormStatus);

            }
            catch (Exception ex)
            {

            }

        }

       
        private void BtnApprovalSave_Clicked(object sender, EventArgs e)
        {
            SaveApproval();
        }

        public async void SaveApproval()
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
            try
            {
                var Basicdatas = GridBasicInfo.Children;
                List<Xamarin.Forms.View> Basicdata = new List<Xamarin.Forms.View>();
                foreach (var item in Basicdatas)
                {
                    var condichkd = item.GetType();
                    var _typesrc = condichkd.FullName;
                    if (_typesrc == "Xamarin.Forms.Grid")
                    {
                        var k = item as Grid;
                        var _gditm = k.Children.ToList();

                        foreach (var itm in _gditm)
                        {
                            Basicdata.Add(itm);
                        }
                    }
                    else
                    {
                        Basicdata.Add(item);

                    }
                }
                Application.Current.Properties.Remove("BasicDetails");
                Application.Current.Properties.Add("BasicDetails", Basicdata);
                SaveAllFields _fields = new SaveAllFields();
                _fields.SaveApproval(formtypelistmodel, btnApprovalSave.StyleId);
                UserDialogs.Instance.HideLoading();
            }
            catch(Exception ex)
            {
                UserDialogs.Instance.HideLoading();
            } 
        }
        public async void PostformDatas(FormTypeUpload formTypeUpload, string status = null)
        {
            try
            {
             
                var postdata = await dynamicFormListService.FormTypeUpload(formTypeUpload);
                if (postdata.result == "Success")
                {
                  
                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert(postdata.message);
                    }
                    else
                    {
                        XFToast.LongMessage(postdata.message);
                    }
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    UserDialogs.Instance.Alert("Something went wrong");
                }
                else
                {
                    XFToast.LongMessage("Something went wrong");
                }
            }
        }

        private async void BackPageTap_Tapped(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            //await Task.Delay(100);
            //App.Current.MainPage = new M3App.Views.DynamicJobList();
            await Navigation.PopAsync();
            UserDialogs.Instance.HideLoading();
        }

        private async void AddressOnFocused(object sender, FocusEventArgs focusEventArgs)
        {
            try
            {
                var Basicdatas = GridBasicInfo.Children;
                var editval = "";
                foreach (var item in Basicdatas)
                {
                    var tlist = item as Grid;
                    var rlist = tlist.Children.ToList();
                    for (int i = 0; i < rlist.Count(); i++)
                    {
                        if (rlist[i].StyleId == "Label")
                        {
                            var lbltext = rlist[i] as Label;
                            var val = lbltext.Text;
                            if (val == "Site Address")
                            {
                                var addressval = rlist[i + 1] as Editor;
                                editval = addressval.Text;
                            }
                        }
                    }
                }

                await PopupNavigation.Instance.PushAsync(new MapPage(editval));
            }
            catch (Exception ex)
            {

            }
        }
        private async void BtnCategoryDetails_Clicked(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 7);
                }
                else
                {
                    btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 10);
                }
                App.Current.MainPage = new Views.CategoryInfoDetails(formtypelistmodel, formType_Listdata);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }

        private void BtnBasicInfo_Clicked(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                //await Task.Delay(100);
                App.Current.MainPage = new Views.BasicInfoDetails(formType_Listdata);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }
        public void ImageSetting(Image p = null)
        {
            Application.Current.Properties.Remove("Image");
            Application.Current.Properties.Add("Image", p);
        }

        public FormTypeListDownLoadModel GetFormTypeList(FormTypeList Formlistmodel)
        {
            //Application.Current.Properties.Remove("FormTypeListObject");
            //Application.Current.Properties.Add("FormTypeListObject", Formlistmodel);

            var Formslistdata = dynamicFormListService.Dynamicformdatas(Formlistmodel);
            return Formslistdata;
        }
        public void BasicInfoDetails_Info(Grid grid, Form_basic_Info form_Basic_Info)
        {
            try
            {
                GridBasicInfo.RowSpacing = 0;
                grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                if (form_Basic_Info.type == "text" && form_Basic_Info.visible == "true")
                {
                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridBasicInfo.Children.ToList();
                    int bggridchild = GridBasicInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;

                    bggridrow = bggridchild;
                    int smgridchild = _sg.Children.Count;
                    smgridrow = smgridchild;
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = form_Basic_Info.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    Entry entry = new Entry
                    {
                        StyleId = "Entry",
                        FontAttributes = FontAttributes.None,
                        TextColor = Color.Blue,
                        Text = form_Basic_Info.value.ToString(),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        HorizontalTextAlignment = TextAlignment.Start,
                        VerticalOptions = LayoutOptions.Center,
                        FontSize = 13

                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(entry, 0, smgridrow + 1);


                    if (form_Basic_Info.editable == "false")
                        entry.IsEnabled = false;

                    GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                    BasicInfoFrame.Content = GridBasicInfo;


                }
                else if (form_Basic_Info.type == "checkbox" && form_Basic_Info.visible == "true")
                {
                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridBasicInfo.Children.ToList();
                    int bggridchild = GridBasicInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;

                    bggridrow = bggridchild;
                    int smgridchild = _sg.Children.Count;
                    smgridrow = smgridchild;
                    var reqrow = bggridrow;

                    List<string> multivalues = new List<string>();
                    string Checkval = form_Basic_Info.options.ToString();
                    string multival = form_Basic_Info.value.ToString();
                    if (multival.Contains("\n"))
                    {
                        multivalues = JsonConvert.DeserializeObject<List<string>>(multival);
                    }
                    Image BasicUnchecbox = new Image();
                    List<string> checkvalues = JsonConvert.DeserializeObject<List<string>>(Checkval);
                    Label Labelname = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = form_Basic_Info.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        HorizontalOptions = LayoutOptions.Start,
                        FontSize = 13,

                    };

                    _sg.Children.Add(Labelname, 0, smgridrow);

                    for (int i = 1; i <= checkvalues.Count; i++)
                    {
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });

                        if (multivalues != null && multivalues.Count > 0)
                        {
                            BasicUnchecbox = new Image
                            {
                                StyleId = multivalues.Contains(checkvalues[i - 1]) ? "Check" : "UnCheck",
                                Source = multivalues.Contains(checkvalues[i - 1]) ? "Checked.png" : "UnChecked.png",
                                Scale = 0.5,
                                Margin = new Thickness(5, 0, 10, 0),
                                HorizontalOptions = LayoutOptions.Start,
                                VerticalOptions = LayoutOptions.Center,
                                AutomationId = checkvalues[i - 1],
                            };
                        }
                        else
                        {
                            BasicUnchecbox = new Image
                            {
                                StyleId = form_Basic_Info.value.ToString() == checkvalues[i - 1] ? "Check" : "UnCheck",
                                Source = form_Basic_Info.value.ToString() == checkvalues[i - 1] ? "Checked.png" : "UnChecked.png",
                                Scale = 0.5,
                                Margin = new Thickness(5, 0, 10, 0),
                                HorizontalOptions = LayoutOptions.Start,
                                VerticalOptions = LayoutOptions.Center,
                                AutomationId = checkvalues[i - 1],
                            };
                        }
                        var basicimage1_tab = new TapGestureRecognizer();
                        basicimage1_tab.Tapped += (s, e) =>
                        {
                            var Checkimgappr = (s) as Image;
                            var SingleGrid = GridBasicInfo.Children.ToList();


                            if (Checkimgappr.StyleId == "Check")
                            {
                                Checkimgappr.StyleId = "UnCheck";
                                Checkimgappr.Source = "UnChecked.png";
                            }
                            else if (Checkimgappr.StyleId == "UnCheck")
                            {
                                Checkimgappr.StyleId = "Check";
                                Checkimgappr.Source = "Checked.png";
                            }
                        };
                        BasicUnchecbox.GestureRecognizers.Add(basicimage1_tab);

                        Label checkvalue = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = checkvalues[i - 1],
                            TextColor = Color.Black,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Center,
                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                            FontSize = 14,
                        };


                        _sg.Children.Add(BasicUnchecbox, 0, i);
                        _sg.Children.Add(checkvalue, 0, i);
                    }

                    grid.Children.Add(_sg, 0, bggridrow);
                    BasicInfoFrame.Content = GridBasicInfo;
                }

                else if (form_Basic_Info.type == "radio" && form_Basic_Info.visible == "true")
                {
                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridBasicInfo.Children.ToList();
                    int bggridchild = GridBasicInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;

                    bggridrow = bggridchild;
                    int smgridchild = _sg.Children.Count;

                    var reqrow = bggridrow;
                    smgridrow = smgridchild;

                    List<string> RadioValues = new List<string>();
                    string Radioval = form_Basic_Info.options.ToString();
                    RadioValues = JsonConvert.DeserializeObject<List<string>>(Radioval);
                    Label radioLabelname = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = form_Basic_Info.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalOptions = LayoutOptions.Start,
                        FontSize = 13,
                    };
                    _sg.Children.Add(radioLabelname, 0, smgridrow);

                    for (int i = 1; i <= RadioValues.Count; i++)
                    {
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        Image UnCheckradiobutton = new Image
                        {
                            StyleId = form_Basic_Info.value.ToString() == RadioValues[i - 1] ? "Radio" : "UnRadio",
                            Source = form_Basic_Info.value.ToString() == RadioValues[i - 1] ? "Check_Button.png" : "Uncheck_Button.png",
                            Scale = 0.4,
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            AutomationId = RadioValues[i - 1],
                        };
                        var unradio1 = new TapGestureRecognizer();
                        unradio1.Tapped += (s, e) =>
                        {
                            var imgappr = (s) as Image;
                            var SingleImg1 = imgappr.ClassId;
                            var selectedStyleId = imgappr.StyleId;
                            var SingleGrid = GridBasicInfo.Children.ToList();

                            foreach (var item in SingleGrid)
                            {
                                var condichkd = item.GetType();
                                var _typesrc = condichkd.FullName;
                                if (_typesrc == "Xamarin.Forms.Grid")
                                {
                                    var k = item as Grid;
                                    var _gditm = k.Children.ToList();
                                    foreach (var itm in _gditm)
                                    {
                                        if (s.GetType() == itm.GetType())
                                        {
                                            if (itm.StyleId == "UnRadio" || itm.StyleId == "Radio")
                                            {
                                                Image img1 = (Image)itm;
                                                img1.StyleId = "UnRadio";
                                                img1.Source = "Uncheck_Button.png";
                                            }
                                        }
                                    }
                                }
                                else
                                {

                                }
                            }

                            if (imgappr.StyleId == "UnRadio")
                            {
                                imgappr.StyleId = "Radio";
                                imgappr.Source = "Check_Button.png";
                            }
                        };
                        UnCheckradiobutton.GestureRecognizers.Add(unradio1);

                        Label radiovalue = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = RadioValues[i - 1],
                            TextColor = Color.Black,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Center,
                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                            FontSize = 14,
                        };
                        _sg.Children.Add(UnCheckradiobutton, 0, i);
                        _sg.Children.Add(radiovalue, 0, i);
                    }

                    grid.Children.Add(_sg, 0, bggridrow);
                    BasicInfoFrame.Content = GridBasicInfo;

                }



                else if (form_Basic_Info.type == "dropdown" && form_Basic_Info.visible == "true")
                {

                    string dropval = form_Basic_Info.options.ToString();
                    string CurrentData = "";

                    List<string> DropValues = JsonConvert.DeserializeObject<List<string>>(dropval);
                    if (!string.IsNullOrEmpty(form_Basic_Info.value.ToString()))
                    {
                        CurrentData = form_Basic_Info.value.ToString();
                    }

                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridBasicInfo.Children.ToList();
                    int bggridchild = GridBasicInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    bggridrow = bggridchild;

                    int smgridchild = _sg.Children.Count;

                    smgridrow = smgridchild;
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = form_Basic_Info.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    CustomPicker customPicker = new CustomPicker
                    {
                        StyleId = "CustomPicker",
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                        ItemsSource = DropValues,
                        SelectedItem = CurrentData,
                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(customPicker, 0, smgridrow + 1);
                    if (form_Basic_Info.editable == "false")
                        customPicker.IsEnabled = false;

                    GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                    BasicInfoFrame.Content = GridBasicInfo;


                }
                else if (form_Basic_Info.type == "textarea" && form_Basic_Info.visible == "true")
                {

                    Grid _sg = new Grid();
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowSpacing = 0;
                    // _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });
                    var _bggrid = GridBasicInfo.Children.ToList();
                    int bggridchild = GridBasicInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;

                    bggridrow = bggridchild;

                    int smgridchild = _sg.Children.Count;

                    smgridrow = smgridchild;
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = form_Basic_Info.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13

                    };
                    //Mapo
                    Editor Address = new Editor
                    {
                        StyleId = "Textarea",
                        FontAttributes = FontAttributes.None,
                        TextColor = Color.Blue,
                        Text = form_Basic_Info.value.ToString(),
                        FontSize = 14,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };
                    Address.Focused += AddressOnFocused;


                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(Address, 0, smgridrow + 1);
                    if (form_Basic_Info.editable == "false")
                        Address.IsEnabled = true;

                    GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                    BasicInfoFrame.Content = GridBasicInfo;
                }
                else if (form_Basic_Info.type == "date" && form_Basic_Info.visible == "true")
                {

                    Grid _sg = new Grid();
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowSpacing = 0;
                    var _bggrid = GridBasicInfo.Children.ToList();
                    int bggridchild = GridBasicInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;


                    bggridrow = bggridchild;

                    int smgridchild = _sg.Children.Count;

                    smgridrow = smgridchild;
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = form_Basic_Info.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13

                    };
                    DateTime reqdate;
                    if (form_Basic_Info.value == "")
                    {
                        reqdate = DateTime.Now;
                    }
                    else
                    {

                        string formattedDates = "";
                        string t = form_Basic_Info.value.ToString();
                        string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "MM-dd-yyyy", "dd-MM-yyyy", "M-dd-yyyy", "M/dd/yyyy" };
                        DateTime date;
                        if (DateTime.TryParseExact(t, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                            formattedDates = date.ToString("dd/MM/yyyy");
                        if (formattedDates != null && formattedDates != "")
                        {
                            string[] _list = formattedDates.Split('/');
                            if (DateTime.TryParseExact(t, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                                formattedDates = date.ToString("MM/dd/yyyy");
                            var culture = System.Globalization.CultureInfo.CurrentCulture;
                            var dt = DateTime.ParseExact(formattedDates, "MM/dd/yyyy", culture);
                            reqdate = dt;
                        }
                        else
                        {
                            reqdate = DateTime.Now;
                        }

                    }


                    DatePicker datePicker = new DatePicker
                    {

                        StyleId = "DatePicker",
                        Format = "MM-dd-yyyy",
                        Date = reqdate,
                        TextColor = Color.Blue,
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(datePicker, 0, smgridrow + 1);
                    if (form_Basic_Info.editable == "false")
                        datePicker.IsEnabled = false;

                    GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                    BasicInfoFrame.Content = GridBasicInfo;
                }
                else if (form_Basic_Info.type == "signature" && form_Basic_Info.visible == "true")
                {


                    Grid _sg = new Grid();

                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                    _sg.RowSpacing = 0;
                    var _bggrid = GridBasicInfo.Children.ToList();
                    int bggridchild = GridBasicInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;

                    bggridrow = bggridchild;
                    int smgridchild = _sg.Children.Count;

                    smgridrow = smgridchild;

                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = form_Basic_Info.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    ImageSource retSource = null;

                    signaturePadView = new SignaturePadView
                    {
                        StyleId = "BasicSignature",
                        BackgroundColor = Color.White,
                        StrokeColor = Color.Black,
                        HeightRequest = 70,
                        StrokeWidth = 2,
                    };

                    var forgetPassword_tap = new TapGestureRecognizer();
                    signaturePadView.ClearLabel.GestureRecognizers.Add(forgetPassword_tap);
                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                    _sg.Children.Add(signaturePadView, 0, smgridrow + 1);

                    if (form_Basic_Info.value.ToString() != null && form_Basic_Info.value.ToString() != "")
                    {
                        signaturePadView.ClearLabel.IsVisible = true;
                        Application.Current.Properties.Remove("SignImage");
                        Application.Current.Properties.Add("SignImage", form_Basic_Info.value.ToString());

                        Application.Current.Properties.Remove("ImageHaving");
                        Application.Current.Properties.Add("ImageHaving", "True");
                        byte[] Base64Stream = Convert.FromBase64String(form_Basic_Info.value.ToString());
                        retSource = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                        Image sign = new Image
                        {
                            Source = retSource,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalOptions = LayoutOptions.Center,
                            Scale = 0.4,
                        };
                        ImageSetting(sign);
                        _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                        _sg.Children.Add(sign, 0, smgridrow + 1);

                    }
                    forgetPassword_tap.Tapped += (s, e) =>
                    {
                        Image mm;

                        if (form_Basic_Info.value.ToString() != null && form_Basic_Info.value.ToString() != "")
                        {
                            var p = Application.Current.Properties["Image"];
                            if (p != null && p != "")
                            {
                                mm = p as Image;
                                mm.IsVisible = false;
                            }
                        }
                    };

                    GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                    BasicInfoFrame.Content = GridBasicInfo;
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void externalVariables(string pagename)
        {
            try
            {
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
                Application.Current.Properties.Remove("SelectedData");
                Application.Current.Properties.Add("SelectedData", "");
                Application.Current.Properties.Remove("SelectedSubData");
                Application.Current.Properties.Add("SelectedSubData", "");
                Application.Current.Properties.Remove("NavPageName");
                Application.Current.Properties.Add("NavPageName", "");
                Application.Current.Properties.Remove("NavPageNameSave");
                Application.Current.Properties.Add("NavPageNameSave", "");
            }
            catch (Exception ex)
            {

            }
        }
    }
}