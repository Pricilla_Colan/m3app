﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using M3App.Models;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CategoryInfoDetails : ContentPage
	{
        FormTypeListDownLoadModel formtypelist_model;
        FormTypeList formTypeList_data;
        FormTypeUpload formupload = new FormTypeUpload();
        RetrieveOfflineDatas _jobliststorage = new RetrieveOfflineDatas();
        DynamicFormListService dynamicFormListService = new DynamicFormListService();
        String selectedYear = "2020";
        public CategoryInfoDetails (FormTypeListDownLoadModel formtypelistmodel, FormTypeList formTypeListdata)
		{
            try
            {
                InitializeComponent();
                LoadData_CategoryList(formtypelistmodel);
                lblJobId.Text = Settings.JobIdKey;
                yearPicker.SelectedItem = Settings.YearKey;
                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    bakicon.Margin = new Thickness(0, 15, 0, 0);
                    bakicon.Scale = 0.5;
                    TitleBasicinfo.Margin = new Thickness(0, 15, 0, 0);
                    btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 7);

                }
                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
                Application.Current.Properties.Remove("NavPageNameSave");
                Application.Current.Properties.Add("NavPageNameSave", pagename);
                sitename.Text = Settings.SiteNameKey;
                TitleBasicinfo.Text = formTypeListdata.form_short_name;
                backPageTap.Tapped += BackPageTap_Tapped;
                

                string currentYear = DateTime.Now.Year.ToString();
                Console.WriteLine(currentYear);

                btnBasicInfo.Clicked += BtnBasicInfo_Clicked;
                cmd_btn.Clicked += Cmd_btn_Clicked;
                formtypelist_model = formtypelistmodel;
                formTypeList_data = formTypeListdata;
                
            }
            catch (Exception ex)
            {

            }
        }
        private async void BtnBasicInfo_Clicked(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
            App.Current.MainPage = new Views.BasicInfoDetails(formTypeList_data);
            UserDialogs.Instance.HideLoading();
        }

        private async void BackPageTap_Tapped(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
            App.Current.MainPage = new M3App.Views.BasicInfoDetails(formTypeList_data);
            UserDialogs.Instance.HideLoading();
        }
        public void LoadData_CategoryList(FormTypeListDownLoadModel formtypelistmodel)
        {
            try
            {
                List<Form_category_Info> form_Category_Infos = new List<Form_category_Info>();
                if (formtypelistmodel != null)
                {
                    if (formtypelistmodel.response.form_json != null)
                    {
                        form_Category_Infos = formtypelistmodel.response.form_json.category_Info;

                        if (form_Category_Infos != null && form_Category_Infos.Count > 0)
                        {
                            //Category_List.ItemsSource = form_Category_Infos;
                            CategoryInfo.ItemsSource = formtypelistmodel.response.form_json.category_Info;
                        }
                        else
                        {
                            //XFToast.LongMessage("No data Available");
                            if (Device.RuntimePlatform.ToLower() == "ios")
                            {
                                UserDialogs.Instance.Alert("No data Available");
                            }
                            else
                            {
                                XFToast.LongMessage("No data Available");
                            }
                        }
                    }
                    else
                    {
                        //XFToast.LongMessage("No data Available");
                        if (Device.RuntimePlatform.ToLower() == "ios")
                        {
                            UserDialogs.Instance.Alert("No data Available");
                        }
                        else
                        {
                            XFToast.LongMessage("No data Available");
                        }
                    }
                }
                else
                {
                    //XFToast.LongMessage("No data Available");
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("No data Available");
                    }
                    else
                    {
                        XFToast.LongMessage("No data Available");
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }
        private async void Cmd_btn_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new Views.CommentsPage(formtypelist_model, formTypeList_data);
        }
        public FormTypeListDownLoadModel GetFormTypeList(FormTypeList Formlistmodel)
        {
            var Formslistdata = dynamicFormListService.Dynamicformdatas(Formlistmodel);
            return Formslistdata;
        }
        private async void buttonSearch_Tapped(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                var headerval = sender as Grid;
                var selecteditem = (Grid)sender;
                var item = selecteditem.BindingContext;
                var infoitem = item as Form_category_Info;
                FormTypeListDownLoadModel infoitem_data = new FormTypeListDownLoadModel();
                if (CrossConnectivity.Current.IsConnected)
                {
                     infoitem_data = GetFormTypeList(formTypeList_data);
                }
                else
                {
                    var tempjoblist = _jobliststorage.SelectAllDatas_FormDisplay(formTypeList_data);
                    FormTypeResponse rs = new FormTypeResponse();
                    if (tempjoblist.Count > 0)
                    {
                        foreach (var itn in tempjoblist)
                        {
                            rs.id = itn.id;
                            rs.job_id = itn.job_id;
                            rs.staff_id = itn.staff_id;
                            rs.site_id = itn.site_id;
                            rs.form_name = itn.form_name;
                            rs.form_type = itn.form_type;
                            rs.form_json = JsonConvert.DeserializeObject<FormJson>(itn.form_json);
                            rs.created_at = itn.created_at;
                            rs.updated_at = itn.updated_at;
                            rs.category_id = itn.category_id;
                            rs.form_id = itn.form_id;
                            rs.access_key = itn.access_key;
                            rs.form_status = itn.form_status;

                        }
                        infoitem_data.response = rs;
                    }

                }
                var t_data = (from m in infoitem_data.response.form_json.category_Info
                              where m.id == infoitem.parent_id
                              select m).ToList();
                App.Current.MainPage = new Views.SubCategoryInfoDetails(formTypeList_data, infoitem, formtypelist_model);
                UserDialogs.Instance.HideLoading();
            }
            catch(Exception ex)
            {

            }
        }

        void yearPicker_SelectedIndexChanged(System.Object sender, System.EventArgs e)
        {
            Picker picker = sender as Picker;
            selectedYear = picker.SelectedItem.ToString();
            if (selectedYear != null)
            {
                Settings.YearKey = selectedYear;
            }
            var formlistModel = GetFormTypeList(formTypeList_data);
            LoadData_CategoryList(formlistModel);
            //   formupload.year = Settings.YearKey;
            //   SaveAllFields _fields = new SaveAllFields();
            //_fields.SaveApproval(formtypelist_model, "CategoryFields_Save");
            //   LoadData_CategoryList(formtypelist_model);

        }

        public async void PostformDatas(FormTypeUpload formTypeUpload, string status = null)
        {
            try
            {
                
                var postdata = await dynamicFormListService.FormTypeUpload(formTypeUpload);
                if (postdata.result == "Success")
                {

                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert(postdata.message);
                    }
                    else
                    {
                        XFToast.LongMessage(postdata.message);
                    }
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    UserDialogs.Instance.Alert("Something went wrong");
                }
                else
                {
                    XFToast.LongMessage("Something went wrong");
                }
            }
        }
    }
}