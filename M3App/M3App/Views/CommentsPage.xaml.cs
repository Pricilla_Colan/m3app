﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using M3App.Models;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CommentsPage : ContentPage
	{
        List<DownloadComments> downloadComments = new List<DownloadComments>();
        DynamicFormListService dynamicFormListService = new DynamicFormListService();
        FormTypeListDownLoadModel formtype_listmodel;
        FormTypeList form_type;
        RetrieveOfflineDatas _jobliststorage = new RetrieveOfflineDatas();
        string GetPageName;
        
        //dynamic Selected_data;
        SaveAllFields _fields = new SaveAllFields();
        public CommentsPage (FormTypeListDownLoadModel formtypelistmodel=null, FormTypeList formTypeListdata=null)
		{
            try
            {
                InitializeComponent();
                TitleBasicinfo.Text = formTypeListdata.form_short_name;
                lblJobId.Text = Settings.JobIdKey;
                sitename.Text = Settings.SiteNameKey;
                backPageTap.Tapped += BackPageTap_Tapped;
                ViewCommentBtn.Clicked += ViewCommentBtn_Clicked;
                CommentsClose.Clicked += CommentsClose_Clicked;
                formtype_listmodel = formtypelistmodel;
                form_type = formTypeListdata;
                GetPageName = Application.Current.Properties["PageName"].ToString();
                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
            }
            catch (Exception ex)
            {

            }
        }
        private void CommentsClose_Clicked(object sender, EventArgs e)
        {
            SaveApproval();
        }
        public async void SaveApproval(string CommentsStatus = null, List<DownloadComments> dts = null)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                var Commentsdatas = CommentsGrid.Children;
                List<Xamarin.Forms.View> comntsdata = new List<Xamarin.Forms.View>();
                foreach (var item in Commentsdatas)
                {
                    var condichkd = item.GetType();
                    var _typesrc = condichkd.FullName;
                    if (_typesrc == "Xamarin.Forms.Grid")
                    {
                        var k = item as Grid;
                        var _gditm = k.Children.ToList();

                        foreach (var itm in _gditm)
                        {
                            comntsdata.Add(itm);
                        }
                    }
                    else
                    {
                        comntsdata.Add(item);
                    }
                }
                DefaultvalueAssign();
                Application.Current.Properties.Remove("CommentsDetails");
                Application.Current.Properties.Add("CommentsDetails", comntsdata);
                _fields.SaveApproval(formtype_listmodel,"AddComments");
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
            }
        }
        public void DefaultvalueAssign()
        {
            try
            {
                formtype_listmodel = null;
                if (formtype_listmodel == null)
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        formtype_listmodel = new FormTypeListDownLoadModel();
                        formtype_listmodel = GetFormTypeList(form_type);
                    }
                    else
                    {
                        formtype_listmodel = new FormTypeListDownLoadModel();
                        var tempjoblist = _jobliststorage.SelectAllDatas_FormDisplay(form_type);
                        FormTypeResponse rs = new FormTypeResponse();
                        if (tempjoblist.Count > 0)
                        {
                            foreach (var itn in tempjoblist)
                            {
                                rs.id = itn.id;
                                rs.job_id = itn.job_id;
                                rs.staff_id = itn.staff_id;
                                rs.site_id = itn.site_id;
                                rs.form_name = itn.form_name;
                                rs.form_type = itn.form_type;
                                rs.form_json = JsonConvert.DeserializeObject<FormJson>(itn.form_json);
                                rs.created_at = itn.created_at;
                                rs.updated_at = itn.updated_at;
                                rs.category_id = itn.category_id;
                                rs.form_id = itn.form_id;
                                rs.access_key = itn.access_key;
                                rs.form_status = itn.form_status;

                            }
                            formtype_listmodel.response = rs;
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private async void BackPageTap_Tapped(object sender, EventArgs e)
        {
            try
            {
                DefaultvalueAssign();
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                if (GetPageName == "ViewComments")
                {
                    GetPageName = Application.Current.Properties["NavPageNameSave"].ToString();
                }
                if (GetPageName == "BasicInfoDetails")
                {
                    App.Current.MainPage = new M3App.Views.BasicInfoDetails(form_type);
                }
                else if (GetPageName == "CategoryInfoDetails")
                {
                   // var formUpload = Application.Current.Properties["FormTypeUpload"] as FormTypeUpload;
                    App.Current.MainPage = new M3App.Views.CategoryInfoDetails(formtype_listmodel, form_type);
                }
                else if (GetPageName == "SubCategoryInfoDetails")
                {
                    var selecteddata = Application.Current.Properties["SelectedData"] as Form_category_Info;
                    App.Current.MainPage = new M3App.Views.SubCategoryInfoDetails(form_type, selecteddata, formtype_listmodel);
                }
                else if (GetPageName == "SubCategoryDetailsDescription")
                {
                    var selecteddata = Application.Current.Properties["SelectedSubData"] as Sub_category_Info;
                    var selectedCategory = Application.Current.Properties["SelectedData"] as Form_category_Info;
                    App.Current.MainPage = new M3App.Views.SubCategoryDetailsDescription(form_type, selecteddata, selectedCategory, formtype_listmodel);
                }
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }
        private async void ViewCommentBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                int i = 0;
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                Application.Current.Properties.Remove("NavPageName");
                Application.Current.Properties.Add("NavPageName", GetPageName);

                if (CrossConnectivity.Current.IsConnected)
                {
                    var tt = form_type as FormTypeList;
                    var Formslistdata = dynamicFormListService.Dynamicformdatas(tt);
                    if (Formslistdata != null)
                    {
                        downloadComments = Formslistdata.response.form_json.comments;
                        if (downloadComments != null)
                        {
                            downloadComments.ForEach(x =>
                            {
                                x.Index = i;
                                i++;
                            });
                        }
                    }
                    else
                    {
                        //XFToast.ShortMessage("No Comments Here!");
                        if (Device.RuntimePlatform.ToLower() == "ios")
                        {
                            UserDialogs.Instance.Alert("No Comments Here!");
                        }
                        else
                        {
                            XFToast.LongMessage("No Comments Here!");
                        }
                    }
                }
                else
                {
                    var tempjoblist = _jobliststorage.SelectAllDatas_FormDisplay(form_type);
                    FormTypeResponse rs = new FormTypeResponse();
                    if (tempjoblist.Count > 0)
                    {
                        foreach (var itn in tempjoblist)
                        {
                            rs.id = itn.id;
                            rs.job_id = itn.job_id;
                            rs.staff_id = itn.staff_id;
                            rs.site_id = itn.site_id;
                            rs.form_name = itn.form_name;
                            rs.form_type = itn.form_type;
                            rs.form_json = JsonConvert.DeserializeObject<FormJson>(itn.form_json);
                            rs.created_at = itn.created_at;
                            rs.updated_at = itn.updated_at;
                            rs.category_id = itn.category_id;
                            rs.form_id = itn.form_id;
                            rs.access_key = itn.access_key;
                            rs.form_status = itn.form_status;

                        }
                        formtype_listmodel.response = rs;
                        var Formslistdata = formtype_listmodel.response;
                        if (Formslistdata != null)
                        {
                            downloadComments = Formslistdata.form_json.comments;
                            if (downloadComments != null)
                            {
                                downloadComments.ForEach(x =>
                                {
                                    x.Index = i;
                                    i++;
                                });
                            }
                        }
                    }
                    else
                    {
                        //XFToast.ShortMessage("No Comments Here!");
                        if (Device.RuntimePlatform.ToLower() == "ios")
                        {
                            UserDialogs.Instance.Alert("No Comments Here!");
                        }
                        else
                        {
                            XFToast.LongMessage("No Comments Here!");
                        }

                    }
                }
                App.Current.MainPage = new M3App.Views.ViewComments(downloadComments, form_type);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }
        public FormTypeListDownLoadModel GetFormTypeList(FormTypeList Formlistmodel)
        {
            var Formslistdata = dynamicFormListService.Dynamicformdatas(Formlistmodel);
            return Formslistdata;
        }
    }
}