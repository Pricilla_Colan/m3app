﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using M3App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
	
	public partial class ComposeMailForm : ContentPage
	{
        List<FormTypeList> CallCheckList = new List<FormTypeList>();
        DynamicFormListService dynamicFormListService = new DynamicFormListService();
        string siteNamedata = null;
        public ComposeMailForm (List<FormTypeList> CheckedList)
		{
            try
            {
                InitializeComponent();
                lblJobId.Text = Settings.JobIdKey;
                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
                
                if (Device.RuntimePlatform.ToLower() == "ios")
				{
					btnSendNow.HeightRequest = 50;
					headerinfo.Margin = new Thickness(0, 15, 0, 0);
					bakicon.Margin = new Thickness(0, 12, 0, 0);
					TitleBasicinfo.Margin = new Thickness(0, 10, 0, 0);
				}

                txtFrom.Text = Settings.UserName;
                string t= Settings.LogEmail;
                txtTo.Text = t;
                var date = DateTime.Now.Date.ToString("MM-dd-yyyy");
                txtSubject.Text = Settings.SiteNameKey + "_" + date;
                siteNamedata=Settings.SiteNameKey + "_" + date;
                CallCheckList = CheckedList;
                if (CheckedList.Count > 0)
                {
                    //int Namecount = 0;
                    //decimal l = Convert.ToDecimal(CheckedList.Count) / Convert.ToDecimal(2);
                    //decimal l1 = CheckedList.Count / 2;
                    //for (int i = 0; i < CheckedList.Count / 2; i++)
                    //{
                    //    BindDynamicForm(i, CheckedList.Count, CheckedList[Namecount].form_name, CheckedList[Namecount + 1].form_name, CheckedList[Namecount], CheckedList[Namecount + 1]);
                    //    Namecount = Namecount + 2;
                    //}
                    //if (CheckedList.Count % 2 != 0)
                    //{
                    //    decimal l2 = l - l1;
                    //    if (l2 < 1)
                    //    {
                    //        for (int i = 0; i < 1; i++)
                    //        {
                    //            BindDynamicForm1(DynamicAttachment.RowDefinitions.Count - 1, CheckedList[CheckedList.Count - 1]);
                    //        }
                    //    }
                    //}
                    for (int i = 0; i < CheckedList.Count ; i++)
                    {
                        DynamicAttachments(CheckedList[i].form_name);
                    }
                }
                btnSendNow.Clicked += BtnSendNow_Clicked;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
        public void DynamicAttachments(string Name)
        {
            DynamicAttachment.RowSpacing = 0;
            DynamicAttachment.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            Grid _sg = new Grid();
            _sg.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(20) });
            _sg.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
            var rwcnt = 0;
            int smgridchild = DynamicAttachment.Children.Count;
           
            Image img = new Image
            {
                Source = "attachment.png",
                Scale = 1,
                Margin = new Thickness(10, 0, 0, 0),
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
            };

            Label Firstlabel = new Label()
            {
                StyleId = "Label",
               // LineBreakMode = LineBreakMode.TailTruncation,
                FontAttributes = FontAttributes.None,
                Text = Name+"_"+ siteNamedata,
                TextColor = Color.Gray,
                VerticalTextAlignment = TextAlignment.Start,
                HorizontalTextAlignment = TextAlignment.Start,
                FontSize = 14,
            };
            _sg.Children.Add(img, 0, 0);
            _sg.Children.Add(Firstlabel, 1, 0);

            DynamicAttachment.Children.Add(_sg, 0, smgridchild);


        }

        public void BindDynamicForm(int a, int rowCount, string Name, string Name1, FormTypeList formTypeLists, FormTypeList formTypeList1)
        {
            try
            {
                Grid framegridfirst = new Grid
                {
                    RowDefinitions =
                {
                    new RowDefinition {Height =  GridLength.Auto},
                    new RowDefinition {Height = GridLength.Auto},
                },
                    ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                }
                };
                Image img = new Image
                {
                    Source = "attachment.png",
                    Scale = 0.5,
                    Margin = new Thickness(10, 0, 0, 0),
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.Start,
                };

                Label Firstlabel = new Label()
                {
                    StyleId = "Label",
                    LineBreakMode = LineBreakMode.TailTruncation,
                    FontAttributes = FontAttributes.None,
                    Text = Name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Start,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 14,
                };
                framegridfirst.Children.Add(Firstlabel, 0, 0);
                DynamicAttachment.Children.Add(framegridfirst, 0, a);

                Grid framegridsecond = new Grid
                {
                    RowDefinitions =
                {
                    new RowDefinition {Height =  GridLength.Auto},
                    new RowDefinition {Height = GridLength.Auto},

                },
                    ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                }

                };
                Label secondlabel = new Label()
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    TextColor = Color.Gray,
                    Text = Name1,
                    LineBreakMode = LineBreakMode.TailTruncation,
                    VerticalTextAlignment = TextAlignment.Start,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 14
                };
                framegridsecond.Children.Add(secondlabel, 1, 0);
                DynamicAttachment.Children.Add(framegridsecond, 1, a);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }




        //public void BindDynamicForm(int a, int rowCount, string Name, string Name1, FormTypeList formTypeLists, FormTypeList formTypeList1)
        //{
        //    var count = 0;
        //    try
        //    {
        //        Grid framegridfirst = new Grid
        //        {
        //            RowDefinitions =
        //        {
        //            new RowDefinition {Height =  GridLength.Auto},
        //            new RowDefinition {Height = GridLength.Auto},
        //        },
        //            ColumnDefinitions =
        //        {
        //            new ColumnDefinition { Width = GridLength.Auto },
        //            new ColumnDefinition { Width = GridLength.Auto },
        //        }
        //        };
        //        Image img = new Image
        //        {
        //            Source = "attachment.png",
        //            Scale = 0.2,
        //            Margin = new Thickness(10, 0, 0, 0),
        //            HorizontalOptions = LayoutOptions.Start,
        //            VerticalOptions = LayoutOptions.Start,
        //        };
                

        //            Label Firstlabel = new Label()
        //            {
        //                StyleId = "Label",
        //                LineBreakMode = LineBreakMode.TailTruncation,
        //                FontAttributes = FontAttributes.None,
        //                Text = Name,
        //                TextColor = Color.Gray,
        //                VerticalTextAlignment = TextAlignment.Start,
        //                HorizontalTextAlignment = TextAlignment.Start,
        //                FontSize = 14,
        //            };
        //            framegridfirst.Children.Add(img, 0, 0);
        //            framegridfirst.Children.Add(Firstlabel, 1, 0);
                
                
        //            Label secondlabel = new Label()
        //            {
        //                StyleId = "Label",
        //                FontAttributes = FontAttributes.None,
        //                TextColor = Color.Gray,
        //                Text = Name1,
        //                LineBreakMode = LineBreakMode.TailTruncation,
        //                VerticalTextAlignment = TextAlignment.Start,
        //                HorizontalTextAlignment = TextAlignment.Start,
        //                FontSize = 14
        //            };
        //            framegridfirst.Children.Add(img, 0, 1);
        //            framegridfirst.Children.Add(secondlabel, 1, 1);

                
                
        //        DynamicAttachment.Children.Add(framegridfirst, 0, a);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        UserDialogs.Instance.HideLoading();
        //    }
        //}


        private void BindDynamicForm1(int a, FormTypeList formTypeLists)
        {
            //try
            //{
            //    Grid bottomgrid = new Grid
            //    {
            //        RowDefinitions =
            //    {
            //       // new RowDefinition {Height = GridLength.Auto},
            //        new RowDefinition {Height = GridLength.Auto},

            //    },
            //        ColumnDefinitions =
            //        {
            //            new ColumnDefinition{Width=20},
            //            new ColumnDefinition{Width=GridLength.Auto},
            //        }

            //    };
            //    //Frame bottomframe = new Frame
            //    //{
            //    //    CornerRadius = 10,
            //    //    HasShadow = true,
            //    //    Margin = new Thickness(5)

            //    //};
            //    Image img = new Image
            //    {
            //        Source = "attachment.png",
            //        Scale = 0.2,
            //        Margin = new Thickness(10, 0, 0, 0),
            //        HorizontalOptions = LayoutOptions.Start,
            //        VerticalOptions = LayoutOptions.Start,
            //    };
            //    Label label = new Label()
            //    {
            //        StyleId = "Label",
            //        LineBreakMode = LineBreakMode.TailTruncation,
            //        FontAttributes = FontAttributes.None,
            //        TextColor = Color.Gray,
            //        Text = formTypeLists.form_name,
            //        VerticalTextAlignment = TextAlignment.Start,
            //        HorizontalTextAlignment = TextAlignment.Start,
            //        FontSize = 14,
            //    };
            //    bottomgrid.Children.Add(img, 0, 0);
            //    bottomgrid.Children.Add(label, 0, 0);
            //    DynamicAttachment.Children.Add(bottomgrid, 0, a);

            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //finally
            //{
            //    UserDialogs.Instance.HideLoading();
            //}


            try
            {
                DynamicAttachment.RowSpacing = 0;
                DynamicAttachment.ColumnSpacing = 0;
                DynamicAttachment.RowDefinitions.Add(new RowDefinition { Height = new GridLength(30) });
                DynamicAttachment.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
                DynamicAttachment.ColumnDefinitions.Add(new ColumnDefinition { Width =  GridLength.Auto });

                Image img = new Image
                {
                    Source = "attachment.png",
                    Scale = 0.2,
                    Margin = new Thickness(10, 0, 0, 0),
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.Start,
                };
                Label label = new Label()
                {
                    StyleId = "Label",
                    LineBreakMode = LineBreakMode.TailTruncation,
                    FontAttributes = FontAttributes.None,
                    TextColor = Color.Gray,
                    Text = formTypeLists.form_name,
                    VerticalTextAlignment = TextAlignment.Start,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 14,
                };

                DynamicAttachment.Children.Add(img,0,0);
                DynamicAttachment.Children.Add(label,1,0);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }


        }
        public async void RedirectEmail()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Navigation.PopToRootAsync();
                //App.Current.MainPage = new Views.EmailForms();
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }
        private async void BtnSendNow_Clicked(object sender, EventArgs e)
        {           
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match;
                List<int> AttachmentID = new List<int>();
                ComposeMail sendMail = new ComposeMail();
                if (CallCheckList.Count > 0)
                {
                    foreach (var item in CallCheckList)
                    {
                        AttachmentID.Add(item.id);
                    }
                }
                if (!string.IsNullOrEmpty(txtTo.Text.Trim()))
                {
                    match = regex.Match(txtTo.Text.Trim());
                    if (!match.Success)
                    {
                        XFToast.LongMessage("Please Enter Valid Email ID");
                    }
                    else
                    {
                        sendMail.email_from = txtFrom.Text;
                        sendMail.email_to = txtTo.Text.TrimEnd();
                        sendMail.attachment = AttachmentID.Count == 0 ? new int[] { } : AttachmentID.ToArray();
                        sendMail.email_subject = txtSubject.Text;
                        sendMail.email_description = txtMessage.Text;
                        sendMail.access_key = Settings.AccessKey;
                        sendMail.job_id = Settings.JobIdKey;
                        await SendMail(sendMail);
                    }
                }
                else
                {
                    XFToast.LongMessage("Please Enter Email ID");
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }            
        } 
		private async void ButtonBack_Tapped(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
			App.Current.MainPage = new Views.EmailForms();
            UserDialogs.Instance.HideLoading();

        }

        public async Task<ComposemailResponse> SendMail(ComposeMail mail)
        {
            try
            {
                var Mailresponse = await dynamicFormListService.Mailresponse(mail);

                if (Mailresponse != null)
                {
                    if (Mailresponse.result == "Success")
                    {
                        XFToast.LongMessage(Mailresponse.message);
                        //await Navigation.PushAsync(new Views.JobList() {Title="Job List"});

                        //var navPage3 = new NavigationPage();
                        //navPage3.ToolbarItems.Clear();
                        //navPage3.BarBackgroundColor = Color.FromHex("#1A257F");
                        //await navPage3.PushAsync(new M3App.Views.JobList() { Title = "Job List" });
                        //App.Current.MainPage = new MasterDetailPage
                        //{
                        //    Master = new MasterPage()
                        //    {
                        //        Title = "Menu",
                        //    },
                        //    Detail = navPage3
                        //};
                        //var navPage3 = new NavigationPage(new M3App.Views.JobList() { Title = "Job List" });
                        //navPage3.ToolbarItems.Clear();
                        //navPage3.BarBackgroundColor = Color.FromHex("#1A257F");
                        ////await navPage3.PushAsync(new M3App.Views.JobList() { Title = "Job List" });
                        //App.Current.MainPage = new MasterDetailPage
                        //{
                        //    Master = new MasterPage()
                        //    {
                        //        Title = "Menu",
                        //    },
                        //    Detail = navPage3
                        //};
                        //await navPage3.PopToRootAsync();
                        UserDialogs.Instance.HideLoading();
                        // await Navigation.PopToRootAsync();
                        //NavigationPage page = new NavigationPage(new M3App.Views.JobList() { Title = "Job List" });
                        //App.Current.MainPage = page;
                        RedirectEmail();



                    }
                    else
                    {
                        XFToast.LongMessage(Mailresponse.message);
                    }
                }
                else
                {
                    XFToast.ShortMessage("Something went wrong please try again");
                }

                return Mailresponse;
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                return null;
            }
        }
    }
}