﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Models;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DisplayAlertMessage 
	{
        DownloadComments onecomment = new DownloadComments();
        List<DownloadComments> multicomments = new List<DownloadComments>();
        SaveAllFields _fields = new SaveAllFields();
        RetrieveOfflineDatas _jobliststorage = new RetrieveOfflineDatas();
        DynamicFormListService dynamicFormListService = new DynamicFormListService();
        public DisplayAlertMessage (DownloadComments singlecmt, List<DownloadComments> multiplecmt)
		{
            try
            {
                InitializeComponent();
                onecomment = singlecmt;
                multicomments = multiplecmt;
                Ok.Clicked += Ok_Clicked;
                Cancel.Clicked += Cancel_Clicked;
                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
            }
            catch (Exception ex)
            {

            }
        }
        private void Cancel_Clicked(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }

        private async void Ok_Clicked(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                var tf = (from m in multicomments
                          where m.Index == onecomment.Index
                          select m).FirstOrDefault();
                multicomments.Remove(tf);
                var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                //var formtypelistmodel = GetFormTypeList(FormListtype);
                FormTypeListDownLoadModel formtype_listmodel = new FormTypeListDownLoadModel();
                if (CrossConnectivity.Current.IsConnected)
                {
                    formtype_listmodel = new FormTypeListDownLoadModel();
                    formtype_listmodel = GetFormTypeList(FormListtype);
                }
                else
                {
                    formtype_listmodel = new FormTypeListDownLoadModel();
                    var tempjoblist = _jobliststorage.SelectAllDatas_FormDisplay(FormListtype);
                    FormTypeResponse rs = new FormTypeResponse();
                    if (tempjoblist.Count > 0)
                    {
                        foreach (var itn in tempjoblist)
                        {
                            rs.id = itn.id;
                            rs.job_id = itn.job_id;
                            rs.staff_id = itn.staff_id;
                            rs.site_id = itn.site_id;
                            rs.form_name = itn.form_name;
                            rs.form_type = itn.form_type;
                            rs.form_json = JsonConvert.DeserializeObject<FormJson>(itn.form_json);
                            rs.created_at = itn.created_at;
                            rs.updated_at = itn.updated_at;
                            rs.category_id = itn.category_id;
                            rs.form_id = itn.form_id;
                            rs.access_key = itn.access_key;
                            rs.form_status = itn.form_status;

                        }
                        formtype_listmodel.response = rs;
                    }

                }
                _fields.SaveApproval(formtype_listmodel, "DeleteComments", null, null, multicomments);
                UserDialogs.Instance.HideLoading();
                PopupNavigation.Instance.PopAsync();
            }
            catch (Exception ex)
            {

            }
        }
        public FormTypeListDownLoadModel GetFormTypeList(FormTypeList Formlistmodel)
        {
            var Formslistdata = dynamicFormListService.Dynamicformdatas(Formlistmodel);
            return Formslistdata;
        }
    }
}