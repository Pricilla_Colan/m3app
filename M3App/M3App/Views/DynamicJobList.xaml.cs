﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using M3App.Cores.Interface;
using M3App.Models;
using M3App.ViewModels;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
    public partial class DynamicJobList : ContentPage
    {
        JobListAccess jobListAccess = new JobListAccess();
        DynamicJobListModel Formlistmodel = new DynamicJobListModel();
        DynamicFormListService Formlistservice = new DynamicFormListService();
        JobListService JobListService = new JobListService();
        SaveAllFields _offsave = new SaveAllFields();
        List<FormTypeListDataStorage> _formlist = new List<FormTypeListDataStorage>();
        RetrieveOfflineDatas _jobliststorage = new RetrieveOfflineDatas();
        List<FormTypeListDownLoadModel> GetListForm = new List<FormTypeListDownLoadModel>();
        RetrieveOfflineDatas _formliststorage = new RetrieveOfflineDatas();
        JobListModel CallListData=new JobListModel();
        static int count = 0;
        string jobid = "";
        string jobstatusselected = "";

        public DynamicJobList()
        {
            try
            {
                InitializeComponent();
                jobid = Settings.JobIdKey;
                sitename.Text = Settings.SiteNameKey;
                backPageTap.Tapped += BackPageTap_Tapped;
                TitleBasicinfo.Text = "Job Info";
                var pagename = base.GetType().Name;
                AcessingVariables(pagename);
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
                List<string> m = new List<string>();
                Settings.idslist=m;
                Jobstatus.WidthRequest = 200;
                jobstatusselected = Settings.JobStatusKey;
                Settings.YearKey = DateTime.Now.Year.ToString();
                if (CrossConnectivity.Current.IsConnected)
                {
                    CallListData = Task.Run(async () => await JobListService.JobList()).Result;
                }
                else
                {
                    var tempjoblist = _formliststorage.SelectAllDatas();
                    List<Messageresponse> _joblist = new List<Messageresponse>();
                    foreach (var item in tempjoblist)
                    {
                        Messageresponse _res = new Messageresponse();
                        _res.job_id =item.job_id;
                        _res.job_scheduled =item.job_scheduled;
                        _res.job_siteid =item.job_siteid;
                        _res.job_status =item.job_status;
                        _res.job_type =item.job_type;
                        _res.site_name =item.site_name;
                        _res.assigned_form_id =item.assigned_form_id;
                        _joblist.Add(_res);
                    }
                    CallListData.message = "Job List";
                    CallListData.result = "Success";
                    CallListData.response = _joblist;


                }
                Jobstatus.SelectedIndexChanged += Jobstatus_SelectedIndexChanged;
                SendMail.Clicked += delegate
                {
                    RedirectEmail();
                };
                lblJobId.Text = jobid;
                if (Device.RuntimePlatform.ToLower() == "ios")
                {
					
                    bakicon.Margin = new Thickness(0, 15, 0, 0);
                    bakicon.Scale = 0.5;
                    TitleBasicinfo.Margin = new Thickness(0, 15, 0, 0);
                }
                LoadDynamicData();
                GetjobstatusList();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();                
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
        public void AcessingVariables(string pagename)
        {
            Application.Current.Properties.Remove("PageName");
            Application.Current.Properties.Add("PageName", pagename);

            Application.Current.Properties.Remove("SignImageCat");
            Application.Current.Properties.Add("SignImageCat", "");

            Application.Current.Properties.Remove("SignImageCatSub");
            Application.Current.Properties.Add("SignImageCatSub", "");

            Application.Current.Properties.Remove("CheckListitem");
            Application.Current.Properties.Add("CheckListitem", null);
        }

        private async void BackPageTap_Tapped(object sender, EventArgs e)
        {
            try
            {
                //var navPage3 = new NavigationPage();
                //navPage3.ToolbarItems.Clear();
                //navPage3.BarBackgroundColor = Color.FromHex("#1A257F");
                //UserDialogs.Instance.ShowLoading("Loading...", MaskType.Black);
                //await Task.Delay(100);
                //await navPage3.PushAsync(new M3App.Views.JobList() { Title = "Job List" });
                //App.Current.MainPage = new MasterDetailPage
                //{
                //    Master = new MasterPage()
                //    {
                //        Title = "Menu",
                //    },
                //    Detail = navPage3
                //};
                await Navigation.PopAsync();
                //App.Current.MainPage = new Views.JobList();
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }
      private void Jobstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Picker data = sender as Picker;
                JobStatus jobstatus = data.SelectedItem as JobStatus;
                string compname = jobstatus.name;
                int compid = jobstatus.id;
                if (CrossConnectivity.Current.IsConnected)
                {
                    var savestatus = JobListService.JobsaveStatus(jobstatus);
                    if (savestatus != null)
                    {
                        if (savestatus.result == "Success")
                        {
                            DisplayAlert("Success", savestatus.message, "Ok");
                        }

                    }
                }
                else
                {
                    JobStatus_Save _sav = new JobStatus_Save();
                    List<JobStatus_Save> _lsav = new List<JobStatus_Save>();
                    _sav.id = jobstatus.id;
                    _sav.jobid= Convert.ToString(jobstatus.id);
                    _sav.name= jobstatus.name;
                    _lsav.Add(_sav);
                    _formliststorage.JobStatusSave(_lsav);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        public async void RedirectEmail()
        {
            try
            {
             UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            MovingDatasApis("SendMail");
            await Navigation.PushAsync(new Views.EmailForms());
            //App.Current.MainPage = new Views.EmailForms();
            }
            catch (Exception ex)
            {

            }
        }
        public async void MovingDatasApis(string Status)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    //await Task.Delay(500);
                    bool status = _offsave.OfflineStorage(Status);
                    if (status)
                    {
                        UserDialogs.Instance.HideLoading();
                        if (Device.RuntimePlatform.ToLower() == "ios")
                        {
                            //UserDialogs.Instance.Alert("Datas saved on forms successfully...");
                        }
                        else
                        {
                            //XFToast.LongMessage("Datas saved on forms successfully...");
                        }
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                    }
                }
                else
                {
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("You're in Offline");
                    }
                    else
                    {
                        XFToast.LongMessage("You're in Offline");
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }


        public async void LoadDynamicData()
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    Formlistmodel = await Formlistservice.DynamicJobList();
                    foreach(var item in Formlistmodel.response)
                    {
                        FormTypeListDataStorage _form = new FormTypeListDataStorage();
                        _form.id = item.id;
                        _form.form_name=item.form_name;
                        _form.form_type = item.form_type;
                        _form.form_json = item.form_json;
                        _form.created_at = item.created_at;
                        _form.updated_at = item.updated_at;
                        _form.form_status = item.form_status;
                        _form.form_short_name = item.form_short_name;
                        _form.Jobidref = Convert.ToInt32(jobid);
                        _formlist.Add(_form);
                    }
                    _formliststorage.FormTypeListDataAccess(_formlist, Convert.ToInt32(jobid));
                }
                else
                {
                    var tempjoblist = _jobliststorage.SelectAllDatas_FormList(Convert.ToInt32(jobid));
                    List<FormTypeList> _formlist = new List<FormTypeList>();
                    if (tempjoblist.Count > 0)
                    {
                        foreach (var item in tempjoblist)
                        {
                            FormTypeList _formtype = new FormTypeList();
                            _formtype.id = item.id;
                            _formtype.form_name = item.form_name;
                            _formtype.form_json = item.form_json;
                            _formtype.created_at = item.created_at;
                            _formtype.updated_at = item.updated_at;
                            _formtype.form_status = item.form_status;
                            _formtype.form_short_name = item.form_short_name;
                            _formlist.Add(_formtype);
                        }
                        Formlistmodel.response = _formlist;
                    }
                    else
                    {
                        // XFToast.LongMessage("This form does not synchronized last time");
                        if (Device.RuntimePlatform.ToLower() == "ios")
                        {
                            UserDialogs.Instance.Alert("This form does not synchronized last time");
                        }
                        else
                        {
                            XFToast.LongMessage("This form does not synchronized last time");
                        }
                    }
                }
                if (Formlistmodel.response != null)
                {
                    var llst = Formlistmodel.response;
                    int Namecount = 0;
                    for (int i = 0; i < llst.Count; i++)
                    {
                        RowDefinition gridRow1 = new RowDefinition();
                        gridRow1.Height = new GridLength(150);
                        dynamicEmail1.RowDefinitions.Add(gridRow1);
                       // BindDynamicForm(i, llst.Count, llst[i].form_name, llst[i].form_name, llst[i], llst[i], llst[i].form_status, llst[i].form_status);
                        BindDynamicForm(i, llst.Count, llst[i].form_short_name, llst[i].form_short_name, llst[i], llst[i], llst[i].form_status, llst[i].form_status);
                    }
                }

                else
                {
                    // XFToast.LongMessage(Formlistmodel.message);
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert(Formlistmodel.message);
                    }
                    else
                    {
                        XFToast.LongMessage(Formlistmodel.message);
                    }

                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();

            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        public void BindDynamicForm(int a, int rowCount, string Name, string Name1, FormTypeList formTypeLists, FormTypeList formTypeList1, string Status1, string Status2)
        {

            try
            {
                Grid framegridfirst = new Grid
                {
                    RowDefinitions =
                {
                    new RowDefinition {Height =  GridLength.Auto},
                    new RowDefinition {Height = GridLength.Auto},
                    new RowDefinition {Height = GridLength.Auto},
                },
                    Margin = new Thickness(-17)

                };

                Frame framefirst = new Frame
                {
                    CornerRadius = 10,
                    HasShadow = true,
                    Margin = new Thickness(5),
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,

                };

                Image Firstimage = new Image
                {
                    StyleId = "Image",
                    Source = "Form1.png",
                    Scale = 1.3,
                    Margin = new Thickness(15, 5, 0, 5),
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Center,
                };


                var ListFormId = CallListData.response.Where(x => x.job_id == jobid).FirstOrDefault().assigned_form_id;
                if (!string.IsNullOrEmpty(ListFormId))
                {
                    var SplitFormId = ListFormId.Split(',');
                    foreach (var formid in SplitFormId)
                    {
                        if (formTypeLists.id.ToString() == formid)
                        {
                            Firstimage.Source = "green_icon.png";
                        }
                    }
                }
                var image1_tab = new TapGestureRecognizer();



                //image1_tab.Tapped += async (s, e) =>
                //{
                //    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                //   await Task.Delay(100);
                //    var t = Name;
                //    var selecteditem = (Image)s;
                //    var item = formTypeLists;
                //    var infoitem = item as FormTypeList;
                //    //  await Navigation.PushAsync(new Views.FormsInformation(infoitem) { Title = Name });

                //    Application.Current.Properties.Remove("FormInformationdata");
                //    Application.Current.Properties.Add("FormInformationdata", infoitem);

                //    Application.Current.Properties.Remove("ViewCommentsEnable");
                //    Application.Current.Properties.Add("ViewCommentsEnable", "false");
                //    // App.Current.MainPage = new Views.FormsInformation(infoitem);
                //    await Navigation.PushAsync(new Views.FormsInformation(infoitem) { Title = Name });
                //};

                //loadjob

                image1_tab.Tapped +=  (s, e) =>
                {
                    SampleModel sr = new SampleModel();
                    jobImageClicked(Name, formTypeLists, s);
                };
                

                Firstimage.GestureRecognizers.Add(image1_tab);



                Label Firstlabel = new Label()
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    LineBreakMode = LineBreakMode.TailTruncation,
                    Text = Name,
                    Margin = new Thickness(0, 8, 0, 0),
                    TextColor = Color.Black,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Center,
                    FontSize = 14,

                };

                Label form_status = new Label()
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = Status1,
                    LineBreakMode = LineBreakMode.TailTruncation,
                    Margin = new Thickness(0, 3, 0, 3),
                    TextColor = Color.Green,
                    VerticalTextAlignment = TextAlignment.Start,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 14,
                };



                if (Status1 == "Pass")
                {
                    form_status.TextColor = Color.DarkGreen;
                }
                else if (Status1 == "Fail")
                {
                    form_status.TextColor = Color.DarkRed;
                }
                else if (Status1 == "Complete")
                {
                    form_status.TextColor = Color.DarkSlateBlue;
                }
                else if (Status1 == "Incomplete")
                {
                    form_status.TextColor = Color.DarkSlateGray;
                    form_status.Text = Status1;
                    framegridfirst.Children.Add(form_status, 0, 0);
                }
                else if (Status1 == "Complete - Attention Required")
                {
                    form_status.TextColor = Color.DarkOrange;
                }
                else if (Status1 == "Not Tested")
                {
                    form_status.TextColor = Color.DarkMagenta;
                }
                else if (Status1 == "null")
                {
                    form_status.TextColor = Color.DarkViolet;
                }
                framegridfirst.Children.Add(form_status, 0, 0);
                framegridfirst.Children.Add(Firstimage, 0, 1);
                framegridfirst.Children.Add(Firstlabel, 0, 2);
                framefirst.Content = framegridfirst;
                dynamicEmail1.Children.Add(framefirst, 0, a);
                count++;
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

       
        public async void jobImageClicked(string Name,FormTypeList formTypeLists, object s)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                var t = Name;
                var selecteditem = (Image)s;
                var item = formTypeLists;
                var infoitem = item as FormTypeList;

                Application.Current.Properties.Remove("BasicDetails");
                Application.Current.Properties.Add("BasicDetails", null);

                Application.Current.Properties.Remove("CategoryDetails");
                Application.Current.Properties.Add("CategoryDetails", null);

                Application.Current.Properties.Remove("TankDetails");
                Application.Current.Properties.Add("TankDetails", null);

                Application.Current.Properties.Remove("CategorySubDetails");
                Application.Current.Properties.Add("CategorySubDetails", null);

                Application.Current.Properties.Remove("CommentsDetails");
                Application.Current.Properties.Add("CommentsDetails", null);

                Application.Current.Properties.Remove("FormTypeListObject");
                Application.Current.Properties.Add("FormTypeListObject", infoitem);

                //App.Current.MainPage = new Views.BasicInfoDetails(infoitem);
                await Navigation.PushAsync(new Views.BasicInfoDetails(infoitem));
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }

        public void imagetapped(object sender, EventArgs e)
        {
            var selecteditem = (Image)sender;
            var item = selecteditem.BindingContext;
            var infoitem = item as FormTypeList;
        }

        private void BindDynamicForm1(int a, string name, FormTypeList formTypeLists, string Status)
        {
            try
            {
                Grid bottomgrid = new Grid
                {
                    RowDefinitions =
                {
                    new RowDefinition {Height = GridLength.Auto},
                    new RowDefinition {Height = GridLength.Auto},
                    new RowDefinition {Height = GridLength.Auto},

                },
                    Margin = new Thickness(-17)

                };
                Frame bottomframe = new Frame
                {
                    CornerRadius = 10,
                    HasShadow = true,
                    Margin = new Thickness(2, 5, 5, 5),
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,

                };
                Image image = new Image
                {
                    StyleId = "Image",
                    Source = "Form1.png",
                    Scale = 1.3,
                    Margin = new Thickness(15, 5, 0, 5),
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Center,
                };

                var ListFormId2 = CallListData.response.Where(x => x.job_id == jobid).FirstOrDefault().assigned_form_id;
                if (!string.IsNullOrEmpty(ListFormId2))
                {
                    var SplitFormId = ListFormId2.Split(',');
                    foreach (var formid in SplitFormId)
                    {
                        if (formTypeLists.id.ToString() == formid)
                        {
                            image.Source = "green_icon.png";
                        }
                    }
                }

                var image_tab_bottom = new TapGestureRecognizer();
                //image_tab_bottom.Tapped += async (s, e) =>
                 image_tab_bottom.Tapped += async (s, e) =>
                  {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                   // await Task.Delay(100);
                    var selecteditem = (Image)s;
                    var item = formTypeLists;
                    var infoitem = item as FormTypeList;

                  //  await Navigation.PushAsync(new Views.FormsInformation(infoitem) { Title = name });

                };
                image.GestureRecognizers.Add(image_tab_bottom);

                Label label = new Label()
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    LineBreakMode = LineBreakMode.TailTruncation,
                    TextColor = Color.Black,
                    Text = name,
                    Margin = new Thickness(0, 8, 0, 0),
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Center,
                    FontSize = 14,
                };

                Label form_status = new Label()
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = Status,
                    LineBreakMode = LineBreakMode.TailTruncation,
                    Margin = new Thickness(0, 3, 0, 3),
                    TextColor = Color.DarkOrange,
                    VerticalTextAlignment = TextAlignment.Start,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 14,
                };

                if (Status == "Pass")
                {
                    form_status.TextColor = Color.DarkGreen;

                }
                else if (Status == "Fail")
                {
                    form_status.TextColor = Color.DarkRed;
                }
                else if (Status == "Complete")
                {
                    form_status.TextColor = Color.DarkSlateBlue;
                }
                else if (Status == "Incomplete")
                {
                    form_status.TextColor = Color.DarkSlateGray;
                }
                else if (Status == "Complete - Attention Required")
                {
                    form_status.TextColor = Color.DarkOrange;
                }
                else if (Status == "Not Tested")
                {
                    form_status.TextColor = Color.DarkMagenta;
                }
                else if (Status == "null")
                {
                    form_status.TextColor = Color.DarkViolet;
                }
                bottomgrid.Children.Add(form_status, 0, 0);
                bottomgrid.Children.Add(image, 0, 1);
                bottomgrid.Children.Add(label, 0, 2);
                bottomframe.Content = bottomgrid;
                dynamicEmail1.Children.Add(bottomframe, 0, a);
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }

        }

        int jobcount = -1; int selectedCount = 0;
        public JobStatusModel GetjobstatusList()
        {
            try
            {
                var defaultvalue = jobstatusselected;
                JobStatusModel jobstatuss = new JobStatusModel();
                if (CrossConnectivity.Current.IsConnected)
                {
                    List<JobStatusStorage> _jss = new List<JobStatusStorage>();
                    jobstatuss = JobListService.JobStatus();
                    foreach(var item in jobstatuss.response)
                    {
                        JobStatusStorage _js = new JobStatusStorage();
                        _js.id = item.id;
                        _js.name = item.name;
                        _jss.Add(_js);
                    }
                    _formliststorage.JobStatusList(_jss);
                }
                else
                {

                    var tempjobstatus = _formliststorage.SelectAllDatas_JobStatus();
                    List<JobStatus> _jbstslist = new List<JobStatus>();
                    foreach (var item in tempjobstatus)
                    {
                        JobStatus _res = new JobStatus();
                        _res.id = item.id;
                        _res.name = item.name;
                        _jbstslist.Add(_res);
                    }
                    jobstatuss.message = "Job Status List";
                    jobstatuss.result = "Success";
                    jobstatuss.response = _jbstslist;
                }
                if (jobstatuss != null && jobstatuss.response != null)
                {
                    Jobstatus.ItemsSource = jobstatuss.response;
                    if (!string.IsNullOrEmpty(defaultvalue))
                    {
                        foreach (var item in jobstatuss.response)
                        {
                            jobcount++;
                            if (defaultvalue == item.name)
                            {
                                selectedCount = jobcount;
                            }
                        }
                        Jobstatus.SelectedIndex = selectedCount;
                    }
                    else
                    {
                        Jobstatus.SelectedIndex = 0;
                    }
                }
                return jobstatuss;
            }
            catch (Exception)
            {                
                UserDialogs.Instance.HideLoading();
                return null;
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
    }
}