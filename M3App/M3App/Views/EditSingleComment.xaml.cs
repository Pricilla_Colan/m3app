﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Models;
using Newtonsoft.Json;
using Plugin.Connectivity;
using SignaturePad.Forms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditSingleComment
	{
        DownloadComments _dtsms = new DownloadComments();
        SignaturePadView signaturePadView;
        DynamicFormListService dynamicFormListService = new DynamicFormListService();
        FormTypeList formtype;
        RetrieveOfflineDatas _jobliststorage = new RetrieveOfflineDatas();
        SaveAllFields _fields = new SaveAllFields();
        public EditSingleComment (DownloadComments valueinfo,FormTypeList form_type)
		{
            try
            {
                InitializeComponent();
                Application.Current.Properties.Remove("SignImage");
                Application.Current.Properties.Add("SignImage", "");
                var pagename = base.GetType().Name;
                formtype = form_type;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
                LoadData(valueinfo);
                CommentsSave.Clicked += CommentsSave_Clicked;
            }
            catch (Exception ex)
            {

            }
        }
        private async void CommentsSave_Clicked(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                var image= Application.Current.Properties["SignImage"];
                byte[] imageBytes = new byte[0];
                string signdata = "";
                if (signaturePadView.Points.Count() > 0)
                {
                    var stream = await signaturePadView.GetImageStreamAsync(SignatureImageFormat.Png);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        imageBytes = new byte[stream.Length];
                        stream.CopyTo(ms);
                        imageBytes = ms.ToArray();
                    }
                    signdata = Convert.ToBase64String(imageBytes);
                }
                else if(image!=null && image != "")
                {
                    signdata = image.ToString();
                }

                 
                List<DownloadComments> _dt = new List<DownloadComments>();
                DownloadComments _dts = new DownloadComments();
                _dts.action_taken = EditComments.Text;
                _dts.date = datepicker.Date.ToString("MM-dd-yyyy");
                _dts.initials = signdata;
                _dts.Index = _dtsms.Index;
                _dt.Add(_dts);

                FormTypeListDownLoadModel formtypelistmodel = new FormTypeListDownLoadModel();
                if (CrossConnectivity.Current.IsConnected)
                {
                     formtypelistmodel = GetFormTypeList(formtype);
                }
                else
                {
                    var tempjoblist = _jobliststorage.SelectAllDatas_FormDisplay(formtype);
                    FormTypeResponse rs = new FormTypeResponse();
                    if (tempjoblist.Count > 0)
                    {
                        foreach (var itn in tempjoblist)
                        {
                            rs.id = itn.id;
                            rs.job_id = itn.job_id;
                            rs.staff_id = itn.staff_id;
                            rs.site_id = itn.site_id;
                            rs.form_name = itn.form_name;
                            rs.form_type = itn.form_type;
                            rs.form_json = JsonConvert.DeserializeObject<FormJson>(itn.form_json);
                            rs.created_at = itn.created_at;
                            rs.updated_at = itn.updated_at;
                            rs.category_id = itn.category_id;
                            rs.form_id = itn.form_id;
                            rs.access_key = itn.access_key;
                            rs.form_status = itn.form_status;

                        }
                        formtypelistmodel.response = rs;
                    }

                }
                _fields.SaveApproval(formtypelistmodel, "EditComments", null,null,_dt);
                UserDialogs.Instance.HideLoading();
            }
            catch(Exception ex)
            {

            }
        }

        public FormTypeListDownLoadModel GetFormTypeList(FormTypeList Formlistmodel)
        {
            var Formslistdata = dynamicFormListService.Dynamicformdatas(Formlistmodel);
            return Formslistdata;
        }

        public void LoadData(DownloadComments valueinfo)
        {
            try
            {
                //SignaturePadView signaturePadView;
                EditComments.Text = valueinfo.action_taken;
                string tstdt = valueinfo.date;

                string formattedDates = "";
                var splcha = tstdt.Substring(0, 3);
                var splchb = splcha.Substring(2).ToCharArray();

                string[] _list1;
                _list1 = tstdt.Split(splchb);
                var ttt1 = new DateTime(Convert.ToInt32(_list1[2]), Convert.ToInt32(_list1[0]), Convert.ToInt32(_list1[1]));
                string mm = ttt1.ToString("dd/MM/yyyy");
                string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "MM-dd-yyyy", "dd-MM-yyyy", "M-dd-yyyy", "M/dd/yyyy" };
                DateTime date;

                if (DateTime.TryParseExact(mm, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    formattedDates = date.ToString("dd/MM/yyyy");
                if (formattedDates != null && formattedDates != "")
                {
                    var splch = formattedDates.Substring(0, 3);
                    var splch1 = splch.Substring(2).ToCharArray();
                    string[] _list = formattedDates.Split(splch1);
                    date = new DateTime(Convert.ToInt32(_list[2]), Convert.ToInt32(_list[1]), Convert.ToInt32(_list[0]));
                    var dt = date.ToString("MM-dd-yyyy");

                }
                else
                {

                }

                DateTime selval = Convert.ToDateTime(date);
                datepicker.Date = selval;
                ImageSource retSource = null;
                _dtsms = valueinfo;
                byte[] Base64Stream = Convert.FromBase64String(valueinfo.initials.ToString());
                retSource = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                if (retSource != null)
                {
                    Grid _sg = new Grid();

                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                    _sg.RowSpacing = 0;
                    var _bggrid = GridBasicInfo.Children.ToList();
                    int bggridchild = GridBasicInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;

                    bggridrow = bggridchild;
                    int smgridchild = _sg.Children.Count;

                    smgridrow = smgridchild;

                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = "Initials",
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    //ImageSource retSource = null;

                    signaturePadView = new SignaturePadView
                    {
                        StyleId = "BasicSignature",
                        BackgroundColor = Color.White,
                        StrokeColor = Color.Black,
                        HeightRequest = 70,
                        StrokeWidth = 2,
                    };

                    var forgetPassword_tap = new TapGestureRecognizer();
                    signaturePadView.ClearLabel.GestureRecognizers.Add(forgetPassword_tap);
                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                    _sg.Children.Add(signaturePadView, 0, smgridrow + 1);

                    if (valueinfo.initials.ToString() != null && valueinfo.initials.ToString() != "")
                    {
                        signaturePadView.ClearLabel.IsVisible = true;

                        //byte[] Base64Stream = Convert.FromBase64String(form_Basic_Info.value.ToString());
                        retSource = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                        Image sign = new Image
                        {
                            Source = retSource,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalOptions = LayoutOptions.Center,
                            Scale = 0.4,
                        };
                        ImageSetting(sign);
                        _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                        _sg.Children.Add(sign, 0, smgridrow + 1);

                        Application.Current.Properties.Remove("SignImage");
                        Application.Current.Properties.Add("SignImage", valueinfo.initials);

                    }
                    forgetPassword_tap.Tapped += (s, e) =>
                    {
                        Image mmm;

                        if (valueinfo.initials.ToString() != null && valueinfo.initials.ToString() != "")
                        {
                            var p = Application.Current.Properties["Image"];
                            if (p != null && p != "")
                            {
                                mmm = p as Image;
                                mmm.IsVisible = false;
                            }
                        }
                    };

                    GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                }
            }
            catch(Exception ex)
            {

            }
        }
        public void ImageSetting(Image p = null)
        {
            Application.Current.Properties.Remove("Image");
            Application.Current.Properties.Add("Image", p);
        }

        public class SaveValue
        {
            public string control { get; set; }
            public string value { get; set; }

        }
    }
}