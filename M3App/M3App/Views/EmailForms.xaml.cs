﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using M3App.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EmailForms : ContentPage
    {
        JobListAccess jobListAccess = new JobListAccess();
        DynamicJobListModel Formlistmodel = new DynamicJobListModel();
        DynamicFormListService Formlistservice = new DynamicFormListService();
        List<FormTypeListDownLoadModel> GetListForm = new List<FormTypeListDownLoadModel>();

        List<Grid> RightGridList = new List<Grid>();
        List<Grid> LeftGridList = new List<Grid>();
        List<Grid> grdlst = new List<Grid>();
        Grid OddGrid = new Grid();
        static int count = 0;
        public EmailForms()
        {
            try
            {
                InitializeComponent();
                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
                UncheckAll.IsVisible = true;
                CheckAll.IsVisible = false;
                UnSelectGrid.IsVisible = true;
                SelectGrid.IsVisible = false;

                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    headerinfo.Margin = new Thickness(0, 15, 0, 0);
                    bakicon.Margin = new Thickness(0, 12, 0, 0);
                    TitleBasicinfo.Margin = new Thickness(0, 12, 0, 0);

                }
                LoadDynamicData();
                lblJobId.Text = Settings.JobIdKey;
                btnSendMail.Clicked += delegate
                {
                    RedirectEmail();
                };
            }
            catch (Exception ex)
            {

            }
        }
        public async  void RedirectEmail()
        {
            try
            {
                List<FormTypeList> CheckedList = new List<FormTypeList>();
                FormTypeList getchecklist;
                var SingleGrid = dynamicEmail1.Children;
                Image SingleImg = new Image();
                Image LeftImg = new Image();
                Image RightImg = new Image();
                Image Form2 = new Image();
                Label lblForm = new Label();
                //for (int i = 0; i < SingleGrid.Count; i++)
                //{
                //    if (SingleGrid[i].StyleId == "RadiocheckImage")
                //    {
                //        string Formid = string.Empty;
                //        Formid = SingleGrid[i].ClassId;
                //        string[] GetName = Formid.Split('/');
                //        getchecklist = new FormTypeList()
                //        {
                //            id = Convert.ToInt32(GetName[0]),
                //            form_name = GetName[1],
                //        };
                //        CheckedList.Add(getchecklist);
                //    }
                //}
                //foreach (var item in LeftGridList)
                //{
                //    var GetChildren = item.Children;
                //    for (int i = 0; i < GetChildren.Count; i++)
                //    {
                //        if (GetChildren[i].StyleId == "RadiocheckImage")
                //        {
                //            string Formid = string.Empty;
                //            Formid = GetChildren[i].ClassId;
                //            string[] GetName = Formid.Split('/');
                //            getchecklist = new FormTypeList()
                //            {
                //                id = Convert.ToInt32(GetName[0]),
                //                form_name = GetName[1],
                //            };
                //            CheckedList.Add(getchecklist);
                //        }
                //    }
                //}
                //foreach (var item in RightGridList)
                //{
                //    var GetChildrenright = item.Children;
                //    for (int i = 0; i < GetChildrenright.Count; i++)
                //    {
                //        if (GetChildrenright[i].StyleId == "RadiocheckImage")
                //        {
                //            string Formid = string.Empty;
                //            Formid = GetChildrenright[i].ClassId;
                //            string[] GetName = Formid.Split('/');
                //            getchecklist = new FormTypeList()
                //            {
                //                id = Convert.ToInt32(GetName[0]),
                //                form_name = GetName[1],
                //            };
                //            CheckedList.Add(getchecklist);
                //        }
                //    }
                //}

                foreach (var item in grdlst)
                {
                    var GetChildrenright = item.Children;
                    for (int i = 0; i < GetChildrenright.Count; i++)
                    {
                        if (GetChildrenright[i].StyleId == "RadiocheckImage")
                        {
                            string Formid = string.Empty;
                            Formid = GetChildrenright[i].ClassId;
                            string[] GetName = Formid.Split('/');
                            getchecklist = new FormTypeList()
                            {
                                id = Convert.ToInt32(GetName[0]),
                                form_name = GetName[1],
                            };
                            CheckedList.Add(getchecklist);
                        }
                    }
                }
                if (CheckedList.Count == 0)
                {
                    //XFToast.ShortMessage("Please select form");
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("Please select form");
                    }
                    else
                    {
                        XFToast.LongMessage("Please select form");
                    }

                }
                else
                {
                    await Navigation.PushAsync(new Views.ComposeMailForm(CheckedList) { Title = "ComposeMail Form" });
                   // App.Current.MainPage = new Views.ComposeMailForm(CheckedList);
                }
            }
            catch (Exception ex)
            {

            }
        }
        //public async void LoadDynamicData()
        //{
        //    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
        //    try
        //    {
        //        Formlistmodel = await Formlistservice.DynamicJobList();
        //        if (Formlistmodel != null)
        //        {
        //            var llst = Formlistmodel.response;
        //            int Namecount = 0;
        //            decimal l = Convert.ToDecimal(llst.Count) / Convert.ToDecimal(2);
        //            decimal l1 = llst.Count / 2;
        //            for (int i = 0; i < llst.Count / 2; i++)
        //            {
        //                RowDefinition gridRow1 = new RowDefinition();
        //                gridRow1.Height = new GridLength(150);
        //                dynamicEmail1.RowDefinitions.Add(gridRow1);
        //                BindDynamicForm(i, llst.Count, llst[Namecount].form_name, llst[Namecount + 1].form_name, llst[Namecount], llst[Namecount + 1]);
        //                Namecount = Namecount + 2;
        //            }
        //            if (llst.Count % 2 != 0)
        //            {
        //                decimal l2 = l - l1;
        //                if (l2 < 1)
        //                {
        //                    for (int i = 0; i < 1; i++)
        //                    {
        //                        RowDefinition gridRow1 = new RowDefinition();
        //                        gridRow1.Height = new GridLength(150);
        //                        dynamicEmail1.RowDefinitions.Add(gridRow1);
        //                        BindDynamicForm1(dynamicEmail1.RowDefinitions.Count - 1, llst[llst.Count - 1].form_name, llst[llst.Count - 1]);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        UserDialogs.Instance.HideLoading();
        //        ex.Message.ToString();
        //    }
        //    finally
        //    {
        //        UserDialogs.Instance.HideLoading();
        //    }
        //}



        public async void LoadDynamicData()
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            try
            {
                Formlistmodel = await Formlistservice.DynamicJobList();
                if (Formlistmodel != null)
                {
                    var llst = Formlistmodel.response;
                    int Namecount = 0;
                    decimal l = Convert.ToDecimal(llst.Count) / Convert.ToDecimal(2);
                    decimal l1 = llst.Count / 2;
                    for (int i = 0; i < llst.Count; i++)
                    {
                        RowDefinition gridRow1 = new RowDefinition();
                        gridRow1.Height = new GridLength(150);
                        dynamicEmail1.RowDefinitions.Add(gridRow1);
                        
                        BindSelectedMail(llst[i].form_name, llst[i].form_status, llst[i].form_type, Convert.ToString(llst[i].id), llst[i]);
                        //BindDynamicForm(i, llst.Count, llst[i].form_name, llst[i + 1].form_name, llst[i], llst[i + 1]);
                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                ex.Message.ToString();
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        //public void BindDynamicForm(int a, int rowCount, string Name, string Name1, FormTypeList formTypeLists, FormTypeList formTypeList1)
        //{
        //    Grid framegridfirst = new Grid
        //    {
        //        RowDefinitions =
        //        {
        //            new RowDefinition {Height = new GridLength(40)},
        //            new RowDefinition {Height =  GridLength.Auto},
        //            new RowDefinition {Height = GridLength.Auto},
        //        },
        //        Margin = new Thickness(-17)

        //    };

        //    Frame framefirst = new Frame
        //    {
        //        CornerRadius = 10,
        //        HasShadow = true,
        //        Margin = new Thickness(2,5,5,5),
        //        HorizontalOptions = LayoutOptions.FillAndExpand,
        //        VerticalOptions = LayoutOptions.FillAndExpand,

        //    };

        //    Image Firstimage = new Image
        //    {
        //        StyleId = "Image",
        //        Source = "Form1.png",
        //        Scale = 1.3,
        //        Margin = new Thickness(10, 3, 0, 3),
        //        HorizontalOptions = LayoutOptions.CenterAndExpand,
        //        VerticalOptions = LayoutOptions.Start
        //    };

        //    Image Uncheckimage = new Image
        //    {
        //        StyleId = "RadioUncheckImage",
        //        Source = "Uncheck_Button.png",
        //        Scale = 0.4,
        //        ClassId = formTypeLists.id + "/" + Name,
        //        Margin = new Thickness(0, 0, 5, 0),
        //        HorizontalOptions = LayoutOptions.Start,
        //        VerticalOptions = LayoutOptions.Start,
        //    };

        //    var Uncheck_Img = new TapGestureRecognizer();
        //    Uncheck_Img.Tapped += (s, e) =>
        //    {
        //        if (Uncheckimage.StyleId == "RadioUncheckImage")
        //        {
        //            Uncheckimage.Source = "radio_btn_v2.png";
        //            Uncheckimage.StyleId = "RadiocheckImage";
        //        }
        //        else
        //        {
        //            Uncheckimage.Source = "Uncheck_Button.png";
        //            Uncheckimage.StyleId = "RadioUncheckImage";
        //        }
        //        if (Firstimage.StyleId == "Image")
        //        {
        //            Firstimage.Source = "Form2.png";
        //            Firstimage.StyleId = "Image1";
        //        }
        //        else
        //        {
        //            Firstimage.Source = "Form1.png";
        //            Firstimage.StyleId = "Image";
        //        }
        //    };
        //    Uncheckimage.GestureRecognizers.Add(Uncheck_Img);

        //    var image1_tab = new TapGestureRecognizer();
        //    image1_tab.Tapped += (s, e) =>
        //    {                
        //        var t = Name;
        //        var selecteditem = (Image)s;
        //        var item = formTypeLists;
        //        var infoitem = item as FormTypeList;               
        //    };
        //    Firstimage.GestureRecognizers.Add(image1_tab);

        //    Label Firstlabel = new Label()
        //    {
        //        StyleId = "Label",
        //        FontAttributes = FontAttributes.None,
        //        LineBreakMode = LineBreakMode.TailTruncation,
        //        Text = Name,
        //        Margin = new Thickness(0, 5, 0, 3),
        //        ClassId = formTypeLists.id.ToString(),
        //        TextColor = Color.Black,
        //        VerticalTextAlignment = TextAlignment.Center,
        //        HorizontalTextAlignment = TextAlignment.Center,
        //        FontSize = 14,
        //    };
        //    framegridfirst.Children.Add(Uncheckimage, 0, 0);
        //    framegridfirst.Children.Add(Firstimage, 0, 1);
        //    framegridfirst.Children.Add(Firstlabel, 0, 2);
        //    framefirst.Content = framegridfirst;
        //    dynamicEmail1.Children.Add(framefirst, 0, a);
        //    LeftGridList.Add(framegridfirst);

        //    Grid framegridsecond = new Grid
        //    {
        //        RowDefinitions =
        //        {
        //            new RowDefinition {Height = new GridLength(40)},
        //            new RowDefinition {Height =  GridLength.Auto},
        //            new RowDefinition {Height = GridLength.Auto},
        //        },
        //        Margin = new Thickness(-17)
        //    };

        //    Frame framesecond = new Frame
        //    {
        //        CornerRadius = 10,
        //        HasShadow = true,
        //        Margin = new Thickness(2,5,5,5),
        //        HorizontalOptions = LayoutOptions.FillAndExpand,
        //        VerticalOptions = LayoutOptions.FillAndExpand,
        //    };

        //    Image secondimage = new Image
        //    {
        //        StyleId = "Image",
        //        Source = "Form1.png",
        //        Scale = 1.3,
        //        Margin = new Thickness(10, 3, 0, 3),
        //        HorizontalOptions = LayoutOptions.CenterAndExpand,
        //        VerticalOptions = LayoutOptions.Start,
        //    };
        //    Image Uncheckimg = new Image
        //    {
        //        StyleId = "RadioUncheckImage",
        //        Source = "Uncheck_Button.png",
        //        Scale = 0.4,
        //        ClassId = formTypeList1.id + "/" + Name1,
        //        Margin = new Thickness(0, 0, 5, 0),
        //        HorizontalOptions = LayoutOptions.Start,
        //        VerticalOptions = LayoutOptions.Center,
        //    };

        //    var image_tab_bottom_Uncheck = new TapGestureRecognizer();
        //    image_tab_bottom_Uncheck.Tapped += (s, e) =>
        //    {
        //        if (Uncheckimg.StyleId == "RadioUncheckImage")
        //        {
        //            Uncheckimg.Source = "radio_btn_v2.png";
        //            Uncheckimg.StyleId = "RadiocheckImage";
        //        }
        //        else
        //        {
        //            Uncheckimg.Source = "Uncheck_Button.png";
        //            Uncheckimg.StyleId = "RadioUncheckImage";
        //        }
        //        if (secondimage.StyleId == "Image")
        //        {
        //            secondimage.Source = "Form2.png";
        //            secondimage.StyleId = "Image1";
        //        }
        //        else
        //        {
        //            secondimage.Source = "Form1.png";
        //            secondimage.StyleId = "Image";
        //        }

        //    };
        //    Uncheckimg.GestureRecognizers.Add(image_tab_bottom_Uncheck);

        //    var UncheckImg_tab = new TapGestureRecognizer();
        //    UncheckImg_tab.Tapped += (s, e) =>
        //    {

        //        var selecteditem = (Image)s;
        //        var item = formTypeList1;
        //        var infoitem = item as FormTypeList;              
        //    };

        //    var image2_tab = new TapGestureRecognizer();
        //    image2_tab.Tapped += (s, e) =>
        //    {

        //        var selecteditem = (Image)s;
        //        var item = formTypeList1;
        //        var infoitem = item as FormTypeList;             
        //    };

        //    secondimage.GestureRecognizers.Add(image2_tab);
        //    Label secondlabel = new Label()
        //    {
        //        StyleId = "Label",
        //        FontAttributes = FontAttributes.None,
        //        TextColor = Color.Black,
        //        LineBreakMode = LineBreakMode.TailTruncation,
        //        Margin = new Thickness(0, 5, 0, 3),
        //        Text = Name1,
        //        ClassId = formTypeLists.id.ToString(),
        //        VerticalTextAlignment = TextAlignment.Center,
        //        HorizontalTextAlignment = TextAlignment.Center,
        //        FontSize = 14
        //    };
        //    framegridsecond.Children.Add(Uncheckimg, 0, 0);
        //    framegridsecond.Children.Add(secondimage, 0, 1);
        //    framegridsecond.Children.Add(secondlabel, 0, 2);
        //    framesecond.Content = framegridsecond;
        //    dynamicEmail1.Children.Add(framesecond, 1, a);
        //    RightGridList.Add(framegridsecond);
        //    count++;
        //}


        //private void BindDynamicForm1(int a, string name, FormTypeList formTypeLists)
        //{
        //    Grid bottomgrid = new Grid
        //    {
        //        RowDefinitions =
        //        {
        //            new RowDefinition {Height = new GridLength(40)},
        //            new RowDefinition {Height = GridLength.Auto},
        //            new RowDefinition {Height = GridLength.Auto},

        //        },
        //        Margin = new Thickness(-17),
        //    };


        //    Frame bottomframe = new Frame
        //    {

        //        CornerRadius = 10,
        //        HasShadow = true,
        //        Margin = new Thickness(2,5,5,5),
        //        HorizontalOptions = LayoutOptions.FillAndExpand,
        //        VerticalOptions = LayoutOptions.FillAndExpand,
        //    };
        //    Image Uncheckimg = new Image
        //    {
        //        StyleId = "RadioUncheckImage",
        //        Source = "Uncheck_Button.png",
        //        Scale = 0.4,
        //        ClassId = formTypeLists.id.ToString() + "/" + name,
        //        Margin = new Thickness(0, 0, 5, 0),
        //        HorizontalOptions = LayoutOptions.Start,
        //        VerticalOptions = LayoutOptions.End,
        //    };
        //    Image image = new Image
        //    {
        //        StyleId = "Image",
        //        Source = "Form1.png",
        //        Scale = 1.3,
        //        Margin = new Thickness(10, 3, 0, 3),
        //        HorizontalOptions = LayoutOptions.CenterAndExpand,
        //        VerticalOptions = LayoutOptions.End,
        //    };

        //    var image_tab_bottom = new TapGestureRecognizer();
        //    image_tab_bottom.Tapped += (s, e) =>
        //    {
        //        var selecteditem = (Image)s;
        //        var item = formTypeLists;
        //        var infoitem = item as FormTypeList;
        //    };
        //    image.GestureRecognizers.Add(image_tab_bottom);

        //    var image_tab_bottom_Uncheck = new TapGestureRecognizer();
        //    image_tab_bottom_Uncheck.Tapped += (s, e) =>
        //    {
        //        if (Uncheckimg.StyleId == "RadioUncheckImage")
        //        {
        //            Uncheckimg.Source = "radio_btn_v2.png";
        //            Uncheckimg.StyleId = "RadiocheckImage";
        //        }
        //        else
        //        {
        //            Uncheckimg.Source = "Uncheck_Button.png";
        //            Uncheckimg.StyleId = "RadioUncheckImage";
        //        }
        //        if (image.StyleId == "Image")
        //        {
        //            image.Source = "Form2.png";
        //            image.StyleId = "Image1";
        //        }
        //        else
        //        {
        //            image.Source = "Form1.png";
        //            image.StyleId = "Image";
        //        }
        //    };
        //    Uncheckimg.GestureRecognizers.Add(image_tab_bottom_Uncheck);

        //    Label label = new Label()
        //    {
        //        StyleId = "Label",
        //        FontAttributes = FontAttributes.None,
        //        TextColor = Color.Black,
        //        Margin = new Thickness(0, 5, 0, 3),
        //        LineBreakMode = LineBreakMode.TailTruncation,
        //        Text = name,
        //        ClassId = formTypeLists.id.ToString(),
        //        VerticalTextAlignment = TextAlignment.Center,
        //        HorizontalTextAlignment = TextAlignment.Center,
        //        FontSize = 14,
        //    };

        //    bottomgrid.Children.Add(Uncheckimg, 0, 0);
        //    bottomgrid.Children.Add(image, 0, 1);
        //    bottomgrid.Children.Add(label, 0, 2);
        //    bottomframe.Content = bottomgrid;
        //    dynamicEmail1.Children.Add(bottomframe, 0, a);
        //    OddGrid = bottomgrid;
        //}



        public void BindDynamicForm(int a, int rowCount, string Name, string Name1, FormTypeList formTypeLists, FormTypeList formTypeList1)
        {
            try
            {
                Grid framegridfirst = new Grid
                {
                    RowDefinitions =
                {
                    new RowDefinition {Height = new GridLength(40)},
                    new RowDefinition {Height =  GridLength.Auto},
                    new RowDefinition {Height = GridLength.Auto},
                },
                };

                Frame framefirst = new Frame
                {
                    CornerRadius = 10,
                    HasShadow = true,
                    Margin = new Thickness(2, 5, 5, 5),
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,

                };

                Image Firstimage = new Image
                {
                    StyleId = "Image",
                    Source = "Form1.png",
                    Scale = 1.3,
                    Margin = new Thickness(10, 3, 0, 3),
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.Start
                };

                Image Uncheckimage = new Image
                {
                    StyleId = "RadioUncheckImage",
                    Source = "Uncheck_Button.png",
                    Scale = 0.4,
                    ClassId = formTypeLists.id + "/" + Name,
                    Margin = new Thickness(0, 0, 5, 0),
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.Start,
                };

                var Uncheck_Img = new TapGestureRecognizer();
                Uncheck_Img.Tapped += (s, e) =>
                {
                    if (Uncheckimage.StyleId == "RadioUncheckImage")
                    {
                        Uncheckimage.Source = "radio_btn_v2.png";
                        Uncheckimage.StyleId = "RadiocheckImage";
                    }
                    else
                    {
                        Uncheckimage.Source = "Uncheck_Button.png";
                        Uncheckimage.StyleId = "RadioUncheckImage";
                    }
                    if (Firstimage.StyleId == "Image")
                    {
                        Firstimage.Source = "Form2.png";
                        Firstimage.StyleId = "Image1";
                    }
                    else
                    {
                        Firstimage.Source = "Form1.png";
                        Firstimage.StyleId = "Image";
                    }
                };
                Uncheckimage.GestureRecognizers.Add(Uncheck_Img);

                var image1_tab = new TapGestureRecognizer();
                image1_tab.Tapped += (s, e) =>
                {
                    var t = Name;
                    var selecteditem = (Image)s;
                    var item = formTypeLists;
                    var infoitem = item as FormTypeList;
                };
                Firstimage.GestureRecognizers.Add(image1_tab);

                Label Firstlabel = new Label()
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    LineBreakMode = LineBreakMode.TailTruncation,
                    Text = Name,
                    Margin = new Thickness(0, 5, 0, 3),
                    ClassId = formTypeLists.id.ToString(),
                    TextColor = Color.Black,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Center,
                    FontSize = 14,
                };
                framegridfirst.Children.Add(Uncheckimage, 0, 0);
                framegridfirst.Children.Add(Firstimage, 0, 1);
                framegridfirst.Children.Add(Firstlabel, 0, 2);
                framefirst.Content = framegridfirst;
                dynamicEmail1.Children.Add(framefirst, 0, a);
                grdlst.Add(framegridfirst);
            }
            catch (Exception ex)
            {

            }
        }



        public void BindSelectedMail(string Name,string Status,string Type,string ID, FormTypeList formTypeLists)
        {
            try
            {
                var a = dynamicEmail1.Children.Count;
                Grid framegridfirst = new Grid
                {
                    RowDefinitions =
                {
                    new RowDefinition {Height = new GridLength(40)},
                    new RowDefinition {Height =  GridLength.Auto},
                    new RowDefinition {Height = GridLength.Auto},
                },
                    Margin = new Thickness(-17),
                };
                framegridfirst.RowSpacing = 0;
                Frame framefirst = new Frame
                {
                    CornerRadius = 10,
                    HasShadow = true,
                    Margin = new Thickness(2, 5, 5, 5),
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,

                };
                Image Firstimage = new Image
                {
                    StyleId = "Image",
                    Source = "Form1.png",
                    Scale = 1.3,
                    // Margin = new Thickness(10, 3, 0, 3),
                    Margin = new Thickness(10, 0, 0, 0),
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.Start
                };

                Image Uncheckimage = new Image
                {
                    StyleId = "RadioUncheckImage",
                    Source = "Uncheck_Button.png",
                    Scale = 0.4,
                    // ClassId = ID,
                    ClassId = formTypeLists.id + "/" + Name,
                    Margin = new Thickness(0, 0, 5, 0),
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.Start,
                };

                var Uncheck_Img = new TapGestureRecognizer();
                Uncheck_Img.Tapped += (s, e) =>
                {
                    if (Uncheckimage.StyleId == "RadioUncheckImage")
                    {
                        Uncheckimage.Source = "radio_btn_v2.png";
                        Uncheckimage.StyleId = "RadiocheckImage";
                    }
                    else
                    {
                        Uncheckimage.Source = "Uncheck_Button.png";
                        Uncheckimage.StyleId = "RadioUncheckImage";
                    }
                    if (Firstimage.StyleId == "Image")
                    {
                        Firstimage.Source = "Form2.png";
                        Firstimage.StyleId = "Image1";
                    }
                    else
                    {
                        Firstimage.Source = "Form1.png";
                        Firstimage.StyleId = "Image";
                    }
                };
                Uncheckimage.GestureRecognizers.Add(Uncheck_Img);

                var image1_tab = new TapGestureRecognizer();
                image1_tab.Tapped += (s, e) =>
                {
                    var t = Name;
                    var selecteditem = (Image)s;
                    var item = formTypeLists;
                    var infoitem = item as FormTypeList;
                };
                Firstimage.GestureRecognizers.Add(image1_tab);

                Label Firstlabel = new Label()
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    LineBreakMode = LineBreakMode.TailTruncation,
                    Text = Name,
                    Margin = new Thickness(0, 5, 0, 5),
                    ClassId = ID,
                    TextColor = Color.Black,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Center,
                    FontSize = 14,
                };
                framegridfirst.Children.Add(Uncheckimage, 0, 0);
                framegridfirst.Children.Add(Firstimage, 0, 1);
                framegridfirst.Children.Add(Firstlabel, 0, 2);
                framefirst.Content = framegridfirst;
                dynamicEmail1.Children.Add(framefirst, 0, a);
                grdlst.Add(framegridfirst);
            }
            catch (Exception ex)
            {

            }
        }


     



        private async void baktap_Tapped(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
            App.Current.MainPage = new Views.DynamicJobList();
            UserDialogs.Instance.HideLoading();

        }



        private async void buttonUnChecked_Tapped(object sender, EventArgs e)
        {
			try
			{
				UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
				//await Task.Delay(2000);
				var SingleGrid = OddGrid.Children;
				Image SingleImg = new Image();
				Image LeftImg = new Image();
				Image RightImg = new Image();
				Image Form2 = new Image();
				Label lblForm = new Label();
                //for (int i = 0; i < SingleGrid.Count; i++)
                //{

                //            if (SingleGrid[i].StyleId == "RadioUncheckImage")
                //            {
                //                SingleImg = (Image)SingleGrid[i];
                //                SingleImg.Source = "radio_btn_v2.png";
                //                SingleImg.StyleId = "RadiocheckImage";
                //            }
                //            if (SingleGrid[i].StyleId == "Image")
                //            {
                //                Form2 = (Image)SingleGrid[i];
                //                Form2.Source = "Form2.png";
                //                Form2.StyleId = "Image1";
                //            }

                //}
                //foreach (var item in LeftGridList)
                //{
                //	var GetChildren = item.Children;
                //	for (int i = 0; i < GetChildren.Count; i++)
                //	{
                //		if (GetChildren[i].StyleId == "RadioUncheckImage")
                //		{
                //			LeftImg = (Image)GetChildren[i];
                //			LeftImg.Source = "radio_btn_v2.png";
                //			LeftImg.StyleId = "RadiocheckImage";
                //		}
                //		if (GetChildren[i].StyleId == "Image")
                //		{
                //			Form2 = (Image)GetChildren[i];
                //			Form2.Source = "Form2.png";
                //			Form2.StyleId = "Image1";
                //		}
                //	}
                //}
                //foreach (var item in RightGridList)
                //{
                //	var GetChildren = item.Children;
                //	for (int i = 0; i < GetChildren.Count; i++)
                //	{
                //		if (GetChildren[i].StyleId == "RadioUncheckImage")
                //		{
                //			RightImg = (Image)GetChildren[i];
                //			RightImg.Source = "radio_btn_v2.png";
                //			RightImg.StyleId = "RadiocheckImage";
                //		}
                //		if (GetChildren[i].StyleId == "Image")
                //		{
                //			Form2 = (Image)GetChildren[i];
                //			Form2.Source = "Form2.png";
                //			Form2.StyleId = "Image1";
                //		}
                //	}
                //}

                foreach (var item in grdlst)
                {
                    var GetChildren = item.Children;
                    for (int i = 0; i < GetChildren.Count; i++)
                    {
                        if (GetChildren[i].StyleId == "RadioUncheckImage")
                        {
                            RightImg = (Image)GetChildren[i];
                            RightImg.Source = "radio_btn_v2.png";
                            RightImg.StyleId = "RadiocheckImage";
                        }
                        if (GetChildren[i].StyleId == "Image")
                        {
                            Form2 = (Image)GetChildren[i];
                            Form2.Source = "Form2.png";
                            Form2.StyleId = "Image1";
                        }
                    }
                }

                UncheckAll.IsVisible = false;
				CheckAll.IsVisible = true;
				UnSelectGrid.IsVisible = false;
				SelectGrid.IsVisible = true;
			}
			catch(Exception)
			{
				UserDialogs.Instance.HideLoading();
			}
			finally
			{
				UserDialogs.Instance.HideLoading();
			}

        }
		private void buttonChecked_Tapped(object sender, EventArgs e)
        {
			try
            {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);		    
            var SingleGrid = OddGrid.Children;
            Image SingleImg = new Image();
            Image LeftImg = new Image();
            Image RightImg = new Image();
            Image Form2 = new Image();
                //for (int i = 0; i < SingleGrid.Count; i++)
                //{

                //    if (SingleGrid[i].StyleId == "RadiocheckImage")
                //    {
                //        SingleImg = (Image)SingleGrid[i];
                //        SingleImg.Source = "Uncheck_Button.png";
                //        SingleImg.StyleId = "RadioUncheckImage";
                //    }
                //    if (SingleGrid[i].StyleId == "Image1")
                //    {
                //        Form2 = (Image)SingleGrid[i];
                //        Form2.Source = "Form1.png";
                //        Form2.StyleId = "Image";
                //    }
                //}
                //foreach (var item in LeftGridList)
                //{
                //    var GetChildren = item.Children;
                //    for (int i = 0; i < GetChildren.Count; i++)
                //    {

                //        if (GetChildren[i].StyleId == "RadiocheckImage")
                //        {
                //            LeftImg = (Image)GetChildren[i];
                //            LeftImg.Source = "Uncheck_Button.png";
                //            LeftImg.StyleId = "RadioUncheckImage";
                //        }
                //        if (GetChildren[i].StyleId == "Image1")
                //        {
                //            Form2 = (Image)GetChildren[i];
                //            Form2.Source = "Form1.png";
                //            Form2.StyleId = "Image";
                //        }
                //    }
                //}
                //foreach (var item in RightGridList)
                //{
                //    var GetChildren = item.Children;
                //    for (int i = 0; i < GetChildren.Count; i++)
                //    {
                //        if (GetChildren[i].StyleId == "RadiocheckImage")
                //        {
                //            RightImg = (Image)GetChildren[i];
                //            RightImg.Source = "Uncheck_Button.png";
                //            RightImg.StyleId = "RadioUncheckImage";
                //        }
                //        if (GetChildren[i].StyleId == "Image1")
                //        {
                //            Form2 = (Image)GetChildren[i];
                //            Form2.Source = "Form1.png";
                //            Form2.StyleId = "Image";
                //        }
                //    }
                //}


                foreach (var item in grdlst)
                {
                    var GetChildren = item.Children;
                    for (int i = 0; i < GetChildren.Count; i++)
                    {
                        if (GetChildren[i].StyleId == "RadiocheckImage")
                        {
                            RightImg = (Image)GetChildren[i];
                            RightImg.Source = "Uncheck_Button.png";
                            RightImg.StyleId = "RadioUncheckImage";
                        }
                        if (GetChildren[i].StyleId == "Image1")
                        {
                            Form2 = (Image)GetChildren[i];
                            Form2.Source = "Form1.png";
                            Form2.StyleId = "Image";
                        }
                    }
                }

            UncheckAll.IsVisible = true;
            CheckAll.IsVisible = false;
            UnSelectGrid.IsVisible = true;
            SelectGrid.IsVisible = false;
			}
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
        private async void ButtonBack_Tapped(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
            Task.Run(() =>
            {              
                Device.BeginInvokeOnMainThread(() =>
                {
                    App.Current.MainPage = new DynamicJobList();
                    UserDialogs.Instance.HideLoading();
                });
            });          
        }        
    }
}