﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ErrorPage : ContentPage
    {
        public ErrorPage()
        {
            InitializeComponent();
            ShowLogger("Logger"); 
        }

        public void ShowLogger(string UserName)
        {
            var htmlSource = new HtmlWebViewSource();
            var value1 = "";
            htmlSource.Html = @"<html><body>
                 <h1>Error Report</h1>
                  <br/>
                  <h4>Page:</h4><p></p>
                 <p>" + value1 + "</p></body></html>";
            lblSource.Source = htmlSource;
        }
    }
}