﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
    public partial class ForgotPassword : ContentPage
    {
        LoginService loginService = new LoginService();
        ForgotPasswordata Emailid = new ForgotPasswordata();
        public ForgotPassword()
        {
            try
            {
                InitializeComponent();
                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);


                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    //ImgMenu.Scale = 1.6;
                    btnSubmit.HeightRequest = 50;
                    bakicon.Margin = new Thickness(0, 12, 0, 0);
                    TitleBasicinfo.Margin = new Thickness(0, 12, 0, 0);
                }

                btnSubmit.Clicked += submit_clicked;
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }
        private  void ButtonBack_Tapped(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                Task.Run(() =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        App.Current.MainPage = new LoginForm();
                    });
                });
            }
            catch (Exception ex)
            {

            }


        }

        public void submit_clicked(object sender, EventArgs e)
        {
            Emailid.email_id = txtEmail.Text;
            forgotpassword(Emailid);
        }

        public ForgotPasswordModel forgotpassword(ForgotPasswordata Emailid)
        {
            try
            {
                var response = loginService.forgotpassword(Emailid);
                if (response.result == "success")
                {
                    //XFToast.LongMessage(response.message);
                    //App.Current.MainPage = new RecoverPassword();

                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {

                       UserDialogs.Instance.Alert(response.message);
                        App.Current.MainPage = new RecoverPassword();
                    }
                    else
                    {
                        XFToast.LongMessage(response.message);
                        App.Current.MainPage = new RecoverPassword();
                    }
                }
                else
                {
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert(response.message);
                    }
                    else
                    {
                        XFToast.LongMessage(response.message);
                    }

                }
                return response;
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                return null;
            }
        }




    }
}