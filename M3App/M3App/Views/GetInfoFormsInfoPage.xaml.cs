﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using M3App.Models;
using M3App.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using SignaturePad.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using M3App.CustomControl;

using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GetInfoFormsInfoPage : ContentPage
    {
        SampleModel s = new SampleModel();
        DynamicJobListModel Formlistmodel = new DynamicJobListModel();
        BackPressModel _logindetails = new BackPressModel();
        FormTypeList FormTypeListcurrentdata = new FormTypeList();
        DynamicFormListService dynamicFormListService = new DynamicFormListService();
        FormTypeListDownLoadModel formtypelistmodel = new FormTypeListDownLoadModel();
        FormTypeUpload upload = new FormTypeUpload();
        FormJsonUpload formJson = new FormJsonUpload();
        UploadComments uploadComments;
        public DateTime? MyDate { get; set; }
        //List<Form_basic_InfoUpload> form_Basic_InfoUploads = new List<Form_basic_InfoUpload>();
        List<Form_category_InfoUpload> form_Category_InfoUploads = new List<Form_category_InfoUpload>();
        List<category_Info_fieldsUpload> category_Info_FieldsUploads = new List<category_Info_fieldsUpload>();
        string signimage;
        private bool isRowEven;
        List<Form_basic_Info> Basic_Infos = new List<Form_basic_Info>();
        List<Form_category_Info> form_Category_Infos = new List<Form_category_Info>();
        List<Sub_category_Info> sub_category = new List<Sub_category_Info>();
        List<category_Info_fields> category_Info_Fields = new List<category_Info_fields>();
        List<Subcategory_Info_fields> sub_category_info_Add = new List<Subcategory_Info_fields>();
        List<DownloadComments> downloadComments = new List<DownloadComments>();
        
        string description = "";
        string Name = "";
        string FormStatus = "";
        int categoryId = 0;
        int SubCategoryID = 0;
        static int gridRowCounter = 0;
        static int categoryRowCounter = 0;
        static int Tankrowcounter = 0;
        int dcounty = 0;
        static int Tankcolumncounter = 0;
        int signcount = 0;
        bool ShowSaveButton = false;
        List<string> infoitem = new List<string>();
        SignaturePadView signaturePadView;
        bool dateselectcheck_ = false;
        public GetInfoFormsInfoPage(FormTypeList formTypeListdata=null)
        {
            try
            {
                InitializeComponent();
                lblJobId.Text = Settings.JobIdKey;
                sitename.Text = Settings.SiteNameKey;
                //TitleBasicinfo.Text = formTypeListdata.form_name;
                TitleBasicinfo.Text = formTypeListdata.form_short_name;
                var pagename = base.GetType().Name;
                AcessingVariables(pagename);
                Formstatus.WidthRequest = 200;
                
                this.BindingContext = _logindetails;
                _logindetails.setInitPage(this);

                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    bakicon.Margin = new Thickness(0, 15, 0, 0);
                    bakicon.Scale = 0.5;
                    TitleBasicinfo.Margin = new Thickness(0, 15, 0, 0);
                    btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 7);
                    saveapp.Margin = new Thickness(3, 3, 3, 10);
                }
                Application.Current.Properties.Remove("Approvesave");
                Application.Current.Properties.Add("Approvesave", "");
                btnApprovalSave.StyleId = "FormBasicSave";
                FormTypeListcurrentdata = formTypeListdata;
                formtypelistmodel = GetFormTypeList(FormTypeListcurrentdata);
                btnBasicInfo.Clicked += BtnBasicInfo_Clicked;
                btnCategoryDetails.Clicked += BtnCategoryDetails_Clicked;
                backPageTap.Tapped += BackPageTap_Tapped;
                btnApprovalSave.Clicked += BtnApprovalSave_Clicked;
                CmdButtonCat.Clicked += CmdButtonCat_Clicked;
                CmdButton.Clicked += CmdButton_Clicked;
                CommentsClose.Clicked += CommentsClose_Clicked;
                ViewCommentBtn.Clicked += ViewCommentBtn_Clicked;
                CmdButton.IsVisible = true;
                Formstatus.IsVisible = true;
                LblFormstatus.IsVisible = true;
                btnApprovalSave.IsEnabled = true;
                Formstatus.SelectedIndexChanged += Formstatus_SelectedIndexChanged;
                GetFormstatusList();
                GridBasicInfo.Children.Clear();
                gridRowCounter = 0;
                var navchck = Application.Current.Properties["ViewCommentsEnable"].ToString();
                if (formtypelistmodel != null)
                {
                    if (formtypelistmodel.response != null)
                    {
                        if (formtypelistmodel.response.form_json != null)
                        {
                            var formid = formtypelistmodel.response.form_id;
                            Settings.FormIdKey = formid;
                            Basic_Infos = formtypelistmodel.response.form_json.basic_Info;
                            form_Category_Infos = formtypelistmodel.response.form_json.category_Info;
                            downloadComments = formtypelistmodel.response.form_json.comments;
                            if (Basic_Infos != null)
                            {
                                foreach (var item in Basic_Infos)
                                {
                                    BasicInfoDetails(GridBasicInfo, item);
                                }
                            }
                            else
                            {
                                BasicInfoFrame.IsVisible = false;
                                btnApprovalSave.IsVisible = false;
                                XFToast.LongMessage("No data Available");
                            }
                        }
                        else
                        {
                            BasicInfoFrame.IsVisible = false;
                            btnApprovalSave.IsVisible = false;
                            XFToast.LongMessage("No data Available");
                        }
                    }
                    else
                    {
                        BasicInfoFrame.IsVisible = false;
                        btnApprovalSave.IsVisible = false;
                        XFToast.LongMessage(formtypelistmodel.message);
                    }
                }
                else
                {
                    BasicInfoFrame.IsVisible = false;
                    btnApprovalSave.IsVisible = false;
                    XFToast.LongMessage("No data Available");
                }
                var save_Enable= Application.Current.Properties["Save_Enable"].ToString();
                if (navchck == "true")
                {
                    if (save_Enable != "true")
                    {
                        CommentsFrame.IsVisible = true;
                        buttonInfo.IsVisible = false;
                        BasicInfoFrame.IsVisible = false;
                        btnApprovalSave.IsVisible = false;
                        CmdButton.IsVisible = false;
                        Formstatus.IsVisible = false;
                        LblFormstatus.IsVisible = false;
                        GridBasicInfoScroll.IsVisible = false;

                        Application.Current.Properties.Remove("RedirectStatus");
                        Application.Current.Properties.Add("RedirectStatus", "");
                    }
                }

            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
        public void AcessingVariables(string pagename)
        {
            Application.Current.Properties.Remove("PageName");
            Application.Current.Properties.Add("PageName", pagename);


            Application.Current.Properties.Remove("RedirectStatus");
            Application.Current.Properties.Add("RedirectStatus", "");

            Application.Current.Properties.Remove("RedirectStatusComments");
            Application.Current.Properties.Add("RedirectStatusComments", "");

            Application.Current.Properties.Remove("Tank");
            Application.Current.Properties.Add("Tank", "");

            Application.Current.Properties.Remove("SignImage");
            Application.Current.Properties.Add("SignImage", "");

            Application.Current.Properties.Remove("Save_Enable");
            Application.Current.Properties.Add("Save_Enable", "true");

            Application.Current.Properties.Remove("ImageHaving");
            Application.Current.Properties.Add("ImageHaving", "");

            Application.Current.Properties.Remove("ImageHavingCat");
            Application.Current.Properties.Add("ImageHavingCat", "");



            Application.Current.Properties.Remove("saveStatus");
            Application.Current.Properties.Add("saveStatus", "");

            Application.Current.Properties.Remove("CommentsSave");
            Application.Current.Properties.Add("CommentsSave", "");

        }
        private void Formstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            Picker data = sender as Picker;
            var item = data.SelectedItem;
            FormStatus = item.ToString();
        }
        private void Cell_OnAppearing(object sender, EventArgs e)
        {
            if (!this.isRowEven)
            {
                var viewCell = (ViewCell)sender;
                if (viewCell.View != null)
                {
                    viewCell.View.BackgroundColor = Color.FromHex("#FFFFFF");
                }
            }
            else
            {
                var viewCell = (ViewCell)sender;
                if (viewCell.View != null)
                {
                    viewCell.View.BackgroundColor = Color.FromHex("#FFFFFF");
                }
            }
            this.isRowEven = !this.isRowEven;
        }
        private async void BackPageTap_Tapped(object sender, EventArgs e)
        {

            Application.Current.Properties.Remove("CommentsSave");
            Application.Current.Properties.Add("CommentsSave", "");

            string status = Application.Current.Properties["RedirectStatus"].ToString();
            var saveyes = Application.Current.Properties["saveStatus"];

            if (status == "Category Details")
            {
                CmdButton.IsVisible = true;
                Formstatus.IsVisible = true;
                LblFormstatus.IsVisible = true;
                btnApprovalSave.IsEnabled = true;

                GridBasicInfoScroll.IsVisible = true;
                CategoryList.IsVisible = false;
                CategoryInfo.IsVisible = false;

                btnApprovalSave.IsVisible = true;
                GridBasicInfo.IsVisible = true;
                BasicInfoFrame.IsVisible = true;

                CommentCatdet.IsVisible = false;

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "");

            }

            //else if (status == "CommentsCat")
            //{
            //    buttonInfo.IsVisible = true;
            //    CategoryList.IsVisible = true;
            //    CategoryInfo.IsVisible = true;
            //    CommentsFrame.IsVisible = false;
            //    btnApprovalSave.IsVisible = false;
            //    GridBasicInfo.IsVisible = false;
            //    BasicInfoFrame.IsVisible = false;
            //    CommentCatdet.IsVisible = true;
            //    Application.Current.Properties.Remove("RedirectStatus");
            //    Application.Current.Properties.Add("RedirectStatus", "");


            //}
            else if (status == "CommentsCat")
            {
                var CommentsRedirectstatus = Application.Current.Properties["RedirectStatusComments"];
                if (CommentsRedirectstatus == "Category Details")
                {
                    buttonInfo.IsVisible = true;
                    CategoryList.IsVisible = true;
                    CategoryInfo.IsVisible = true;
                    CommentsFrame.IsVisible = false;
                    btnApprovalSave.IsVisible = false;
                    GridBasicInfo.IsVisible = false;
                    BasicInfoFrame.IsVisible = false;
                    CommentCatdet.IsVisible = true;
                    Application.Current.Properties.Remove("RedirectStatus");
                    Application.Current.Properties.Add("RedirectStatus", "Category Details");

                }
                if (CommentsRedirectstatus == "Category Details List")
                {
                    btnApprovalSave.IsVisible = false;
                    SubCategoryScroll.IsVisible = true;
                    GridCategoryInfo.IsVisible = false;
                    CategoryFrame.IsVisible = false;
                    SubCategoryList.IsVisible = true;
                    SubCategoryInfo.IsVisible = true;
                    CommentsFrame.IsVisible = false;
                    CategoryList.IsVisible = false;
                    CategoryInfo.IsVisible = false;
                    Headergrid.IsVisible = false;
                    CommentCatdet.IsVisible = true;
                    buttonInfo.IsVisible = true;

                    Application.Current.Properties.Remove("RedirectStatus");
                    Application.Current.Properties.Add("RedirectStatus", "Category Details List");

                }
                else if (CommentsRedirectstatus == "Category Sub List")
                {
                  
                    GridCategoryInfo.IsVisible = true;
                    SubCategoryScroll.IsVisible = true;
                    SubCategoryList.IsVisible = true;
                    CategoryFrame.IsVisible = true;
                    CategoryList.IsVisible = false;
                    GridBasicInfoScroll.IsVisible = false;
                    GridApplicableStatusScroll.IsVisible = false;
                    Formstatus.IsVisible = false;
                    LblFormstatus.IsVisible = false;
                    buttonInfo.IsVisible = true;
                    CommentsFrame.IsVisible = false;
                    CommentCatdet.IsVisible = true;
                    categorygrid.IsVisible = true;
                    btnApprovalSave.IsVisible = true;

                    Headergrid.IsVisible = true;
                    Header.IsVisible = true;
                    Description.IsVisible = true;

                    Application.Current.Properties.Remove("RedirectStatus");
                    Application.Current.Properties.Add("RedirectStatus", "Category Sub List");

                }
                

            }
            else if (status == "Category Details List")
            {
                if (saveyes == "SavedValue")
                {
                    var tmb = Application.Current.Properties["CategorySearch"];
                    BtnCategoryDetails_Clickedalise(tmb);
                }

                CategoryList.IsVisible = true;
                CategoryInfo.IsVisible = true;

                CategoryFrame.IsVisible = false;
                GridCategoryInfo.IsVisible = false;

                TankFrame.IsVisible = false;
                // btnApprovalSave.IsEnabled = false;

                btnApprovalSave.IsVisible = false;
                TankFrame.IsVisible = false;
                CmdButton.IsVisible = false;
                BasicInfoFrame.IsVisible = false;
                GridBasicInfoScroll.IsVisible = false;
                GridApplicableStatusScroll.IsVisible = false;
                SubCategoryList.IsVisible = false;
                SubCategoryInfo.IsVisible = false;
                Formstatus.IsVisible = false;
                LblFormstatus.IsVisible = false;
                Headergrid.IsVisible = false;
                GridApplicableStatus.IsVisible = false;

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "Category Details");

                // var tmb = Application.Current.Properties["CategorySearch"];
                // BtnCategoryDetails_Clickedalise(tmb);
            }
            else if (status == "Category Sub List")
            {
                //GridCategoryInfo.IsVisible = true;
                //SubCategoryScroll.IsVisible = true;
                //CategoryList.IsVisible = false;
                //CategoryInfo.IsVisible = false;
                ////btnApprovalSave.IsEnabled = false;

                btnApprovalSave.IsVisible = false;
                SubCategoryScroll.IsVisible = true;
                GridCategoryInfo.IsVisible = false;
                CategoryFrame.IsVisible = false;
                SubCategoryList.IsVisible = true;
                SubCategoryInfo.IsVisible = true;
                CommentsFrame.IsVisible = false;
                CategoryList.IsVisible = false;
                CategoryInfo.IsVisible = false;
                Headergrid.IsVisible = false;


                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "Category Details List");
            }
            else if (status == "Comments")
            {
                CmdButton.IsVisible = true;
                Formstatus.IsVisible = true;
                LblFormstatus.IsVisible = true;
                btnApprovalSave.IsEnabled = true;
                buttonInfo.IsVisible = true;
                GridBasicInfoScroll.IsVisible = true;
                CategoryList.IsVisible = false;
                CategoryInfo.IsVisible = false;

                CommentsFrame.IsVisible = false;

                btnApprovalSave.IsVisible = true;
                GridBasicInfo.IsVisible = true;
                BasicInfoFrame.IsVisible = true;

                CommentCatdet.IsVisible = false;
                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "");

            }
            else if (status == "ViewComments")
            {

                //FormInformationdata
                var t = Application.Current.Properties["FormInformationdata"];
                var tt = t as FormTypeList;

                Application.Current.Properties.Remove("ViewCommentsEnable");
                Application.Current.Properties.Add("ViewCommentsEnable", "true");

                App.Current.MainPage = new Views.GetInfoFormsInfoPage(tt);

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "Comments");

                CommentCatdet.IsVisible = false;

            }
            else
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                App.Current.MainPage = new M3App.Views.DynamicJobList();
                UserDialogs.Instance.HideLoading();
            }

        }

        private async void ViewCommentBtn_Clicked(object sender, EventArgs e)
        {
            int i = 0;
            //await Navigation.PushAsync(new ViewComments(downloadComments) { Title = "ViewComments" });
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
            var t = Application.Current.Properties["FormTypeListObject"];
            var tt = t as FormTypeList;
            var Formslistdata = dynamicFormListService.Dynamicformdatas(tt);
            if (Formslistdata != null)
            {
                downloadComments = Formslistdata.response.form_json.comments;
                if (downloadComments != null)
                {
                    downloadComments.ForEach(x =>
                    {
                        x.Index = i;
                        i++;
                    });
                }
            }
            else
            {
                XFToast.ShortMessage("No Comments Here!");
            }
            App.Current.MainPage = new M3App.Views.ViewComments(downloadComments);
            UserDialogs.Instance.HideLoading();
        }

        private void CommentsClose_Clicked(object sender, EventArgs e)
        {
			UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            var statusgetting= Application.Current.Properties["RedirectStatus"];
            if (statusgetting == "CommentsCat")
            {
                Application.Current.Properties.Remove("CommentsSave");
                Application.Current.Properties.Add("CommentsSave", "CloseSaveComment");
                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "CommentsCat");
                var status = Application.Current.Properties["RedirectStatus"];
                SaveApproval();
                var CommentsRedirectstatus = Application.Current.Properties["RedirectStatusComments"];
                if (CommentsRedirectstatus == "Category Details")
                {
                    buttonInfo.IsVisible = true;
                    CategoryList.IsVisible = true;
                    CategoryInfo.IsVisible = true;
                    CommentsFrame.IsVisible = false;
                    btnApprovalSave.IsVisible = false;
                    GridBasicInfo.IsVisible = false;
                    BasicInfoFrame.IsVisible = false;
                    CommentCatdet.IsVisible = true;
                    Application.Current.Properties.Remove("RedirectStatus");
                    Application.Current.Properties.Add("RedirectStatus", "Category Details");

                }
                if (CommentsRedirectstatus == "Category Details List")
                {
                    btnApprovalSave.IsVisible = false;
                    SubCategoryScroll.IsVisible = true;
                    GridCategoryInfo.IsVisible = false;
                    CategoryFrame.IsVisible = false;
                    SubCategoryList.IsVisible = true;
                    SubCategoryInfo.IsVisible = true;
                    CommentsFrame.IsVisible = false;
                    CategoryList.IsVisible = false;
                    CategoryInfo.IsVisible = false;
                    Headergrid.IsVisible = false;
                    CommentCatdet.IsVisible = true;
                    buttonInfo.IsVisible = true;

                    Application.Current.Properties.Remove("RedirectStatus");
                    Application.Current.Properties.Add("RedirectStatus", "Category Details List");

                }
                else if (CommentsRedirectstatus == "Category Sub List")
                {

                    GridCategoryInfo.IsVisible = true;
                    SubCategoryScroll.IsVisible = true;
                    SubCategoryList.IsVisible = true;
                    CategoryFrame.IsVisible = true;
                    CategoryList.IsVisible = false;
                    GridBasicInfoScroll.IsVisible = false;
                    GridApplicableStatusScroll.IsVisible = false;
                    Formstatus.IsVisible = false;
                    LblFormstatus.IsVisible = false;
                    buttonInfo.IsVisible = true;
                    CommentsFrame.IsVisible = false;
                    CommentCatdet.IsVisible = true;
                    categorygrid.IsVisible = true;
                    btnApprovalSave.IsVisible = true;

                    Headergrid.IsVisible = true;
                    Header.IsVisible = true;
                    Description.IsVisible = true;

                    Application.Current.Properties.Remove("RedirectStatus");
                    Application.Current.Properties.Add("RedirectStatus", "Category Sub List");

                }
                UserDialogs.Instance.HideLoading();
            }
            else
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                CommentsFrame.IsVisible = false;
                buttonInfo.IsVisible = true;
                BasicInfoFrame.IsVisible = true;
                btnApprovalSave.IsVisible = true;
                CmdButton.IsVisible = true;
                Formstatus.IsVisible = true;
                LblFormstatus.IsVisible = true;
                GridBasicInfoScroll.IsVisible = true;

                Application.Current.Properties.Remove("CommentsSave");
                Application.Current.Properties.Add("CommentsSave", "CloseSave");

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "");

                SaveApproval();
                
                UserDialogs.Instance.HideLoading();
            }

        }

        private void CmdButtonCat_Clicked(object sender, EventArgs e)
        {
            CommentsFrame.IsVisible = true;
            EditComments.Text = "";
            Signature.Clear();
            datepicker.Date = DateTime.Today;
            Signature.Margin = -10;
            Headergrid.IsVisible = false;
            buttonInfo.IsVisible = false;
            BasicInfoFrame.IsVisible = false;
            btnApprovalSave.IsVisible = false;
            CmdButton.IsVisible = false;
            Formstatus.IsVisible = false;
            LblFormstatus.IsVisible = false;
            GridBasicInfoScroll.IsVisible = false;


            //CategoryDet

            CategoryList.IsVisible = false;
            CategoryInfo.IsVisible = false;
            CommentCatdet.IsVisible = false;
            SubCategoryList.IsVisible = false;
            SubCategoryInfo.IsVisible = false;
            categorygrid.IsVisible = false;
            Tankdetails.IsVisible = false;
            btnApprovalSave.IsVisible = false;

            var CommentsRedirectstatus = Application.Current.Properties["RedirectStatus"];

            Application.Current.Properties.Remove("RedirectStatus");
            Application.Current.Properties.Add("RedirectStatus", "CommentsCat");

            Application.Current.Properties.Remove("RedirectStatusComments");
            Application.Current.Properties.Add("RedirectStatusComments", CommentsRedirectstatus);
        }
            
        //CmdButtonCat
        private void CmdButton_Clicked(object sender, EventArgs e)
        {
            CommentsFrame.IsVisible = true;
            EditComments.Text = "";
            Signature.Clear();
            datepicker.Date = DateTime.Today;
            //signcomments.Children.Add(signaturePadView.ClearLabel, 0, 0);
            Signature.Margin = -10;
            Headergrid.IsVisible = false;
            buttonInfo.IsVisible = false;
            BasicInfoFrame.IsVisible = false;
            btnApprovalSave.IsVisible = false;
            CmdButton.IsVisible = false;
            Formstatus.IsVisible = false;
            LblFormstatus.IsVisible = false;
            GridBasicInfoScroll.IsVisible = false;

            Application.Current.Properties.Remove("RedirectStatus");
            Application.Current.Properties.Add("RedirectStatus", "Comments");

        }
        private void BtnCategorySave_Clicked(object sender, EventArgs e)
        {
            GridCategoryInfo.IsVisible = false;
            GridApplicableStatus.IsVisible = true;
        }

        public async void Testdata(string status)
        {
            Application.Current.Properties.Remove("saveStatus");
            Application.Current.Properties.Add("saveStatus", "SavedValue");
            // Buttonsubsearch
            if (status == "Category Details")
            {
                CmdButton.IsVisible = true;
                Formstatus.IsVisible = true;
                LblFormstatus.IsVisible = true;
                btnApprovalSave.IsEnabled = true;
                GridBasicInfoScroll.IsVisible = true;
                CategoryList.IsVisible = false;
                CategoryInfo.IsVisible = false;
                CommentsFrame.IsVisible = false;
                btnApprovalSave.IsVisible = true;
                GridBasicInfo.IsVisible = true;
                BasicInfoFrame.IsVisible = true;
                //CommentCatdet.IsVisible = true;
                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "");
            }
            else if (status == "Comments")
            {
                CmdButton.IsVisible = true;
                Formstatus.IsVisible = true;
                LblFormstatus.IsVisible = true;
                btnApprovalSave.IsEnabled = true;
                buttonInfo.IsVisible = true;
                GridBasicInfoScroll.IsVisible = true;
                CategoryList.IsVisible = false;
                CategoryInfo.IsVisible = false;
                CommentsFrame.IsVisible = false;
                btnApprovalSave.IsVisible = true;
                GridBasicInfo.IsVisible = true;
                BasicInfoFrame.IsVisible = true;
                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "");

            }

            else if (status == "Category Details List")
            {
                var tjk = Application.Current.Properties["ButtonSearch"];
                if (tjk != "" && tjk != null)
                {
                    var selecteditem = (Grid)tjk;
                    var item = selecteditem.BindingContext;
                    var infoitem = item as Form_category_Info;
                    var kt = infoitem.id;

                    var t = Settings.idslist;
                    t.Add(Convert.ToString(infoitem.id));

                    //Application.Current.Properties.Remove("ItemList");
                    // Application.Current.Properties.Add("ItemList", kt);
                }

                CommentCatdet.IsVisible = true;
                CategoryList.IsVisible = true;
                CategoryInfo.IsVisible = true;

                CategoryFrame.IsVisible = false;
                GridCategoryInfo.IsVisible = false;
                CommentsFrame.IsVisible = false;
                TankFrame.IsVisible = false;
                btnApprovalSave.IsVisible = false;
                TankFrame.IsVisible = false;
                Headergrid.IsVisible = false;
                Tankdetails.IsVisible = false;
                CmdButton.IsVisible = false;
                BasicInfoFrame.IsVisible = false;
                GridBasicInfoScroll.IsVisible = false;
                GridApplicableStatusScroll.IsVisible = false;
                SubCategoryList.IsVisible = false;
                SubCategoryInfo.IsVisible = false;
                Formstatus.IsVisible = false;
                LblFormstatus.IsVisible = false;

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "Category Details");

                var tmb = Application.Current.Properties["CategorySearch"];
                BtnCategoryDetails_Clickedalise(tmb);


            }
            else if (status == "Category Sub List")
            {
                var tjk = Application.Current.Properties["Buttonsubsearch"];
                if (tjk != "" && tjk != null)
                {
                    var selecteditem = (Grid)tjk;
                    var item = selecteditem.BindingContext;
                    var infoitem = item as Sub_category_Info;
                    var kt = infoitem.id;

                    var tk = Settings.idslist;
                    tk.Add(Convert.ToString(infoitem.id));

                    // Application.Current.Properties.Remove("ItemList");
                    //  Application.Current.Properties.Add("ItemList", kt);
                }

                CommentCatdet.IsVisible = true;
                GridBasicInfoScroll.IsVisible = false;
                BasicInfoFrame.IsVisible = false;
                GridBasicInfo.IsVisible = false;
                LblFormstatus.IsVisible = false;
                CmdButton.IsVisible = false;
                Formstatus.IsVisible = false;


                SubCategoryScroll.IsVisible = true;
                GridCategoryInfo.IsVisible = false;
                CategoryFrame.IsVisible = false;
                SubCategoryList.IsVisible = true;
                SubCategoryInfo.IsVisible = true;
                CommentsFrame.IsVisible = false;
                CategoryList.IsVisible = false;
                CategoryInfo.IsVisible = false;
                btnApprovalSave.IsVisible = false;


                var t = Application.Current.Properties["ButtonSearch"];
                ListItemRefresh(t);
                buttonSearch_Tappedalise(t);
                var tt = Application.Current.Properties["Buttonsubsearch"];



                var entity = ((Grid)tt);
                entity.BackgroundColor = Color.Gray;

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "Category Details List");
            }
            else
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                App.Current.MainPage = new M3App.Views.DynamicJobList();
                UserDialogs.Instance.HideLoading();
            }

        }
        ////var tmb = Application.Current.Properties["CategorySearch"];
        //BtnCategoryDetails_Clickedalise(tmb);
        public void ListItemRefresh(Object sen)
        {
            var selecteditem = (Grid)sen;
            var item = selecteditem.BindingContext;
            var catid = item as Form_category_Info;
            var kt = catid.id;
            var testing = formtypelistmodel.response.form_json.category_Info.Where(m => m.id == kt).FirstOrDefault();
            selecteditem.BindingContext = testing;

        }
        public async void RedirectGridPageInfo(string status)
        {
            //if(status== "Category Details")
            //  {
            //      CmdButton.IsVisible = true;
            //      Formstatus.IsVisible = true;
            //      LblFormstatus.IsVisible = true;
            //      btnApprovalSave.IsEnabled = true;

            //      GridBasicInfoScroll.IsVisible = true;
            //      CategoryList.IsVisible = false;
            //      CategoryInfo.IsVisible = false;

            //      CommentsFrame.IsVisible = false;

            //      btnApprovalSave.IsVisible = true;
            //      GridBasicInfo.IsVisible = true;
            //      BasicInfoFrame.IsVisible = true;

            //      Application.Current.Properties.Remove("RedirectStatus");
            //      Application.Current.Properties.Add("RedirectStatus", "");

            //  }
            //else if(status == "Comments")
            //  {
            //      CmdButton.IsVisible = true;
            //      Formstatus.IsVisible = true;
            //      LblFormstatus.IsVisible = true;
            //      btnApprovalSave.IsEnabled = true;
            //      buttonInfo.IsVisible = true;
            //      GridBasicInfoScroll.IsVisible = true;
            //      CategoryList.IsVisible = false;
            //      CategoryInfo.IsVisible = false;

            //      CommentsFrame.IsVisible = false;

            //      btnApprovalSave.IsVisible = true;
            //      GridBasicInfo.IsVisible = true;
            //      BasicInfoFrame.IsVisible = true;

            //      Application.Current.Properties.Remove("RedirectStatus");
            //      Application.Current.Properties.Add("RedirectStatus", "");

            //  }
            //else if (status == "ViewComments")
            //  {
            //      //FormInformationdata
            //      var t = Application.Current.Properties["FormInformationdata"];
            //      var tt = t as FormTypeList;

            //      Application.Current.Properties.Remove("ViewCommentsEnable");
            //      Application.Current.Properties.Add("ViewCommentsEnable", "true");

            //      App.Current.MainPage = new Views.FormsInformation(tt);


            //      CommentsFrame.IsVisible = true;
            //      buttonInfo.IsVisible = false;
            //      BasicInfoFrame.IsVisible = false;
            //      btnApprovalSave.IsVisible = false;
            //      CmdButton.IsVisible = false;
            //      Formstatus.IsVisible = false;
            //      LblFormstatus.IsVisible = false;
            //      GridBasicInfoScroll.IsVisible = false;

            //      Application.Current.Properties.Remove("RedirectStatus");
            //      Application.Current.Properties.Add("RedirectStatus", "Comments");
            //  }
            //else if(status == "Category Details List")
            //  {
            //      CategoryList.IsVisible = true;
            //      CategoryInfo.IsVisible = true;

            //      CategoryFrame.IsVisible = false;
            //      GridCategoryInfo.IsVisible = false;
            //      CommentsFrame.IsVisible = false;
            //      TankFrame.IsVisible = false;
            //      //btnApprovalSave.IsEnabled = false;

            //      btnApprovalSave.IsVisible = false;
            //      TankFrame.IsVisible = false;
            //      Headergrid.IsVisible = false;
            //      Tankdetails.IsVisible = false;
            //      CmdButton.IsVisible = false;
            //      BasicInfoFrame.IsVisible = false;
            //      GridBasicInfoScroll.IsVisible = false;
            //      GridApplicableStatusScroll.IsVisible = false;
            //      SubCategoryList.IsVisible = false;
            //      SubCategoryInfo.IsVisible = false;
            //      Formstatus.IsVisible = false;
            //      LblFormstatus.IsVisible = false;

            //      // Application.Current.Properties.Remove("RedirectStatus");
            //      // Application.Current.Properties.Add("RedirectStatus", "Category Details List");
            //      Application.Current.Properties.Remove("RedirectStatus");
            //      Application.Current.Properties.Add("RedirectStatus", "Category Details");
            //  }
            //else if(status== "Category Sub List")
            //  {

            //      //GridCategoryInfo.IsVisible = true;
            //      SubCategoryScroll.IsVisible = true;
            //      GridCategoryInfo.IsVisible = false;
            //      CategoryFrame.IsVisible = false;
            //      SubCategoryList.IsVisible = true;
            //      SubCategoryInfo.IsVisible = true;
            //      CommentsFrame.IsVisible = false;
            //      CategoryList.IsVisible = false;
            //      CategoryInfo.IsVisible = false;
            //      //btnApprovalSave.IsEnabled = false;
            //      btnApprovalSave.IsVisible = false;

            //      Application.Current.Properties.Remove("RedirectStatus");
            //      Application.Current.Properties.Add("RedirectStatus", "Category Details List");
            //  }
            //  else
            //  {
            //      UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            //      await Task.Delay(100);
            //      App.Current.MainPage = new M3App.Views.DynamicJobList();
            //      UserDialogs.Instance.HideLoading();
            //  }



            //  string status = Application.Current.Properties["RedirectStatus"].ToString();


            var saveyes = Application.Current.Properties["saveStatus"];

            Application.Current.Properties.Remove("CommentsSave");
            Application.Current.Properties.Add("CommentsSave", "");


            if (status == "Category Details")
            {
                CmdButton.IsVisible = true;
                Formstatus.IsVisible = true;
                LblFormstatus.IsVisible = true;
                btnApprovalSave.IsEnabled = true;

                GridBasicInfoScroll.IsVisible = true;
                CategoryList.IsVisible = false;
                CategoryInfo.IsVisible = false;

                btnApprovalSave.IsVisible = true;
                GridBasicInfo.IsVisible = true;
                BasicInfoFrame.IsVisible = true;
                CommentCatdet.IsVisible = false;


                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "");

            }
            else if (status == "Category Details List")
            {
                if (saveyes == "SavedValue")
                {
                    var tmb = Application.Current.Properties["CategorySearch"];
                    BtnCategoryDetails_Clickedalise(tmb);
                }

                CategoryList.IsVisible = true;
                CategoryInfo.IsVisible = true;

                CategoryFrame.IsVisible = false;
                GridCategoryInfo.IsVisible = false;

                TankFrame.IsVisible = false;
                // btnApprovalSave.IsEnabled = false;

                btnApprovalSave.IsVisible = false;
                TankFrame.IsVisible = false;
                CmdButton.IsVisible = false;
                BasicInfoFrame.IsVisible = false;
                GridBasicInfoScroll.IsVisible = false;
                GridApplicableStatusScroll.IsVisible = false;
                SubCategoryList.IsVisible = false;
                SubCategoryInfo.IsVisible = false;
                Formstatus.IsVisible = false;
                LblFormstatus.IsVisible = false;
                Headergrid.IsVisible = false;
                GridApplicableStatus.IsVisible = false;
                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "Category Details");

            }
            else if (status == "Category Sub List")
            {
                if (saveyes == "SavedValue")
                {

                }

                //GridCategoryInfo.IsVisible = true;
                //SubCategoryScroll.IsVisible = true;
                //CategoryList.IsVisible = false;
                //CategoryInfo.IsVisible = false;
                ////btnApprovalSave.IsEnabled = false;

                SubCategoryScroll.IsVisible = true;
                GridCategoryInfo.IsVisible = false;
                CategoryFrame.IsVisible = false;
                SubCategoryList.IsVisible = true;
                SubCategoryInfo.IsVisible = true;
                CommentsFrame.IsVisible = false;
                CategoryList.IsVisible = false;
                CategoryInfo.IsVisible = false;
                btnApprovalSave.IsVisible = false;
                Headergrid.IsVisible = false;


                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "Category Details List");
            }
            else if (status == "Comments")
            {
                CmdButton.IsVisible = true;
                Formstatus.IsVisible = true;
                LblFormstatus.IsVisible = true;
                btnApprovalSave.IsEnabled = true;
                buttonInfo.IsVisible = true;
                GridBasicInfoScroll.IsVisible = true;
                CategoryList.IsVisible = false;
                CategoryInfo.IsVisible = false;

                CommentsFrame.IsVisible = false;

                btnApprovalSave.IsVisible = true;
                GridBasicInfo.IsVisible = true;
                BasicInfoFrame.IsVisible = true;

                CommentCatdet.IsVisible = false;

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "");

            }
            else if (status == "CommentsCat")
            {
                var CommentsRedirectstatus = Application.Current.Properties["RedirectStatusComments"];
                if (CommentsRedirectstatus == "Category Details")
                {
                    buttonInfo.IsVisible = true;
                    CategoryList.IsVisible = true;
                    CategoryInfo.IsVisible = true;
                    CommentsFrame.IsVisible = false;
                    btnApprovalSave.IsVisible = false;
                    GridBasicInfo.IsVisible = false;
                    BasicInfoFrame.IsVisible = false;
                    CommentCatdet.IsVisible = true;
                    Application.Current.Properties.Remove("RedirectStatus");
                    Application.Current.Properties.Add("RedirectStatus", "Category Details");

                }
                if (CommentsRedirectstatus == "Category Details List")
                {
                    btnApprovalSave.IsVisible = false;
                    SubCategoryScroll.IsVisible = true;
                    GridCategoryInfo.IsVisible = false;
                    CategoryFrame.IsVisible = false;
                    SubCategoryList.IsVisible = true;
                    SubCategoryInfo.IsVisible = true;
                    CommentsFrame.IsVisible = false;
                    CategoryList.IsVisible = false;
                    CategoryInfo.IsVisible = false;
                    Headergrid.IsVisible = false;
                    CommentCatdet.IsVisible = true;
                    buttonInfo.IsVisible = true;

                    Application.Current.Properties.Remove("RedirectStatus");
                    Application.Current.Properties.Add("RedirectStatus", "Category Details List");

                }
                else if (CommentsRedirectstatus == "Category Sub List")
                {

                    GridCategoryInfo.IsVisible = true;
                    SubCategoryScroll.IsVisible = true;
                    SubCategoryList.IsVisible = true;
                    CategoryFrame.IsVisible = true;
                    CategoryList.IsVisible = false;
                    GridBasicInfoScroll.IsVisible = false;
                    GridApplicableStatusScroll.IsVisible = false;
                    Formstatus.IsVisible = false;
                    LblFormstatus.IsVisible = false;
                    buttonInfo.IsVisible = true;
                    CommentsFrame.IsVisible = false;
                    CommentCatdet.IsVisible = true;
                    categorygrid.IsVisible = true;
                    btnApprovalSave.IsVisible = true;

                    Headergrid.IsVisible = true;
                    Header.IsVisible = true;
                    Description.IsVisible = true;

                    Application.Current.Properties.Remove("RedirectStatus");
                    Application.Current.Properties.Add("RedirectStatus", "Category Sub List");

                }
            }
            else if (status == "ViewComments")
            {

                //FormInformationdata
                var t = Application.Current.Properties["FormInformationdata"];
                var tt = t as FormTypeList;

                Application.Current.Properties.Remove("ViewCommentsEnable");
                Application.Current.Properties.Add("ViewCommentsEnable", "true");

                App.Current.MainPage = new Views.GetInfoFormsInfoPage(tt);

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "Comments");

                CommentCatdet.IsVisible = false;

            }
            else
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                App.Current.MainPage = new M3App.Views.DynamicJobList();
                UserDialogs.Instance.HideLoading();
            }


        }
        public void BtnCategoryDetails_Clickedalise(object sender)
        {
            if (Device.RuntimePlatform.ToLower() == "ios")
            {
                btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 7);
            }
            else
            {
                btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 10);
            }

            Application.Current.Properties.Remove("CategorySearch");
            Application.Current.Properties.Add("CategorySearch", sender);

            var btn = sender as Button;
            var name = btn.Text;
            Application.Current.Properties.Remove("RedirectStatus");
            Application.Current.Properties.Add("RedirectStatus", name);
            if (formtypelistmodel != null)
            {
                if (formtypelistmodel.response.form_json != null)
                {
                    form_Category_Infos = formtypelistmodel.response.form_json.category_Info;

                    if (form_Category_Infos != null && form_Category_Infos.Count > 0)
                    {
                        CategoryInfo.ItemsSource = form_Category_Infos;
                    }
                    else
                    {
                        XFToast.LongMessage("No data Available");
                    }
                }
                else
                {
                    XFToast.LongMessage("No data Available");
                }
            }
            else
            {
                XFToast.LongMessage("No data Available");
            }
            btnBasicInfo.BackgroundColor = Color.FromHex("#FFFFFF");
            btnBasicInfo.TextColor = Color.FromHex("#1A257F");
            //buttonn
            btnCategoryDetails.BackgroundColor = Color.FromHex("#1A257F");
            btnCategoryDetails.TextColor = Color.FromHex("#FFFFFF");

            if (Device.RuntimePlatform.ToLower() == "ios")
            {
                btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 7);
            }
            else
            {
                btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 10);
            }
            BasicInfoFrame.IsVisible = false;

            CategoryFrame.IsVisible = false;
            SubCategoryScroll.IsVisible = false;
            GridCategoryInfo.IsVisible = false;
            btnApprovalSave.IsVisible = false;
            TankFrame.IsVisible = false;
            CmdButton.IsVisible = false;
            GridBasicInfoScroll.IsVisible = false;
            GridApplicableStatusScroll.IsVisible = false;
            CategoryList.IsVisible = true;
            CategoryInfo.IsVisible = true;
            Formstatus.IsVisible = false;
            LblFormstatus.IsVisible = false;

        }

        private void BtnCategoryDetails_Clicked(object sender, EventArgs e)
        {
            CommentCatdet.IsVisible = true;
           

            Application.Current.Properties.Remove("CategorySearch");
            Application.Current.Properties.Add("CategorySearch", sender);
            Headergrid.IsVisible = false;
            var btn = sender as Button;
            var name = btn.Text;
            Application.Current.Properties.Remove("RedirectStatus");
            Application.Current.Properties.Add("RedirectStatus", name);
            if (formtypelistmodel != null)
            {
                if (formtypelistmodel.response.form_json != null)
                {
                    form_Category_Infos = formtypelistmodel.response.form_json.category_Info;

                    if (form_Category_Infos != null && form_Category_Infos.Count > 0)
                    {
                        CategoryInfo.ItemsSource = form_Category_Infos;
                    }
                    else
                    {
                        XFToast.LongMessage("No data Available");
                    }
                }
                else
                {
                    XFToast.LongMessage("No data Available");
                }
            }
            else
            {
                XFToast.LongMessage("No data Available");
            }
            btnBasicInfo.BackgroundColor = Color.FromHex("#FFFFFF");
            btnBasicInfo.TextColor = Color.FromHex("#1A257F");
            //buttonn
            btnCategoryDetails.BackgroundColor = Color.FromHex("#1A257F");
            btnCategoryDetails.TextColor = Color.FromHex("#FFFFFF");
            //btnCategoryDetails.Margin = new Thickness(0, 10, 0, 10);
            //Margin = "-15,10,10,8"

            if (Device.RuntimePlatform.ToLower() == "ios")
            {
                btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 7);
            }
            else
            {
                btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 10);
            }
            BasicInfoFrame.IsVisible = false;

            CategoryFrame.IsVisible = false;
            SubCategoryScroll.IsVisible = false;
            GridCategoryInfo.IsVisible = false;
            btnApprovalSave.IsVisible = false;
            TankFrame.IsVisible = false;
            CmdButton.IsVisible = false;
            GridBasicInfoScroll.IsVisible = false;
            GridApplicableStatusScroll.IsVisible = false;
            CategoryList.IsVisible = true;
            CategoryInfo.IsVisible = true;
            Formstatus.IsVisible = false;
            LblFormstatus.IsVisible = false;

        }
        //NeedCheck
        public async void buttonSearch_Tappedalise(object sender)
        {
            try
            {
                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 7);
                }
                else
                {
                    btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 10);
                }

                // var entity = ((Grid)sender);
                // entity.BackgroundColor = Color.Gray;

                var headerval = sender as Grid;
                var child = headerval.Children.ToList();
                foreach (var _ch in child)
                {
                    var headervals = _ch as Grid;
                    var child2 = headervals.Children.ToList();

                    foreach (var itm in child2)
                    {
                        var chktype = itm.GetType().FullName;
                        if (chktype == "Xamarin.Forms.Label")
                        {
                            var header = itm.GetType().FullName;

                            Categorynamendes.Text = header;
                        }
                    }
                }
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                var text = "Category Details List";

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", text);

                categoryRowCounter = 0;
                GridApplicableStatus.Children.Clear();
                Tankcolumncounter = 0;
                Tankrowcounter = 0;
                description = string.Empty;
                Name = string.Empty;
                btnBasicInfo.BackgroundColor = Color.FromHex("#FFFFFF");
                btnBasicInfo.TextColor = Color.FromHex("#1A257F");
                btnCategoryDetails.BackgroundColor = Color.FromHex("#1A257F");
                btnCategoryDetails.TextColor = Color.FromHex("#FFFFFF");
                GridCategoryInfo.Children.Clear();
                CategoryInfo.IsVisible = false;
                //cut
                GridCategoryInfo.IsVisible = true;
               // GridCategoryInfo.IsVisible = false;
                //End
                btnApprovalSave.IsVisible = false;
                //cut
                TankFrame.IsVisible = true;
                //TankFrame.IsVisible = false;
                //End
                CmdButton.IsVisible = false;
                BasicInfoFrame.IsVisible = false;
                GridBasicInfoScroll.IsVisible = false;
                GridApplicableStatusScroll.IsVisible = false;
                SubCategoryList.IsVisible = false;
                SubCategoryInfo.IsVisible = false;
                Formstatus.IsVisible = false;
                LblFormstatus.IsVisible = false;
                CategoryList.IsVisible = false;
                //Cut
                CategoryFrame.IsVisible =true;
                //End

                var selecteditem = (Grid)sender;
                var item = selecteditem.BindingContext;
                var infoitem = item as Form_category_Info;

                description = infoitem.description;
                Name = infoitem.name;
                categoryId = infoitem.id;
                var t = Settings.idslist;
                t.Add(Convert.ToString(categoryId));
                //nodoo
                //Settings.idslist.Add();
                if (Name != null)
                {
                    Header.Text = Name;
                }

                if (!string.IsNullOrEmpty(description))
                {
                    Header.IsVisible = true;
                    Headergrid.IsVisible = true;
                    Description.IsVisible = true;
                    Description.Text = description;

                }


                int count = 0;
                if (infoitem.sub_category != null)
                {
                    SubCategoryScroll.IsVisible = false;
                    //Cut
                    SubCategoryList.IsVisible = true;
                    SubCategoryInfo.IsVisible = true;
                   // btnApprovalSave.IsEnabled = false;
                   // btnApprovalSave.IsVisible = false;
                    //TankFrame.IsVisible = false;
                    //End
                    if (infoitem.sub_category.Count > 0)
                        SubCategoryInfo.ItemsSource = infoitem.sub_category.OrderBy(s => s.id);
                    //  var sen=  Application.Current.Properties["Buttonsubsearch"];
                    // ListView _vid = (ListView)sen;
                    // _vid.BackgroundColor = Color.Pink;

                }
                else if (infoitem.sub_category == null)
                {
                    GridCategoryInfo.IsVisible = true;
                    SubCategoryList.IsVisible = false;
                    SubCategoryInfo.IsVisible = false;
                    SubCategoryScroll.IsVisible = true;
                    btnApprovalSave.IsVisible = true;
                    btnApprovalSave.StyleId = "CategoryFields_Save";
                    CategoryFrame.IsVisible = true;
                    btnApprovalSave.IsEnabled = true;
                    category_Info_Fields = infoitem.fields;

                    if (infoitem.fields != null)
                    {
                        //Valli ji 
                        if (formtypelistmodel.response.form_name == "SAMPLE FORM FOR MONTHLY UNDERGROUND STORAGE SYSTEM INSPECTION CHECKLIST")
                        {
                            foreach (var data in infoitem.fields)
                            {
                                if (count <= 1)
                                {
                                    categoryinfodetails(GridCategoryInfo, data);
                                    Application.Current.Properties.Remove("SignImageCat");
                                    Application.Current.Properties.Add("SignImageCat", infoitem.fields);
                                    count++;
                                }
                            }

                            if (count >= 2)
                            {
                                for (int i = 2; i < infoitem.fields.Count; i++)
                                {
                                    TankDetails(GridApplicableStatus, infoitem.fields[i]);
                                }
                            }

                        }
                        else
                        {
                            foreach (var data in infoitem.fields)
                            {
                                categoryinfodetails(GridCategoryInfo, data);
                                Application.Current.Properties.Remove("SignImageCat");
                                Application.Current.Properties.Add("SignImageCat", infoitem.fields);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }

        }
        //fff
        //Categoryclick
        private async void buttonSearch_Tapped(object sender, EventArgs e)
        {
            Application.Current.Properties.Remove("ButtonSearch");
            Application.Current.Properties.Add("ButtonSearch", sender);
            try
            {
                Header.IsVisible = false;
                Headergrid.IsVisible = false;
                Description.IsVisible = false;

                var headerval = sender as Grid;
                var child = headerval.Children.ToList();
                foreach (var _ch in child)
                {
                    var headervals = _ch as Grid;
                    var child2 = headervals.Children.ToList();

                    foreach (var itm in child2)
                    {
                        var chktype = itm.GetType().FullName;
                        if (chktype == "Xamarin.Forms.Label")
                        {
                            var header = itm.GetType().FullName;

                            Categorynamendes.Text = header;
                        }
                    }
                }
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                var text = "Category Details List";

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", text);

                categoryRowCounter = 0;
                GridApplicableStatus.Children.Clear();
                Tankcolumncounter = 0;
                Tankrowcounter = 0;
                description = string.Empty;
                Name = string.Empty;
                btnBasicInfo.BackgroundColor = Color.FromHex("#FFFFFF");
                btnBasicInfo.TextColor = Color.FromHex("#1A257F");
                btnCategoryDetails.BackgroundColor = Color.FromHex("#1A257F");
                btnCategoryDetails.TextColor = Color.FromHex("#FFFFFF");
                GridCategoryInfo.Children.Clear();
                CategoryInfo.IsVisible = false;
                //CategoryFrame.IsVisible = false;
                GridCategoryInfo.IsVisible = true;
                btnApprovalSave.IsVisible = false;
                // TankFrame.IsVisible = false;
                TankFrame.IsVisible = true;
                CmdButton.IsVisible = false;
                BasicInfoFrame.IsVisible = false;
                GridBasicInfoScroll.IsVisible = false;
                GridApplicableStatusScroll.IsVisible = false;
                SubCategoryList.IsVisible = false;
                SubCategoryInfo.IsVisible = false;
                Formstatus.IsVisible = false;
                LblFormstatus.IsVisible = false;
                CategoryList.IsVisible = false;
                CategoryFrame.IsVisible = true;


                var selecteditem = (Grid)sender;
                var item = selecteditem.BindingContext;
                var infoitem = item as Form_category_Info;

                description = infoitem.description;
                Name = infoitem.name;
                categoryId = infoitem.id;
                //Working
                // GridCategoryInfo.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });


                //if (Name != null)
                //{
                //    //Label deslabel = new Label
                //    //{
                //    //    StyleId = "Label",
                //    //    FontAttributes = FontAttributes.Bold,
                //    //    Text = Name,
                //    //    TextColor = Color.Blue,
                //    //    VerticalTextAlignment = TextAlignment.Center,
                //    //    HorizontalTextAlignment = TextAlignment.Center,
                //    //    HorizontalOptions = LayoutOptions.FillAndExpand,
                //    //    FontSize = 14,
                //    //};

                //    //GridCategoryInfo.Children.Add(deslabel, 0, categoryRowCounter);
                //    //categoryRowCounter++;
                //    //CategoryFrame.Content = GridCategoryInfo;


                //    GridCategoryInfo.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                //    Grid _sg = new Grid();
                //    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                //    _sg.RowSpacing = 0;

                //    var _bggrid = GridCategoryInfo.Children.ToList();
                //    int bggridchild = GridCategoryInfo.Children.Count;

                //    var bggridrow = 0;
                //    var smgridrow = 0;
                //    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //    {
                //        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                //        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                //    }

                //    int smgridchild = _sg.Children.Count;
                //    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                //    {
                //        var column = Grid.GetColumn(_sg.Children[childIndex]);
                //        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                //    }

                //    Label deslabel = new Label
                //    {
                //        StyleId = "Label",
                //        FontAttributes = FontAttributes.Bold,
                //        Text = Name,
                //        TextColor = Color.Blue,
                //        VerticalTextAlignment = TextAlignment.Center,
                //        HorizontalTextAlignment = TextAlignment.Center,
                //        HorizontalOptions = LayoutOptions.FillAndExpand,
                //        FontSize = 14,
                //    };
                //    _sg.Children.Add(deslabel, 0, smgridrow);
                //    GridCategoryInfo.Children.Add(deslabel, 0, bggridrow);
                //    CategoryFrame.Content = GridCategoryInfo;

                //}

                if (Name != null)
                {
                    Header.Text = Name;
                }

                if (!string.IsNullOrEmpty(description))
                {
                    Header.IsVisible = true;
                    Headergrid.IsVisible = true;
                    Description.IsVisible = true;
                    Description.Text = description;
                    //Label deslabel = new Label
                    //{
                    //    StyleId = "Label",
                    //    FontAttributes = FontAttributes.None,
                    //    Text = "Description",
                    //    TextColor = Color.Gray,
                    //    VerticalTextAlignment = TextAlignment.Center,
                    //    HorizontalTextAlignment = TextAlignment.Start,
                    //    HorizontalOptions = LayoutOptions.FillAndExpand,
                    //    FontSize = 13,
                    //};

                    //Entry entry = new Entry
                    //{
                    //    StyleId = "Entry",
                    //    FontAttributes = FontAttributes.None,
                    //    TextColor = Color.Blue,
                    //    Text = description,
                    //    IsEnabled = false,
                    //    HorizontalOptions = LayoutOptions.FillAndExpand,
                    //    FontSize = 13,
                    //};

                    //GridCategoryInfo.Children.Add(deslabel, 0, categoryRowCounter);
                    //categoryRowCounter++;
                    //GridCategoryInfo.Children.Add(entry, 0, categoryRowCounter);
                    //CategoryFrame.Content = GridCategoryInfo;
                    //categoryRowCounter++;

                    //Description label change
                    //GridCategoryInfo.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                    //Grid _sg = new Grid();
                    //_sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    //_sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    //_sg.RowSpacing = 0;

                    //var _bggrid = GridCategoryInfo.Children.ToList();
                    //int bggridchild = GridCategoryInfo.Children.Count;

                    //var bggridrow = 0;
                    //var smgridrow = 0;
                    //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    //{
                    //    var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                    //    bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    //}

                    //int smgridchild = _sg.Children.Count;
                    //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    //{
                    //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                    //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    //}

                    //Label deslabel = new Label
                    //{
                    //    StyleId = "Label",
                    //    FontAttributes = FontAttributes.None,
                    //    Text = "Description",
                    //    TextColor = Color.Gray,
                    //    VerticalTextAlignment = TextAlignment.Center,
                    //    HorizontalTextAlignment = TextAlignment.Start,
                    //    HorizontalOptions = LayoutOptions.FillAndExpand,
                    //    FontSize = 13,
                    //};

                    //Entry entry = new Entry
                    //{
                    //    StyleId = "Entry",
                    //    FontAttributes = FontAttributes.None,
                    //    TextColor = Color.Blue,
                    //    Text = description,
                    //    IsEnabled = false,
                    //    HorizontalOptions = LayoutOptions.FillAndExpand,
                    //    FontSize = 13,
                    //};
                    //_sg.Children.Add(deslabel, 0, smgridrow);
                    //_sg.Children.Add(entry, 0, smgridrow + 1);
                    //GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    //CategoryFrame.Content = GridCategoryInfo;
                    //End

                    //Start Description



                    //GridCategoryInfo.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                    //Grid _sg = new Grid();
                    //_sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    //_sg.RowSpacing = 0;

                    //var _bggrid = GridCategoryInfo.Children.ToList();
                    //int bggridchild = GridCategoryInfo.Children.Count;

                    //var bggridrow = 0;
                    //var smgridrow = 0;
                    //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    //{
                    //    var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                    //    bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    //}

                    //int smgridchild = _sg.Children.Count;
                    //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    //{
                    //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                    //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    //}

                    //Label deslabel = new Label
                    //{
                    //    StyleId = "Label",
                    //    FontAttributes = FontAttributes.None,
                    //    Text = description,
                    //    TextColor = Color.Gray,
                    //    IsEnabled = false,
                    //    VerticalTextAlignment = TextAlignment.Center,
                    //    HorizontalTextAlignment = TextAlignment.Start,
                    //    HorizontalOptions = LayoutOptions.FillAndExpand,
                    //    FontSize = 13,
                    //};
                    //_sg.Children.Add(deslabel, 0, smgridrow);
                    //GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    //CategoryFrame.Content = GridCategoryInfo;


                    //End

                }
                else
                {
                    Header.IsVisible = true;
                    Headergrid.IsVisible = true;
                }


                int count = 0;
                if (infoitem.sub_category != null)
                {
                    SubCategoryScroll.IsVisible = false;
                    SubCategoryList.IsVisible = true;
                    SubCategoryInfo.IsVisible = true;

                    if (infoitem.sub_category.Count > 0)
                        SubCategoryInfo.ItemsSource = infoitem.sub_category;



                }
                else if (infoitem.sub_category == null)
                {
                    GridCategoryInfo.IsVisible = true;
                    SubCategoryList.IsVisible = false;
                    SubCategoryInfo.IsVisible = false;
                    SubCategoryScroll.IsVisible = true;
                    btnApprovalSave.IsVisible = true;
                    btnApprovalSave.StyleId = "CategoryFields_Save";
                    CategoryFrame.IsVisible = true;
                    btnApprovalSave.IsEnabled = true;
                    category_Info_Fields = infoitem.fields;

                    if (infoitem.fields != null)
                    {
                        //Valli ji 
                        if (formtypelistmodel.response.form_name == "SAMPLE FORM FOR MONTHLY UNDERGROUND STORAGE SYSTEM INSPECTION CHECKLIST")
                        {
                            foreach (var data in infoitem.fields)
                            {
                                if (count <= 1)
                                {
                                    categoryinfodetails(GridCategoryInfo, data);
                                    Application.Current.Properties.Remove("SignImageCat");
                                    Application.Current.Properties.Add("SignImageCat", infoitem.fields);
                                    count++;
                                }
                            }

                            if (count >= 2)
                            {
                                for (int i = 2; i < infoitem.fields.Count; i++)
                                {
                                    TankDetails(GridApplicableStatus, infoitem.fields[i]);
                                }
                            }


                            //foreach (var data in infoitem.fields)
                            //{
                            //    categoryinfodetails(GridCategoryInfo, data);
                            //    count++;
                            //}
                            //for (int i = 0; i < infoitem.fields.Count; i++)
                            //{
                            //     TankDetails(GridApplicableStatus, infoitem.fields[i]);
                            //}

                        }
                        else
                        {
                            foreach (var data in infoitem.fields)
                            {
                                categoryinfodetails(GridCategoryInfo, data);
                                Application.Current.Properties.Remove("SignImageCat");
                                Application.Current.Properties.Add("SignImageCat", infoitem.fields);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
        //imagechange //headre

        
        public async void buttonSubSearch_Tapped(object sender, EventArgs args)
        {
            try
            {
                // var entity = ((Grid)sender);
                //  entity.BackgroundColor = Color.Gray;

                Application.Current.Properties.Remove("Buttonsubsearch");
                Application.Current.Properties.Add("Buttonsubsearch", sender);

                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                var text = "Category Sub List";
                
                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", text);


                categoryRowCounter = 0;
                description = string.Empty;
                Name = string.Empty;
                GridCategoryInfo.Children.Clear();
                categoryRowCounter = 0;
                GridCategoryInfo.IsVisible = true;
                SubCategoryScroll.IsVisible = true;
                SubCategoryList.IsVisible = false;
                CategoryFrame.IsVisible = true;
                CategoryList.IsVisible = false;
                GridBasicInfoScroll.IsVisible = false;
                GridApplicableStatusScroll.IsVisible = false;
                Formstatus.IsVisible = false;
                LblFormstatus.IsVisible = false;

                var selecteditem = (Grid)sender;
                var item = selecteditem.BindingContext;
                var infoitem = item as Sub_category_Info;
                
                description = infoitem.description;
                Name = infoitem.name;
                SubCategoryID = infoitem.id;

                //Present Fields 
                if (infoitem.fields != null)
                {
                    sub_category_info_Add = infoitem.fields;
                    btnApprovalSave.IsVisible = true;
                    btnApprovalSave.StyleId = "SubCategoryFields_Save";
                    foreach (var data in infoitem.fields)
                    {
                        //Subsearchbind
                        Subcategoryinfodetails(GridCategoryInfo, data);
                        Application.Current.Properties.Remove("SignImageSubCatList");
                        Application.Current.Properties.Add("SignImageSubCatList", infoitem.fields);
                    }

                    Headergrid.IsVisible = true;
                    Header.Text = infoitem.name;
                    Header.IsVisible = true;
                    Description.IsVisible = true;
                    Description.Text = infoitem.description;
                }
                //Fields Empty
                else
                {
                    btnApprovalSave.IsVisible = false;
                    BasicInfoFrame.IsVisible = false;
                    CategoryFrame.IsVisible = false;
                    XFToast.LongMessage("No data Available");
                }
                if (!ShowSaveButton)
                {
                    //GridCategoryInfo.IsVisible = false;
                    ////SubCategoryScroll.IsVisible = false;
                    //btnApprovalSave.IsVisible = false;
                    //XFToast.LongMessage("No data Available");



                   // GridCategoryInfo.IsVisible = false;
                    //SubCategoryScroll.IsVisible = false;
                    //btnApprovalSave.IsVisible = false;
                   // XFToast.LongMessage("No data Available");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }



        public void TankDetails(Grid grid, category_Info_fields fields)
        {

            try
            {

                string status = Application.Current.Properties["Tank"].ToString();

                Tankdetails.IsVisible = true;
                GridApplicableStatusScroll.IsVisible = true;
                TankFrame.IsVisible = true;
                GridApplicableStatus.IsVisible = true;





                //New Added--ps


                /*  if (status == "True")
                  {
                      Tankdetails.IsVisible = true;
                      GridApplicableStatusScroll.IsVisible = true;
                      TankFrame.IsVisible = true;
                      GridApplicableStatus.IsVisible = true;

                  }
                  else
                  {
                      Tankdetails.IsVisible = false;
                      GridApplicableStatusScroll.IsVisible = false;
                      TankFrame.IsVisible = false;
                      GridApplicableStatus.IsVisible = false;
                  }*/


                //End--ps
                if (fields.type == "text" && fields.visible == "true")
                {
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        FontSize = 13,
                    };

                    Entry entry = new Entry
                    {
                        StyleId = "Entry",
                        FontAttributes = FontAttributes.None,
                        TextColor = Color.Blue,
                        Text = fields.value.ToString(),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        FontSize = 13,
                    };
                    if (fields.editable == "false")
                    {
                        entry.IsEnabled = false;
                    }

                    if (Tankcolumncounter == 2)
                    {
                        Tankrowcounter = 2;
                        Tankcolumncounter = 0;
                        GridApplicableStatus.Children.Add(label, Tankcolumncounter, Tankrowcounter);
                        Tankrowcounter++;
                        GridApplicableStatus.Children.Add(entry, Tankcolumncounter, Tankrowcounter);
                        TankFrame.Content = GridApplicableStatus;
                        Tankrowcounter--;
                        Tankcolumncounter++;
                    }
                    else
                    {
                        GridApplicableStatus.Children.Add(label, Tankcolumncounter, Tankrowcounter);
                        Tankrowcounter++;
                        GridApplicableStatus.Children.Add(entry, Tankcolumncounter, Tankrowcounter);
                        TankFrame.Content = GridApplicableStatus;
                        Tankrowcounter--;
                        Tankcolumncounter++;
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }





        }
        Image Unchecbox;
        Image Checkedchecbox;



        //catnodo
        public void categoryinfodetails(Grid grid, category_Info_fields fields)
        {
            grid.RowSpacing = 0;
            grid.ColumnSpacing = 0;
            categorygrid.IsVisible = true;
            SubCategoryScroll.IsVisible = true;
            if (formtypelistmodel.response.form_name == "SAMPLE FORM FOR MONTHLY UNDERGROUND STORAGE SYSTEM INSPECTION CHECKLIST")
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                if (fields.type == "text" && fields.visible == "true")
                {



                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13,
                    };

                    Entry entry = new Entry
                    {
                        StyleId = "Entry",
                        FontAttributes = FontAttributes.None,
                        TextColor = Color.Blue,
                        Text = fields.value.ToString(),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.Center,
                        FontSize = 13,
                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(entry, 0, smgridrow + 1);


                    if (fields.editable == "false")
                        entry.IsEnabled = false;

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;



                }
                else if (fields.type == "checkbox" && fields.visible == "true")
                {

                    //New source

                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    var reqrow = bggridrow;


                    Unchecbox = new Image
                    {
                        StyleId = "UnCheck",
                        Source = "UnChecked.png",
                        Scale = 0.5,
                        Margin = new Thickness(5, 0, 10, 0),
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,

                    };
                    Checkedchecbox = new Image
                    {
                        StyleId = "Check",
                        Source = "Checked.png",
                        Scale = 0.5,
                        Margin = new Thickness(5, 0, 10, 0),
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                        IsVisible = false

                    };
                    //needed
                    if (fields.value.ToString() == "true")
                    {
                        Checkedchecbox.IsVisible = true;
                        Unchecbox.IsVisible = false;
                        TankFrame.IsVisible = false;
                        //Tankdetails.IsVisible = false;
                        GridApplicableStatusScroll.IsVisible = false;

                        Application.Current.Properties.Remove("Tank");
                        Application.Current.Properties.Add("Tank", "false");

                    }
                    else
                    {
                        Unchecbox.IsVisible = true;
                        Checkedchecbox.IsVisible = false;
                        //Tankdetails.IsVisible = true;
                        TankFrame.IsVisible = true;
                        GridApplicableStatusScroll.IsVisible = true;

                        Application.Current.Properties.Remove("Tank");
                        Application.Current.Properties.Add("Tank", "true");

                    }
                    var GestureRecognizer = new TapGestureRecognizer();
                    GestureRecognizer.Tapped += (s, e) =>
                    {
                        Checkedchecbox.IsVisible = true;
                        Unchecbox.IsVisible = false;
                        TankFrame.IsVisible = false;
                        GridApplicableStatusScroll.IsVisible = false;


                    };
                    Unchecbox.GestureRecognizers.Add(GestureRecognizer);

                    var GestureRecognizer1 = new TapGestureRecognizer();
                    GestureRecognizer1.Tapped += (s, e) =>
                    {
                        Unchecbox.IsVisible = true;
                        Checkedchecbox.IsVisible = false;
                        TankFrame.IsVisible = true;
                        GridApplicableStatusScroll.IsVisible = true;

                    };
                    Checkedchecbox.GestureRecognizers.Add(GestureRecognizer1);

                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = "Not Applicable",
                        TextColor = Color.Blue,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Center,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        FontSize = 13,
                    };

                    //GridCategoryInfo.Children.Add(Unchecbox, 0, categoryRowCounter);
                    //GridCategoryInfo.Children.Add(Checkedchecbox, 0, categoryRowCounter);
                    //GridCategoryInfo.Children.Add(label, 0, categoryRowCounter);
                    ////GridCategoryInfo.Children.Add(label, 1, categoryRowCounter);
                    //CategoryFrame.Content = GridCategoryInfo;
                    //categoryRowCounter++;

                    _sg.Children.Add(Unchecbox, 0, smgridrow);
                    _sg.Children.Add(Checkedchecbox, 0, smgridrow);
                    _sg.Children.Add(label, 0, smgridrow);

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }
            }
            else
            {
                GridCategoryInfo.RowSpacing = 0;
                grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                if (fields.type == "text" && fields.visible == "true")
                {

                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    Entry entry = new Entry
                    {
                        StyleId = "Entry",
                        FontAttributes = FontAttributes.None,
                        TextColor = Color.Blue,
                        Text = fields.value.ToString(),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        HorizontalTextAlignment = TextAlignment.Start,
                        VerticalOptions = LayoutOptions.Center,
                        FontSize = 13

                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(entry, 0, smgridrow + 1);


                    if (fields.editable == "false")
                        entry.IsEnabled = false;

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }
                else if (fields.type == "checkbox" && fields.visible == "true")
                {

                    //New Change

                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    var reqrow = bggridrow;
                    List<string> multivalues = new List<string>();
                    string multival = fields.value.ToString();
                    if (multival.Contains("\n"))
                    {
                        multivalues = JsonConvert.DeserializeObject<List<string>>(multival);
                    }


                    if (fields.options != "" && fields.options != null)
                    {

                        string val = fields.options.ToString();
                        List<string> lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                        Image RepeatUnchecbox = new Image();
                        Label Labelname = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            HorizontalOptions = LayoutOptions.Start,
                            FontSize = 13,
                        };

                        _sg.Children.Add(Labelname, 0, smgridrow);
                        for (int i = 1; i <= lstValues.Count; i++)
                        {
                            _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                            if (multivalues != null && multivalues.Count > 0)
                            {
                                RepeatUnchecbox = new Image
                                {
                                    StyleId = multivalues.Contains(lstValues[i - 1]) ? "Check" : "UnCheck",
                                    Source = multivalues.Contains(lstValues[i - 1]) ? "Checked.png" : "UnChecked.png",
                                    Scale = 0.5,
                                    Margin = new Thickness(5, 0, 10, 0),
                                    HorizontalOptions = LayoutOptions.Start,
                                    VerticalOptions = LayoutOptions.Center,
                                    AutomationId = lstValues[i - 1],
                                };
                            }
                            else
                            {
                                RepeatUnchecbox = new Image
                                {
                                    StyleId = fields.value.ToString() == lstValues[i - 1] ? "Check" : "UnCheck",
                                    Source = fields.value.ToString() == lstValues[i - 1] ? "Checked.png" : "UnChecked.png",
                                    Scale = 0.5,
                                    Margin = new Thickness(5, 0, 10, 0),
                                    HorizontalOptions = LayoutOptions.Start,
                                    VerticalOptions = LayoutOptions.Center,
                                    AutomationId = lstValues[i - 1],
                                };
                            }


                            var Repeat_image = new TapGestureRecognizer();
                            Repeat_image.Tapped += (s, e) =>
                            {
                                var imgappr1 = (s) as Image;
                                var SingleGrid1 = GridCategoryInfo.Children.ToList();



                                if (imgappr1.StyleId == "Check")
                                {
                                    imgappr1.StyleId = "UnCheck";
                                    imgappr1.Source = "UnChecked.png";
                                }
                                else if (imgappr1.StyleId == "UnCheck")
                                {
                                    imgappr1.StyleId = "Check";
                                    imgappr1.Source = "Checked.png";
                                }
                            };
                            RepeatUnchecbox.GestureRecognizers.Add(Repeat_image);

                            Label checkvalue = new Label
                            {

                                StyleId = "Label",
                                FontAttributes = FontAttributes.None,
                                Text = lstValues[i - 1],
                                TextColor = Color.Black,
                                VerticalTextAlignment = TextAlignment.Center,
                                HorizontalTextAlignment = TextAlignment.Center,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,
                                FontSize = 13,

                            };

                            _sg.Children.Add(RepeatUnchecbox, 0, i);
                            _sg.Children.Add(checkvalue, 0, i);

                        }
                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;
                    }
                    else
                    {

                        Image RepeatUnchecbox = new Image();
                        Label Labelname = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            HorizontalOptions = LayoutOptions.Start,
                            FontSize = 13,
                        };

                        _sg.Children.Add(Labelname, 0, smgridrow);
                        var _val = "";
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        if (fields.value == "false")
                        {
                            _val = "UnCheck";
                        }

                        RepeatUnchecbox = new Image
                        {
                            StyleId = fields.value.ToString() == _val ? "Check" : "UnCheck",
                            Source = fields.value.ToString() == _val ? "Checked.png" : "UnChecked.png",
                            Scale = 0.5,
                            Margin = new Thickness(5, 0, 10, 0),
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            AutomationId = _val,
                        };
                        var Repeat_image = new TapGestureRecognizer();
                        Repeat_image.Tapped += (s, e) =>
                        {
                            var imgappr1 = (s) as Image;
                            var SingleGrid1 = GridCategoryInfo.Children.ToList();
                            if (imgappr1.StyleId == "Check")
                            {
                                imgappr1.StyleId = "UnCheck";
                                imgappr1.Source = "UnChecked.png";
                            }
                            else if (imgappr1.StyleId == "UnCheck")
                            {
                                imgappr1.StyleId = "Check";
                                imgappr1.Source = "Checked.png";
                            }
                        };
                        RepeatUnchecbox.GestureRecognizers.Add(Repeat_image);

                        _sg.Children.Add(RepeatUnchecbox, 0, smgridrow + 1);

                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;

                    }
                }
                else if (fields.type == "radio" && fields.visible == "true")
                {


                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }

                    //int _grdrwcnt = bggridrow;
                    var reqrow = bggridrow;


                    List<string> RadioValues = new List<string>();
                    string Radioval = fields.options.ToString();
                    RadioValues = JsonConvert.DeserializeObject<List<string>>(Radioval);
                    Label radioLabelname = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalOptions = LayoutOptions.Start,
                        FontSize = 13,
                    };
                    _sg.Children.Add(radioLabelname, 0, smgridrow);
                    //Grid _sg = new Grid();

                    for (int i = 1; i <= RadioValues.Count; i++)
                    {
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        Image UnCheckradiobutton = new Image
                        {
                            StyleId = fields.value.ToString() == RadioValues[i - 1] ? "Radio" : "UnRadio",
                            Source = fields.value.ToString() == RadioValues[i - 1] ? "Check_Button.png" : "Uncheck_Button.png",
                            Scale = 0.4,
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            AutomationId = RadioValues[i - 1],
                        };

                        var unradio1 = new TapGestureRecognizer();
                        unradio1.Tapped += (s, e) =>
                        {
                            var imgappr = (s) as Image;
                            var SingleImg1 = imgappr.ClassId;
                            var selectedStyleId = imgappr.StyleId;
                            var SingleGrid = GridCategoryInfo.Children.ToList();


                            foreach (var item in SingleGrid)
                            {
                                var condichkd = item.GetType();
                                var _typesrc = condichkd.FullName;
                                if (_typesrc == "Xamarin.Forms.Grid")
                                {
                                    var k = item as Grid;
                                    var _gditm = k.Children.ToList();
                                    foreach (var itm in _gditm)
                                    {
                                        if (s.GetType() == itm.GetType())
                                        {
                                            if (itm.StyleId == "UnRadio" || itm.StyleId == "Radio")
                                            {
                                                Image img1 = (Image)itm;
                                                img1.StyleId = "UnRadio";
                                                img1.Source = "Uncheck_Button.png";
                                            }
                                        }
                                    }
                                }
                                else
                                {

                                }
                            }
                            if (imgappr.StyleId == "UnRadio")
                            {
                                imgappr.StyleId = "Radio";
                                imgappr.Source = "Check_Button.png";
                            }
                        };
                        UnCheckradiobutton.GestureRecognizers.Add(unradio1);

                        Label radiovalue = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = RadioValues[i - 1],
                            TextColor = Color.Black,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Center,
                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                            FontSize = 14,

                        };

                        _sg.Children.Add(UnCheckradiobutton, 0, i);
                        _sg.Children.Add(radiovalue, 0, i);
                    }
                    //already End
                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }

                else if (fields.type == "signature" && fields.visible == "true")
                {


                    Grid _sg = new Grid();

                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                    _sg.RowSpacing = 0;
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    //{
                    //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                    //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                    //}

                    bggridrow = bggridchild;
                    int smgridchild = _sg.Children.Count;
                    //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    //{
                    //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                    //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    //}
                    smgridrow = smgridchild;

                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    ImageSource retSource = null;

                    signaturePadView  = new SignaturePadView
                    {
                        StyleId = "CategorySignature",
                        BackgroundColor = Color.White,
                        StrokeColor = Color.Black,
                        HeightRequest = 70,
                        StrokeWidth = 2,
                    };
                    // signaturePadView.ClearLabel.Margin = new Thickness(0, -14, 25, 0);

                    var forgetPassword_tap = new TapGestureRecognizer();
                    signaturePadView.ClearLabel.GestureRecognizers.Add(forgetPassword_tap);
                    //signaturePadView.StrokeCompleted += OnStrokeCompleted;
                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                    _sg.Children.Add(signaturePadView, 0, smgridrow + 1);

                    if (fields.value.ToString() != null && fields.value.ToString() != "")
                    {
                        signaturePadView.ClearLabel.IsVisible = true;

                        //SignCom
                        //Application.Current.Properties.Remove("SignImageCat");
                        //Application.Current.Properties.Add("SignImageCat", fields.value.ToString());

                        Application.Current.Properties.Remove("ImageHavingCat");
                        Application.Current.Properties.Add("ImageHavingCat", "True");
                        byte[] Base64Stream = Convert.FromBase64String(fields.value.ToString());
                        retSource = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                        Image sign = new Image
                        {
                            Source = retSource,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalOptions = LayoutOptions.Center,
                            Scale = 0.4,
                        };
                        ImageSetting(sign);
                        _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                        _sg.Children.Add(sign, 0, smgridrow + 1);

                    }
                    //forgetPassword_tap.Tapped += (s, e) =>
                    //{
                    //    Image mm;

                    //    if (fields.value.ToString() != null && fields.value.ToString() != "")
                    //    {
                    //        var p = Application.Current.Properties["Image"];
                    //        if (p != null && p != "")
                    //        {
                    //            mm = p as Image;
                    //            mm.IsVisible = false;
                    //        }
                    //    }
                    //};

                    forgetPassword_tap.Tapped += (s, e) =>
                    {
                        var button = (Label)s;
                        var item = GridCategoryInfo.Children.ToList();
                        for(int i=0; i<item.Count;i++)
                        {
                            var mk = item[i] as Grid;
                            var con = mk.Children.Contains(button);
                            if (con)
                            {
                                var kl = mk as Grid;
                                var kl1 = kl.Children.ToList();
                                foreach (var t in kl1)
                                {
                                    var _mm = t as Image;
                                    if (_mm != null)
                                    {
                                        _mm.IsVisible = false;
                                    }
                                }
                            }
                        }
                        var row = Grid.GetRow(button);
                        var gridm = button.Parent as Grid;
                        var image = gridm.Children.Where(c => Grid.GetRow(c) == row && Grid.GetColumn(c) == 1);
                    };

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }

                //Category End.
                else if (fields.type == "dropdown" && fields.visible == "true")
                {



                    string val = fields.options.ToString();
                    string CurrentData = "";

                    List<string> lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                    if (!string.IsNullOrEmpty(fields.value.ToString()))
                    {
                        CurrentData = fields.value.ToString();
                    }
                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    CustomPicker customPicker = new CustomPicker
                    {
                        StyleId = "CustomPicker",
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                        ItemsSource = lstValues,
                        SelectedItem = CurrentData,

                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(customPicker, 0, smgridrow + 1);


                    //if (fields.editable == "false")
                    //    entry.IsEnabled = false;

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }
                else if (fields.type == "textarea" && fields.visible == "true")
                {


                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    Editor Address = new Editor
                    {
                        StyleId = "Textarea",
                        FontAttributes = FontAttributes.None,
                        TextColor = Color.Blue,
                        Text = fields.value.ToString(),
                        FontSize = 13,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(Address, 0, smgridrow + 1);


                    if (fields.editable == "false")
                        Address.IsEnabled = false;

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;


                }
                else if (fields.type == "date" && fields.visible == "true")
                {


                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }

                    DateTime reqdate;
                    if (fields.value == "")
                    {
                        reqdate = DateTime.Now;
                    }
                    else
                    {

                        string formattedDates = "";
                        string t = fields.value.ToString();
                        string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "MM-dd-yyyy", "dd-MM-yyyy", "M-dd-yyyy", "M/dd/yyyy" };
                        DateTime date;
                        if (DateTime.TryParseExact(t, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                            formattedDates = date.ToString("dd/MM/yyyy");
                        if (formattedDates != null && formattedDates != "")
                        {
                            string[] _list = formattedDates.Split('/');
                            //var list = Convert.ToInt32(_list[0]) > 10 ? _list[0] : "0" + _list[0];
                            var ttt = new DateTime(Convert.ToInt32(_list[2]), Convert.ToInt32(_list[1]), Convert.ToInt32(_list[0]));
                            // var dt = DateTime.Parse(ttt.ToString(), CultureInfo.InvariantCulture, DateTimeStyles.None);
                            var dt = ttt;
                            reqdate = dt;
                        }
                        else
                        {
                            reqdate = DateTime.Now;
                        }

                    }
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13

                    };
                    DatePicker datePicker = new DatePicker
                    {
                        StyleId = "DatePicker",
                        //Format = "dd-MM-yyyy",
                        Format = "MM-dd-yyyy",
                        //Date = Convert.ToDateTime(fields.value, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat),
                        // Date = DateTime.ParseExact(fields.value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture),
                        Date = reqdate,
                        TextColor = Color.Blue,
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.FillAndExpand,

                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(datePicker, 0, smgridrow + 1);


                    //if (fields.editable == "false")
                    // Address.IsEnabled = false;

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;


                }
            }


        }

        public void Subcategoryinfodetails(Grid grid, Subcategory_Info_fields fields)
        {
            categorygrid.IsVisible = true;

            // GridLength(1, GridUnitType.Star)
            //grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            // grid.RowDefinitions.Add(new GridLength(1, GridUnitType.Star));


            if (fields.type == "text" && fields.visible == "true")
            {
                //ShowSaveButton = true;
                //Label label = new Label
                //{
                //    StyleId = "Label",
                //    ClassId = "SubCategoryLabel",
                //    FontAttributes = FontAttributes.None,
                //    Text = fields.label_name,
                //    TextColor = Color.Gray,
                //    HorizontalOptions = LayoutOptions.FillAndExpand,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    FontSize = 13,
                //};

                //Entry entry = new Entry
                //{
                //    StyleId = "Entry",
                //    ClassId = "SubCategoryEntry",
                //    FontAttributes = FontAttributes.None,
                //    TextColor = Color.Blue,
                //    Text = fields.value.ToString(),
                //    HorizontalOptions = LayoutOptions.FillAndExpand,
                //    FontSize = 14
                //};
                //if (fields.editable == "false")
                //    entry.IsEnabled = false;

                //GridCategoryInfo.Children.Add(label, 0, categoryRowCounter);
                //categoryRowCounter++;
                //GridCategoryInfo.Children.Add(entry, 0, categoryRowCounter);
                //CategoryFrame.Content = GridCategoryInfo;
                //categoryRowCounter++;


                ShowSaveButton = true;
                Grid _sg = new Grid();
                _sg.RowSpacing = 0;
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                var _bggrid = GridCategoryInfo.Children.ToList();
                int bggridchild = GridCategoryInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                {
                    var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                    bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                }

                int smgridchild = _sg.Children.Count;
                for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                {
                    var column = Grid.GetColumn(_sg.Children[childIndex]);
                    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                }
                Label label = new Label
                {
                    StyleId = "Label",
                    ClassId = "SubCategoryLabel",
                    FontAttributes = FontAttributes.None,
                    Text = fields.label_name,
                    TextColor = Color.Gray,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 13,
                };

                Entry entry = new Entry
                {
                    StyleId = "Entry",
                    ClassId = "SubCategoryEntry",
                    FontAttributes = FontAttributes.None,
                    TextColor = Color.Blue,
                    Text = fields.value.ToString(),
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    FontSize = 14
                };

                _sg.Children.Add(label, 0, smgridrow);
                _sg.Children.Add(entry, 0, smgridrow + 1);


                if (fields.editable == "false")
                    entry.IsEnabled = false;

                GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                //BasicInfoFrame.Content = GridCategoryInfo;
                CategoryFrame.Content = GridCategoryInfo;

            }
            else if (fields.type == "checkbox" && fields.visible == "true")
            {
                //List<string> multivalues = new List<string>();
                //string multival = fields.value.ToString();
                //if (multival.Contains("\n"))
                //{
                //    multivalues = JsonConvert.DeserializeObject<List<string>>(multival);
                //}

                //string val = fields.options.ToString();
                //List<string> lstValues = JsonConvert.DeserializeObject<List<string>>(val);

                //Label Labelname = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = fields.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    HorizontalOptions = LayoutOptions.Start,
                //    FontSize = 13,

                //};
                //GridCategoryInfo.Children.Add(Labelname, 0, categoryRowCounter);
                //categoryRowCounter++;

                //ShowSaveButton = true;

                //for (int i = 0; i < lstValues.Count; i++)
                //{
                //    if (multivalues != null && multivalues.Count > 0)
                //    {
                //        Unchecbox = new Image
                //        {
                //            StyleId = multivalues.Contains(lstValues[i]) ? "Check" : "UnCheck",
                //            Source = multivalues.Contains(lstValues[i]) ? "Checked.png" : "UnChecked.png",
                //            Scale = 0.5,
                //            Margin = new Thickness(5, 0, 10, 0),
                //            HorizontalOptions = LayoutOptions.Start,
                //            VerticalOptions = LayoutOptions.Center,
                //            AutomationId = lstValues[i],
                //        };
                //    }
                //    else
                //    {
                //        Unchecbox = new Image
                //        {
                //            StyleId = fields.value.ToString() == lstValues[i] ? "Check" : "UnCheck",
                //            Source = fields.value.ToString() == lstValues[i] ? "Checked.png" : "UnChecked.png",
                //            Scale = 0.5,
                //            Margin = new Thickness(5, 0, 10, 0),
                //            HorizontalOptions = LayoutOptions.Start,
                //            VerticalOptions = LayoutOptions.Center,
                //            AutomationId = lstValues[i],
                //            ClassId = "Checked",
                //        };
                //    }
                //    var Repeat_image = new TapGestureRecognizer();
                //    Repeat_image.Tapped += (s, e) =>
                //    {
                //        var imgappr = (s) as Image;
                //        var SingleGrid = GridCategoryInfo.Children.ToList();



                //        if (imgappr.StyleId == "Check")
                //        {
                //            imgappr.StyleId = "UnCheck";
                //            imgappr.Source = "UnChecked.png";
                //        }
                //        else if (imgappr.StyleId == "UnCheck")
                //        {
                //            imgappr.StyleId = "Check";
                //            imgappr.Source = "Checked.png";
                //        }
                //    };
                //    Unchecbox.GestureRecognizers.Add(Repeat_image);

                //    Label checkvalue = new Label
                //    {

                //        StyleId = "Label",
                //        FontAttributes = FontAttributes.None,
                //        Text = lstValues[i],
                //        TextColor = Color.Black,
                //        VerticalTextAlignment = TextAlignment.Center,
                //        HorizontalTextAlignment = TextAlignment.Center,
                //        HorizontalOptions = LayoutOptions.CenterAndExpand,
                //        FontSize = 13,

                //    };

                //    GridCategoryInfo.Children.Add(Unchecbox, 0, categoryRowCounter);
                //    //GridCategoryInfo.Children.Add(Checkedchecbox, 0, categoryRowCounter);
                //    GridCategoryInfo.Children.Add(checkvalue, 0, categoryRowCounter);
                //    categoryRowCounter++;
                //}



                //New Change
                Grid _sg = new Grid();
                _sg.RowSpacing = 0;
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                var _bggrid = GridCategoryInfo.Children.ToList();
                int bggridchild = GridCategoryInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                {
                    var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                    bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                }

                int smgridchild = _sg.Children.Count;
                for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                {
                    var column = Grid.GetColumn(_sg.Children[childIndex]);
                    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                }
                var reqrow = bggridrow;
                List<string> multivalues = new List<string>();
                string multival = fields.value.ToString();
                if (multival.Contains("\n"))
                {
                    multivalues = JsonConvert.DeserializeObject<List<string>>(multival);
                }

                string val = fields.options.ToString();
                List<string> lstValues = JsonConvert.DeserializeObject<List<string>>(val);

                Label Labelname = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = fields.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    HorizontalOptions = LayoutOptions.Start,
                    FontSize = 13,

                };
                _sg.Children.Add(Labelname, 0, smgridrow);

                ShowSaveButton = true;

                for (int i = 0; i <= lstValues.Count; i++)
                {
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    if (multivalues != null && multivalues.Count > 0)
                    {
                        Unchecbox = new Image
                        {
                            StyleId = multivalues.Contains(lstValues[i - 1]) ? "Check" : "UnCheck",
                            Source = multivalues.Contains(lstValues[i - 1]) ? "Checked.png" : "UnChecked.png",
                            Scale = 0.5,
                            Margin = new Thickness(5, 0, 10, 0),
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            AutomationId = lstValues[i - 1],
                        };
                    }
                    else
                    {
                        Unchecbox = new Image
                        {
                            StyleId = fields.value.ToString() == lstValues[i - 1] ? "Check" : "UnCheck",
                            Source = fields.value.ToString() == lstValues[i - 1] ? "Checked.png" : "UnChecked.png",
                            Scale = 0.5,
                            Margin = new Thickness(5, 0, 10, 0),
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            AutomationId = lstValues[i - 1],
                            ClassId = "Checked",
                        };
                    }
                    var Repeat_image = new TapGestureRecognizer();
                    Repeat_image.Tapped += (s, e) =>
                    {
                        var imgappr = (s) as Image;
                        var SingleGrid = GridCategoryInfo.Children.ToList();



                        if (imgappr.StyleId == "Check")
                        {
                            imgappr.StyleId = "UnCheck";
                            imgappr.Source = "UnChecked.png";
                        }
                        else if (imgappr.StyleId == "UnCheck")
                        {
                            imgappr.StyleId = "Check";
                            imgappr.Source = "Checked.png";
                        }
                    };
                    Unchecbox.GestureRecognizers.Add(Repeat_image);

                    Label checkvalue = new Label
                    {

                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = lstValues[i],
                        TextColor = Color.Black,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Center,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        FontSize = 13,

                    };

                    _sg.Children.Add(Unchecbox, 0, i);
                    _sg.Children.Add(checkvalue, 0, i);
                }

                GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                // BasicInfoFrame.Content = GridCategoryInfo;
                CategoryFrame.Content = GridCategoryInfo;


            }
            else if (fields.type == "radio" && fields.visible == "true")
            {
                //List<string> lstValues = new List<string>();
                //string val = fields.options.ToString();
                //lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                //Label radioLabelname = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = fields.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalOptions = LayoutOptions.Start,
                //    FontSize = 13,
                //};
                //GridCategoryInfo.Children.Add(radioLabelname, 0, categoryRowCounter);
                //categoryRowCounter++;
                //for (int i = 0; i < lstValues.Count; i++)
                //{
                //    Image UnCheckradiobutton = new Image
                //    {
                //        StyleId = fields.value.ToString() == lstValues[i] ? "Radio" : "UnRadio",
                //        Source = fields.value.ToString() == lstValues[i] ? "Check_Button.png" : "Uncheck_Button.png",
                //        Scale = 0.2,
                //        HorizontalOptions = LayoutOptions.Start,
                //        VerticalOptions = LayoutOptions.Center,
                //        AutomationId = lstValues[i],
                //        ClassId = "Checkbutton",
                //    };
                //    var unradio = new TapGestureRecognizer();
                //    unradio.Tapped += (s, e) =>
                //    {
                //        var imgappr = (s) as Image;
                //        var SingleImg1 = imgappr.ClassId;
                //        var selectedStyleId = imgappr.StyleId;
                //        var SingleGrid = GridCategoryInfo.Children.ToList();
                //        foreach (var item in SingleGrid)
                //        {
                //            if (s.GetType() == item.GetType())
                //            {
                //                if (item.StyleId == "UnRadio" || item.StyleId == "Radio")
                //                {
                //                    Image img1 = (Image)item;
                //                    img1.StyleId = "UnRadio";
                //                    img1.Source = "Uncheck_Button.png";
                //                }
                //            }
                //        }
                //        if (imgappr.StyleId == "UnRadio")
                //        {
                //            imgappr.StyleId = "Radio";
                //            imgappr.Source = "Check_Button.png";
                //        }
                //    };
                //    UnCheckradiobutton.GestureRecognizers.Add(unradio);

                //    Label radiovalue = new Label
                //    {
                //        StyleId = "Label",
                //        FontAttributes = FontAttributes.None,
                //        Text = lstValues[i],
                //        TextColor = Color.Black,
                //        VerticalTextAlignment = TextAlignment.Center,
                //        HorizontalTextAlignment = TextAlignment.Center,
                //        HorizontalOptions = LayoutOptions.CenterAndExpand,
                //        FontSize = 13,
                //    };

                //    GridCategoryInfo.Children.Add(UnCheckradiobutton, 0, categoryRowCounter);
                //    GridCategoryInfo.Children.Add(radiovalue, 0, categoryRowCounter);
                //    categoryRowCounter++;
                //}
                //CategoryFrame.Content = GridCategoryInfo;


                //New Change



                Grid _sg = new Grid();
                _sg.RowSpacing = 0;
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                var _bggrid = GridCategoryInfo.Children.ToList();
                int bggridchild = GridCategoryInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                {
                    var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                    bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                }

                int smgridchild = _sg.Children.Count;
                for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                {
                    var column = Grid.GetColumn(_sg.Children[childIndex]);
                    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                }

                //int _grdrwcnt = bggridrow;
                var reqrow = bggridrow;

                List<string> lstValues = new List<string>();
                string val = fields.options.ToString();
                lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                Label radioLabelname = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = fields.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalOptions = LayoutOptions.Start,
                    FontSize = 13,
                };
                // GridCategoryInfo.Children.Add(radioLabelname, 0, categoryRowCounter);
                //categoryRowCounter++;
                _sg.Children.Add(radioLabelname, 0, smgridrow);
                for (int i = 1; i <= lstValues.Count; i++)
                {
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    Image UnCheckradiobutton = new Image
                    {
                        StyleId = fields.value.ToString() == lstValues[i - 1] ? "Radio" : "UnRadio",
                        Source = fields.value.ToString() == lstValues[i - 1] ? "Check_Button.png" : "Uncheck_Button.png",
                        Scale = 0.2,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                        AutomationId = lstValues[i - 1],
                        ClassId = "Checkbutton",
                    };
                    var unradio = new TapGestureRecognizer();
                    unradio.Tapped += (s, e) =>
                    {
                        var imgappr = (s) as Image;
                        var SingleImg1 = imgappr.ClassId;
                        var selectedStyleId = imgappr.StyleId;
                        var SingleGrid = GridCategoryInfo.Children.ToList();
                        //foreach (var item in SingleGrid)
                        //{
                        //    if (s.GetType() == item.GetType())
                        //    {
                        //        if (item.StyleId == "UnRadio" || item.StyleId == "Radio")
                        //        {
                        //            Image img1 = (Image)item;
                        //            img1.StyleId = "UnRadio";
                        //            img1.Source = "Uncheck_Button.png";
                        //        }
                        //    }
                        //}
                        //foreach (var item in SingleGrid)
                        //{
                        //    var k = item as Grid;
                        //    var _gditm = k.Children.ToList();
                        //    foreach (var itm in _gditm)
                        //    {
                        //        if (s.GetType() == itm.GetType())
                        //        {
                        //            if (itm.StyleId == "UnRadio" || itm.StyleId == "Radio")
                        //            {
                        //                Image img1 = (Image)itm;
                        //                img1.StyleId = "UnRadio";
                        //                img1.Source = "Uncheck_Button.png";
                        //            }
                        //        }
                        //    }
                        //}

                        foreach (var item in SingleGrid)
                        {
                            var condichkd = item.GetType();
                            var _typesrc = condichkd.FullName;
                            if (_typesrc == "Xamarin.Forms.Grid")
                            {
                                var k = item as Grid;
                                var _gditm = k.Children.ToList();
                                foreach (var itm in _gditm)
                                {
                                    if (s.GetType() == itm.GetType())
                                    {
                                        if (itm.StyleId == "UnRadio" || itm.StyleId == "Radio")
                                        {
                                            Image img1 = (Image)itm;
                                            img1.StyleId = "UnRadio";
                                            img1.Source = "Uncheck_Button.png";
                                        }
                                    }
                                }
                            }
                            else
                            {

                            }
                        }
                        if (imgappr.StyleId == "UnRadio")
                        {
                            imgappr.StyleId = "Radio";
                            imgappr.Source = "Check_Button.png";
                        }
                    };
                    UnCheckradiobutton.GestureRecognizers.Add(unradio);

                    Label radiovalue = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = lstValues[i - 1],
                        TextColor = Color.Black,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Center,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        FontSize = 13,
                    };

                    _sg.Children.Add(UnCheckradiobutton, 0, i);
                    _sg.Children.Add(radiovalue, 1, i);
                }
                GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                CategoryFrame.Content = GridCategoryInfo;
                // CategoryFrame.Content = GridCategoryInfo;
            }
            else if (fields.type == "dropdown" && fields.visible == "true")
            {
                //string val = fields.options.ToString();
                //string CurrentData = "";

                //List<string> lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                //if (!string.IsNullOrEmpty(fields.value.ToString()))
                //{
                //    CurrentData = fields.value.ToString();
                //}
                //Label label = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = fields.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    FontSize = 13
                //};
                //CustomPicker customPicker = new CustomPicker
                //{
                //    StyleId = "CustomPicker",
                //    HorizontalOptions = LayoutOptions.Start,
                //    VerticalOptions = LayoutOptions.Center,
                //    ItemsSource = lstValues,
                //    SelectedItem = CurrentData,
                //    ClassId = "Picker",
                //};
                //GridCategoryInfo.Children.Add(label, 0, categoryRowCounter);
                //categoryRowCounter++;
                //GridCategoryInfo.Children.Add(customPicker, 0, categoryRowCounter);
                //categoryRowCounter++;
                //CategoryFrame.Content = GridCategoryInfo;

                List<string> lstValues = new List<string>();
                List<string> lst = new List<string>();
                if (fields.options != null)
                {
                    string val = fields.options.ToString();
                    lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                }
                else
                {
                    //lstValues = fields.value.ToString();
                }
                //string val = fields.options.ToString();
                string CurrentData = "";

                //List<string> lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                if (!string.IsNullOrEmpty(fields.value.ToString()))
                {
                    CurrentData = fields.value.ToString();
                }
                Grid _sg = new Grid();
                _sg.RowSpacing = 0;
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                var _bggrid = GridCategoryInfo.Children.ToList();
                int bggridchild = GridCategoryInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                {
                    var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                    bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                }

                int smgridchild = _sg.Children.Count;
                for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                {
                    var column = Grid.GetColumn(_sg.Children[childIndex]);
                    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                }
                Label label = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = fields.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 13
                };

                CustomPicker customPicker = new CustomPicker
                {
                    StyleId = "CustomPicker",
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.Center,
                    ItemsSource = lstValues,
                    SelectedItem = CurrentData,
                    ClassId = "Picker",
                };

                _sg.Children.Add(label, 0, smgridrow);
                _sg.Children.Add(customPicker, 0, smgridrow + 1);
                if (fields.editable == "false")
                    customPicker.IsEnabled = false;

                GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                CategoryFrame.Content = GridCategoryInfo;

            }
            else if (fields.type == "textarea" && fields.visible == "true")
            {
                //Label label = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = fields.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    FontSize = 13

                //};
                //Editor Address = new Editor
                //{
                //    StyleId = "Textarea",
                //    FontAttributes = FontAttributes.None,
                //    TextColor = Color.Blue,
                //    Text = fields.value.ToString(),
                //    FontSize = 13,
                //    HorizontalOptions = LayoutOptions.FillAndExpand,
                //    VerticalOptions = LayoutOptions.FillAndExpand,
                //    ClassId = "Areatext",

                //};

                //if (fields.editable == "false")
                //    Address.IsEnabled = false;

                //GridCategoryInfo.Children.Add(label, 0, categoryRowCounter);
                //categoryRowCounter++;
                //GridCategoryInfo.Children.Add(Address, 0, categoryRowCounter);
                //categoryRowCounter++;
                //CategoryFrame.Content = GridCategoryInfo;


                Grid _sg = new Grid();
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowSpacing = 0;
                // _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });
                var _bggrid = GridCategoryInfo.Children.ToList();
                int bggridchild = GridCategoryInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                {
                    var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                    bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                }

                int smgridchild = _sg.Children.Count;
                for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                {
                    var column = Grid.GetColumn(_sg.Children[childIndex]);
                    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                }
                Label label = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = fields.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 13

                };

                Editor Address = new Editor
                {
                    StyleId = "Textarea",
                    FontAttributes = FontAttributes.None,
                    TextColor = Color.Blue,
                    Text = fields.value.ToString(),
                    FontSize = 13,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    ClassId = "Areatext",

                };

                _sg.Children.Add(label, 0, smgridrow);
                _sg.Children.Add(Address, 0, smgridrow + 1);
                if (fields.editable == "false")
                    Address.IsEnabled = false;

                GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                CategoryFrame.Content = GridCategoryInfo;


            }
            //else if (fields.type == "date" && fields.visible == "true")
            //{
            //    //Label label = new Label
            //    //{
            //    //    StyleId = "Label",
            //    //    FontAttributes = FontAttributes.None,
            //    //    Text = fields.label_name,
            //    //    TextColor = Color.Gray,
            //    //    VerticalTextAlignment = TextAlignment.Center,
            //    //    HorizontalTextAlignment = TextAlignment.Start,
            //    //    FontSize = 13

            //    //};
            //    //DatePicker datePicker = new DatePicker
            //    //{
            //    //    StyleId = "DatePicker",
            //    //    Format = "dd-MM-yyyy",
            //    //    Date = Convert.ToDateTime(fields.value, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat),
            //    //    //Date = Convert.ToDateTime(fields.value.ToString()),
            //    //    TextColor = Color.Blue,
            //    //    VerticalOptions = LayoutOptions.Center,
            //    //    HorizontalOptions = LayoutOptions.FillAndExpand,
            //    //    ClassId = "PickerDate",

            //    //};
            //    //GridCategoryInfo.Children.Add(label, 0, categoryRowCounter);
            //    //categoryRowCounter++;
            //    //GridCategoryInfo.Children.Add(datePicker, 0, categoryRowCounter);
            //    //categoryRowCounter++;
            //    //CategoryFrame.Content = GridCategoryInfo;

            //    try
            //    {

            //        Grid _sg = new Grid();
            //        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
            //        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
            //        _sg.RowSpacing = 0;
            //        var _bggrid = GridCategoryInfo.Children.ToList();
            //        int bggridchild = GridCategoryInfo.Children.Count;

            //        var bggridrow = 0;
            //        var smgridrow = 0;
            //        for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
            //        {
            //            var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
            //            bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
            //        }

            //        int smgridchild = _sg.Children.Count;
            //        for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
            //        {
            //            var column = Grid.GetColumn(_sg.Children[childIndex]);
            //            smgridrow = Grid.GetRow(_sg.Children[childIndex]);
            //        }


            //        DateTime reqdate;
            //        if (fields.value == "")
            //        {
            //            reqdate = Convert.ToDateTime(null);
            //        }
            //        else
            //        {

            //            string formattedDates = "";
            //            string t = fields.value.ToString();
            //            string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "MM-dd-yyyy", "dd-MM-yyyy", "M-dd-yyyy", "M/dd/yyyy" };
            //            DateTime date;
            //            if (DateTime.TryParseExact(t, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            //                formattedDates = date.ToString("dd/MM/yyyy");
            //            if (formattedDates != null && formattedDates != "")
            //            {
            //                var splch = formattedDates.Substring(0,3);
            //                var splch1 = splch.Substring(2).ToCharArray();
            //                //string[] _list = formattedDates.Split('-');
            //                string[] _list = formattedDates.Split(splch1);
            //                //var list = Convert.ToInt32(_list[0]) > 10 ? _list[0] : "0" + _list[0];
            //                var ttt = new DateTime(Convert.ToInt32(_list[2]), Convert.ToInt32(_list[1]), Convert.ToInt32(_list[0]));
            //                 var dt = DateTime.Parse(ttt.ToString(), CultureInfo.InvariantCulture, DateTimeStyles.None);
            //                 reqdate = dt;
            //               // reqdate = ttt;
            //            }
            //            else
            //            {
            //                 reqdate = Convert.ToDateTime(null);
            //            }

            //        }

            //        Label label = new Label
            //        {
            //            StyleId = "Label",
            //            FontAttributes = FontAttributes.None,
            //            Text = fields.label_name,
            //            TextColor = Color.Gray,
            //            VerticalTextAlignment = TextAlignment.Center,
            //            HorizontalTextAlignment = TextAlignment.Start,
            //            FontSize = 13
            //        };
            //        //MyDatePicker datePicker;
            //        DatePicker datePicker;
            //         var tchk = fields.value;
            //        //if (tchk != "" && tchk != null)
            //        //{

            //        //    datePicker = new DatePicker
            //        //    {
            //        //        StyleId = "DatePicker",
            //        //        // Format = "dd-MM-yyyy",
            //        //        Format = "MM-dd-yyyy",
            //        //        //Date = Convert.ToDateTime(fields.value, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat),
            //        //        //Date = DateTime.ParseExact(fields.value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture),
            //        //        Date = reqdate,
            //        //        TextColor = Color.Blue,
            //        //        VerticalOptions = LayoutOptions.Center,
            //        //        HorizontalOptions = LayoutOptions.FillAndExpand,
            //        //        ClassId = "PickerDate",
            //        //    };
            //        //}
            //        //else
            //        //{
            //        //    datePicker = new DatePicker
            //        //    {
            //        //        StyleId = "DatePicker",
            //        //        TextColor = Color.Blue,
            //        //        VerticalOptions = LayoutOptions.Center,
            //        //        HorizontalOptions = LayoutOptions.FillAndExpand,
            //        //        ClassId = "PickerDate",
            //        //    };

            //        //}


            //        if (tchk != "" && tchk != null)
            //        {

            //            datePicker = new DatePicker
            //            {
            //                StyleId = "DatePicker",
            //                Format = "MM-dd-yyyy",
            //                Date = reqdate,
            //                TextColor = Color.Blue,
            //                VerticalOptions = LayoutOptions.Center,
            //                HorizontalOptions = LayoutOptions.FillAndExpand,
            //                ClassId = "PickerDate",
            //            };
            //        }
            //        else
            //        {
            //            datePicker = new DatePicker
            //            {
            //                StyleId = "DatePicker",
            //                Date = reqdate,
            //                TextColor = Color.Blue,
            //                VerticalOptions = LayoutOptions.Center,
            //                HorizontalOptions = LayoutOptions.FillAndExpand,
            //                ClassId = "PickerDate",
            //            };

            //            //Entry entryp = new Entry
            //            //{
            //            //    StyleId = "Entry",
            //            //    ClassId = "SubCategoryEntry",
            //            //    FontAttributes = FontAttributes.None,
            //            //    TextColor = Color.Blue,
            //            //    Text = fields.value.ToString(),
            //            //    HorizontalOptions = LayoutOptions.FillAndExpand,
            //            //    FontSize = 14
            //            //};
            //            //entryp.Focused += Entryp_Focused;
            //        }
            //        if (tchk == "")
            //        {
            //            //MyDatePicker d = new MyPickerForms();
            //            //var tt = Convert.ToDateTime("00/00/0000");
            //            //datePicker.Date = tt;
            //        }


            //        _sg.Children.Add(label, 0, smgridrow);
            //        _sg.Children.Add(datePicker, 0, smgridrow + 1);
            //        if (fields.editable == "false")
            //            datePicker.IsEnabled = false;

            //        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
            //        CategoryFrame.Content = GridCategoryInfo;
            //    }
            //    catch(Exception ex)
            //    {
            //        UserDialogs.Instance.HideLoading();
            //        ex.Message.ToString();
            //    }
            //}
            //Tried Source
            //Subbinding
            else if (fields.type == "date" && fields.visible == "true")
            {
                try
                {
                    Grid _sg = new Grid();
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowSpacing = 0;
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }


                    //DateTime reqdate;
                    string reqdate;
                    if (fields.value == "")
                    {
                        reqdate = null;
                    }
                    else
                    {

                        if(fields.value!=null && fields.value != "")
                        {
                            reqdate = fields.value;
                        }
                        else
                        {
                            reqdate = null;
                        }

                        //klo
                        //string formattedDates = "";
                        //var splcha = fields.value.Substring(0, 3);
                        //var splchb = splcha.Substring(2).ToCharArray();

                        //string[] _list1;
                        //_list1 = fields.value.Split(splchb);
                        //var ttt1 = new DateTime(Convert.ToInt32(_list1[2]), Convert.ToInt32(_list1[0]), Convert.ToInt32(_list1[1]));
                        //string mm = ttt1.ToString("dd/MM/yyyy");
                        //string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "MM-dd-yyyy", "dd-MM-yyyy", "M-dd-yyyy", "M/dd/yyyy" };
                        //DateTime date;
                        //if (DateTime.TryParseExact(mm, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        //    formattedDates = date.ToString("dd/MM/yyyy");
                        //if (formattedDates != null && formattedDates != "")
                        //{
                        //    var splch = formattedDates.Substring(0, 3);
                        //    var splch1 = splch.Substring(2).ToCharArray();
                        //    string[] _list = formattedDates.Split(splch1);
                        //    var ttt = new DateTime(Convert.ToInt32(_list[2]), Convert.ToInt32(_list[1]), Convert.ToInt32(_list[0]));
                        //    var dt = ttt.ToString("MM-dd-yyyy");
                        //    reqdate = dt;
                        //}
                        //else
                        //{
                        //    reqdate = null;
                        //}

                    }

                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    // DatePicker datePicker;
                    //Entry datePicker;
					Label datePicker;
                    var tchk = fields.value;

                    //if (Device.RuntimePlatform.ToLower() != "ios")
                    //{
                    //    string[] _yrs1;
                    //    var estdate = "";
                    //    _yrs1 = fields.value.Split(' ');
                    //    if (_yrs1.Count() > 1)
                    //    {

                    //    }
                    //}
                    //if (tchk != "" && tchk != null)
                    //{

                    //    datePicker = new DatePicker
                    //    {
                    //        StyleId = "DatePicker",
                    //        // Format = "dd-MM-yyyy",
                    //        Format = "MM-dd-yyyy",
                    //        //Date = Convert.ToDateTime(fields.value, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat),
                    //        //Date = DateTime.ParseExact(fields.value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture),
                    //        Date = reqdate,
                    //        TextColor = Color.Blue,
                    //        VerticalOptions = LayoutOptions.Center,
                    //        HorizontalOptions = LayoutOptions.FillAndExpand,
                    //        ClassId = "PickerDate",
                    //    };
                    //}
                    //else
                    //{
                    //    datePicker = new DatePicker
                    //    {
                    //        StyleId = "DatePicker",
                    //        TextColor = Color.Blue,
                    //        VerticalOptions = LayoutOptions.Center,
                    //        HorizontalOptions = LayoutOptions.FillAndExpand,
                    //        ClassId = "PickerDate",
                    //    };

                    //}
                    StackLayout _lyt = new StackLayout();

                    if (tchk != "" && tchk != null)
                    {

                        //datePicker = new Entry
                        //{
                        //    StyleId = "DatePicker",
                        //    Text = reqdate.ToString(),
                        //    TextColor = Color.Blue,
                        //    VerticalOptions = LayoutOptions.Center,
                        //    HorizontalOptions = LayoutOptions.FillAndExpand,
                        //    ClassId = "PickerDate",
                        //};
						datePicker = new Label
                        {
                            StyleId = "DatePicker",
                            Text = reqdate.ToString(),
                            TextColor = Color.Blue,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            ClassId = "PickerDate",
                        };
                    }
                    else
                    {
                        //datePicker = new Entry
                        //{
                        //    StyleId = "DatePicker",
                        //    Text = "",
                        //    TextColor = Color.Blue,
                        //    VerticalOptions = LayoutOptions.Center,
                        //    HorizontalOptions = LayoutOptions.FillAndExpand,
                        //    ClassId = "PickerDate",

                        //};
						datePicker = new Label
                        {
                            StyleId = "DatePicker",
                            Text = "Select Date",
                            TextColor = Color.White,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            ClassId = "PickerDate",
                            
                        };

                    }
					BoxView bv = new BoxView
					{
						HeightRequest = 1,
						Margin = new Thickness(0,0,5,0),
						Color = Color.LightGray,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						VerticalOptions = LayoutOptions.Center,
					};


					var forgetPassword_tap = new TapGestureRecognizer();
                    forgetPassword_tap.Tapped += (s, e) =>
                    {
						//var entryfield = s as Entry;
						var entryfield = s as Label;
                        EntrySaveMthd(entryfield);
						Device.BeginInvokeOnMainThread(() => {
							StartDatePickerShow.Focus();
                        });
                        
                        StartDatePickerShow.Format="MM-dd-yyyy";
						StartDatePickerShow.DateSelected += DatePicker_DateSelected;
                    };
					datePicker.GestureRecognizers.Add(forgetPassword_tap);

					//_lyt.Children.Add(datePicker);
					// var tapEntryRecognizer = new TapGestureRecognizer();
					//_lyt.GestureRecognizers.Add(tapEntryRecognizer);

                    //StackLayout _stl = new StackLayout
                    //{
                    //Children =
                    //  {
                    //        new Entry
                    //       {
                    //        StyleId = "DatePicker",
                    //        Text = reqdate.ToString(),
                    //        TextColor = Color.Blue,
                    //        VerticalOptions = LayoutOptions.Center,
                    //        HorizontalOptions = LayoutOptions.FillAndExpand,
                    //        ClassId = "PickerDate",

                    //       }
                    //   }
                    //};

                    //var tapEntryRecognizer = new TapGestureRecognizer();
                    //_stl.GestureRecognizers.Add(tapEntryRecognizer);

                    //tapEntryRecognizer.Tapped += (s, e) =>
                    //{
                    //};

                    // var tapEntryRecognizer = new TapGestureRecognizer();
                    // datePicker.GestureRecognizers.Add(tapEntryRecognizer);
                   // datePicker.TextChanged += DatePicker_TextChanged;
                    //datePicker.Completed += DatePicker_Completed;


      //              datePicker.Focused += async (s, e) => 
      //              {
      //                  var entryfield = s as Entry;
      //                  EntrySaveMthd(entryfield);
						//StartDatePickerShow.Focus();
						//StartDatePickerShow.Format="MM-dd-yyyy";
      
                    //};

                    
                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(datePicker, 0, smgridrow + 1);
					_sg.Children.Add(bv, 0, smgridrow + 2);
                    if (fields.editable == "false")
                        datePicker.IsEnabled = false;
						//_lyt.IsEnabled = false;
                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }
                catch (Exception ex)
                {
                    UserDialogs.Instance.HideLoading();
                    ex.Message.ToString();
                }
            }


            //else if (fields.type == "signature" && fields.visible == "true")
            //{
            //    try
            //    {
            //        Grid _sg = new Grid();
            //        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
            //        _sg.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

            //        _sg.RowSpacing = 0;
            //        var _bggrid = GridCategoryInfo.Children.ToList();
            //        int bggridchild = GridCategoryInfo.Children.Count;

            //        var bggridrow = 0;
            //        var smgridrow = 0;

            //        bggridrow = bggridchild;
            //        int smgridchild = _sg.Children.Count;

            //        smgridrow = smgridchild;

            //        Label label = new Label
            //        {
            //            StyleId = "Label",
            //            FontAttributes = FontAttributes.None,
            //            Text = fields.label_name,
            //            TextColor = Color.Gray,
            //            VerticalTextAlignment = TextAlignment.Center,
            //            HorizontalTextAlignment = TextAlignment.Start,
            //            FontSize = 13
            //        };

            //        ImageSource retSource = null;

            //        signaturePadView = new SignaturePadView
            //        {
            //            StyleId = "CategorySignature",
            //            BackgroundColor = Color.White,
            //            StrokeColor = Color.Black,
            //            HeightRequest = 70,
            //            StrokeWidth = 2,
            //        };
            //        var forgetPassword_tap = new TapGestureRecognizer();
            //        signaturePadView.ClearLabel.GestureRecognizers.Add(forgetPassword_tap);
            //        _sg.Children.Add(label, 0, smgridrow);
            //        _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
            //        _sg.Children.Add(signaturePadView, 0, smgridrow + 1);

            //        if (fields.value.ToString() != null && fields.value.ToString() != "")
            //        {
            //            signaturePadView.ClearLabel.IsVisible = true;
            //            Application.Current.Properties.Remove("SignImageCatSub");
            //            Application.Current.Properties.Add("SignImageCatSub", fields.value.ToString());

            //            Application.Current.Properties.Remove("ImageHavingCat");
            //            Application.Current.Properties.Add("ImageHavingCat", "True");
            //            byte[] Base64Stream = Convert.FromBase64String(fields.value.ToString());
            //            retSource = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
            //            Image sign = new Image
            //            {
            //                Source = retSource,
            //                HorizontalOptions = LayoutOptions.FillAndExpand,
            //                VerticalOptions = LayoutOptions.Center,
            //                Scale = 0.4,
            //            };
            //            ImageSetting(sign);
            //            _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
            //            _sg.Children.Add(sign, 0, smgridrow + 1);

            //        }

            //        forgetPassword_tap.Tapped += (s, e) =>
            //        {
            //            var button = (Label)s;
            //            var item = GridCategoryInfo.Children.ToList();
            //            for (int i = 0; i < item.Count; i++)
            //            {
            //                var mk = item[i] as Grid;
            //                var con = mk.Children.Contains(button);
            //                if (con)
            //                {
            //                    var kl = mk as Grid;
            //                    var kl1 = kl.Children.ToList();
            //                    foreach (var t in kl1)
            //                    {
            //                        var _mm = t as Image;
            //                        if (_mm != null)
            //                        {
            //                            _mm.IsVisible = false;
            //                        }
            //                    }
            //                }
            //            }
            //            var row = Grid.GetRow(button);
            //            var gridm = button.Parent as Grid;
            //            var image = gridm.Children.Where(c => Grid.GetRow(c) == row && Grid.GetColumn(c) == 1);
            //        };

            //        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
            //        CategoryFrame.Content = GridCategoryInfo;
            //    }
            //    catch (Exception ex)
            //    {
            //        UserDialogs.Instance.HideLoading();
            //        ex.Message.ToString();
            //    }
            //}
            else if (fields.type == "signature" && fields.visible == "true")
            {
                try
                {

                Grid _sg = new Grid();

                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                _sg.RowSpacing = 0;
                var _bggrid = GridCategoryInfo.Children.ToList();
                int bggridchild = GridCategoryInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                //}

                bggridrow = bggridchild;
                int smgridchild = _sg.Children.Count;
                //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                //}
                smgridrow = smgridchild;

                Label label = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = fields.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 13
                };

                ImageSource retSource = null;

                signaturePadView = new SignaturePadView
                {
                    StyleId = "CategorySignature",
                    BackgroundColor = Color.White,
                    StrokeColor = Color.Black,
                    HeightRequest = 70,
                    StrokeWidth = 2,
                };
                // signaturePadView.ClearLabel.Margin = new Thickness(0, -14, 25, 0);

                var forgetPassword_tap = new TapGestureRecognizer();
                signaturePadView.ClearLabel.GestureRecognizers.Add(forgetPassword_tap);
                //signaturePadView.StrokeCompleted += OnStrokeCompleted;
                _sg.Children.Add(label, 0, smgridrow);
                _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                _sg.Children.Add(signaturePadView, 0, smgridrow + 1);

                if (fields.value.ToString() != null && fields.value.ToString() != "")
                {
                    signaturePadView.ClearLabel.IsVisible = true;

                        Application.Current.Properties.Remove("SignImageCatSub");
                        Application.Current.Properties.Add("SignImageCatSub", fields.value.ToString());

                    Application.Current.Properties.Remove("ImageHavingCat");
                    Application.Current.Properties.Add("ImageHavingCat", "True");
                    byte[] Base64Stream = Convert.FromBase64String(fields.value.ToString());
                    retSource = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                    Image sign = new Image
                    {
                        Source = retSource,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.Center,
                        Scale = 0.4,
                    };
                    ImageSetting(sign);
                    _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                    _sg.Children.Add(sign, 0, smgridrow + 1);

                }
                //forgetPassword_tap.Tapped += (s, e) =>
                //{
                //    Image mm;

                //    if (fields.value.ToString() != null && fields.value.ToString() != "")
                //    {
                //        var p = Application.Current.Properties["Image"];
                //        if (p != null && p != "")
                //        {
                //            mm = p as Image;
                //            mm.IsVisible = false;
                //        }
                //    }
                //};

                forgetPassword_tap.Tapped += (s, e) =>
                {
                    var button = (Label)s;
                    var item = GridCategoryInfo.Children.ToList();
                    for (int i = 0; i < item.Count; i++)
                    {
                        var mk = item[i] as Grid;
                        var con = mk.Children.Contains(button);
                        if (con)
                        {
                            var kl = mk as Grid;
                            var kl1 = kl.Children.ToList();
                            foreach (var t in kl1)
                            {
                                var _mm = t as Image;
                                if (_mm != null)
                                {
                                    _mm.IsVisible = false;
                                }
                            }
                        }
                    }
                    var row = Grid.GetRow(button);
                    var gridm = button.Parent as Grid;
                    var image = gridm.Children.Where(c => Grid.GetRow(c) == row && Grid.GetColumn(c) == 1);
                };

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }
                catch (Exception ex)
                {
                    UserDialogs.Instance.HideLoading();
                    ex.Message.ToString();
                }
            }

        }
	/*	void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
			if (dateselectcheck_ == false)
			{
				if (!StartDatePickerShow.IsFocused)
				{
					//var tdate = StartDatePickerShow.Date;
					var _startDate = sender as DatePicker;
					//var dateselectedm = _startDate.Date.ToString("MM-dd-yyyy");
					DateTime today = DateTime.Today; // As DateTime
					string s_today = today.ToString("MM/dd/yyyy");
					var tc = Application.Current.Properties["EntrySubDate"];
					Label ptk = tc as Label;
					ptk.TextColor = Color.Blue;
					ptk.Text = s_today;

				}
			}
        }*/
		private void DatePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
			
         
        }
        
        //public void EntrySaveMthd(Entry p)
        //{

        //    Application.Current.Properties.Remove("EntrySubDate");
        //    Application.Current.Properties.Add("EntrySubDate", p);

        //}
        void Handle_Selected()
        {

        }

        public void EntrySaveMthd(Label p)
        {
            Application.Current.Properties.Remove("EntrySubDate");
            Application.Current.Properties.Add("EntrySubDate", p);
        }


   //     private void DatePicker_TextChanged(object sender, TextChangedEventArgs e)
   //     {
   //         //throw new NotImplementedException();
   //         var entryfield = sender as Entry;
   //         EntrySaveMthd(entryfield);
			//StartDatePickerShow.Focus();
        //}

        //private void OnDateSelected(object sender, DateChangedEventArgs e)
        //{
        //    var _startDate = sender as DatePicker;
        //    var dateselectedm = _startDate.Date.ToString("MM-dd-yyyy");
        //    var tc= Application.Current.Properties["EntrySubDate"];
        //    Entry ptk = tc as Entry;
        //    ptk.Text = dateselectedm;
        //}
		private void OnDateSelected(object sender, DateChangedEventArgs e)
        {
            var _startDate = sender as DatePicker;
            var dateselectedm = _startDate.Date.ToString("MM-dd-yyyy");
            var tc = Application.Current.Properties["EntrySubDate"];
			Label ptk = tc as Label;
			ptk.TextColor = Color.Blue;
            ptk.Text = dateselectedm;
            dateselectcheck_ = true;
        }

        void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            if (dateselectcheck_ == false)
            {
                if (!StartDatePickerShow.IsFocused)
                {
                    //var tdate = StartDatePickerShow.Date;
                    var _startDate = sender as DatePicker;
                    //var dateselectedm = _startDate.Date.ToString("MM-dd-yyyy");
                    DateTime today = DateTime.Today; // As DateTime
                    string s_today = today.ToString("MM-dd-yyyy");
                    var tc = Application.Current.Properties["EntrySubDate"];
                    Label ptk = tc as Label;
                    ptk.TextColor = Color.Blue;
                    ptk.Text = s_today;

                }
            }
        }

        private void Entryp_Focused(object sender, FocusEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void BtnBasicInfo_Clicked(object sender, EventArgs e)
        {

            var showdata = Application.Current.Properties["saveStatus"].ToString();
            Headergrid.IsVisible = false;
            CommentCatdet.IsVisible = false;
            
            if (showdata == "SavedValue")
            {

                CmdButton.IsVisible = true;
                Formstatus.IsVisible = true;
                LblFormstatus.IsVisible = true;
                btnApprovalSave.IsEnabled = true;

                GridBasicInfoScroll.IsVisible = true;
                CategoryList.IsVisible = false;
                CategoryInfo.IsVisible = false;

                btnApprovalSave.IsVisible = true;
                GridBasicInfo.IsVisible = true;
                BasicInfoFrame.IsVisible = true;
            }



            Application.Current.Properties.Remove("RedirectStatus");
            Application.Current.Properties.Add("RedirectStatus", "");

            Application.Current.Properties.Remove("RedirectStatus");
            Application.Current.Properties.Add("RedirectStatus", "");


            gridRowCounter = 0;
            SubCategoryList.IsVisible = false;
            GridBasicInfo.Children.Clear();
            //BasicInfoFrame.Content = null;
            btnCategoryDetails.BackgroundColor = Color.FromHex("#FFFFFF");
            btnCategoryDetails.TextColor = Color.FromHex("#1A257F");
            //buttonn
            //Margin = "-15,10,10,8"
            btnBasicInfo.BackgroundColor = Color.FromHex("#1A257F");
            btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 8);
            btnBasicInfo.TextColor = Color.FromHex("#FFFFFF");
            GridBasicInfoScroll.IsVisible = true;
            BasicInfoFrame.IsVisible = true;
            CmdButton.IsVisible = true;
            CategoryFrame.IsVisible = false;
            btnApprovalSave.IsVisible = true;
            TankFrame.IsVisible = false;
            CategoryList.IsVisible = false;
            GridApplicableStatusScroll.IsVisible = false;
            SubCategoryScroll.IsVisible = false;
            Formstatus.IsVisible = true;
            LblFormstatus.IsVisible = true;
            btnApprovalSave.IsEnabled = true;
            if (Basic_Infos != null && Basic_Infos.Count > 0)
            {
                foreach (var item in Basic_Infos)
                {
                    BasicInfoDetails(GridBasicInfo, item);
                }
            }
            else
            {
                btnApprovalSave.IsVisible = false;
                BasicInfoFrame.IsVisible = false;
                XFToast.LongMessage("No data Available");
            }
        }

        public void BasicInfoDetails(Grid grid, Form_basic_Info form_Basic_Info)
        {
            GridBasicInfo.RowSpacing = 0;
            grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            if (form_Basic_Info.type == "text" && form_Basic_Info.visible == "true")
            {
                //Label label = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = form_Basic_Info.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    FontSize = 13

                //};

                //Entry entry = new Entry
                //{
                //    StyleId = "Entry",
                //    FontAttributes = FontAttributes.None,
                //    TextColor = Color.Blue,
                //    Text = form_Basic_Info.value.ToString(),
                //    HorizontalOptions = LayoutOptions.FillAndExpand,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    VerticalOptions = LayoutOptions.Center,
                //    FontSize = 14

                //};
                //if (form_Basic_Info.editable == "false")
                //    entry.IsEnabled = false;

                //GridBasicInfo.Children.Add(label, 0, gridRowCounter);
                //gridRowCounter++;
                //GridBasicInfo.Children.Add(entry, 0, gridRowCounter);
                //gridRowCounter++;
                //BasicInfoFrame.Content = GridBasicInfo;

                //start
                //var glist = GridBasicInfo.Children.ToList();
                //gridRowCounter = glist.Count;
                //Grid _tg = new Grid();
                //Label label = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = form_Basic_Info.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    FontSize = 13
                //};

                //Entry entry = new Entry
                //{
                //    StyleId = "Entry",
                //    FontAttributes = FontAttributes.None,
                //    TextColor = Color.Blue,
                //    Text = form_Basic_Info.value.ToString(),
                //    HorizontalOptions = LayoutOptions.FillAndExpand,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    VerticalOptions = LayoutOptions.Center,
                //    FontSize = 14
                //};
                //var rowcnt = _tg.Children.ToList();
                //var cnt = rowcnt.Count;

                //_tg.Children.Add(label, 0, cnt);
                //_tg.Children.Add(entry, 0, cnt + 1);


                //if (form_Basic_Info.editable == "false")
                //    entry.IsEnabled = false;

                //GridBasicInfo.Children.Add(_tg, 0, gridRowCounter);
                //BasicInfoFrame.Content = GridBasicInfo;

                //start=================
                Grid _sg = new Grid();
                _sg.RowSpacing = 0;
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                var _bggrid = GridBasicInfo.Children.ToList();
                int bggridchild = GridBasicInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                //}
                bggridrow = bggridchild;

                int smgridchild = _sg.Children.Count;
                //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                //}

                smgridrow = smgridchild;
                Label label = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = form_Basic_Info.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 13
                };

                Entry entry = new Entry
                {
                    StyleId = "Entry",
                    FontAttributes = FontAttributes.None,
                    TextColor = Color.Blue,
                    Text = form_Basic_Info.value.ToString(),
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    HorizontalTextAlignment = TextAlignment.Start,
                    VerticalOptions = LayoutOptions.Center,
                    FontSize = 13

                };

                _sg.Children.Add(label, 0, smgridrow);
                _sg.Children.Add(entry, 0, smgridrow + 1);


                if (form_Basic_Info.editable == "false")
                    entry.IsEnabled = false;

                GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                BasicInfoFrame.Content = GridBasicInfo;


            }
            else if (form_Basic_Info.type == "checkbox" && form_Basic_Info.visible == "true")
            {
                //List<string> multivalues = new List<string>();
                //string Checkval = form_Basic_Info.options.ToString();
                //string multival = form_Basic_Info.value.ToString();
                //if (multival.Contains("\n"))
                //{
                //    multivalues = JsonConvert.DeserializeObject<List<string>>(multival);
                //}


                //List<string> checkvalues = JsonConvert.DeserializeObject<List<string>>(Checkval);
                //Image BasicUnchecbox = new Image();

                //Label Labelname = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = form_Basic_Info.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    HorizontalOptions = LayoutOptions.Start,
                //    FontSize = 13,

                //};
                //GridBasicInfo.Children.Add(Labelname, 0, gridRowCounter);
                //gridRowCounter++;
                //for (int i = 0; i < checkvalues.Count; i++)
                //{
                //    if (multivalues != null && multivalues.Count > 0)
                //    {
                //        BasicUnchecbox = new Image
                //        {
                //            StyleId = multivalues.Contains(checkvalues[i]) ? "Check" : "UnCheck",
                //            Source = multivalues.Contains(checkvalues[i]) ? "Checked.png" : "UnChecked.png",
                //            Scale = 0.5,
                //            Margin = new Thickness(5, 0, 10, 0),
                //            HorizontalOptions = LayoutOptions.Start,
                //            VerticalOptions = LayoutOptions.Center,
                //            AutomationId = checkvalues[i],
                //        };
                //    }
                //    else
                //    {
                //        BasicUnchecbox = new Image
                //        {
                //            StyleId = form_Basic_Info.value.ToString() == checkvalues[i] ? "Check" : "UnCheck",
                //            Source = form_Basic_Info.value.ToString() == checkvalues[i] ? "Checked.png" : "UnChecked.png",
                //            Scale = 0.5,
                //            Margin = new Thickness(5, 0, 10, 0),
                //            HorizontalOptions = LayoutOptions.Start,
                //            VerticalOptions = LayoutOptions.Center,
                //            AutomationId = checkvalues[i],
                //        };
                //    }
                //    var basicimage1_tab = new TapGestureRecognizer();
                //    basicimage1_tab.Tapped += (s, e) =>
                //    {
                //        var Checkimgappr = (s) as Image;
                //        var SingleGrid = GridBasicInfo.Children.ToList();


                //        if (Checkimgappr.StyleId == "Check")
                //        {
                //            Checkimgappr.StyleId = "UnCheck";
                //            Checkimgappr.Source = "UnChecked.png";
                //        }
                //        else if (Checkimgappr.StyleId == "UnCheck")
                //        {
                //            Checkimgappr.StyleId = "Check";
                //            Checkimgappr.Source = "Checked.png";
                //        }
                //    };
                //    BasicUnchecbox.GestureRecognizers.Add(basicimage1_tab);

                //    Label checkvalue = new Label
                //    {
                //        StyleId = "Label",
                //        FontAttributes = FontAttributes.None,
                //        Text = checkvalues[i],
                //        TextColor = Color.Black,
                //        VerticalTextAlignment = TextAlignment.Center,
                //        HorizontalTextAlignment = TextAlignment.Center,
                //        HorizontalOptions = LayoutOptions.CenterAndExpand,
                //        FontSize = 14,
                //    };
                //    GridBasicInfo.Children.Add(BasicUnchecbox, 0, gridRowCounter);
                //    GridBasicInfo.Children.Add(checkvalue, 0, gridRowCounter);
                //    gridRowCounter++;
                //    BasicInfoFrame.Content = GridBasicInfo;
                //}


                Grid _sg = new Grid();
                _sg.RowSpacing = 0;
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                var _bggrid = GridBasicInfo.Children.ToList();
                int bggridchild = GridBasicInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                //}
                bggridrow = bggridchild;

                int smgridchild = _sg.Children.Count;
                //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                //}

                smgridrow = smgridchild;
                var reqrow = bggridrow;

                List<string> multivalues = new List<string>();
                string Checkval = form_Basic_Info.options.ToString();
                string multival = form_Basic_Info.value.ToString();
                if (multival.Contains("\n"))
                {
                    multivalues = JsonConvert.DeserializeObject<List<string>>(multival);
                }
                Image BasicUnchecbox = new Image();
                List<string> checkvalues = JsonConvert.DeserializeObject<List<string>>(Checkval);
                Label Labelname = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = form_Basic_Info.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    HorizontalOptions = LayoutOptions.Start,
                    FontSize = 13,

                };
                //GridBasicInfo.Children.Add(Labelname, 0, reqrow);
                // grid.Children.Add(radioLabelname, 0, reqrow);
                _sg.Children.Add(Labelname, 0, smgridrow);

                for (int i = 1; i <= checkvalues.Count; i++)
                {
                    //_sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });

                    if (multivalues != null && multivalues.Count > 0)
                    {
                        BasicUnchecbox = new Image
                        {
                            StyleId = multivalues.Contains(checkvalues[i - 1]) ? "Check" : "UnCheck",
                            Source = multivalues.Contains(checkvalues[i - 1]) ? "Checked.png" : "UnChecked.png",
                            Scale = 0.5,
                            Margin = new Thickness(5, 0, 10, 0),
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            AutomationId = checkvalues[i - 1],
                        };
                    }
                    else
                    {
                        BasicUnchecbox = new Image
                        {
                            StyleId = form_Basic_Info.value.ToString() == checkvalues[i - 1] ? "Check" : "UnCheck",
                            Source = form_Basic_Info.value.ToString() == checkvalues[i - 1] ? "Checked.png" : "UnChecked.png",
                            Scale = 0.5,
                            Margin = new Thickness(5, 0, 10, 0),
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            AutomationId = checkvalues[i - 1],
                        };
                    }
                    var basicimage1_tab = new TapGestureRecognizer();
                    basicimage1_tab.Tapped += (s, e) =>
                    {
                        var Checkimgappr = (s) as Image;
                        var SingleGrid = GridBasicInfo.Children.ToList();


                        if (Checkimgappr.StyleId == "Check")
                        {
                            Checkimgappr.StyleId = "UnCheck";
                            Checkimgappr.Source = "UnChecked.png";
                        }
                        else if (Checkimgappr.StyleId == "UnCheck")
                        {
                            Checkimgappr.StyleId = "Check";
                            Checkimgappr.Source = "Checked.png";
                        }
                    };
                    BasicUnchecbox.GestureRecognizers.Add(basicimage1_tab);

                    Label checkvalue = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = checkvalues[i - 1],
                        TextColor = Color.Black,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Center,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        FontSize = 14,
                    };


                    _sg.Children.Add(BasicUnchecbox, 0, i);
                    _sg.Children.Add(checkvalue, 0, i);
                }
                //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                //}
                grid.Children.Add(_sg, 0, bggridrow);
                BasicInfoFrame.Content = GridBasicInfo;
            }

            else if (form_Basic_Info.type == "radio" && form_Basic_Info.visible == "true")
            {
                //var test = GridBasicInfo.Children.ToList();
                //int _grdrwcnt = test.Count;
                //var reqrow = _grdrwcnt;

                //List<string> RadioValues = new List<string>();
                //string Radioval = form_Basic_Info.options.ToString();
                //RadioValues = JsonConvert.DeserializeObject<List<string>>(Radioval);
                //Label radioLabelname = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = form_Basic_Info.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalOptions = LayoutOptions.Start,
                //    FontSize = 13,
                //};
                //grid.Children.Add(radioLabelname, 0, reqrow);
                //Grid _sg = new Grid();
                //_sg.RowSpacing = 0;
                //for (int i = 0; i < RadioValues.Count; i++)
                //{
                //    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });

                //    Image UnCheckradiobutton = new Image
                //    {
                //        StyleId = form_Basic_Info.value.ToString() == RadioValues[i] ? "Radio" : "UnRadio",
                //        Source = form_Basic_Info.value.ToString() == RadioValues[i] ? "Check_Button.png" : "Uncheck_Button.png",
                //        Scale = 0.4,
                //        HorizontalOptions = LayoutOptions.Start,
                //        VerticalOptions = LayoutOptions.Center,
                //        AutomationId = RadioValues[i],
                //    };
                //    var unradio1 = new TapGestureRecognizer();
                //    unradio1.Tapped += (s, e) =>
                //    {
                //        var imgappr = (s) as Image;
                //        var SingleImg1 = imgappr.ClassId;
                //        var selectedStyleId = imgappr.StyleId;
                //        var SingleGrid = GridBasicInfo.Children.ToList();

                //        foreach (var item in SingleGrid)
                //        {
                //            if (s.GetType() == item.GetType())
                //            {
                //                if (item.StyleId == "UnRadio" || item.StyleId == "Radio")
                //                {
                //                    Image img1 = (Image)item;
                //                    img1.StyleId = "UnRadio";
                //                    img1.Source = "Uncheck_Button.png";
                //                }
                //            }
                //        }
                //        if (imgappr.StyleId == "UnRadio")
                //        {
                //            imgappr.StyleId = "Radio";
                //            imgappr.Source = "Check_Button.png";
                //        }
                //    };
                //    UnCheckradiobutton.GestureRecognizers.Add(unradio1);

                //    Label radiovalue = new Label
                //    {
                //        StyleId = "Label",
                //        FontAttributes = FontAttributes.None,
                //        Text = RadioValues[i],
                //        TextColor = Color.Black,
                //        VerticalTextAlignment = TextAlignment.Center,
                //        HorizontalTextAlignment = TextAlignment.Center,
                //        HorizontalOptions = LayoutOptions.CenterAndExpand,
                //        FontSize = 14,
                //    };
                //    _sg.Children.Add(UnCheckradiobutton, 0, i);
                //    _sg.Children.Add(radiovalue, 1, i);
                //}
                //reqrow = reqrow + 1;
                //grid.Children.Add(_sg, 0, reqrow);
                //BasicInfoFrame.Content = GridBasicInfo;

                //Working Source Radio uncheck
                //Grid _sg = new Grid();
                //_sg.RowSpacing = 0;
                //_sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });
                //var _bggrid = GridBasicInfo.Children.ToList();
                //int bggridchild = GridBasicInfo.Children.Count;

                //var bggridrow = 0;
                //var smgridrow = 0;
                //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                //}

                //int smgridchild = _sg.Children.Count;
                //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                //}
                //var reqrow = bggridrow;

                //List<string> RadioValues = new List<string>();
                //string Radioval = form_Basic_Info.options.ToString();
                //RadioValues = JsonConvert.DeserializeObject<List<string>>(Radioval);
                //Label radioLabelname = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = form_Basic_Info.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalOptions = LayoutOptions.Start,
                //    FontSize = 13,
                //};
                //_sg.Children.Add(radioLabelname, 0, smgridrow);

                //for (int i = 1; i <= RadioValues.Count; i++)
                //{
                //    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });
                //    Image UnCheckradiobutton = new Image
                //    {
                //        StyleId = form_Basic_Info.value.ToString() == RadioValues[i-1] ? "Radio" : "UnRadio",
                //        Source = form_Basic_Info.value.ToString() == RadioValues[i-1] ? "Check_Button.png" : "Uncheck_Button.png",
                //        Scale = 0.4,
                //        HorizontalOptions = LayoutOptions.Start,
                //        VerticalOptions = LayoutOptions.Center,
                //        AutomationId = RadioValues[i-1],
                //    };
                //    var unradio1 = new TapGestureRecognizer();
                //    unradio1.Tapped += (s, e) =>
                //    {
                //        var imgappr = (s) as Image;
                //        var SingleImg1 = imgappr.ClassId;
                //        var selectedStyleId = imgappr.StyleId;
                //        var SingleGrid = GridBasicInfo.Children.ToList();

                //        foreach (var item in SingleGrid)
                //        {
                //            if (s.GetType() == item.GetType())
                //            {
                //                if (item.StyleId == "UnRadio" || item.StyleId == "Radio")
                //                {
                //                    Image img1 = (Image)item;
                //                    img1.StyleId = "UnRadio";
                //                    img1.Source = "Uncheck_Button.png";
                //                }
                //            }
                //        }
                //        if (imgappr.StyleId == "UnRadio")
                //        {
                //            imgappr.StyleId = "Radio";
                //            imgappr.Source = "Check_Button.png";
                //        }
                //    };
                //    UnCheckradiobutton.GestureRecognizers.Add(unradio1);

                //    Label radiovalue = new Label
                //    {
                //        StyleId = "Label",
                //        FontAttributes = FontAttributes.None,
                //        Text = RadioValues[i-1],
                //        TextColor = Color.Black,
                //        VerticalTextAlignment = TextAlignment.Center,
                //        HorizontalTextAlignment = TextAlignment.Center,
                //        HorizontalOptions = LayoutOptions.CenterAndExpand,
                //        FontSize = 14,
                //    };
                //    _sg.Children.Add(UnCheckradiobutton, 0, i);
                //    _sg.Children.Add(radiovalue, 0, i);
                //}

                //grid.Children.Add(_sg, 0, bggridrow);
                //BasicInfoFrame.Content = GridBasicInfo;

                //End Uncheck

                Grid _sg = new Grid();
                _sg.RowSpacing = 0;
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                var _bggrid = GridBasicInfo.Children.ToList();
                int bggridchild = GridBasicInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                //}

                bggridrow = bggridchild;
                int smgridchild = _sg.Children.Count;
                //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                //}
                var reqrow = bggridrow;
                smgridrow = smgridchild;

                List<string> RadioValues = new List<string>();
                string Radioval = form_Basic_Info.options.ToString();
                RadioValues = JsonConvert.DeserializeObject<List<string>>(Radioval);
                Label radioLabelname = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = form_Basic_Info.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalOptions = LayoutOptions.Start,
                    FontSize = 13,
                };
                _sg.Children.Add(radioLabelname, 0, smgridrow);

                for (int i = 1; i <= RadioValues.Count; i++)
                {
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    Image UnCheckradiobutton = new Image
                    {
                        StyleId = form_Basic_Info.value.ToString() == RadioValues[i - 1] ? "Radio" : "UnRadio",
                        Source = form_Basic_Info.value.ToString() == RadioValues[i - 1] ? "Check_Button.png" : "Uncheck_Button.png",
                        Scale = 0.4,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                        AutomationId = RadioValues[i - 1],
                    };
                    var unradio1 = new TapGestureRecognizer();
                    unradio1.Tapped += (s, e) =>
                    {
                        var imgappr = (s) as Image;
                        var SingleImg1 = imgappr.ClassId;
                        var selectedStyleId = imgappr.StyleId;
                        var SingleGrid = GridBasicInfo.Children.ToList();

                        //foreach (var item in SingleGrid)
                        //{
                        //    if (s.GetType() == item.GetType())
                        //    {
                        //        if (item.StyleId == "UnRadio" || item.StyleId == "Radio")
                        //        {
                        //            Image img1 = (Image)item;
                        //            img1.StyleId = "UnRadio";
                        //            img1.Source = "Uncheck_Button.png";
                        //        }
                        //    }
                        //}


                        //foreach (var item in SingleGrid)
                        //{
                        //    var k = item as Grid;
                        //    var _gditm = k.Children.ToList();
                        //    foreach(var itm in _gditm)
                        //    {
                        //        if (s.GetType() == itm.GetType())
                        //        {
                        //            if (itm.StyleId == "UnRadio" || itm.StyleId == "Radio")
                        //            {
                        //                Image img1 = (Image)itm;
                        //                img1.StyleId = "UnRadio";
                        //                img1.Source = "Uncheck_Button.png";
                        //            }
                        //        }
                        //    }
                        //}
                        foreach (var item in SingleGrid)
                        {
                            var condichkd = item.GetType();
                            var _typesrc = condichkd.FullName;
                            if (_typesrc == "Xamarin.Forms.Grid")
                            {
                                var k = item as Grid;
                                var _gditm = k.Children.ToList();
                                foreach (var itm in _gditm)
                                {
                                    if (s.GetType() == itm.GetType())
                                    {
                                        if (itm.StyleId == "UnRadio" || itm.StyleId == "Radio")
                                        {
                                            Image img1 = (Image)itm;
                                            img1.StyleId = "UnRadio";
                                            img1.Source = "Uncheck_Button.png";
                                        }
                                    }
                                }
                            }
                            else
                            {

                            }
                        }

                        if (imgappr.StyleId == "UnRadio")
                        {
                            imgappr.StyleId = "Radio";
                            imgappr.Source = "Check_Button.png";
                        }
                    };
                    UnCheckradiobutton.GestureRecognizers.Add(unradio1);

                    Label radiovalue = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = RadioValues[i - 1],
                        TextColor = Color.Black,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Center,
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        FontSize = 14,
                    };
                    _sg.Children.Add(UnCheckradiobutton, 0, i);
                    _sg.Children.Add(radiovalue, 0, i);
                }

                grid.Children.Add(_sg, 0, bggridrow);
                BasicInfoFrame.Content = GridBasicInfo;

            }



            else if (form_Basic_Info.type == "dropdown" && form_Basic_Info.visible == "true")
            {
                //string dropval = form_Basic_Info.options.ToString();
                //string CurrentData = "";

                //List<string> DropValues = JsonConvert.DeserializeObject<List<string>>(dropval);
                //if (!string.IsNullOrEmpty(form_Basic_Info.value.ToString()))
                //{
                //    CurrentData = form_Basic_Info.value.ToString();
                //}


                //Label label = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = form_Basic_Info.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    FontSize = 13
                //};

                //CustomPicker customPicker = new CustomPicker
                //{
                //    StyleId = "CustomPicker",
                //    HorizontalOptions = LayoutOptions.Start,
                //    VerticalOptions = LayoutOptions.Center,
                //    ItemsSource = DropValues,
                //    SelectedItem = CurrentData,

                //};

                //GridBasicInfo.Children.Add(label, 0, gridRowCounter);
                //gridRowCounter++;
                //GridBasicInfo.Children.Add(customPicker, 0, gridRowCounter);
                //gridRowCounter++;
                //BasicInfoFrame.Content = GridBasicInfo;

                string dropval = form_Basic_Info.options.ToString();
                string CurrentData = "";

                List<string> DropValues = JsonConvert.DeserializeObject<List<string>>(dropval);
                if (!string.IsNullOrEmpty(form_Basic_Info.value.ToString()))
                {
                    CurrentData = form_Basic_Info.value.ToString();
                }

                Grid _sg = new Grid();
                _sg.RowSpacing = 0;
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                var _bggrid = GridBasicInfo.Children.ToList();
                int bggridchild = GridBasicInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                //}
                bggridrow = bggridchild;

                int smgridchild = _sg.Children.Count;
                //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                //}
                smgridrow = smgridchild;
                Label label = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = form_Basic_Info.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 13
                };

                CustomPicker customPicker = new CustomPicker
                {
                    StyleId = "CustomPicker",
                    HorizontalOptions = LayoutOptions.Start,
                    VerticalOptions = LayoutOptions.Center,
                    ItemsSource = DropValues,
                    SelectedItem = CurrentData,
                };

                _sg.Children.Add(label, 0, smgridrow);
                _sg.Children.Add(customPicker, 0, smgridrow + 1);
                if (form_Basic_Info.editable == "false")
                    customPicker.IsEnabled = false;

                GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                BasicInfoFrame.Content = GridBasicInfo;


            }
            else if (form_Basic_Info.type == "textarea" && form_Basic_Info.visible == "true")
            {
                //Label label = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = form_Basic_Info.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    FontSize = 13

                //};
                //Editor Address = new Editor
                //{
                //    StyleId = "Textarea",
                //    FontAttributes = FontAttributes.None,
                //    TextColor = Color.Blue,
                //    Text = form_Basic_Info.value.ToString(),
                //    FontSize = 14,
                //    HorizontalOptions = LayoutOptions.FillAndExpand,
                //    VerticalOptions = LayoutOptions.FillAndExpand

                //};

                //if (form_Basic_Info.editable == "false")
                //    Address.IsEnabled = false;

                //GridBasicInfo.Children.Add(label, 0, gridRowCounter);
                //gridRowCounter++;
                //GridBasicInfo.Children.Add(Address, 0, gridRowCounter);
                //gridRowCounter++;
                //BasicInfoFrame.Content = GridBasicInfo;

                Grid _sg = new Grid();
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowSpacing = 0;
                // _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });
                var _bggrid = GridBasicInfo.Children.ToList();
                int bggridchild = GridBasicInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                //}
                bggridrow = bggridchild;

                int smgridchild = _sg.Children.Count;
                //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                //}
                smgridrow = smgridchild;
                Label label = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = form_Basic_Info.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 13

                };
                //Mapo
                Editor Address = new Editor
                {
                    StyleId = "Textarea",
                    FontAttributes = FontAttributes.None,
                    TextColor = Color.Blue,
                    Text = form_Basic_Info.value.ToString(),
                    FontSize = 14,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                Address.Focused += AddressOnFocused;


                _sg.Children.Add(label, 0, smgridrow);
                _sg.Children.Add(Address, 0, smgridrow + 1);
                if (form_Basic_Info.editable == "false")
                    //Address.IsEnabled = false;
                    Address.IsEnabled = true;

                GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                BasicInfoFrame.Content = GridBasicInfo;
            }
            else if (form_Basic_Info.type == "date" && form_Basic_Info.visible == "true")
            {
                //Label label = new Label
                //{
                //    StyleId = "Label",
                //    FontAttributes = FontAttributes.None,
                //    Text = form_Basic_Info.label_name,
                //    TextColor = Color.Gray,
                //    VerticalTextAlignment = TextAlignment.Center,
                //    HorizontalTextAlignment = TextAlignment.Start,
                //    FontSize = 13

                //};
                //DatePicker datePicker = new DatePicker
                //{
                //    StyleId = "DatePicker",
                //    Format = "dd/MM/yyyy",
                //    // Date = Convert.ToDateTime(form_Basic_Info.value.ToString()),
                //    Date = Convert.ToDateTime(form_Basic_Info.value, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat),
                //    TextColor = Color.Blue,
                //    VerticalOptions = LayoutOptions.Center,
                //    HorizontalOptions = LayoutOptions.FillAndExpand,
                //};
                //GridBasicInfo.Children.Add(label, 0, gridRowCounter);
                //gridRowCounter++;
                //GridBasicInfo.Children.Add(datePicker, 0, gridRowCounter);
                //gridRowCounter++;
                //BasicInfoFrame.Content = GridBasicInfo;

                Grid _sg = new Grid();
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowSpacing = 0;
                var _bggrid = GridBasicInfo.Children.ToList();
                int bggridchild = GridBasicInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                //}

                bggridrow = bggridchild;

                int smgridchild = _sg.Children.Count;
                //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                //}
                smgridrow = smgridchild;
                Label label = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = form_Basic_Info.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 13

                };
                //neew
                DateTime reqdate;
                if (form_Basic_Info.value == "")
                {
                    reqdate = DateTime.Now;
                }
                else
                {

                    string formattedDates = "";
                    string t = form_Basic_Info.value.ToString();
                    string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "MM-dd-yyyy", "dd-MM-yyyy", "M-dd-yyyy", "M/dd/yyyy" };
                    DateTime date;
                    if (DateTime.TryParseExact(t, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        formattedDates = date.ToString("dd/MM/yyyy");
                    if (formattedDates != null && formattedDates != "")
                    {
                        string[] _list = formattedDates.Split('/');
                        //var list = Convert.ToInt32(_list[0]) > 10 ? _list[0] : "0" + _list[0];
                        //var ttt = new DateTime(Convert.ToInt32(_list[2]), Convert.ToInt32(_list[1]), Convert.ToInt32(_list[0]));
                        if (DateTime.TryParseExact(t, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                            formattedDates = date.ToString("MM/dd/yyyy");
                        var culture = System.Globalization.CultureInfo.CurrentCulture;
                        var dt = DateTime.ParseExact(formattedDates, "MM/dd/yyyy", culture);
                        //var dt = DateTime.Parse(formattedDates, CultureInfo.InvariantCulture, DateTimeStyles.None);
                        reqdate = dt;
                    }
                    else
                    {
                        reqdate = DateTime.Now;
                    }
                    //reqdate = Convert.ToDateTime(form_Basic_Info.value, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat);
                    //reqdate = DateTime.ParseExact(form_Basic_Info.value.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    //reqdate = tt;
                    // reqdate = DateTime.MinValue;
                }


                DatePicker datePicker = new DatePicker
                {

                    StyleId = "DatePicker",
                    Format = "MM-dd-yyyy",
                    //Format = "dd-MM-yyyy",
                    // Date = Convert.ToDateTime(form_Basic_Info.value.ToString()),
                    //Date = Convert.ToDateTime(form_Basic_Info.value, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat),
                    //Date = form_Basic_Info.value==""?DateTime.MinValue: Convert.ToDateTime(form_Basic_Info.value, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat),
                    Date = reqdate,
                    TextColor = Color.Blue,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                };

                _sg.Children.Add(label, 0, smgridrow);
                _sg.Children.Add(datePicker, 0, smgridrow + 1);
                if (form_Basic_Info.editable == "false")
                    datePicker.IsEnabled = false;

                GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                BasicInfoFrame.Content = GridBasicInfo;
            }
            else if (form_Basic_Info.type == "signature" && form_Basic_Info.visible == "true")
            {


                Grid _sg = new Grid();

                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                _sg.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                _sg.RowSpacing = 0;
                var _bggrid = GridBasicInfo.Children.ToList();
                int bggridchild = GridBasicInfo.Children.Count;

                var bggridrow = 0;
                var smgridrow = 0;
                //for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(GridBasicInfo.Children[childIndex]);
                //    bggridrow = Grid.GetRow(GridBasicInfo.Children[childIndex]) + 1;
                //}

                bggridrow = bggridchild;
                int smgridchild = _sg.Children.Count;
                //for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                //{
                //    var column = Grid.GetColumn(_sg.Children[childIndex]);
                //    smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                //}
                smgridrow = smgridchild;

                Label label = new Label
                {
                    StyleId = "Label",
                    FontAttributes = FontAttributes.None,
                    Text = form_Basic_Info.label_name,
                    TextColor = Color.Gray,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    FontSize = 13
                };

                ImageSource retSource = null;

                signaturePadView = new SignaturePadView
                {
                    StyleId = "BasicSignature",
                    BackgroundColor = Color.White,
                    StrokeColor = Color.Black,
                    HeightRequest = 70,
                    StrokeWidth = 2,
                };
                // signaturePadView.ClearLabel.Margin = new Thickness(0, -14, 25, 0);

                var forgetPassword_tap = new TapGestureRecognizer();
                signaturePadView.ClearLabel.GestureRecognizers.Add(forgetPassword_tap);
                //signaturePadView.StrokeCompleted += OnStrokeCompleted;
                _sg.Children.Add(label, 0, smgridrow);
                _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                _sg.Children.Add(signaturePadView, 0, smgridrow + 1);

                if (form_Basic_Info.value.ToString() != null && form_Basic_Info.value.ToString() != "")
                {
                    signaturePadView.ClearLabel.IsVisible = true;
                    Application.Current.Properties.Remove("SignImage");
                    Application.Current.Properties.Add("SignImage", form_Basic_Info.value.ToString());

                    Application.Current.Properties.Remove("ImageHaving");
                    Application.Current.Properties.Add("ImageHaving", "True");
                    byte[] Base64Stream = Convert.FromBase64String(form_Basic_Info.value.ToString());
                    retSource = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                    Image sign = new Image
                    {
                        Source = retSource,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.Center,
                        Scale = 0.4,
                    };
                    ImageSetting(sign);
                    _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                    _sg.Children.Add(sign, 0, smgridrow + 1);

                }
                forgetPassword_tap.Tapped += (s, e) =>
                {
                    Image mm;

                    if (form_Basic_Info.value.ToString() != null && form_Basic_Info.value.ToString() != "")
                    {
                        var p = Application.Current.Properties["Image"];
                        if (p != null && p != "")
                        {
                            mm = p as Image;
                            mm.IsVisible = false;
                        }
                    }
                };

                GridBasicInfo.Children.Add(_sg, 0, bggridrow);
                BasicInfoFrame.Content = GridBasicInfo;
            }

        }

        private async void AddressOnFocused(object sender, FocusEventArgs focusEventArgs)
        {
            try
            {
                var Basicdatas = GridBasicInfo.Children;
                var editval = "";
                foreach (var item in Basicdatas)
                {
                    var tlist = item as Grid;
                    var rlist = tlist.Children.ToList();
                    for (int i = 0; i < rlist.Count(); i++)
                    {
                        if (rlist[i].StyleId == "Label")
                        {
                            var lbltext = rlist[i] as Label;
                            var val = lbltext.Text;
                            if (val == "Site Address")
                            {
                                var addressval = rlist[i + 1] as Editor;
                                editval = addressval.Text;
                            }
                        }
                    }
                }

                await PopupNavigation.Instance.PushAsync(new MapPage(editval));
            }
            catch(Exception ex)
            {

            }
        }

        public void ImageSetting(Image p = null)
        {
            Application.Current.Properties.Remove("Image");
            Application.Current.Properties.Add("Image", p);
        }

        public class SaveValue
        {
            public string control { get; set; }
            public string value { get; set; }

        }

        private void BtnApprovalSave_Clicked(object sender, EventArgs e)
        {
            //UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            //await Task.Delay(5000);
            SaveApproval();
            // UserDialogs.Instance.HideLoading();
        }



        public async void SaveApproval(string CommentsStatus=null, List<DownloadComments> dts=null)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            //await Task.Delay(5000);
            List<Form_basic_InfoUpload> form_Basic_InfoUploads = new List<Form_basic_InfoUpload>();

            try
            {
                var ButtonType = btnApprovalSave.StyleId;
                //await Task.Delay(100);
                var FormId = Settings.FormIdKey;
                var JobId = Settings.JobIdKey;
                var Access_key = Settings.AccessKey;
                upload.form_id = FormId;
                upload.job_id = JobId;
                upload.form_status = FormStatus;
                upload.access_key = Access_key;
                Label label = new Label();
                Entry entry = new Entry();
                CustomPicker customPicker = new CustomPicker();
                Image imagecheck = new Image();
                Image imageuncheck = new Image();
                Image CheckImge = new Image();
                DatePicker datePicker = new DatePicker();
                Editor editor = new Editor();
                List<string> BaseCheckvalues = new List<string>();
                List<string> CatCheckvalues = new List<string>();

                int catcheckedcount = 0;
                int catuncheckedcount = 0;
                int catradiocount = 0;
                int catradiocheckedcount = 0;
                int catcheckboxcount = 0;

                int subcatcheckedcount = 0;
                int subcatuncheckedcount = 0;
                int subcatradiocount = 0;
                int subcatradiocheckedcount = 0;
                int subcatcheckboxcount = 0;


                int basiccheckedcount = 0;
                int basicuncheckedcount = 0;
                int basicradiocheckedcount = 0;
                int basicradiocount = 0;
                int basiccheckboxcount = 0;


                ArrayList Bdata = new ArrayList();
                ArrayList Cdata = new ArrayList();
                // var Basicdata = GridBasicInfo.Children;
                //  var categorydata = GridCategoryInfo.Children;
                // var TankData = GridApplicableStatus.Children;

                var Basicdatas = GridBasicInfo.Children;
                var categorydatas = GridCategoryInfo.Children;
                var TankData = GridApplicableStatus.Children;

                //Changed
                List<Xamarin.Forms.View> Basicdata = new List<Xamarin.Forms.View>();
                List<Xamarin.Forms.View> categorydata = new List<Xamarin.Forms.View>();
                foreach (var item in Basicdatas)
                {
                    var condichkd = item.GetType();
                    var _typesrc = condichkd.FullName;
                    if (_typesrc == "Xamarin.Forms.Grid")
                    {
                        var k = item as Grid;
                        var _gditm = k.Children.ToList();

                        foreach (var itm in _gditm)
                        {
                            Basicdata.Add(itm);
                        }
                    }
                    else
                    {
                        Basicdata.Add(item);

                    }
                }
                //Category
                foreach (var item in categorydatas)
                {
                    var condichkd = item.GetType();
                    var _typesrc = condichkd.FullName;
                    if (_typesrc == "Xamarin.Forms.Grid")
                    {
                        var k = item as Grid;
                        var _gditm = k.Children.ToList();
                        foreach (var itm in _gditm)
                        {
                            categorydata.Add(itm);
                        }
                    }
                    else
                    {
                        categorydata.Add(item);

                    }
                }
                //End

                for (int i = 0; i < Basicdata.Count; i++)
                {
                    if (Basicdata[i].StyleId == "Check")
                    {
                        basiccheckedcount++;
                    }

                    if (Basicdata[i].StyleId == "Radio" || Basicdata[i].StyleId == "UnRadio")
                    {
                        basicradiocount++;
                        if (Basicdata[i].StyleId == "Radio")
                        {
                            basicradiocheckedcount++;
                        }
                    }
                    if (Basicdata[i].StyleId == "Check" || Basicdata[i].StyleId == "UnCheck")
                    {
                        basiccheckboxcount++;
                    }
                }
                var chkcnt = 0;
                var catchkcnt = 0;
                var subcatchkcnt = 0;
                for (int i = 0; i < Basicdata.Count; i++)
                {
                    if (Basicdata[i].StyleId != "Label")
                    {

                        if (Basicdata[i].StyleId == "Entry")
                        {
                            entry = (Entry)Basicdata[i];
                            if (entry.Text != "" && entry.Text != null)
                            {
                                Bdata.Add(entry.Text);
                            }
                            else
                            {
                                Bdata.Add("");
                            }
                        }
                        else if (Basicdata[i].StyleId == "Textarea")
                        {
                            editor = (Editor)Basicdata[i];
                            // Bdata.Add(editor.Text);
                            if (editor.Text != "" && editor.Text != null)
                            {
                                Bdata.Add(editor.Text);
                            }
                            else
                            {
                                Bdata.Add("");
                            }
                        }
                        else if (Basicdata[i].StyleId == "CustomPicker")
                        {
                            customPicker = (CustomPicker)Basicdata[i];
                            var pickerval = customPicker.SelectedItem;
                            if (pickerval != "" && pickerval != null)
                            {
                                Bdata.Add(pickerval);
                            }
                            else
                            {
                                Bdata.Add("");
                            }
                            // Bdata.Add(customPicker.SelectedItem);
                        }
                        else if (Basicdata[i].StyleId == "DatePicker")
                        {
                            datePicker = (DatePicker)Basicdata[i];
                            //datePicker.Format = "dd-MM-yyyy";
                            datePicker.Format = "MM-dd-yyyy";
                            var dateonly = datePicker.Date.ToShortDateString();

                            if (dateonly != "" && dateonly != null)
                            {
                                Bdata.Add(dateonly);
                            }
                            else
                            {
                                Bdata.Add("");
                            }
                        }
                        else if (Basicdata[i].StyleId == "Radio" || Basicdata[i].StyleId == "UnRadio")
                        {
                            if (Basicdata[i].StyleId == "Radio")
                            {
                                CheckImge = (Image)Basicdata[i];
                                var RadioData = CheckImge.AutomationId;
                                if (RadioData != "" && RadioData != null)
                                {
                                    Bdata.Add(RadioData);
                                }
                            }
                            else if (basicradiocheckedcount == 0)
                            {
                                if (chkcnt == 0)
                                {
                                    Bdata.Add("");
                                    chkcnt++;
                                }
                            }
                        }
                        else if (Basicdata[i].StyleId == "Check" || Basicdata[i].StyleId == "UnCheck")
                        {
                            imagecheck = (Image)Basicdata[i];
                            Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)imagecheck.Source;
                            string strFileName = objFileImageSource.File;

                            if (strFileName == "UnChecked.png")
                            {
                                basicuncheckedcount++;
                                if (basiccheckedcount == 0)
                                {
                                    if (basiccheckboxcount == basicuncheckedcount)
                                    {
                                        Bdata.Add("");
                                    }
                                }
                            }
                            else
                            {
                                var CheckData = imagecheck.AutomationId;
                                if (CheckData != "" && CheckData != null)
                                {
                                    BaseCheckvalues.Add(CheckData);
                                }
                                if (basiccheckedcount == BaseCheckvalues.Count)
                                {
                                    Bdata.Add(BaseCheckvalues);
                                }
                            }
                        }
                        else if (Basicdata[i].StyleId == "BasicSignature")
                        {
                            var bytearr = "";
                            var sign = "";
                            byte[] Bytes = new byte[0];
                            signimage = Application.Current.Properties["SignImage"].ToString();
                            // var signaturepadviewobj = categorydata[i] as SignaturePadView;
                            var signaturepadviewobj = Basicdata[i] as SignaturePadView;
                            if (signaturepadviewobj.Points.Count() > 0)
                            {
                                var stream = await signaturePadView.GetImageStreamAsync(SignatureImageFormat.Png);
                                if (stream != null)
                                {
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        Bytes = new byte[stream.Length];
                                        stream.CopyTo(ms);
                                        Bytes = ms.ToArray();
                                    }
                                }
                                sign = Convert.ToBase64String(Bytes);
                            }
                            else if (signimage != null && signimage != "")
                            {
                                sign = Application.Current.Properties["SignImage"].ToString();
                            }

                            if (!string.IsNullOrEmpty(sign))
                            {
                                Bdata.Add(sign);
                            }
                            else if (signimage != null)
                            {
                                Bdata.Add(bytearr);
                            }
                            else
                            {
                                Bdata.Add("");
                            }
                        }
                    }
                }
                for (int i = 0; i < Bdata.Count; i++)
                {
                    form_Basic_InfoUploads.Add(new Form_basic_InfoUpload { label_name = Basic_Infos[i].label_name, type = Basic_Infos[i].type, value = Bdata[i], visible = Basic_Infos[i].visible, editable = Basic_Infos[i].editable, options = Basic_Infos[i].options });
                }
                formJson.basic_Info = form_Basic_InfoUploads;
                upload.form_json = formJson;
                List<category_Info_fields> UpdateFields = new List<category_Info_fields>();
                List<Subcategory_Info_fields> UpdateSubCategoryFields = new List<Subcategory_Info_fields>();
                if (ButtonType == "FormBasicSave")
                {
                    upload.form_json.category_Info = form_Category_Infos;
                }
                else if (ButtonType == "CategoryFields_Save")
                {
                    if (formtypelistmodel.response.form_name == "SAMPLE FORM FOR MONTHLY UNDERGROUND STORAGE SYSTEM INSPECTION CHECKLIST")
                    {
                        if (categorydata.Count > 0)
                        {
                            for (int i = 0; i < categorydata.Count; i++)
                            {
                                if (categorydata[i].StyleId == "Entry")
                                {
                                    entry = (Entry)categorydata[i];
                                    Cdata.Add(entry.Text);
                                }
                                if (categorydata[i].StyleId == "UnCheck")
                                {
                                    imageuncheck = (Image)categorydata[i];
                                    if (imageuncheck.IsVisible == true)
                                    {
                                        Cdata.Add("false");
                                    }
                                }
                                if (categorydata[i].StyleId == "Check")
                                {
                                    imagecheck = (Image)categorydata[i];
                                    if (imagecheck.IsVisible == true)
                                    {
                                        Cdata.Add("true");
                                    }
                                }
                            }
                            for (int i = 0; i < TankData.Count; i++)
                            {
                                if (TankData[i].StyleId == "Entry")
                                {
                                    entry = (Entry)TankData[i];
                                    Cdata.Add(entry.Text);
                                }
                            }
                        }
                    }
                    else
                    {
                        
                        for (int i = 0; i < categorydata.Count; i++)
                        {
                            if (categorydata[i].StyleId == "Check")
                            {
                                catcheckedcount++;
                            }
                            if (categorydata[i].StyleId == "Radio" || categorydata[i].StyleId == "UnRadio")
                            {
                                catradiocount++;
                                if (categorydata[i].StyleId == "Radio")
                                {
                                    catradiocheckedcount++;
                                }
                            }
                            if (categorydata[i].StyleId == "Check" || categorydata[i].StyleId == "UnCheck")
                            {
                                catcheckboxcount++;
                            }
                        }

                        if (categorydata.Count > 0)
                        {
                            for (int jk = 0; jk < categorydata.Count; jk++)
                            {
                                if (categorydata[jk].StyleId != "Label")
                                {

                                    if (categorydata[jk].StyleId == "Entry")
                                    {
                                        entry = (Entry)categorydata[jk];
                                        Cdata.Add(entry.Text);
                                    }
                                    else if (categorydata[jk].StyleId == "Textarea")
                                    {
                                        editor = (Editor)categorydata[jk];
                                        Cdata.Add(editor.Text);

                                    }
                                    else if (categorydata[jk].StyleId == "CustomPicker")
                                    {
                                        customPicker = (CustomPicker)categorydata[jk];
                                        Cdata.Add(customPicker.SelectedItem);
                                    }
                                    else if (categorydata[jk].StyleId == "DatePicker")
                                    {
                                        datePicker = (DatePicker)categorydata[jk];
                                        //datePicker.Format = "dd-MM-yyyy";
                                        datePicker.Format = "MM-dd-yyyy";
                                        var dateonly = datePicker.Date.ToShortDateString();
                                        Cdata.Add(dateonly);
                                    }
                                    else if (categorydata[jk].StyleId == "Radio" || categorydata[jk].StyleId == "UnRadio")
                                    {
                                        if (categorydata[jk].StyleId == "Radio")
                                        {
                                            CheckImge = (Image)categorydata[jk];
                                            var RadioData = CheckImge.AutomationId;
                                            // if (RadioData != "" && RadioData == null)
                                            // {
                                            Cdata.Add(RadioData);
                                            // }
                                        }
                                        else if (catradiocheckedcount == 0)
                                        {
                                            if (catchkcnt == 0)
                                            {
                                                Cdata.Add("");
                                                catchkcnt++;
                                            }
                                        }
                                        //Cdata.Add(RadioData);
                                    }
                                    //
                                    else if(categorydata[jk].StyleId == "CategorySignature")
                                    {
                                        
                                        var bytearr = "";
                                        var sign = "";
                                        byte[] Bytes = new byte[0];
                                        signimage = Application.Current.Properties["SignImageCat"].ToString();
                                        var imagelist = Application.Current.Properties["SignImageCat"];
                                        //if (signaturePadView.Points.Count() > 0)
                                        //{
                                        //    var stream = await signaturePadView.GetImageStreamAsync(SignatureImageFormat.Png);
                                        //    if (stream != null)
                                        //    {
                                        //        using (MemoryStream ms = new MemoryStream())
                                        //        {
                                        //            Bytes = new byte[stream.Length];
                                        //            stream.CopyTo(ms);
                                        //            Bytes = ms.ToArray();
                                        //        }
                                        //    }
                                        //    sign = Convert.ToBase64String(Bytes);
                                        //}
                                        var signaturepadviewobj = categorydata[jk] as SignaturePadView;
                                        if (signaturepadviewobj.Points.Count() > 0)
                                        {
                                            var stream = await signaturepadviewobj.GetImageStreamAsync(SignatureImageFormat.Png);
                                            if (stream != null)
                                            {
                                                using (MemoryStream ms = new MemoryStream())
                                                {
                                                    Bytes = new byte[stream.Length];
                                                    stream.CopyTo(ms);
                                                    Bytes = ms.ToArray();
                                                }
                                            }
                                            sign = Convert.ToBase64String(Bytes);
                                        }
                                        else if (signimage != null && signimage != "")
                                        {
                                            Image imgchk = null;
                                            if (jk < categorydata.Count-1)
                                            {
                                                 imgchk = categorydata[jk + 1] as Image;
                                            }
                                            if (imgchk != null)
                                            {
                                                if (imgchk.IsVisible)
                                                {

                                                    var signimg = imagelist;
                                                    IList collection = (IList)imagelist;
                                                    var t = collection[signcount] as M3App.Cores.DataModel.category_Info_fields;
                                                    var signimgval = t.value;
                                                    //sign = Application.Current.Properties["SignImageCat"].ToString();
                                                    sign = signimgval.ToString();
                                                }
                                                else
                                                {
                                                    sign = "";
                                                }
                                            }
                                            else
                                            {
                                                sign = "";
                                            }
                                        }

                                        if (!string.IsNullOrEmpty(sign))
                                        {
                                            Cdata.Add(sign);
                                        }
                                        //else if (signimage != null)
                                        //{
                                        //    Cdata.Add(bytearr);
                                        //}
                                        else
                                        {
                                            Cdata.Add("");
                                        }
                                        signcount = signcount + 1;

                                    }
                                    else if (categorydata[jk].StyleId == "Check" || categorydata[jk].StyleId == "UnCheck")
                                    {
                                        //imagecheck = (Image)categorydata[i];
                                        //var CheckData = imagecheck.AutomationId;
                                        //CatCheckvalues.Add(CheckData);
                                        //if (catcheckedcount == CatCheckvalues.Count)
                                        //{
                                        //    Cdata.Add(CatCheckvalues);
                                        //}

                                        imagecheck = (Image)categorydata[jk];
                                        Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)imagecheck.Source;
                                        string strFileName = objFileImageSource.File;

                                        if (strFileName == "UnChecked.png")
                                        {
                                            catuncheckedcount++;
                                            if (catcheckedcount == 0)
                                            {
                                                if (catcheckboxcount == catuncheckedcount)
                                                {
                                                    Cdata.Add("");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var CheckData = imagecheck.AutomationId;
                                            if (CheckData != "" && CheckData != null)
                                            {
                                                CatCheckvalues.Add(CheckData);
                                            }
                                            if (catcheckedcount == CatCheckvalues.Count)
                                            {
                                                Cdata.Add(CatCheckvalues);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Need Save 
                    var a = Cdata;

                    for (int i = 0; i < a.Count; i++)
                    {

                        UpdateFields.Add(new category_Info_fields { label_name = category_Info_Fields[i].label_name, type = category_Info_Fields[i].type, value = Cdata[i], visible = category_Info_Fields[i].visible, editable = category_Info_Fields[i].editable, options = category_Info_Fields[i].options });

                    }
                }

                else if (ButtonType == "SubCategoryFields_Save")
                {
                    for (int ij = 0; ij < categorydata.Count; ij++)
                    {
                        if (categorydata[ij].StyleId == "Check")
                        {
                            catcheckedcount++;
                        }

                        if (categorydata[ij].StyleId == "Radio" || categorydata[ij].StyleId == "UnRadio")
                        {
                            subcatradiocount++;
                            if (categorydata[ij].StyleId == "Radio")
                            {
                                subcatradiocheckedcount++;
                            }
                        }
                        if (categorydata[ij].StyleId == "Check" || categorydata[ij].StyleId == "UnCheck")
                        {
                            subcatcheckboxcount++;
                        }
                    }

                    if (categorydata.Count > 0)
                    {
                        for (int ik = 0; ik < categorydata.Count; ik++)
                        {
                            if (categorydata[ik].ClassId == "SubCategoryEntry")
                            {
                                entry = (Entry)categorydata[ik];
                                Cdata.Add(entry.Text);
                            }
                            else if (categorydata[ik].StyleId == "Check" || categorydata[ik].StyleId == "UnCheck")
                            {
                                //imagecheck = (Image)categorydata[i];
                                //var CheckData = imagecheck.AutomationId;
                                //CatCheckvalues.Add(CheckData);
                                //if (catcheckedcount == CatCheckvalues.Count)
                                //{
                                //    Cdata.Add(CatCheckvalues);
                                //}

                                imagecheck = (Image)categorydata[ik];
                                Xamarin.Forms.FileImageSource objFileImageSource = (Xamarin.Forms.FileImageSource)imagecheck.Source;
                                string strFileName = objFileImageSource.File;

                                if (strFileName == "UnChecked.png")
                                {
                                    subcatuncheckedcount++;
                                    if (subcatcheckedcount == 0)
                                    {
                                        if (subcatcheckboxcount == subcatuncheckedcount)
                                        {
                                            Cdata.Add("");
                                        }
                                    }
                                }
                                else
                                {
                                    var CheckData = imagecheck.AutomationId;
                                    if (CheckData != "" && CheckData != null)
                                    {
                                        CatCheckvalues.Add(CheckData);
                                    }
                                    if (subcatcheckedcount == CatCheckvalues.Count)
                                    {
                                        Cdata.Add(CatCheckvalues);
                                    }
                                }

                            }
                            else if (categorydata[ik].StyleId == "Radio" || categorydata[ik].StyleId == "UnRadio")
                            {
                                //CheckImge = (Image)categorydata[i];
                                //var RadioData = CheckImge.AutomationId;
                                //Cdata.Add(RadioData);
                                if (categorydata[ik].StyleId == "Radio")
                                {
                                    CheckImge = (Image)categorydata[ik];
                                    var RadioData = CheckImge.AutomationId;
                                    if (RadioData != "" && RadioData != null)
                                    {
                                        Cdata.Add(RadioData);
                                    }
                                }
                                else if (subcatradiocheckedcount == 0)
                                {
                                    if (subcatchkcnt == 0)
                                    {
                                        Cdata.Add("");
                                        subcatchkcnt++;
                                    }
                                }


                            }
                            else if (categorydata[ik].ClassId == "Picker")
                            {
                                customPicker = (CustomPicker)categorydata[ik];
                                Cdata.Add(customPicker.SelectedItem);
                            }
                            else if (categorydata[ik].ClassId == "Areatext")
                            {
                                editor = (Editor)categorydata[ik];
                                Cdata.Add(editor.Text);
                            }
                            else if (categorydata[ik].ClassId == "PickerDate")
                            {
                                //Subcatdate

                                //datePicker = (DatePicker)categorydata[i];
                                ////datePicker.Format = "dd-MM-yyyy";
                                //datePicker.Format = "MM-dd-yyyy";
                                //var dateonly = datePicker.Date.ToShortDateString();
                                //Cdata.Add(dateonly);

                                //datePicker = (DatePicker)categorydata[i];
                                ////datePicker.Format = "dd-MM-yyyy";
                                //datePicker.Format = "MM-dd-yyyy";
                                //var dateonly = datePicker.Date.ToShortDateString();
                                //if (dateonly == "01-01-1900")
                                //{
                                //    dateonly = "";
                                //}

                                // var dateonly= (Entry)categorydata[i];


                                var dateonly = (Label)categorydata[ik];
                                var txtval = dateonly.Text;
								string tt = null;
								if (txtval == "" || txtval == null|| txtval=="Select Date")
                                {
									tt = null;
                                }
                                else
                                {
                                   //string[] t = txtval.Split('-');
                                   // string mm = t[0] + '/' + t[1] + '/' + t[2];
								   //tt = mm
                                   tt= txtval;
                                }
                                
                                
								Cdata.Add(tt);
                            }
                            else if (categorydata[ik].StyleId == "CategorySignature")
                            {
                                //var bytearr = "";
                                //var sign = "";
                                //byte[] Bytes = new byte[0];
                                //signimage = Application.Current.Properties["SignImageCatSub"].ToString();

                                //var signaturepadviewobj = categorydata[i] as SignaturePadView;
                                //if (signaturepadviewobj.Points.Count() > 0)
                                //{
                                //    var stream = await signaturepadviewobj.GetImageStreamAsync(SignatureImageFormat.Png);
                                //    if (stream != null)
                                //    {
                                //        using (MemoryStream ms = new MemoryStream())
                                //        {
                                //            Bytes = new byte[stream.Length];
                                //            stream.CopyTo(ms);
                                //            Bytes = ms.ToArray();
                                //        }
                                //    }
                                //    sign = Convert.ToBase64String(Bytes);
                                //}
                                //else if (signimage != null && signimage != "")
                                //{
                                //    sign = Application.Current.Properties["SignImageCat"].ToString();
                                //}

                                //if (!string.IsNullOrEmpty(sign))
                                //{
                                //    Cdata.Add(sign);
                                //}
                                //else if (signimage != null)
                                //{
                                //    Cdata.Add(bytearr);
                                //}
                                //else
                                //{
                                //    Cdata.Add("");
                                //}

                                //Category sample save

                                var bytearr = "";
                                var sign = "";
                                byte[] Bytes = new byte[0];
                                signimage = Application.Current.Properties["SignImageSubCatList"].ToString();
                                var imagelist = Application.Current.Properties["SignImageSubCatList"];

                                var signaturepadviewobj = categorydata[ik] as SignaturePadView;
                                if (signaturepadviewobj.Points.Count() > 0)
                                {
                                    var stream = await signaturepadviewobj.GetImageStreamAsync(SignatureImageFormat.Png);
                                    if (stream != null)
                                    {
                                        using (MemoryStream ms = new MemoryStream())
                                        {
                                            Bytes = new byte[stream.Length];
                                            stream.CopyTo(ms);
                                            Bytes = ms.ToArray();
                                        }
                                    }
                                    sign = Convert.ToBase64String(Bytes);
                                }
                                else if (signimage != null && signimage != "")
                                {
                                    Image imgchk = null;
                                    if (ik < categorydata.Count - 1)
                                    {
                                        imgchk = categorydata[ik + 1] as Image;
                                    }
                                    if (imgchk != null)
                                    {
                                        if (imgchk.IsVisible)
                                        {

                                            var signimg = imagelist;
                                            IList collection = (IList)imagelist;
                                            var t = collection[signcount] as M3App.Cores.DataModel.Subcategory_Info_fields;
                                            var signimgval = t.value;
                                            sign = signimgval.ToString();
                                        }
                                        else
                                        {
                                            sign = "";
                                        }
                                    }
                                    else
                                    {
                                        sign = "";
                                    }
                                }

                                if (!string.IsNullOrEmpty(sign))
                                {
                                    Cdata.Add(sign);
                                }

                                else
                                {
                                    Cdata.Add("");
                                }
                                signcount = signcount + 1;

                            }
                        }
                    }


                    var b = Cdata;
                    for (int i = 0; i < b.Count; i++)
                    {
                        if (Cdata[i] == null)
                        {
                            UpdateSubCategoryFields.Add(new Subcategory_Info_fields { label_name = sub_category_info_Add[i].label_name, options = sub_category_info_Add[i].options, type = sub_category_info_Add[i].type, value = "", visible = sub_category_info_Add[i].visible, editable = sub_category_info_Add[i].editable });
                        }
                        else
                        {
                            UpdateSubCategoryFields.Add(new Subcategory_Info_fields { label_name = sub_category_info_Add[i].label_name, options = sub_category_info_Add[i].options, type = sub_category_info_Add[i].type, value = Cdata[i].ToString(), visible = sub_category_info_Add[i].visible, editable = sub_category_info_Add[i].editable });
                        }
                    }
                }

                if (categoryId != 0)
                {
                    var Category = UpdateFields;
                    var SubCategory = UpdateSubCategoryFields;
                    if (Category.Count > 0)
                    {
                        for (int i = 0; i < form_Category_Infos.Count; i++)
                        {
                            if (categoryId == form_Category_Infos[i].id)
                            {
                                var StoringData = form_Category_Infos[i];
                                form_Category_Infos.RemoveAt(i);
                                form_Category_Infos.Add(new Form_category_Info { id = StoringData.id, parent_id = StoringData.parent_id, name = StoringData.name, description = description, fields = UpdateFields, sublevel = StoringData.sublevel });
                            }
                        }
                    }
                    if (SubCategory.Count > 0)
                    {
                        for (int i = 0; i < form_Category_Infos.Count; i++)
                        {
                            if (categoryId == form_Category_Infos[i].id)
                            {
                                foreach (var item in form_Category_Infos)
                                {
                                    sub_category = item.sub_category;
                                    if (sub_category != null)
                                    {
                                        for (int j = 0; j < sub_category.Count; j++)
                                        {
                                            if (SubCategoryID == sub_category[j].id)
                                            {
                                                var StoringData = sub_category[j];
                                                sub_category.RemoveAt(j);
                                                sub_category.Add(new Sub_category_Info { id = StoringData.id, parent_id = StoringData.parent_id, fields = SubCategory, description = StoringData.description, sublevel = StoringData.sublevel, name = StoringData.name });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                byte[] imageBytes = new byte[0];
                if (Signature.Points.Count() > 0)
                {
                    var stream = await Signature.GetImageStreamAsync(SignatureImageFormat.Png);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        imageBytes = new byte[stream.Length];
                        stream.CopyTo(ms);
                        imageBytes = ms.ToArray();
                    }
                }
                formJson.comments = new List<UploadComments>();
                uploadComments = new UploadComments();
                string signdata = Convert.ToBase64String(imageBytes);

                //nodo
                if (Xamarin.Forms.Application.Current.Properties.ContainsKey("CommentsSave"))
                {
                    if (CommentsStatus != "DeleteComments" && CommentsStatus != "EditComments")
                    {
                        formtypelistmodel = GetFormTypeList(FormTypeListcurrentdata);
                        downloadComments = formtypelistmodel.response.form_json.comments;
                    }
                    var saveInfo = Xamarin.Forms.Application.Current.Properties["CommentsSave"];
                    if (saveInfo == "CloseSave")
                    {
                        if (downloadComments != null)
                        {
                            foreach (var itm in downloadComments)
                            {
                                UploadComments uc = new UploadComments();
                                uc.action_taken = itm.action_taken;
                                uc.date = itm.date;
                                uc.initials = itm.initials;
                                formJson.comments.Add(uc);
                            }
                        }
                        if (!string.IsNullOrEmpty(EditComments.Text))
                        {
                            UploadComments uc = new UploadComments();
                            uc.action_taken = EditComments.Text;
                            //uc.date = datepicker.Date.ToShortDateString();
                            uc.date = datepicker.Date.ToString("MM-dd-yyyy");
                            uc.initials = signdata;
                            formJson.comments.Add(uc);
                        }
                    }
					else if (saveInfo == "CloseSaveComment")
                    {
                        if (downloadComments != null)
                        {
                            foreach (var itm in downloadComments)
                            {
                                UploadComments uc = new UploadComments();
                                uc.action_taken = itm.action_taken;
                                uc.date = itm.date;
                                uc.initials = itm.initials;
                                formJson.comments.Add(uc);
                            }
                        }
                        if (!string.IsNullOrEmpty(EditComments.Text))
                        {
                            UploadComments uc = new UploadComments();
                            uc.action_taken = EditComments.Text;
                            //uc.date = datepicker.Date.ToShortDateString();
                            uc.date = datepicker.Date.ToString("MM-dd-yyyy");
                            uc.initials = signdata;
                            formJson.comments.Add(uc);
                        }
                    }
                    else if(CommentsStatus == "EditComments")
                    {
                        var mn = downloadComments;
                        if (dts != null)
                        {
                            var t = (from m in downloadComments
                                     where m.Index==dts[0].Index
                                     select m).FirstOrDefault();
                            t.action_taken = dts[0].action_taken;
                            t.date = dts[0].date;
                            t.initials = dts[0].initials;
                            List<UploadComments> upc = new List<UploadComments>();
                            
                            foreach (var itm in downloadComments)
                            {
                                UploadComments uc = new UploadComments();
                                uc.action_taken = itm.action_taken;
                                uc.date = itm.date;
                                //uc.date = Convert.ToDateTime(itm.date).ToString("MM-dd-yyyy");
                                uc.initials =itm.initials;
                                upc.Add(uc);
                            }
                            formJson.comments = upc;
                        }
                    }
                    else if(CommentsStatus == "DeleteComments")
                    {
                        List<UploadComments> upc = new List<UploadComments>();
                        foreach (var itm in dts)
                        {
                            UploadComments uc = new UploadComments();
                            uc.action_taken = itm.action_taken;
                            uc.date = itm.date;
                            //uc.date= Convert.ToDateTime(itm.date).ToString("MM-dd-yyyy");
                            uc.initials = itm.initials;
                            upc.Add(uc);

                            //var splcha = itm.date.Substring(0, 3);
                            //var splchb = splcha.Substring(2).ToCharArray();

                            //string[] _list1;
                            //string formattedDates = "";
                            //_list1 = itm.date.Split(splchb);
                            //var ttt1 = new DateTime(Convert.ToInt32(_list1[2]), Convert.ToInt32(_list1[1]), Convert.ToInt32(_list1[0]));
                            //string mm = ttt1.ToString("dd/MM/yyyy");
                            //string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "MM-dd-yyyy", "dd-MM-yyyy", "M-dd-yyyy", "M/dd/yyyy" };
                            //DateTime date;
                            //var dt = "";

                            //if (DateTime.TryParseExact(mm, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                            //    formattedDates = date.ToString("dd/MM/yyyy");
                            //if (formattedDates != null && formattedDates != "")
                            //{
                            //    var splch = formattedDates.Substring(0, 3);
                            //    var splch1 = splch.Substring(2).ToCharArray();
                            //    string[] _list = formattedDates.Split(splch1);
                            //    date = new DateTime(Convert.ToInt32(_list[2]), Convert.ToInt32(_list[1]), Convert.ToInt32(_list[0]));
                            //    dt = date.ToString("MM-dd-yyyy");
                            //}
                            //uc.date = dt;

                        }
                        formJson.comments = upc;
                    }
                    else
                    {
                        formJson.comments = null;
                        //List<UploadComments> upc = new List<UploadComments>();
                        //foreach (var itm in downloadComments)
                        //{
                        //    UploadComments uc = new UploadComments();
                        //    uc.action_taken = itm.action_taken;
                        //    uc.date = itm.date;
                        //    uc.initials = itm.initials;
                        //    upc.Add(uc);
                        //}
                        //formJson.comments = upc;
                    }
                }

                if (categoryId != 0)
                {
                    formJson.category_Info = form_Category_Infos.OrderBy(x => x.id).ToList();
                }

                upload.form_json = formJson;
                PostformDatas(upload, CommentsStatus);
                
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
            }

        }
        public void NavigationPage(string Message=null)
        {
            string status = Application.Current.Properties["RedirectStatus"].ToString();
            string RedirectStatus = Application.Current.Properties["RedirectStatus"].ToString();
            if (status != "")
            {
                var t = Application.Current.Properties["FormInformationdata"];
                var tt = t as FormTypeList;
                App.Current.MainPage = new GetInfoFormsInfoPage(tt);

                var tm = Application.Current.Properties["Navdetails"];
                GetInfoFormsInfoPage _form = tm as GetInfoFormsInfoPage;
                _form.Testdata(RedirectStatus);
                UserDialogs.Instance.HideLoading();
            }
            else
            {
                // var tm = Application.Current.Properties["Navdetails"];
                // GetInfoFormsInfoPage _form = tm as GetInfoFormsInfoPage;
                // _form.RedirectGridPageInfo(RedirectStatus);


                //var t = Application.Current.Properties["FormInformationdata"];
                //var tt = t as FormTypeList;
                //App.Current.MainPage = new GetInfoFormsInfoPage(tt);
                //UserDialogs.Instance.HideLoading();

                //App.Current.MainPage = new DynamicJobList();
                
                var t = Application.Current.Properties["FormInformationdata"];
                var tt = t as FormTypeList;
                App.Current.MainPage = new GetInfoFormsInfoPage(tt);
                XFToast.LongMessage(Message);
                Application.Current.Properties.Remove("Save_Enable");
                Application.Current.Properties.Add("Save_Enable", "true");
                UserDialogs.Instance.HideLoading();
            }

        }



        private void buttonChecked_Tapped(object sender, EventArgs e)
        {
            GridCategoryInfo.IsVisible = false;
            GridApplicableStatus.IsVisible = true;
        }
        private void ButtonBack_Tapped(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            Task.Run(async () =>
            {
                //  await Task.Delay(3000);
                Device.BeginInvokeOnMainThread(() =>
                {
                    App.Current.MainPage = new DynamicJobList();
                });
            });

        }
        //work
        public FormTypeListDownLoadModel GetFormTypeList(FormTypeList Formlistmodel)
        {
            Application.Current.Properties.Remove("FormTypeListObject");
            Application.Current.Properties.Add("FormTypeListObject", Formlistmodel);

            var Formslistdata = dynamicFormListService.Dynamicformdatas(Formlistmodel);
            return Formslistdata;
        }
        public async void PostformDatas(FormTypeUpload formTypeUpload,string status=null)
        {
            try
            {
                var postdata = await dynamicFormListService.FormTypeUpload(formTypeUpload);
                if (postdata.result == "Success")
                {
                    if (Xamarin.Forms.Application.Current.Properties.ContainsKey("CommentsSave"))
                    {
                        var saveInfo = Xamarin.Forms.Application.Current.Properties["CommentsSave"];
                        if (saveInfo != "CloseSave")
                        {
                            if (status == "EditComments")
                            {
                                XFToast.LongMessage(postdata.message);
                                UserDialogs.Instance.HideLoading();
                                PopupNavigation.Instance.PopAsync();
                                App.Current.MainPage = new M3App.Views.ViewComments(downloadComments);
                            }
                            else if (status == "DeleteComments")
                            {
                                XFToast.LongMessage("Deleted Successfully");
                                UserDialogs.Instance.HideLoading();
                                App.Current.MainPage = new M3App.Views.ViewComments(downloadComments);
                            }
                            else if (saveInfo == "CloseSaveComment")
                            {
                                XFToast.LongMessage(postdata.message);
                                UserDialogs.Instance.HideLoading();
                            }
                            //ffff
                            else
                            {
                                //XFToast.LongMessage(postdata.message);
                                //UserDialogs.Instance.HideLoading();
                                NavigationPage(postdata.message);
                            }
                        }
                        
                        else
                        {
                            XFToast.LongMessage(postdata.message);
                            UserDialogs.Instance.HideLoading();
                            Application.Current.Properties.Remove("CommentsSave");
                            Application.Current.Properties.Add("CommentsSave", "");
                        }
                    }
                    //await Navigation.PushAsync(new Views.DynamicJobList() { Title = "Job List" });
                    //btnApprovalSave.IsEnabled = false;
                    // UserDialogs.Instance.HideLoading();
                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                    XFToast.LongMessage(postdata.message);
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                XFToast.LongMessage("Something went wrong");
            }
        }
        public FormStatusModel GetFormstatusList()
        {
            var formstatus = dynamicFormListService.FormStatus();
            if (formstatus != null)
            {
                if (!string.IsNullOrEmpty(FormTypeListcurrentdata.form_status))
                {
                    Formstatus.ItemsSource = formstatus.response;
                    Formstatus.SelectedItem = FormTypeListcurrentdata.form_status;
                }
                else
                {
                    Formstatus.ItemsSource = formstatus.response;
                    Formstatus.SelectedIndex = 0;
                }
            }
            return formstatus;
        }


    }
}