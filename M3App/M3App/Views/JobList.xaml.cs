﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using M3App.Cores.Interface;
using M3App.Models;
using Plugin.Connectivity;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
    public partial class JobList : ContentPage
    {

        JobListAccess jobListAccess = new JobListAccess();
        JobListModel jobListmodel = new JobListModel();
        JobListService jobListService = new JobListService();
        SaveAllFields _offsave = new SaveAllFields();
        private SQLiteConnection jobl;
        RetrieveOfflineDatas _jobliststorage = new RetrieveOfflineDatas();
        
        List<JobListDataStorage> _joblist = new List<JobListDataStorage>();
        public ObservableCollection<JobListDataStorage> localformdata { get; set; }
        public JobList()
        {
            try
            {
                InitializeComponent();
                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
                //var data= Task.Run(async () => await getjoblist()).Result;
                LoadJobList();
                MovingDatasApi();
            }
            catch (Exception ex)
            {

            }
        }
        public async Task<JobListModel> getjoblist()
        {
            try
            {
                var list = await jobListService.JobList();
                if (list.response != null)
                {
                    foreach (var itm in list.response)
                    {
                        if (itm.job_scheduled != null && itm.job_scheduled != "")
                        {
                            var t = itm.job_scheduled;
                            DateTime n = Convert.ToDateTime(t);
                            string m = n.ToString("MM-dd-yyyy HH:mm:ss");
                            itm.job_scheduled = m;
                        }

                    }
                }
                else
                {
                    DisplayAlert("Error", list.message, "Cancel");
                }

                listitemsSummary.ItemsSource = list.response;
                return list;
            }
            catch (Exception ex)
            {
                throw;

            }
        }
        public async void MovingDatasApi()
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                    //await Task.Delay(500);
                    bool status = _offsave.OfflineStorage();
                    if (status)
                    {
                        UserDialogs.Instance.HideLoading();
                        if (Device.RuntimePlatform.ToLower() == "ios")
                        {
                            UserDialogs.Instance.Alert("Datas saved on forms successfully...");
                        }
                        else
                        {
                            XFToast.LongMessage("Datas saved on forms successfully...");
                        }
                        //XFToast.LongMessage("Datas saved on forms successfully...");
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        private async void Refresh_Tapped(object sender,EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Refreshing...", MaskType.Gradient);
                JobListModel list = new JobListModel();
                if (CrossConnectivity.Current.IsConnected)
                {
                     list = await jobListService.JobList();
                    foreach (var itm in list.response)
                {
                    if (itm.job_scheduled != null && itm.job_scheduled != "")
                    {
                        var t = itm.job_scheduled;
                        DateTime n = Convert.ToDateTime(t);
                        string m = n.ToString("MM-dd-yyyy HH:mm:ss");
                        itm.job_scheduled = m;
                    }
                }
                listitemsSummary.ItemsSource = list.response;
                UserDialogs.Instance.HideLoading();
                }
                else
                {
                    var tempjoblist = _jobliststorage.SelectAllDatas();
                    listitemsSummary.ItemsSource = tempjoblist;
                }
                
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
        private async  void buttonSearch_Tapped(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                var selecteditem = (Grid)sender;
                var item = selecteditem.BindingContext;
                Messageresponse infoitem =new Messageresponse();
                if (CrossConnectivity.Current.IsConnected)
                {
                    infoitem = item as Messageresponse;
                    if (infoitem == null)
                    {
                        infoitem = new Messageresponse();
                        var joblisttemp = item as JobListDataStorage;
                        infoitem.job_id = joblisttemp.job_id;
                        infoitem.job_scheduled = joblisttemp.job_scheduled;
                        infoitem.job_siteid = joblisttemp.job_siteid;
                        infoitem.job_status = joblisttemp.job_status;
                        infoitem.job_type = joblisttemp.job_type;
                        infoitem.site_name = joblisttemp.site_name;
                        infoitem.assigned_form_id = joblisttemp.assigned_form_id;
                    }
                }
                else
                {
                    var joblisttemp = item as JobListDataStorage;
                    if (joblisttemp == null)
                    {
                        infoitem = item as Messageresponse;
                    }
                    else
                    {
                        joblisttemp = item as JobListDataStorage;
                        infoitem.job_id = joblisttemp.job_id;
                        infoitem.job_scheduled = joblisttemp.job_scheduled;
                        infoitem.job_siteid = joblisttemp.job_siteid;
                        infoitem.job_status = joblisttemp.job_status;
                        infoitem.job_type = joblisttemp.job_type;
                        infoitem.site_name = joblisttemp.site_name;
                        infoitem.assigned_form_id = joblisttemp.assigned_form_id;
                    }
                }
               
                var navPage3 = new NavigationPage();

                if (infoitem.job_id != null)
                {
                    Settings.JobIdKey = infoitem.job_id;
                    Settings.JobStatusKey = infoitem.job_status;
                    Settings.SiteNameKey = infoitem.site_name;
                }
                await Navigation.PushAsync(new Views.DynamicJobList() { Title = "Job Info" });

               // App.Current.MainPage = new Views.DynamicJobList();
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            } 
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
           
        }
        public void LoadJobList()
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var data = Task.Run(async () => await getjoblist()).Result;
                    if (data.response != null)
                    {
                        foreach (var item in data.response)
                        {
                            JobListDataStorage _joblistdatas = new JobListDataStorage();
                            _joblistdatas.job_id = item.job_id;
                            _joblistdatas.job_scheduled = item.job_scheduled;
                            _joblistdatas.job_siteid = item.job_siteid;
                            _joblistdatas.job_status = item.job_status;
                            _joblistdatas.job_type = item.job_type;
                            _joblistdatas.site_name = item.site_name;
                            _joblistdatas.assigned_form_id = item.assigned_form_id;
                            _joblist.Add(_joblistdatas);
                        }
                        _jobliststorage.JobListDataAccess(_joblist);
                    }
                    else
                    {
                        DisplayAlert("Error", data.message, "Cancel");
                    }
                }
                else
                {
                    if (Settings.checkTrue == "True")
                    {
                        var tempjoblist = _jobliststorage.SelectAllDatas();
                        listitemsSummary.ItemsSource = tempjoblist;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
   }

}