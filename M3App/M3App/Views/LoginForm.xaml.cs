﻿using Acr.UserDialogs;
using M3App.Cores.Helpers;
using M3App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
    public partial class LoginForm : ContentPage
    {
        static int usercount = 0, passwordcount = 0;


        public LoginForm()
        {
            try
            {
                InitializeComponent();
                var pagename = base.GetType().Name;

                
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);

                Application.Current.Properties.Remove("RedirectStatus");
                Application.Current.Properties.Add("RedirectStatus", "");

                BtnIsNewUser.Clicked += BtnIsNewUser_Clicked;
                LoadBlueImages();
                txtEmail.TextChanged += TxtEmail_TextChanged;


                if (Device.RuntimePlatform.ToLower() == "ios")
                {

                }

                    txtPassword.TextChanged += TxtPassword_TextChanged;
                var registerModelView = new LoginViewModel();
                this.BindingContext = registerModelView;


                if (Settings.checkTrue == "True")
                {
                    passwordCheckImg.IsVisible = true;
                    passwordUnCheckImg.IsVisible = false;

                    txtEmail.Text = Settings.UserName;
                    txtPassword.Text = Settings.Password;

                }
                else if (Settings.unCheckTrue == "True")
                {
                    passwordCheckImg.IsVisible = false;
                    passwordUnCheckImg.IsVisible = true;


                    txtEmail.Text = "";
                    txtPassword.Text = "";
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();               
            }
            finally
            {
                UserDialogs.Instance.HideLoading();

            }
        }




        //private void passwordChecked(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        passwordCheckImg.IsVisible = false;
        //        passwordUnCheckImg.IsVisible = true;
        //        if (passwordUnCheckImg.IsVisible == true)
        //        {
        //            //txtEmail.Text = "";
        //            //txtPassword.Text = "";
        //            Application.Current.Properties.Remove("Check");
        //            Application.Current.Properties.Add("Check", "Yes");
        //        }
        //        Settings.checkTrue = passwordCheckImg.IsVisible.ToString();
        //        passwordCheckImg.IsVisible = Convert.ToBoolean(Settings.checkTrue);

        //        Settings.unCheckTrue = passwordUnCheckImg.IsVisible.ToString();
        //        passwordUnCheckImg.IsVisible = Convert.ToBoolean(Settings.unCheckTrue);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        //private void passwordUnChecked(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Settings.UserName = txtEmail.Text;
        //        Settings.Password = txtPassword.Text;
        //        passwordCheckImg.IsVisible = true;
        //        passwordUnCheckImg.IsVisible = false;
        //        if (passwordCheckImg.IsVisible == true)
        //        {
        //           txtEmail.Text = Settings.UserName.ToString();
        //           txtPassword.Text = Settings.Password.ToString();
        //        }

        //        Settings.checkTrue = passwordCheckImg.IsVisible.ToString();
        //        passwordCheckImg.IsVisible = Convert.ToBoolean(Settings.checkTrue);

        //        Settings.unCheckTrue = passwordUnCheckImg.IsVisible.ToString();
        //        passwordUnCheckImg.IsVisible = Convert.ToBoolean(Settings.unCheckTrue);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}





        private void passwordChecked(object sender, EventArgs e)
        {
            try
            {
                passwordCheckImg.IsVisible = false;
                passwordUnCheckImg.IsVisible = true;
                if (passwordUnCheckImg.IsVisible == true)
                {
                    Application.Current.Properties.Remove("Check");
                    Application.Current.Properties.Add("Check", "Yes");
                }
                Settings.checkTrue = passwordCheckImg.IsVisible.ToString();
                passwordCheckImg.IsVisible = Convert.ToBoolean(Settings.checkTrue);

                Settings.unCheckTrue = passwordUnCheckImg.IsVisible.ToString();
                passwordUnCheckImg.IsVisible = Convert.ToBoolean(Settings.unCheckTrue);
            }
            catch (Exception ex)
            {
            }
        }
        private void passwordUnChecked(object sender, EventArgs e)
        {
            try
            {
                //Settings.UserName = txtEmail.Text;
                //Settings.Password = txtPassword.Text;
                var Username= txtEmail.Text;
                var Password= txtPassword.Text;
                passwordCheckImg.IsVisible = true;
                passwordUnCheckImg.IsVisible = false;
                if (passwordCheckImg.IsVisible == true)
                {
                    txtEmail.Text = Username.ToString();
                    txtPassword.Text = Password.ToString();
                }

                Settings.checkTrue = passwordCheckImg.IsVisible.ToString();
                passwordCheckImg.IsVisible = Convert.ToBoolean(Settings.checkTrue);

                Settings.unCheckTrue = passwordUnCheckImg.IsVisible.ToString();
                passwordUnCheckImg.IsVisible = Convert.ToBoolean(Settings.unCheckTrue);
            }
            catch (Exception ex)
            {
            }
        }

        private async void BtnIsNewUser_Clicked(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
            await Navigation.PushModalAsync(new M3App.Views.Register());
        }

        private async void btnForgotPassword_Tapped(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
            await Navigation.PushModalAsync(new Views.ForgotPassword());

        }
        public void LoadBlueImages()
        {
            imguser.Source = ImageSource.FromFile("UserInactive.png");
            imguser1.Source = ImageSource.FromFile("UserInactive.png");
            imgpassword.Source = ImageSource.FromFile("PasswordInactive.png");
            imgpassword1.Source = ImageSource.FromFile("PasswordInactive.png");
        }
        private void TxtEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (usercount == 0)
            {
                imguser.Source = ImageSource.FromFile("UserActive.png");
                imguser1.Source = ImageSource.FromFile("UserActive.png");
                BoxViewEmail.Color = Color.FromHex("#141A68");
                usercount++;
            }
        }
        private void TxtPassword_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (passwordcount == 0)
            {
                imgpassword.Source = ImageSource.FromFile("PasswordActive.png");
                imgpassword1.Source = ImageSource.FromFile("PasswordActive.png");
                BoxViewPassword.Color = Color.FromHex("#141A68");
                passwordcount++;
            }
        }
    }
}