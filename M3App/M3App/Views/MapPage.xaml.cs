﻿//using GoogleMaps.LocationServices;
//using GoogleMaps.LocationServices;
using M3App.Cores.DataModel;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using static M3App.Cores.DataModel.GarageAddressGoogle;

namespace M3App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage 
	{
        double Latitude, Longitude;
		public MapPage (string address)
		{
			InitializeComponent ();
            GetLatitudelongitude(address);
        }

        public async void GetLatitudelongitude(string addressval)
        {
            try
            {
                IList<Position> Pos_Val = new List<Position>();
                var addressPosition = new Position();
                Geocoder coder = new Geocoder();
                var LocationPoints = await coder.GetPositionsForAddressAsync(addressval);
                foreach (var pos in LocationPoints)
                {
                    addressPosition = new Position(pos.Latitude, pos.Longitude);
                }
                Pos_Val.Add(addressPosition);

                foreach (var position in Pos_Val)
                {
                    var pin = new Pin
                    {
                        Type = PinType.Place,
                        Position = position,
                        Label = addressval
                    };

                    customMap.Pins.Add(pin);
                }

                if (Pos_Val.Count() > 0)
                {
                    MapSpan span = new MapSpan(Pos_Val.First(), 360, 360);
                    customMap.MoveToRegion(span);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private async void ButtonBack_Tapped(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }


        //var position = new Position(Latitude,Longitude);
        ////var position = new Position(36.9628066, -122.0194722); // Latitude, Longitude
        //var pin = new Pin
        //{
        //    Type = PinType.Place,
        //    Position = position,
        //    Label = address,
        //    Address = address
        //};
        ////customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(Latitude, Longitude), Distance.FromMiles(500)));
        //customMap.Pins.Add(pin);

        //try
        //{
        //    Geocoder coder = new Geocoder();
        //    var point = await coder.GetPositionsForAddressAsync(addressval);
        //    var latvalues = (from i in point
        //                     select i).FirstOrDefault();
        //    Latitude = latvalues.Latitude;
        //    Longitude = latvalues.Longitude;
        //}
        //catch(Exception ex)
        //{

        //}


        //public async void NotLoadGoogleMap(double latNavigation, double longNavigation)
        //{
        //    try
        //    {
        //        if (CrossConnectivity.Current.IsConnected)
        //        {
        //            aLongitude = longNavigation;

        //            bLatitude = latNavigation;

        //            var v = await LoadStartAddress(bLatitude, aLongitude);
        //            if (v.status == "OK")
        //            {
        //                if (v.results != null)
        //                {
        //                    for (int i = 0; i < v.results.Count; i++)
        //                    {
        //                        if (i == 0)
        //                        {
        //                            startAddress = v.results[i].formatted_address;
        //                        }
        //                    }
        //                }
        //            }

        //            var pin1 = new Custompin
        //            {
        //                Type = PinType.Place,
        //                Position = new Position(bLatitude, aLongitude),
        //                Label = startAddress,
        //                //  Url= "http://xamarin.com/about/"
        //                //Address = "Mound Road",
        //                //Id = "Source",

        //            };
        //            // customMap.CustomPins = new List<Custompin> { pin1 };
        //            //customMap.CustomPins = pin1;
        //            //customMap.Pins.Add(pin1);

        //            //Current Region
        //            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(bLatitude, aLongitude), Distance.FromMiles(0.5)));

        //        }

        //        else
        //        {

        //        }
        //        var Latlng = aLongitude + " " + bLatitude;
        //        Application.Current.Properties.Remove("CurrentLatLng");
        //        Application.Current.Properties.Add("CurrentLatLng", Latlng);
        //        //    return aLongitude + " " + bLatitude;

        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);

        //        Debug.WriteLine(ex.Message);


        //        //return ex.ToString();
        //    }

        //}
        //public async void LoadGoogleMapNavigation(double latNavigation, double longNavigation)
        //{
        //    try
        //    {
        //        if (CrossConnectivity.Current.IsConnected)
        //        {

        //            double lat = 0;
        //            double lng = 0;
        //            ILocation loc = DependencyService.Get<ILocation>();
        //            loc.locationObtained += (object ss, ILocationEventArgs ee) =>
        //            {

        //                lat = ee.lat;
        //                lng = ee.lng;
        //                longNavigation = lng;
        //                latNavigation = lat;
        //                aLongitude = longNavigation;
        //                bLatitude = latNavigation;
        //            };

        //            loc.ObtainMyLocation();
        //            loc = null;

        //            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Black);
        //            await Task.Delay(4000);
        //            aLongitude = longNavigation;

        //            bLatitude = latNavigation;


        //            var v = await LoadStartAddress(bLatitude, aLongitude);
        //            if (v.status == "OK")
        //            {
        //                if (v.results != null)
        //                {
        //                    for (int i = 0; i < v.results.Count; i++)
        //                    {
        //                        if (i == 0)
        //                        {
        //                            startAddress = v.results[i].formatted_address;
        //                        }
        //                    }
        //                }
        //            }

        //            var pin1 = new Custompin
        //            {
        //                Type = PinType.Place,
        //                Position = new Position(bLatitude, aLongitude),
        //                Label = startAddress,
        //                //  Url= "http://xamarin.com/about/"
        //                //Address = "Mound Road",
        //                //Id = "Source",

        //            };
        //            // customMap.CustomPins = new List<Custompin> { pin1 };
        //            //customMap.CustomPins = pin1;
        //            //customMap.Pins.Add(pin1);

        //            //Current Region
        //            // customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(bLatitude, aLongitude), Distance.FromMiles(0.5)));

        //        }

        //        else
        //        {

        //        }
        //        var Latlng = aLongitude + " " + bLatitude;
        //        Application.Current.Properties.Remove("CurrentLatLng");
        //        Application.Current.Properties.Add("CurrentLatLng", Latlng);
        //        //    return aLongitude + " " + bLatitude;

        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);

        //        Debug.WriteLine(ex.Message);


        //        //return ex.ToString();
        //    }
        //    if (pageNameTow == "")
        //    {
        //        navigateGoogleMapGarage(latNavigation, longNavigation);
        //        UserDialogs.Instance.HideLoading();
        //    }
        //}
        //public async void LoadGoogleMap(double latNavigation, double longNavigation)
        //{
        //    try
        //    {
        //        if (CrossConnectivity.Current.IsConnected)
        //        {

        //            double lat = 0;
        //            double lng = 0;
        //            ILocation loc = DependencyService.Get<ILocation>();
        //            loc.locationObtained += (object ss, ILocationEventArgs ee) =>
        //            {
        //                lat = ee.lat;
        //                lng = ee.lng;
        //                longNavigation = lng;
        //                latNavigation = lat;
        //                aLongitude = longNavigation;
        //                bLatitude = latNavigation;
        //            };

        //            loc.ObtainMyLocation();
        //            loc = null;

        //            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Black);
        //            await Task.Delay(4000);
        //            aLongitude = longNavigation;

        //            bLatitude = latNavigation;


        //            var v = await LoadStartAddress(bLatitude, aLongitude);
        //            if (v.status == "OK")
        //            {
        //                if (v.results != null)
        //                {
        //                    for (int i = 0; i < v.results.Count; i++)
        //                    {
        //                        if (i == 0)
        //                        {
        //                            startAddress = v.results[i].formatted_address;
        //                        }
        //                    }
        //                }
        //            }

        //            var pin1 = new Custompin
        //            {
        //                Type = PinType.Place,
        //                Position = new Position(bLatitude, aLongitude),
        //                Label = startAddress,
        //                //  Url= "http://xamarin.com/about/"
        //                //Address = "Mound Road",
        //                //Id = "Source",

        //            };
        //            // customMap.CustomPins = new List<Custompin> { pin1 };
        //            //customMap.CustomPins = pin1;
        //            //customMap.Pins.Add(pin1);

        //            //Current Region
        //            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(bLatitude, aLongitude), Distance.FromMiles(0.5)));

        //        }

        //        else
        //        {

        //        }
        //        var Latlng = aLongitude + " " + bLatitude;
        //        Application.Current.Properties.Remove("CurrentLatLng");
        //        Application.Current.Properties.Add("CurrentLatLng", Latlng);
        //        //    return aLongitude + " " + bLatitude;

        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);

        //        Debug.WriteLine(ex.Message);


        //        //return ex.ToString();
        //    }
        //    if (pageNameTow == "")
        //    {
        //        navigateGoogleMapGarage(latNavigation, longNavigation);
        //        UserDialogs.Instance.HideLoading();
        //    }
        //}

        //public async void navigateGoogleMapGarage(double lat, double longgitude, string mapScroll = null)
        //{

        //    if (mapScroll == "mapScroll")
        //    {

        //        try
        //        {

        //            customMap.Pins.Clear();
        //            if (lat != 0 && longgitude != 0)
        //            {
        //                Custompin pinInfo;
        //                List<Custompin> _pin = new List<Custompin>();
        //                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Black);
        //                var v = await LoadGoogleMapGarageAddress1(lat, longgitude, "Garage");
        //                if (v.status == "OK")
        //                {
        //                    if (v.results != null)
        //                    {
        //                        for (int i = 0; i < v.results.Count; i++)
        //                        {
        //                            var Gargaelat = v.results[i].geometry.location.lat;
        //                            var Gargaelong = v.results[i].geometry.location.lng;
        //                            var address = v.results[i].name;

        //                            var fullAddress = v.results[i].vicinity;
        //                            var ratings = v.results[i].rating;
        //                            var typesofServiceList = v.results[i].types;
        //                            var scope = v.results[i].scope;
        //                            var openingHours = v.results[i].opening_hours;
        //                            var photos = v.results[i].photos;


        //                            pinInfo = new Custompin
        //                            {
        //                                Position = new Xamarin.Forms.Maps.Position(Convert.ToDouble(Gargaelat), Convert.ToDouble(Gargaelong)),
        //                                Type = PinType.Place,
        //                                UniqueId = "GoogleGarage",
        //                                Label = address,
        //                                Id = "garageGoogleIcon",

        //                                //GarageProperty
        //                                shopNameInfo = address,
        //                                GarageFullAddress = fullAddress,
        //                                ratings = ratings,
        //                                typesofServiceList = typesofServiceList,
        //                                Scope = scope,
        //                                opening_hours = openingHours,
        //                                photosICon = photos,
        //                                latNavigation = Gargaelat,
        //                                longitudeNavigation = Gargaelong

        //                            };

        //                            _pin.Add(pinInfo);
        //                            customMap.Pins.Add(pinInfo);
        //                        }
        //                        customMap.CustomPins = _pin;
        //                        //  UserDialogs.Instance.HideLoading();
        //                        customMap.HasScrollEnabled = true;
        //                        customMap.HasZoomEnabled = true;
        //                        UserDialogs.Instance.HideLoading();
        //                    }
        //                }
        //                else
        //                {
        //                    //await DisplayAlert("Alert", v.status, "OK");
        //                }


        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Debug.WriteLine(ex.Message);
        //            UserDialogs.Instance.HideLoading();
        //        }

        //        UserDialogs.Instance.HideLoading();
        //    }
        //    else
        //    {
        //        try
        //        {
        //            customMap.Pins.Clear();
        //            if (lat != 0 && longgitude != 0)
        //            {
        //                Custompin pinInfo;
        //                List<Custompin> _pin = new List<Custompin>();
        //                var v = await LoadGoogleMapGarageAddress(lat, longgitude, "Garage");
        //                if (v.status == "OK")
        //                {
        //                    if (v.results != null)
        //                    {
        //                        for (int i = 0; i < v.results.Count; i++)
        //                        {
        //                            var Gargaelat = v.results[i].geometry.location.lat;
        //                            var Gargaelong = v.results[i].geometry.location.lng;
        //                            var address = v.results[i].name;

        //                            var fullAddress = v.results[i].vicinity;
        //                            var ratings = v.results[i].rating;
        //                            var typesofServiceList = v.results[i].types;
        //                            var scope = v.results[i].scope;
        //                            var openingHours = v.results[i].opening_hours;
        //                            var photos = v.results[i].photos;


        //                            pinInfo = new Custompin
        //                            {
        //                                Position = new Xamarin.Forms.Maps.Position(Convert.ToDouble(Gargaelat), Convert.ToDouble(Gargaelong)),
        //                                Type = PinType.Place,
        //                                UniqueId = "GoogleGarage",
        //                                Label = address,
        //                                Id = "garageGoogleIcon",

        //                                //GarageProperty
        //                                shopNameInfo = address,
        //                                GarageFullAddress = fullAddress,
        //                                ratings = ratings,
        //                                typesofServiceList = typesofServiceList,
        //                                Scope = scope,
        //                                opening_hours = openingHours,
        //                                photosICon = photos,
        //                                latNavigation = Gargaelat,
        //                                longitudeNavigation = Gargaelong

        //                            };

        //                            _pin.Add(pinInfo);
        //                            customMap.Pins.Add(pinInfo);
        //                        }
        //                        customMap.CustomPins = _pin;
        //                        //  UserDialogs.Instance.HideLoading();
        //                        customMap.HasScrollEnabled = true;
        //                        customMap.HasZoomEnabled = true;
        //                    }
        //                }
        //                else
        //                {
        //                    // await DisplayAlert("Alert", v.status, "OK");
        //                }


        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Debug.WriteLine(ex.Message);
        //        }
        //    }
        //}


        //public async void NavigationRoute(double latNavition = 0, double longinavigation = 0, int? ShopOwnerIDS = 0, int serviceEnumIDInfo = 0, string priceInfo = "", int ReceiverIDStatic = 0, string Typeofservicechoosed = null)
        //{
        //    try
        //    {
        //        customMap.Pins.Clear();

        //        if (latNavition != 0 && longinavigation != 0)
        //        {
        //            string startAddressInfo = string.Empty;
        //            //UserDialogs.Instance.ShowLoading("Loading...", MaskType.Black);
        //            var v = await LoadStartAddress(latNavition, longinavigation);
        //            if (v.status == "OK")
        //            {
        //                if (v.results != null)
        //                {
        //                    for (int i = 0; i < v.results.Count; i++)
        //                    {
        //                        if (i == 0)
        //                        {
        //                            startAddressInfo = v.results[i].formatted_address;
        //                        }
        //                    }
        //                }
        //            }
        //            var NavigationRoute = new Custompin
        //            {
        //                Type = PinType.Place,

        //                Position = new Position(latNavition, longinavigation),
        //                Label = startAddressInfo,
        //                shopOwnerId = (serviceEnumIDInfo),
        //                ServiceEnumId = Convert.ToInt16(ShopOwnerIDS),
        //                Price = priceInfo,
        //                Receiverid = ReceiverIDStatic,
        //                PageName = "ShopOwnerShopListPopup",
        //                latNavigation = latNavition,
        //                longitudeNavigation = longinavigation,


        //            };
        //            customMap.CustomPins = new List<Custompin> { NavigationRoute };
        //            //customMap.CustomPins = pin1;
        //            customMap.Pins.Add(NavigationRoute);
        //            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latNavition, longinavigation), Distance.FromMiles(0.5)));
        //            customMap.HasZoomEnabled = true;
        //            //  UserDialogs.Instance.HideLoading();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //}
        //public async Task<GarageAddressGoogleInfo> LoadGoogleMapGarageAddress(double sorceLat, double sourceLong, string service)
        //{
        //    GarageAddressGoogleInfo getdetails = null;
        //    try
        //    {
        //        // https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=13.060499,80.254417&radius=5000&name=Garage&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s

        //        if (service == "Garage")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Garage" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "OilChange")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=OilChange" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "CarWash")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=CarWash" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }

        //        else if (service == "Tint")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Tint" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "Tires")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Tires" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "Brakes")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Brakes" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "Detailing")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Detailing" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "Tow")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Tow" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "CarRepair")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=CarRepair" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "E-test")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=E-test" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "Safety-test")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Safety-test" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Unable to getInfo " + ex.ToString());

        //        Debug.WriteLine(ex.Message);
        //    }
        //    return getdetails;
        //}
        //public async Task<GarageAddressGoogleInfo> LoadGoogleMapGarageAddress1(double sorceLat, double sourceLong, string service)
        //{
        //    GarageAddressGoogleInfo getdetails = null;
        //    try
        //    {
        //        // https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=13.060499,80.254417&radius=5000&name=Garage&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s

        //        if (service == "Garage")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Garage" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "OilChange")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=OilChange" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "CarWash")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=CarWash" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }

        //        else if (service == "Tint")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Tint" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "Tires")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Tires" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "Brakes")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Brakes" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "Detailing")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Detailing" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "Tow")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Tow" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "CarRepair")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=CarRepair" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "E-test")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=E-test" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //        else if (service == "Safety-test")
        //        {
        //            var requestUri = "location=" + sorceLat + "," + sourceLong + "&radius=500&name=Safety-test" + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //            HttpClient client = new HttpClient();
        //            client.BaseAddress = new Uri(coreApp.GarageMapUri);
        //            HttpResponseMessage response = await client.GetAsync("?" + requestUri);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var str = await response.Content.ReadAsStringAsync();
        //                getdetails = JsonConvert.DeserializeObject<GarageAddressGoogleInfo>(str);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Unable to getInfo " + ex.ToString());

        //        Debug.WriteLine(ex.Message);
        //    }
        //    return getdetails;
        //}
        //public async Task<StartAddressInfo> LoadStartAddress(double sorceLat, double sourceLong)
        //{
        //    StartAddressInfo getdetails = null;
        //    try
        //    {
        //        // https://maps.googleapis.com/maps/api/geocode/json?address=13.0822294,80.2755023&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s
        //        var requestUri = "address=" + sorceLat + "," + sourceLong + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //        HttpClient client = new HttpClient();
        //        client.BaseAddress = new Uri(coreApp.StartAddressApiUri);
        //        HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var str = await response.Content.ReadAsStringAsync();
        //            getdetails = JsonConvert.DeserializeObject<StartAddressInfo>(str);
        //        }
        //    }
        //    catch (Exception ex)

        //    {
        //        Debug.WriteLine(ex.Message);
        //    }

        //    return getdetails;
        //}


        //public async void DrawRoute(double destnationLat, double destnationLongitutde)
        //{
        //    try
        //    {
        //        string _routeDistance = string.Empty;
        //        if (destnationLat != 0 || destnationLongitutde != 0)
        //        {
        //            customMap.RouteCoordinates.Clear();
        //            customMap.Pins.Clear();

        //            var v = await IndividualDetails(destnationLat, destnationLongitutde);

        //            if (v.status == "OK")
        //            {
        //                customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(bLatitude, aLongitude), Distance.FromMiles(1.0)));
        //                // customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(43.6981483, -79.5898204), Distance.FromMiles(1.0)));
        //                foreach (var route in v.routes)
        //                {
        //                    if (route.legs != null)
        //                    {
        //                        foreach (var leg in route.legs)
        //                        {

        //                            _routeDistance = leg.distance.text;
        //                            endAddress = leg.end_address;
        //                            startAddress = leg.start_address;
        //                            if (leg.steps != null)
        //                            {
        //                                for (int i = 0; i < leg.steps.Count; i++)
        //                                {
        //                                    if (i == 0)
        //                                    {
        //                                        customMap.RouteCoordinates.Add(new Position(leg.steps[i].start_location.lat, leg.steps[i].start_location.lng));
        //                                    }
        //                                    else
        //                                    {
        //                                        customMap.RouteCoordinates.Add(new Position(leg.steps[i].end_location.lat, leg.steps[i].end_location.lng));
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            // customMap.CustomPins.Clear();
        //            List<Custompin> _pin = new List<Custompin>();
        //            var pinstart = new Custompin
        //            {
        //                Type = PinType.Place,

        //                Position = new Position(bLatitude, aLongitude),
        //                Label = startAddress,
        //                UniqueId = "Source",
        //                //  Url= "http://xamarin.com/about/"
        //                //Address = "Mound Road",
        //                Id = "Source",

        //            };
        //            // customMap.CustomPins = new List<Custompin> { pin1};
        //            //customMap.CustomPins = pin1;
        //            //customMap.Pins.Add(pinstart);
        //            var pin1 = new Custompin
        //            {
        //                Type = PinType.Place,
        //                Position = new Position(destnationLat, destnationLongitutde),
        //                Label = endAddress,
        //                UniqueId = "Destination",
        //                // Address = endAddress,
        //                Id = "Destination",

        //            };

        //            _pin.Add(pinstart);
        //            _pin.Add(pin1);
        //            //  customMap.CustomPins.Clear();

        //            customMap.Pins.Add(pinstart);
        //            customMap.Pins.Add(pin1);

        //            customMap.CustomPins = _pin;
        //            customMap.HasScrollEnabled = true;
        //            customMap.HasZoomEnabled = true;



        //            await PopupNavigation.Instance.PushAsync(new DistancePopup(_routeDistance));
        //        }
        //        else
        //        {

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //}




        //public async Task<DrawRouteInfo> IndividualDetails(double Deslatitude, double Deslangitude)
        //{
        //    DrawRouteInfo getdetails = null;
        //    try
        //    {
        //        var requestUri = "origin=" + bLatitude + "," + aLongitude + "&" + "destination=" + Deslatitude + "," + Deslangitude + "&key=AIzaSyB9fsbfXDpS39C7kGaHeY_HUC7kdDv5b7s";

        //        HttpClient client = new HttpClient();
        //        client.BaseAddress = new Uri(coreApp.MapUri);
        //        HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var str = await response.Content.ReadAsStringAsync();
        //            getdetails = JsonConvert.DeserializeObject<DrawRouteInfo>(str);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.Message);
        //    }
        //    return getdetails;
        //}
        //public async Task<string> geoLocation()
        //{

        //    try
        //    {
        //        if (CrossConnectivity.Current.IsConnected)
        //        {
        //            var status = PermissionStatus.Unknown;

        //            status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);


        //            if (status == PermissionStatus.Granted)
        //            {
        //                var locator = CrossGeolocator.Current;
        //                //await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 10, true);
        //                locator.DesiredAccuracy = 50;

        //                if (locator.IsGeolocationEnabled && locator.IsGeolocationAvailable)
        //                {
        //                    // var position = await locator.GetLastKnownLocationAsync();
        //                    var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));

        //                    // var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

        //                    aLongitude = position.Longitude;

        //                    bLatitude = position.Latitude;

        //                    locator.PositionChanged += Locator_PositionChanged;
        //                    var v = await LoadStartAddress(bLatitude, aLongitude);
        //                    if (v.status == "OK")
        //                    {
        //                        if (v.results != null)
        //                        {
        //                            for (int i = 0; i < v.results.Count; i++)
        //                            {
        //                                if (i == 0)
        //                                {
        //                                    startAddress = v.results[i].formatted_address;
        //                                }
        //                            }
        //                        }
        //                    }

        //                    var pin1 = new Custompin
        //                    {
        //                        Type = PinType.Place,
        //                        Position = new Position(bLatitude, aLongitude),
        //                        Label = startAddress,
        //                        //  Url= "http://xamarin.com/about/"
        //                        //Address = "Mound Road",
        //                        Id = "Source",

        //                    };
        //                    customMap.CustomPins = new List<Custompin> { pin1 };
        //                    //customMap.CustomPins = pin1;
        //                    customMap.Pins.Add(pin1);

        //                    //Current Region
        //                    customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(bLatitude, aLongitude), Distance.FromMiles(0.5)));

        //                }

        //                else
        //                {
        //                    //bool displayFlag = false;
        //                    //if (!displayFlag)
        //                    //{
        //                    //    displayFlag = true;
        //                    //    await DisplayAlert("Alert", "Please turn on your location", "Ok");

        //                    //    displayFlag = false;
        //                    //}
        //                }
        //            }
        //            else
        //            {

        //            }
        //        }
        //        else
        //        {
        //            //  await DisplayAlert("ERROR", "CONNECTION TO THE INTERNET HAS BEEN LOST", "OK");
        //        }
        //        var Latlng = aLongitude + " " + bLatitude;
        //        Application.Current.Properties.Remove("CurrentLatLng");
        //        Application.Current.Properties.Add("CurrentLatLng", Latlng);
        //        return aLongitude + " " + bLatitude;

        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);

        //        Debug.WriteLine(ex.Message);

        //        //bool displayFlag = false;
        //        //if (!displayFlag)
        //        //{
        //        //    displayFlag = true;
        //        //    await  DisplayAlert("Alert", "Please turn on your location", "Ok");
        //        //    //if (resp)
        //        //    //{
        //        //    //    //Logout
        //        //    //}
        //        //    displayFlag = false;
        //        //}

        //        return ex.ToString();
        //    }

        //}
        //// var GarageMapUri = "https://maps.googleapis.com/maps/api/geocode/json";
        // var GarageMapUri = "https://maps.googleapis.com/maps/api/geocode/xml";

        // //AIzaSyAAi7YGeGIOFbDLHuemVJTynHsQ1QJFBgs
        // //https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY
        // //http://maps.googleapis.com/maps/api/geocode/xml?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=true_or_false
        // // var requestUri = "address=" + addressval + "&key=AIzaSyCF_vDPXmp3W80K_q6HbyzHGFOBs36f724";
        // var requestUri = "address=" + addressval + "&sensor=true";
        // HttpClient client = new HttpClient();
        // client.BaseAddress = new Uri(GarageMapUri);
        // HttpResponseMessage response = client.GetAsync("?" + requestUri).Result;
        // if (response.IsSuccessStatusCode)
        // {
        //     var str = await response.Content.ReadAsStringAsync();
        //     var getdetails = JsonConvert.DeserializeObject<LatitudeLongitudeApi>(str);
        // }

    }
}