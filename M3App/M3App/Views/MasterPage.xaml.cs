﻿using M3App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterPage : ContentPage
	{
		public MasterPage ()
		{   
			InitializeComponent ();
            var pagename = base.GetType().Name;
            Application.Current.Properties.Remove("PageName");
            Application.Current.Properties.Add("PageName", pagename);
            Logoutimg.Source = "logout.png";    
            BindingContext = new MasterPageViewModel();            
        }
	}
}