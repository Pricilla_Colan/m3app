﻿using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{	
	public partial class RecoverPassword : ContentPage
	{
        LoginService loginService = new LoginService();
        PasswordRecoverdata Recover = new PasswordRecoverdata();
        public RecoverPassword ()
		{
            try
            {
                InitializeComponent();
                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);

                btnRecoverPassword.Clicked += submit_clicked;

                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    bakicon.Margin = new Thickness(0, 12, 0, 0);
                    TitleBasicinfo.Margin = new Thickness(0, 12, 0, 0);
                }
            }
            catch (Exception ex)
            {

            }

        }
        private void ButtonBack_Tapped(object sender, EventArgs e)
        {
            App.Current.MainPage = new Views.ForgotPassword();
        }
        public void submit_clicked(object sender, EventArgs e)
        {
            Recover.email_id = txtEmail.Text;
            Recover.recover_code = txtCode.Text;
            forgotpassword(Recover);
        }
        public RecoverPasswordModel forgotpassword(PasswordRecoverdata datas)
        {
                var response = loginService.recoverPassword(datas);
                if (response.result == "Success")
                {
                    XFToast.LongMessage(response.message);
                    App.Current.MainPage = new Views.LoginForm();
                }
                else
                {
                    XFToast.LongMessage(response.message);
                }
           
            return response;
        }
    }
}