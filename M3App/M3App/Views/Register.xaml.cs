﻿using Acr.UserDialogs;
using M3App.Cores.APIService;
using M3App.Cores.DataModel;
using M3App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
    public partial class Register : ContentPage
    {
        LoginService Loginservice = new LoginService();
        CompanyListModel companylist = new CompanyListModel();
        RegisterViewModel _registerModel = new RegisterViewModel();
        static int fnamecount = 0, lnamecount = 0, emailcount = 0, passwordcount = 0, confirmpasswordcount, companycount = 0;
        public Register()
        {
            try
            {
                InitializeComponent();
                LoadBlueImages();
                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    //bakbtn.Scale = 1.8;
                    btnSignup.HeightRequest = 50;
                    bakicon.Margin = new Thickness(0, 12, 0, 0);
                    TitleBasicinfo.Margin = new Thickness(0, 12, 0, 0);
                }
                ddlCompany.SelectedIndexChanged += DdlCompany_SelectedIndexChanged;
                txtFirstName.TextChanged += TxtFirstName_TextChanged;
                txtLastName.TextChanged += TxtLastName_TextChanged;
                txtEmail.TextChanged += TxtEmail_TextChanged;
                txtPassword.TextChanged += TxtPassword_TextChanged;
                txtConfirmPassword.TextChanged += TxtConfirmPassword_TextChanged;
                BtnIsNewUser.Clicked += BtnIsNewUser_Clicked;
                this.BindingContext = _registerModel;
                GetCompanyList();
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }
        private void BtnIsNewUser_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new LoginForm();
        }
        public void LoadBlueImages()
        {
            try
            {
                ImgFirstName.Source = ImageSource.FromFile("UserInactive.png");
                ImgFirstName1.Source = ImageSource.FromFile("UserInactive.png");
                ImgLastName.Source = ImageSource.FromFile("UserInactive.png");
                ImgLastName1.Source = ImageSource.FromFile("UserInactive.png");
                ImgEmail.Source = ImageSource.FromFile("MailInactive.png");
                ImgEmail1.Source = ImageSource.FromFile("MailInactive.png");
                ImgPassword.Source = ImageSource.FromFile("PasswordInactive.png");
                ImgPassword1.Source = ImageSource.FromFile("PasswordInactive.png");
                ImgConfirmPassword.Source = ImageSource.FromFile("PasswordInactive.png");
                ImgConfirmPassword1.Source = ImageSource.FromFile("PasswordInactive.png");
                ImgCompany.Source = ImageSource.FromFile("CompanyInactive.png");
                ImgCompany1.Source = ImageSource.FromFile("CompanyInactive.png");
            }
            catch (Exception ex)
            {

            }
        }  
      
        private void TxtFirstName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (fnamecount == 0)
            {
                ImgFirstName.Source = ImageSource.FromFile("UserActive.png");
                ImgFirstName1.Source = ImageSource.FromFile("UserActive.png");
                BoxViewFirstName.Color = Color.FromHex("#141A68");
                fnamecount++;
            }
        }

        private void TxtLastName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lnamecount == 0)
            {
                ImgLastName.Source = ImageSource.FromFile("UserActive.png");
                ImgLastName1.Source = ImageSource.FromFile("UserActive.png");
                BoxViewLastName.Color = Color.FromHex("#141A68");
                lnamecount++;
            }
        }

        private void DdlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (companycount == 0)
            {
                ImgCompany.Source = ImageSource.FromFile("CompanyActive.png");
                ImgCompany1.Source = ImageSource.FromFile("CompanyActive.png");
                ddlCompany.TextColor = Color.FromHex("##1A257F");
                BoxViewCompany.Color = Color.FromHex("#141A68");                
                companycount++;
            }
            Picker data = sender as Picker;
            Company companylist = data.SelectedItem as Company;
            string compname = companylist.name;
            int compid = companylist.id;
            _registerModel.setcompanydata(compname, compid);
        }
        private void TxtEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (emailcount == 0)
            {
                ImgEmail.Source = ImageSource.FromFile("MailActive.png");
                ImgEmail1.Source = ImageSource.FromFile("MailActive.png");
                BoxViewEmail.Color = Color.FromHex("#141A68");
                emailcount++;
            }
        }
        private void TxtPassword_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (passwordcount == 0)
            {
                ImgPassword.Source = ImageSource.FromFile("PasswordActive.png");
                ImgPassword1.Source = ImageSource.FromFile("PasswordActive.png");
                BoxViewPassword.Color = Color.FromHex("#141A68");
                passwordcount++;
            }
        }
        private void TxtConfirmPassword_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (confirmpasswordcount == 0)
            {
                ImgConfirmPassword.Source = ImageSource.FromFile("PasswordActive.png");
                ImgConfirmPassword1.Source = ImageSource.FromFile("PasswordActive.png");
                BoxViewConfirmPassword.Color = Color.FromHex("#141A68");
                confirmpasswordcount++;
            }
        }
        private void ButtonBack_Tapped(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);           
            App.Current.MainPage = new LoginForm();
        }
        public CompanyListModel GetCompanyList()
        {
            try
            {
                var companylist11 = Loginservice.CompanyList();
                ddlCompany.ItemsSource = companylist11.response;
                ddlCompany.SelectedIndex = 0;
                return companylist11;
            }
            catch (Exception ex)
            {                
                return null;
                throw;
            }
        }
    }
}