﻿using Acr.UserDialogs;
using M3App.Cores.DataModel;
using M3App.Cores.Helpers;
using M3App.Models;
using Newtonsoft.Json;
using SignaturePad.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M3App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SubCategoryDetailsDescription : ContentPage
    {
        FormTypeListDownLoadModel formtypelist_model;
        FormTypeList formTypeList_data;
        Form_category_Info CategoryInfo;
        Sub_category_Info _savedatas;
        SignaturePadView signaturePadView;
        public SubCategoryDetailsDescription (FormTypeList formTypeListdata, Sub_category_Info selecteddata, Form_category_Info _Category_Info,FormTypeListDownLoadModel formlistmodel)
		{
            try
            {
                InitializeComponent();
                lblJobId.Text = Settings.JobIdKey;
                sitename.Text = Settings.SiteNameKey;
                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    bakicon.Margin = new Thickness(0, 15, 0, 0);
                    bakicon.Scale = 0.5;
                    TitleBasicinfo.Margin = new Thickness(0, 15, 0, 0);
                    btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 7);
                    saveapp.Margin = new Thickness(3, 3, 3, 10);
                }
                TitleBasicinfo.Text = formTypeListdata.form_short_name;
                cmd_btn.Clicked += Cmd_btn_Clicked;
                backPageTap.Tapped += BackPageTap_Tapped;
                CategoryInfo = _Category_Info;
                formtypelist_model = formlistmodel;
                btnBasicInfo.Clicked += BtnBasicInfo_Clicked;
                _savedatas = selecteddata;
                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
                Application.Current.Properties.Remove("NavPageNameSave");
                Application.Current.Properties.Add("NavPageNameSave", pagename);
                Application.Current.Properties.Remove("SelectedSubData");
                Application.Current.Properties.Add("SelectedSubData", selecteddata);
                btnApprovalSave.Clicked += BtnApprovalSave_Clicked;
                formTypeList_data = formTypeListdata;
                LoadSubCategoryListDescription(selecteddata);
            }
            catch (Exception ex)
            {

            }
        }
        private void BtnBasicInfo_Clicked(object sender, EventArgs e)
        {

            App.Current.MainPage = new Views.BasicInfoDetails(formTypeList_data);
        }
        private async void BackPageTap_Tapped(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                App.Current.MainPage = new M3App.Views.SubCategoryInfoDetails(formTypeList_data, CategoryInfo, formtypelist_model);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }

        public void LoadSubCategoryListDescription(Sub_category_Info infoitem)
        {
            try
            {
                if (infoitem.fields != null)
                {
                    var sub_category_info_Add = infoitem.fields;
                    btnApprovalSave.IsVisible = true;
                    btnApprovalSave.StyleId = "SubCategoryFields_Save";
                    foreach (var data in infoitem.fields)
                    {
                        //Subsearchbind
                        Subcategoryinfodetails(GridCategoryInfo, data);
                        Application.Current.Properties.Remove("SignImageSubCatList");
                        Application.Current.Properties.Add("SignImageSubCatList", infoitem.fields);
                    }

                    Headergrid.IsVisible = true;
                    Header.Text = infoitem.name;
                    Header.IsVisible = true;
                    Description.IsVisible = true;
                    Description.Text = infoitem.description;
                }
                //Fields Empty
                else
                {
                    btnApprovalSave.IsVisible = false;
                    //BasicInfoFrame.IsVisible = false;
                    CategoryFrame.IsVisible = false;
                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("No data Available");
                    }
                    else
                    {
                        XFToast.LongMessage("No data Available");
                    }
                    //XFToast.LongMessage("No data Available");
                }
            }
            catch (Exception ex)
            {

            }

        }

        void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {

        }

        private void OnDateSelected(object sender, DateChangedEventArgs e)
        {
            try
            {
                var _startDate = sender as DatePicker;
                var dateselectedm = _startDate.Date.ToString("MM-dd-yyyy");
                var tc = Application.Current.Properties["EntrySubDate"];
                Label ptk = tc as Label;
                ptk.TextColor = Color.Blue;
                ptk.Text = dateselectedm;
            }
            catch (Exception ex)
            {

            }
        }

        private void BtnApprovalSave_Clicked(object sender, EventArgs e)
        {
            SaveApproval();
        }

        public async void SaveApproval(string CommentsStatus = null, List<DownloadComments> dts = null)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
            try
            {
                var Subcategorydatas = GridCategoryInfo.Children;
                var Categorydatas = GridCategoryInfo.Children;
                List<Xamarin.Forms.View> Subcategorydata = new List<Xamarin.Forms.View>();
                foreach (var item in Categorydatas)
                {
                    var condichkd = item.GetType();
                    var _typesrc = condichkd.FullName;
                    if (_typesrc == "Xamarin.Forms.Grid")
                    {
                        var k = item as Grid;
                        var _gditm = k.Children.ToList();
                        foreach (var itm in _gditm)
                        {
                            Subcategorydata.Add(itm);
                        }
                    }
                    else
                    {
                        Subcategorydatas.Add(item);
                    }
                }
                Application.Current.Properties.Remove("CategorySubDetails");
                Application.Current.Properties.Add("CategorySubDetails", Subcategorydata);
                SaveAllFields _fields = new SaveAllFields();
                _fields.SaveApproval(formtypelist_model, btnApprovalSave.StyleId,null, _savedatas);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
            }
        }
        private void DatePicker_DateSelected(object sender, DateChangedEventArgs e)
        {


        }
        Image Unchecbox;
        Image Checkedchecbox;
        public void Subcategoryinfodetails(Grid grid, Subcategory_Info_fields fields)
        {
            try
            {
                categorygrid.IsVisible = true;
                if (fields.type == "text" && fields.visible == "true")
                {
                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    Label label = new Label
                    {
                        StyleId = "Label",
                        ClassId = "SubCategoryLabel",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13,
                    };

                    Entry entry = new Entry
                    {
                        StyleId = "Entry",
                        ClassId = "SubCategoryEntry",
                        FontAttributes = FontAttributes.None,
                        TextColor = Color.Blue,
                        Text = fields.value.ToString(),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        FontSize = 14
                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(entry, 0, smgridrow + 1);
                    if (fields.editable == "false")
                        entry.IsEnabled = false;
                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }
                else if (fields.type == "checkbox" && fields.visible == "true")
                {
                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    var reqrow = bggridrow;
                    List<string> multivalues = new List<string>();
                    string multival = fields.value.ToString();
                    if (multival.Contains("\n"))
                    {
                        multivalues = JsonConvert.DeserializeObject<List<string>>(multival);
                    }

                    string val = fields.options.ToString();
                    List<string> lstValues = JsonConvert.DeserializeObject<List<string>>(val);

                    Label Labelname = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        HorizontalOptions = LayoutOptions.Start,
                        FontSize = 13,

                    };
                    _sg.Children.Add(Labelname, 0, smgridrow);

                    for (int i = 0; i <= lstValues.Count; i++)
                    {
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        if (multivalues != null && multivalues.Count > 0)
                        {
                            Unchecbox = new Image
                            {
                                StyleId = multivalues.Contains(lstValues[i - 1]) ? "Check" : "UnCheck",
                                Source = multivalues.Contains(lstValues[i - 1]) ? "Checked.png" : "UnChecked.png",
                                Scale = 0.5,
                                Margin = new Thickness(5, 0, 10, 0),
                                HorizontalOptions = LayoutOptions.Start,
                                VerticalOptions = LayoutOptions.Center,
                                AutomationId = lstValues[i - 1],
                            };
                        }
                        else
                        {
                            Unchecbox = new Image
                            {
                                StyleId = fields.value.ToString() == lstValues[i - 1] ? "Check" : "UnCheck",
                                Source = fields.value.ToString() == lstValues[i - 1] ? "Checked.png" : "UnChecked.png",
                                Scale = 0.5,
                                Margin = new Thickness(5, 0, 10, 0),
                                HorizontalOptions = LayoutOptions.Start,
                                VerticalOptions = LayoutOptions.Center,
                                AutomationId = lstValues[i - 1],
                                ClassId = "Checked",
                            };
                        }
                        var Repeat_image = new TapGestureRecognizer();
                        Repeat_image.Tapped += (s, e) =>
                        {
                            var imgappr = (s) as Image;
                            var SingleGrid = GridCategoryInfo.Children.ToList();
                            if (imgappr.StyleId == "Check")
                            {
                                imgappr.StyleId = "UnCheck";
                                imgappr.Source = "UnChecked.png";
                            }
                            else if (imgappr.StyleId == "UnCheck")
                            {
                                imgappr.StyleId = "Check";
                                imgappr.Source = "Checked.png";
                            }
                        };
                        Unchecbox.GestureRecognizers.Add(Repeat_image);

                        Label checkvalue = new Label
                        {

                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = lstValues[i],
                            TextColor = Color.Black,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Center,
                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                            FontSize = 13,

                        };

                        _sg.Children.Add(Unchecbox, 0, i);
                        _sg.Children.Add(checkvalue, 0, i);
                    }

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }
                else if (fields.type == "radio" && fields.visible == "true")
                {
                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    var reqrow = bggridrow;

                    List<string> lstValues = new List<string>();
                    string val = fields.options.ToString();
                    lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                    Label radioLabelname = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalOptions = LayoutOptions.Start,
                        FontSize = 13,
                    };
                    _sg.Children.Add(radioLabelname, 0, smgridrow);
                    for (int i = 1; i <= lstValues.Count; i++)
                    {
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        Image UnCheckradiobutton = new Image
                        {
                            StyleId = fields.value.ToString() == lstValues[i - 1] ? "Radio" : "UnRadio",
                            Source = fields.value.ToString() == lstValues[i - 1] ? "Check_Button.png" : "Uncheck_Button.png",
                            Scale = 0.2,
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            AutomationId = lstValues[i - 1],
                            ClassId = "Checkbutton",
                        };
                        var unradio = new TapGestureRecognizer();
                        unradio.Tapped += (s, e) =>
                        {
                            var imgappr = (s) as Image;
                            var SingleImg1 = imgappr.ClassId;
                            var selectedStyleId = imgappr.StyleId;
                            var SingleGrid = GridCategoryInfo.Children.ToList();
                            foreach (var item in SingleGrid)
                            {
                                var condichkd = item.GetType();
                                var _typesrc = condichkd.FullName;
                                if (_typesrc == "Xamarin.Forms.Grid")
                                {
                                    var k = item as Grid;
                                    var _gditm = k.Children.ToList();
                                    foreach (var itm in _gditm)
                                    {
                                        if (s.GetType() == itm.GetType())
                                        {
                                            if (itm.StyleId == "UnRadio" || itm.StyleId == "Radio")
                                            {
                                                Image img1 = (Image)itm;
                                                img1.StyleId = "UnRadio";
                                                img1.Source = "Uncheck_Button.png";
                                            }
                                        }
                                    }
                                }
                                else
                                {

                                }
                            }
                            if (imgappr.StyleId == "UnRadio")
                            {
                                imgappr.StyleId = "Radio";
                                imgappr.Source = "Check_Button.png";
                            }
                        };
                        UnCheckradiobutton.GestureRecognizers.Add(unradio);

                        Label radiovalue = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = lstValues[i - 1],
                            TextColor = Color.Black,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Center,
                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                            FontSize = 13,
                        };

                        _sg.Children.Add(UnCheckradiobutton, 0, i);
                        _sg.Children.Add(radiovalue, 1, i);
                    }
                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }
                else if (fields.type == "dropdown" && fields.visible == "true")
                {
                    List<string> lstValues = new List<string>();
                    List<string> lst = new List<string>();
                    if (fields.options != null)
                    {
                        string val = fields.options.ToString();
                        lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                    }
                    else
                    {
                    }
                    string CurrentData = "";
                    if (!string.IsNullOrEmpty(fields.value.ToString()))
                    {
                        CurrentData = fields.value.ToString();
                    }
                    Grid _sg = new Grid();
                    _sg.RowSpacing = 0;
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    CustomPicker customPicker = new CustomPicker
                    {
                        StyleId = "CustomPicker",
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.Center,
                        ItemsSource = lstValues,
                        SelectedItem = CurrentData,
                        ClassId = "Picker",
                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(customPicker, 0, smgridrow + 1);
                    if (fields.editable == "false")
                        customPicker.IsEnabled = false;

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;

                }
                else if (fields.type == "textarea" && fields.visible == "true")
                {
                    Grid _sg = new Grid();
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                    _sg.RowSpacing = 0;
                    var _bggrid = GridCategoryInfo.Children.ToList();
                    int bggridchild = GridCategoryInfo.Children.Count;

                    var bggridrow = 0;
                    var smgridrow = 0;
                    for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                        bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                    }

                    int smgridchild = _sg.Children.Count;
                    for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                    {
                        var column = Grid.GetColumn(_sg.Children[childIndex]);
                        smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                    }
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 13
                    };

                    Editor Address = new Editor
                    {
                        StyleId = "Textarea",
                        FontAttributes = FontAttributes.None,
                        TextColor = Color.Blue,
                        Text = fields.value.ToString(),
                        FontSize = 13,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        ClassId = "Areatext",

                    };

                    _sg.Children.Add(label, 0, smgridrow);
                    _sg.Children.Add(Address, 0, smgridrow + 1);
                    if (fields.editable == "false")
                        Address.IsEnabled = false;

                    GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                    CategoryFrame.Content = GridCategoryInfo;
                }
                else if (fields.type == "date" && fields.visible == "true")
                {
                    try
                    {
                        Grid _sg = new Grid();
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        _sg.RowSpacing = 0;
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;
                        for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                            bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                        }

                        int smgridchild = _sg.Children.Count;
                        for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(_sg.Children[childIndex]);
                            smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                        }
                        string reqdate;
                        if (fields.value == "")
                        {
                            reqdate = null;
                        }
                        else
                        {

                            if (fields.value != null && fields.value != "")
                            {
                                reqdate = fields.value;
                            }
                            else
                            {
                                reqdate = null;
                            }
                        }

                        Label label = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            FontSize = 13
                        };

                        Label datePicker;
                        var tchk = fields.value;
                        StackLayout _lyt = new StackLayout();

                        if (tchk != "" && tchk != null)
                        {
                            datePicker = new Label
                            {
                                StyleId = "DatePicker",
                                Text = reqdate.ToString(),
                                TextColor = Color.Blue,
                                VerticalOptions = LayoutOptions.Center,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                ClassId = "PickerDate",
                            };
                        }
                        else
                        {
                            datePicker = new Label
                            {
                                StyleId = "DatePicker",
                                Text = "Select Date",
                                TextColor = Color.White,
                                VerticalOptions = LayoutOptions.Center,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                ClassId = "PickerDate",
                            };

                        }
                        BoxView bv = new BoxView
                        {
                            HeightRequest = 1,
                            Margin = new Thickness(0, 0, 5, 0),
                            Color = Color.LightGray,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalOptions = LayoutOptions.Center,
                        };


                        var forgetPassword_tap = new TapGestureRecognizer();
                        forgetPassword_tap.Tapped += (s, e) =>
                        {
                            var entryfield = s as Label;
                            EntrySaveMthd(entryfield);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                StartDatePickerShow.Focus();
                            });

                            StartDatePickerShow.Format = "MM-dd-yyyy";
                            StartDatePickerShow.DateSelected += DatePicker_DateSelected;
                        };
                        datePicker.GestureRecognizers.Add(forgetPassword_tap);

                        _sg.Children.Add(label, 0, smgridrow);
                        _sg.Children.Add(datePicker, 0, smgridrow + 1);
                        _sg.Children.Add(bv, 0, smgridrow + 2);
                        if (fields.editable == "false")
                            datePicker.IsEnabled = false;
                        //_lyt.IsEnabled = false;
                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;
                    }
                    catch (Exception ex)
                    {
                        UserDialogs.Instance.HideLoading();
                        ex.Message.ToString();
                    }
                }
                else if (fields.type == "signature" && fields.visible == "true")
                {
                    try
                    {

                        Grid _sg = new Grid();

                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        _sg.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                        _sg.RowSpacing = 0;
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;

                        bggridrow = bggridchild;
                        int smgridchild = _sg.Children.Count;
                        smgridrow = smgridchild;

                        Label label = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            FontSize = 13
                        };

                        ImageSource retSource = null;

                        signaturePadView = new SignaturePadView
                        {
                            StyleId = "CategorySignature",
                            BackgroundColor = Color.White,
                            StrokeColor = Color.Black,
                            HeightRequest = 70,
                            StrokeWidth = 2,
                        };
                        var forgetPassword_tap = new TapGestureRecognizer();
                        signaturePadView.ClearLabel.GestureRecognizers.Add(forgetPassword_tap);
                        _sg.Children.Add(label, 0, smgridrow);
                        _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                        _sg.Children.Add(signaturePadView, 0, smgridrow + 1);

                        if (fields.value.ToString() != null && fields.value.ToString() != "")
                        {
                            signaturePadView.ClearLabel.IsVisible = true;

                            Application.Current.Properties.Remove("SignImageCatSub");
                            Application.Current.Properties.Add("SignImageCatSub", fields.value.ToString());

                            Application.Current.Properties.Remove("ImageHavingCat");
                            Application.Current.Properties.Add("ImageHavingCat", "True");
                            byte[] Base64Stream = Convert.FromBase64String(fields.value.ToString());
                            retSource = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                            Image sign = new Image
                            {
                                Source = retSource,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                VerticalOptions = LayoutOptions.Center,
                                Scale = 0.4,
                            };
                            ImageSetting(sign);
                            _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                            _sg.Children.Add(sign, 0, smgridrow + 1);
                        }
                        forgetPassword_tap.Tapped += (s, e) =>
                        {
                            var button = (Label)s;
                            var item = GridCategoryInfo.Children.ToList();
                            for (int i = 0; i < item.Count; i++)
                            {
                                var mk = item[i] as Grid;
                                var con = mk.Children.Contains(button);
                                if (con)
                                {
                                    var kl = mk as Grid;
                                    var kl1 = kl.Children.ToList();
                                    foreach (var t in kl1)
                                    {
                                        var _mm = t as Image;
                                        if (_mm != null)
                                        {
                                            _mm.IsVisible = false;
                                        }
                                    }
                                }
                            }
                            var row = Grid.GetRow(button);
                            var gridm = button.Parent as Grid;
                            var image = gridm.Children.Where(c => Grid.GetRow(c) == row && Grid.GetColumn(c) == 1);
                        };

                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;
                    }
                    catch (Exception ex)
                    {
                        UserDialogs.Instance.HideLoading();
                        ex.Message.ToString();
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }

        public void ImageSetting(Image p = null)
        {
            Application.Current.Properties.Remove("Image");
            Application.Current.Properties.Add("Image", p);
        }
        public void EntrySaveMthd(Label p)
        {
            Application.Current.Properties.Remove("EntrySubDate");
            Application.Current.Properties.Add("EntrySubDate", p);
        }

        private async void Cmd_btn_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new Views.CommentsPage(formtypelist_model, formTypeList_data);
        }

    }
}