﻿using M3App.Cores.DataModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SignaturePad.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using M3App.Cores.Helpers;
using Acr.UserDialogs;
using M3App.Models;
using M3App.Cores.APIService;
using Plugin.Connectivity;

namespace M3App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SubCategoryInfoDetails : ContentPage
	{
        Form_category_Info _categorydata;
        FormTypeList formdatas;
        SignaturePadView signaturePadView;
        static int Tankcolumncounter = 0;
        static int categoryRowCounter = 0;
        string description = "";
        string Name = "";
        int categoryId = 0;
        static int Tankrowcounter = 0;
        RetrieveOfflineDatas _jobliststorage = new RetrieveOfflineDatas();
        DynamicFormListService dynamicFormListService = new DynamicFormListService();
        FormTypeListDownLoadModel Form_List_Model;
        List<category_Info_fields> category_Info_Fields = new List<category_Info_fields>();
        public SubCategoryInfoDetails (FormTypeList formTypeListdata, Form_category_Info selecteddata, FormTypeListDownLoadModel formlistmodel)
		{
            try
            {
                InitializeComponent();
                _categorydata = selecteddata;
                lblJobId.Text = Settings.JobIdKey;
                sitename.Text = Settings.SiteNameKey;
                backPageTap.Tapped += BackPageTap_Tapped;
                if (Device.RuntimePlatform.ToLower() == "ios")
                {
                    bakicon.Margin = new Thickness(0, 15, 0, 0);
                    bakicon.Scale = 0.5;
                    TitleBasicinfo.Margin = new Thickness(0, 15, 0, 0);
                    btnCategoryDetails.Margin = new Thickness(-15, 10, 10, 7);
                    saveapp.Margin = new Thickness(3, 3, 3, 10);
                }

                btnBasicInfo.Clicked += BtnBasicInfo_Clicked;
                btnApprovalSave.Clicked += BtnApprovalSave_Clicked;
                formdatas = formTypeListdata;
                var pagename = base.GetType().Name;
                cmd_btn.Clicked += Cmd_btn_Clicked;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
                Application.Current.Properties.Remove("NavPageNameSave");
                Application.Current.Properties.Add("NavPageNameSave", pagename);
                Application.Current.Properties.Remove("SelectedData");
                Application.Current.Properties.Add("SelectedData", selecteddata);
                Form_List_Model = formlistmodel;
                Form_List_Model.response.job_id = Settings.JobIdKey;
                Form_List_Model.response.site_id = Settings.SiteNameKey;
                
                Console.WriteLine("Job ID :",Form_List_Model.response.job_id);
                TitleBasicinfo.Text = formTypeListdata.form_short_name;
                LoadData_SubCategoryList(_categorydata);
            }
            catch (Exception ex)
            {

            }
        }
        private async void BtnBasicInfo_Clicked(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                App.Current.MainPage = new Views.BasicInfoDetails(formdatas);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }

        private async void BackPageTap_Tapped(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                //var formUpload = Application.Current.Properties["FormTypeUpload"] as FormTypeUpload;
                App.Current.MainPage = new M3App.Views.CategoryInfoDetails(Form_List_Model, formdatas);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }
        private async void Cmd_btn_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new Views.CommentsPage(Form_List_Model, formdatas);
        }
        private void BtnApprovalSave_Clicked(object sender, EventArgs e)
        {
            SaveApproval();
        }

        public async void SaveApproval(string CommentsStatus = null, List<DownloadComments> dts = null)
        {
            UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
            await Task.Delay(100);
            try
            {
                var Categorydatas = GridCategoryInfo.Children;
                List<Xamarin.Forms.View> categorydata = new List<Xamarin.Forms.View>();
                foreach (var item in Categorydatas)
                {
                    var condichkd = item.GetType();
                    var _typesrc = condichkd.FullName;
                    if (_typesrc == "Xamarin.Forms.Grid")
                    {
                        var k = item as Grid;
                        var _gditm = k.Children.ToList();
                        foreach (var itm in _gditm)
                        {
                            categorydata.Add(itm);
                        }
                    }
                    else
                    {
                        categorydata.Add(item);
                    }
                }
                Application.Current.Properties.Remove("CategoryDetails");
                Application.Current.Properties.Add("CategoryDetails", categorydata);
                var tankdatas = GridApplicableStatus.Children;
                List<Xamarin.Forms.View> tankdata = new List<Xamarin.Forms.View>();
                foreach (var item in tankdatas)
                {
                    var condichkd = item.GetType();
                    var _typesrc = condichkd.FullName;
                    if (_typesrc == "Xamarin.Forms.Grid")
                    {
                        var k = item as Grid;
                        var _gditm = k.Children.ToList();
                        foreach (var itm in _gditm)
                        {
                            tankdata.Add(itm);
                        }
                    }
                    else
                    {
                        tankdata.Add(item);
                    }
                }
                Application.Current.Properties.Remove("TankDetails");
                Application.Current.Properties.Add("TankDetails", tankdata);
              
                SaveAllFields _fields = new SaveAllFields();
                _fields.SaveApproval(Form_List_Model, btnApprovalSave.StyleId, _categorydata);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
            }
        }
        public void LoadData_SubCategoryList(Form_category_Info infoitem)
        {
            try
            {
                if (infoitem.sub_category != null)
                {
                    SubCategoryScroll.IsVisible = false;
                    SubCategoryList.IsVisible = true;
                    SubCategoryInfo.IsVisible = true;

                    if (infoitem.sub_category.Count > 0)
                        SubCategoryInfo.ItemsSource = infoitem.sub_category;
                }
                else if (infoitem.sub_category == null)
                {
                    //Start
                    Header.IsVisible = false;
                    Headergrid.IsVisible = false;
                    Description.IsVisible = false;

                    categoryRowCounter = 0;
                    GridApplicableStatus.Children.Clear();
                    Tankcolumncounter = 0;
                    Tankrowcounter = 0;
                    description = string.Empty;
                    Name = string.Empty;
                    btnBasicInfo.BackgroundColor = Color.FromHex("#FFFFFF");
                    btnBasicInfo.TextColor = Color.FromHex("#1A257F");
                    btnCategoryDetails.BackgroundColor = Color.FromHex("#1A257F");
                    btnCategoryDetails.TextColor = Color.FromHex("#FFFFFF");
                    GridCategoryInfo.Children.Clear();

                    description = infoitem.description;
                    Name = infoitem.name;
                    categoryId = infoitem.id;
                    if (Name != null)
                    {
                        Header.Text = Name;
                    }

                    if (!string.IsNullOrEmpty(description))
                    {
                        Header.IsVisible = true;
                        Headergrid.IsVisible = true;
                        Description.IsVisible = true;
                        Description.Text = description;
                    }
                    else
                    {
                        Header.IsVisible = true;
                        Headergrid.IsVisible = true;
                    }

                    //end
                    category_Info_Fields = infoitem.fields;
                    int count = 0;

                    if (infoitem.sub_category != null)
                    {
                        SubCategoryScroll.IsVisible = false;
                        SubCategoryList.IsVisible = true;
                        SubCategoryInfo.IsVisible = true;
                        if (infoitem.sub_category.Count > 0)
                            SubCategoryInfo.ItemsSource = infoitem.sub_category.OrderBy(s => s.id);
                    }
                    else if (infoitem.sub_category == null)
                    {
                        GridCategoryInfo.IsVisible = true;
                        SubCategoryList.IsVisible = false;
                        SubCategoryInfo.IsVisible = false;
                        SubCategoryScroll.IsVisible = true;
                        btnApprovalSave.IsVisible = true;
                        btnApprovalSave.StyleId = "CategoryFields_Save";
                        CategoryFrame.IsVisible = true;
                        btnApprovalSave.IsEnabled = true;
                        category_Info_Fields = infoitem.fields;
                        if (infoitem.fields != null)
                        {
                            //Valli ji 
                            if (Form_List_Model.response.form_name == "SAMPLE FORM FOR MONTHLY UNDERGROUND STORAGE SYSTEM INSPECTION CHECKLIST")
                            {
                                saveapp.IsVisible = true;
                                foreach (var data in infoitem.fields)
                                {
                                    if (count <= 1)
                                    {
                                        categoryinfodetails(GridCategoryInfo, data);
                                        Application.Current.Properties.Remove("SignImageCat");
                                        Application.Current.Properties.Add("SignImageCat", infoitem.fields);
                                        count++;
                                    }
                                }

                                if (count >= 2)
                                {
                                    for (int i = 2; i < infoitem.fields.Count; i++)
                                    {
                                        TankDetails(GridApplicableStatus, infoitem.fields[i]);
                                    }
                                }
                            }
                            else
                            {
                                saveapp.IsVisible = true;
                                foreach (var data in infoitem.fields)
                                {
                                    categoryinfodetails(GridCategoryInfo, data);
                                    Application.Current.Properties.Remove("SignImageCat");
                                    Application.Current.Properties.Add("SignImageCat", infoitem.fields);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }
        public void TankDetails(Grid grid, category_Info_fields fields)
        {
            try
            {
                //string status = Application.Current.Properties["Tank"].ToString();
                Tankdetails.IsVisible = true;
                GridApplicableStatusScroll.IsVisible = true;
                TankFrame.IsVisible = true;
                GridApplicableStatus.IsVisible = true;
                if (fields.type == "text" && fields.visible == "true")
                {
                    Label label = new Label
                    {
                        StyleId = "Label",
                        FontAttributes = FontAttributes.None,
                        Text = fields.label_name,
                        TextColor = Color.Gray,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        FontSize = 13,
                    };

                    Entry entry = new Entry
                    {
                        StyleId = "Entry",
                        FontAttributes = FontAttributes.None,
                        TextColor = Color.Blue,
                        Text = fields.value.ToString(),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        FontSize = 13,
                    };
                    if (fields.editable == "false")
                    {
                        entry.IsEnabled = false;
                    }

                    if (Tankcolumncounter == 2)
                    {
                        Tankrowcounter = 2;
                        Tankcolumncounter = 0;
                        GridApplicableStatus.Children.Add(label, Tankcolumncounter, Tankrowcounter);
                        Tankrowcounter++;
                        GridApplicableStatus.Children.Add(entry, Tankcolumncounter, Tankrowcounter);
                        TankFrame.Content = GridApplicableStatus;
                        Tankrowcounter--;
                        Tankcolumncounter++;
                    }
                    else
                    {
                        GridApplicableStatus.Children.Add(label, Tankcolumncounter, Tankrowcounter);
                        Tankrowcounter++;
                        GridApplicableStatus.Children.Add(entry, Tankcolumncounter, Tankrowcounter);
                        TankFrame.Content = GridApplicableStatus;
                        Tankrowcounter--;
                        Tankcolumncounter++;
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        Image Unchecbox;
        Image Checkedchecbox;
        public void categoryinfodetails(Grid grid, category_Info_fields fields)
        {
            try
            {
                grid.RowSpacing = 0;
                grid.ColumnSpacing = 0;
                categorygrid.IsVisible = true;
                SubCategoryScroll.IsVisible = true;
                btnApprovalSave.IsVisible = true;
                if (Form_List_Model.response.form_name == "SAMPLE FORM FOR MONTHLY UNDERGROUND STORAGE SYSTEM INSPECTION CHECKLIST")
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                    if (fields.type == "text" && fields.visible == "true")
                    {
                        Grid _sg = new Grid();
                        _sg.RowSpacing = 0;
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;
                        for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                            bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                        }

                        int smgridchild = _sg.Children.Count;
                        for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(_sg.Children[childIndex]);
                            smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                        }
                        Label label = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            FontSize = 13,
                        };

                        Entry entry = new Entry
                        {
                            StyleId = "Entry",
                            FontAttributes = FontAttributes.None,
                            TextColor = Color.Blue,
                            Text = fields.value.ToString(),
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalOptions = LayoutOptions.Center,
                            FontSize = 13,
                        };

                        _sg.Children.Add(label, 0, smgridrow);
                        _sg.Children.Add(entry, 0, smgridrow + 1);


                        if (fields.editable == "false")
                            entry.IsEnabled = false;

                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;



                    }
                    else if (fields.type == "checkbox" && fields.visible == "true")
                    {

                        //New source

                        Grid _sg = new Grid();
                        _sg.RowSpacing = 0;
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;
                        for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                            bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                        }

                        int smgridchild = _sg.Children.Count;
                        for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(_sg.Children[childIndex]);
                            smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                        }
                        var reqrow = bggridrow;


                        Unchecbox = new Image
                        {
                            StyleId = "UnCheck",
                            Source = "UnChecked.png",
                            Scale = 0.5,
                            Margin = new Thickness(5, 0, 10, 0),
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,

                        };
                        Checkedchecbox = new Image
                        {
                            StyleId = "Check",
                            Source = "Checked.png",
                            Scale = 0.5,
                            Margin = new Thickness(5, 0, 10, 0),
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            IsVisible = false

                        };
                        //needed
                        if (fields.value.ToString() == "true")
                        {
                            Checkedchecbox.IsVisible = true;
                            Unchecbox.IsVisible = false;
                            TankFrame.IsVisible = false;
                            GridApplicableStatusScroll.IsVisible = false;
                            Application.Current.Properties.Remove("Tank");
                            Application.Current.Properties.Add("Tank", "false");
                        }
                        else
                        {
                            Unchecbox.IsVisible = true;
                            Checkedchecbox.IsVisible = false;
                            TankFrame.IsVisible = true;
                            GridApplicableStatusScroll.IsVisible = true;
                            Application.Current.Properties.Remove("Tank");
                            Application.Current.Properties.Add("Tank", "true");

                        }
                        var GestureRecognizer = new TapGestureRecognizer();
                        GestureRecognizer.Tapped += (s, e) =>
                        {
                            Checkedchecbox.IsVisible = true;
                            Unchecbox.IsVisible = false;
                            TankFrame.IsVisible = false;
                            GridApplicableStatusScroll.IsVisible = false;


                        };
                        Unchecbox.GestureRecognizers.Add(GestureRecognizer);

                        var GestureRecognizer1 = new TapGestureRecognizer();
                        GestureRecognizer1.Tapped += (s, e) =>
                        {
                            Unchecbox.IsVisible = true;
                            Checkedchecbox.IsVisible = false;
                            TankFrame.IsVisible = true;
                            GridApplicableStatusScroll.IsVisible = true;

                        };
                        Checkedchecbox.GestureRecognizers.Add(GestureRecognizer1);

                        Label label = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = "Not Applicable",
                            TextColor = Color.Blue,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Center,
                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                            FontSize = 13,
                        };
                        _sg.Children.Add(Unchecbox, 0, smgridrow);
                        _sg.Children.Add(Checkedchecbox, 0, smgridrow);
                        _sg.Children.Add(label, 0, smgridrow);

                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;
                    }
                }
                else
                {
                    GridCategoryInfo.RowSpacing = 0;
                    grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                    if (fields.type == "text" && fields.visible == "true")
                    {

                        Grid _sg = new Grid();
                        _sg.RowSpacing = 0;
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;
                        for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                            bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                        }

                        int smgridchild = _sg.Children.Count;
                        for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(_sg.Children[childIndex]);
                            smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                        }
                        Label label = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            FontSize = 13
                        };

                        Entry entry = new Entry
                        {
                            StyleId = "Entry",
                            FontAttributes = FontAttributes.None,
                            TextColor = Color.Blue,
                            Text = fields.value.ToString(),
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            HorizontalTextAlignment = TextAlignment.Start,
                            VerticalOptions = LayoutOptions.Center,
                            FontSize = 13

                        };

                        _sg.Children.Add(label, 0, smgridrow);
                        _sg.Children.Add(entry, 0, smgridrow + 1);


                        if (fields.editable == "false")
                            entry.IsEnabled = false;

                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;
                    }
                    else if (fields.type == "checkbox" && fields.visible == "true")
                    {
                        Grid _sg = new Grid();
                        _sg.RowSpacing = 0;
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;
                        for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                            bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                        }

                        int smgridchild = _sg.Children.Count;
                        for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(_sg.Children[childIndex]);
                            smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                        }
                        var reqrow = bggridrow;
                        List<string> multivalues = new List<string>();
                        string multival = fields.value.ToString();
                        if (multival.Contains("\n"))
                        {
                            multivalues = JsonConvert.DeserializeObject<List<string>>(multival);
                        }


                        if (fields.options != "" && fields.options != null)
                        {

                            string val = fields.options.ToString();
                            List<string> lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                            Image RepeatUnchecbox = new Image();
                            Label Labelname = new Label
                            {
                                StyleId = "Label",
                                FontAttributes = FontAttributes.None,
                                Text = fields.label_name,
                                TextColor = Color.Gray,
                                VerticalTextAlignment = TextAlignment.Center,
                                HorizontalTextAlignment = TextAlignment.Start,
                                HorizontalOptions = LayoutOptions.Start,
                                FontSize = 13,
                            };

                            _sg.Children.Add(Labelname, 0, smgridrow);
                            for (int i = 1; i <= lstValues.Count; i++)
                            {
                                _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                                if (multivalues != null && multivalues.Count > 0)
                                {
                                    RepeatUnchecbox = new Image
                                    {
                                        StyleId = multivalues.Contains(lstValues[i - 1]) ? "Check" : "UnCheck",
                                        Source = multivalues.Contains(lstValues[i - 1]) ? "Checked.png" : "UnChecked.png",
                                        Scale = 0.5,
                                        Margin = new Thickness(5, 0, 10, 0),
                                        HorizontalOptions = LayoutOptions.Start,
                                        VerticalOptions = LayoutOptions.Center,
                                        AutomationId = lstValues[i - 1],
                                    };
                                }
                                else
                                {
                                    RepeatUnchecbox = new Image
                                    {
                                        StyleId = fields.value.ToString() == lstValues[i - 1] ? "Check" : "UnCheck",
                                        Source = fields.value.ToString() == lstValues[i - 1] ? "Checked.png" : "UnChecked.png",
                                        Scale = 0.5,
                                        Margin = new Thickness(5, 0, 10, 0),
                                        HorizontalOptions = LayoutOptions.Start,
                                        VerticalOptions = LayoutOptions.Center,
                                        AutomationId = lstValues[i - 1],
                                    };
                                }


                                var Repeat_image = new TapGestureRecognizer();
                                Repeat_image.Tapped += (s, e) =>
                                {
                                    var imgappr1 = (s) as Image;
                                    var SingleGrid1 = GridCategoryInfo.Children.ToList();



                                    if (imgappr1.StyleId == "Check")
                                    {
                                        imgappr1.StyleId = "UnCheck";
                                        imgappr1.Source = "UnChecked.png";
                                    }
                                    else if (imgappr1.StyleId == "UnCheck")
                                    {
                                        imgappr1.StyleId = "Check";
                                        imgappr1.Source = "Checked.png";
                                    }
                                };
                                RepeatUnchecbox.GestureRecognizers.Add(Repeat_image);

                                Label checkvalue = new Label
                                {

                                    StyleId = "Label",
                                    FontAttributes = FontAttributes.None,
                                    Text = lstValues[i - 1],
                                    TextColor = Color.Black,
                                    VerticalTextAlignment = TextAlignment.Center,
                                    HorizontalTextAlignment = TextAlignment.Center,
                                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                                    FontSize = 13,

                                };

                                _sg.Children.Add(RepeatUnchecbox, 0, i);
                                _sg.Children.Add(checkvalue, 0, i);

                            }
                            GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                            CategoryFrame.Content = GridCategoryInfo;
                        }
                        else
                        {

                            Image RepeatUnchecbox = new Image();
                            Label Labelname = new Label
                            {
                                StyleId = "Label",
                                FontAttributes = FontAttributes.None,
                                Text = fields.label_name,
                                TextColor = Color.Gray,
                                VerticalTextAlignment = TextAlignment.Center,
                                HorizontalTextAlignment = TextAlignment.Start,
                                HorizontalOptions = LayoutOptions.Start,
                                FontSize = 13,
                            };

                            _sg.Children.Add(Labelname, 0, smgridrow);
                            var _val = "";
                            _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                            if (fields.value == "false")
                            {
                                _val = "UnCheck";
                            }

                            RepeatUnchecbox = new Image
                            {
                                StyleId = fields.value.ToString() == _val ? "Check" : "UnCheck",
                                Source = fields.value.ToString() == _val ? "Checked.png" : "UnChecked.png",
                                Scale = 0.5,
                                Margin = new Thickness(5, 0, 10, 0),
                                HorizontalOptions = LayoutOptions.Start,
                                VerticalOptions = LayoutOptions.Center,
                                AutomationId = _val,
                            };
                            var Repeat_image = new TapGestureRecognizer();
                            Repeat_image.Tapped += (s, e) =>
                            {
                                var imgappr1 = (s) as Image;
                                var SingleGrid1 = GridCategoryInfo.Children.ToList();
                                if (imgappr1.StyleId == "Check")
                                {
                                    imgappr1.StyleId = "UnCheck";
                                    imgappr1.Source = "UnChecked.png";
                                }
                                else if (imgappr1.StyleId == "UnCheck")
                                {
                                    imgappr1.StyleId = "Check";
                                    imgappr1.Source = "Checked.png";
                                }
                            };
                            RepeatUnchecbox.GestureRecognizers.Add(Repeat_image);

                            _sg.Children.Add(RepeatUnchecbox, 0, smgridrow + 1);

                            GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                            CategoryFrame.Content = GridCategoryInfo;

                        }
                    }
                    else if (fields.type == "radio" && fields.visible == "true")
                    {
                        Grid _sg = new Grid();
                        _sg.RowSpacing = 0;
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;
                        for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                            bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                        }

                        int smgridchild = _sg.Children.Count;
                        for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(_sg.Children[childIndex]);
                            smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                        }
                        var reqrow = bggridrow;
                        List<string> RadioValues = new List<string>();
                        string Radioval = fields.options.ToString();
                        RadioValues = JsonConvert.DeserializeObject<List<string>>(Radioval);
                        Label radioLabelname = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalOptions = LayoutOptions.Start,
                            FontSize = 13,
                        };
                        _sg.Children.Add(radioLabelname, 0, smgridrow);
                        for (int i = 1; i <= RadioValues.Count; i++)
                        {
                            _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                            Image UnCheckradiobutton = new Image
                            {
                                StyleId = fields.value.ToString() == RadioValues[i - 1] ? "Radio" : "UnRadio",
                                Source = fields.value.ToString() == RadioValues[i - 1] ? "Check_Button.png" : "Uncheck_Button.png",
                                Scale = 0.4,
                                HorizontalOptions = LayoutOptions.Start,
                                VerticalOptions = LayoutOptions.Center,
                                AutomationId = RadioValues[i - 1],
                            };

                            var unradio1 = new TapGestureRecognizer();
                            unradio1.Tapped += (s, e) =>
                            {
                                var imgappr = (s) as Image;
                                var SingleImg1 = imgappr.ClassId;
                                var selectedStyleId = imgappr.StyleId;
                                var SingleGrid = GridCategoryInfo.Children.ToList();


                                foreach (var item in SingleGrid)
                                {
                                    var condichkd = item.GetType();
                                    var _typesrc = condichkd.FullName;
                                    if (_typesrc == "Xamarin.Forms.Grid")
                                    {
                                        var k = item as Grid;
                                        var _gditm = k.Children.ToList();
                                        foreach (var itm in _gditm)
                                        {
                                            if (s.GetType() == itm.GetType())
                                            {
                                                if (itm.StyleId == "UnRadio" || itm.StyleId == "Radio")
                                                {
                                                    Image img1 = (Image)itm;
                                                    img1.StyleId = "UnRadio";
                                                    img1.Source = "Uncheck_Button.png";
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                                if (imgappr.StyleId == "UnRadio")
                                {
                                    imgappr.StyleId = "Radio";
                                    imgappr.Source = "Check_Button.png";
                                }
                            };
                            UnCheckradiobutton.GestureRecognizers.Add(unradio1);

                            Label radiovalue = new Label
                            {
                                StyleId = "Label",
                                FontAttributes = FontAttributes.None,
                                Text = RadioValues[i - 1],
                                TextColor = Color.Black,
                                VerticalTextAlignment = TextAlignment.Center,
                                HorizontalTextAlignment = TextAlignment.Center,
                                HorizontalOptions = LayoutOptions.CenterAndExpand,
                                FontSize = 14,
                            };

                            _sg.Children.Add(UnCheckradiobutton, 0, i);
                            _sg.Children.Add(radiovalue, 0, i);
                        }
                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;
                    }

                    else if (fields.type == "signature" && fields.visible == "true")
                    {
                        Grid _sg = new Grid();
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        _sg.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                        _sg.RowSpacing = 0;
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;
                        bggridrow = bggridchild;
                        int smgridchild = _sg.Children.Count;
                        smgridrow = smgridchild;

                        Label label = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            FontSize = 13
                        };

                        ImageSource retSource = null;

                        signaturePadView = new SignaturePadView
                        {
                            StyleId = "CategorySignature",
                            BackgroundColor = Color.White,
                            StrokeColor = Color.Black,
                            HeightRequest = 70,
                            StrokeWidth = 2,
                        };

                        var forgetPassword_tap = new TapGestureRecognizer();
                        signaturePadView.ClearLabel.GestureRecognizers.Add(forgetPassword_tap);
                        _sg.Children.Add(label, 0, smgridrow);
                        _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                        _sg.Children.Add(signaturePadView, 0, smgridrow + 1);

                        if (fields.value.ToString() != null && fields.value.ToString() != "")
                        {
                            signaturePadView.ClearLabel.IsVisible = true;
                            Application.Current.Properties.Remove("ImageHavingCat");
                            Application.Current.Properties.Add("ImageHavingCat", "True");
                            byte[] Base64Stream = Convert.FromBase64String(fields.value.ToString());
                            retSource = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                            Image sign = new Image
                            {
                                Source = retSource,
                                HorizontalOptions = LayoutOptions.FillAndExpand,
                                VerticalOptions = LayoutOptions.Center,
                                Scale = 0.4,
                            };
                            ImageSetting(sign);
                            _sg.Children.Add(signaturePadView.ClearLabel, 0, smgridrow);
                            _sg.Children.Add(sign, 0, smgridrow + 1);
                        }
                        forgetPassword_tap.Tapped += (s, e) =>
                        {
                            var button = (Label)s;
                            var item = GridCategoryInfo.Children.ToList();
                            for (int i = 0; i < item.Count; i++)
                            {
                                var mk = item[i] as Grid;
                                var con = mk.Children.Contains(button);
                                if (con)
                                {
                                    var kl = mk as Grid;
                                    var kl1 = kl.Children.ToList();
                                    foreach (var t in kl1)
                                    {
                                        var _mm = t as Image;
                                        if (_mm != null)
                                        {
                                            _mm.IsVisible = false;
                                        }
                                    }
                                }
                            }
                            var row = Grid.GetRow(button);
                            var gridm = button.Parent as Grid;
                            var image = gridm.Children.Where(c => Grid.GetRow(c) == row && Grid.GetColumn(c) == 1);
                        };

                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;
                    }
                    else if (fields.type == "dropdown" && fields.visible == "true")
                    {
                        string val = fields.options.ToString();
                        string CurrentData = "";

                        List<string> lstValues = JsonConvert.DeserializeObject<List<string>>(val);
                        if (!string.IsNullOrEmpty(fields.value.ToString()))
                        {
                            CurrentData = fields.value.ToString();
                        }
                        Grid _sg = new Grid();
                        _sg.RowSpacing = 0;
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;
                        for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                            bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                        }

                        int smgridchild = _sg.Children.Count;
                        for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(_sg.Children[childIndex]);
                            smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                        }
                        Label label = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            FontSize = 13
                        };

                        CustomPicker customPicker = new CustomPicker
                        {
                            StyleId = "CustomPicker",
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Center,
                            ItemsSource = lstValues,
                            SelectedItem = CurrentData,
                        };
                        _sg.Children.Add(label, 0, smgridrow);
                        _sg.Children.Add(customPicker, 0, smgridrow + 1);
                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;
                    }
                    else if (fields.type == "textarea" && fields.visible == "true")
                    {
                        Grid _sg = new Grid();
                        _sg.RowSpacing = 0;
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;
                        for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                            bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                        }

                        int smgridchild = _sg.Children.Count;
                        for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(_sg.Children[childIndex]);
                            smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                        }
                        Label label = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            FontSize = 13
                        };

                        Editor Address = new Editor
                        {
                            StyleId = "Textarea",
                            FontAttributes = FontAttributes.None,
                            TextColor = Color.Blue,
                            Text = fields.value.ToString(),
                            FontSize = 13,
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalOptions = LayoutOptions.FillAndExpand
                        };

                        _sg.Children.Add(label, 0, smgridrow);
                        _sg.Children.Add(Address, 0, smgridrow + 1);


                        if (fields.editable == "false")
                            Address.IsEnabled = false;

                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;


                    }
                    else if (fields.type == "date" && fields.visible == "true")
                    {
                        Grid _sg = new Grid();
                        _sg.RowSpacing = 0;
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        _sg.RowDefinitions.Add(new RowDefinition { Height = new GridLength(40) });
                        var _bggrid = GridCategoryInfo.Children.ToList();
                        int bggridchild = GridCategoryInfo.Children.Count;

                        var bggridrow = 0;
                        var smgridrow = 0;
                        for (int childIndex = 0; childIndex < bggridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(GridCategoryInfo.Children[childIndex]);
                            bggridrow = Grid.GetRow(GridCategoryInfo.Children[childIndex]) + 1;
                        }

                        int smgridchild = _sg.Children.Count;
                        for (int childIndex = 0; childIndex < smgridchild; ++childIndex)
                        {
                            var column = Grid.GetColumn(_sg.Children[childIndex]);
                            smgridrow = Grid.GetRow(_sg.Children[childIndex]);
                        }

                        DateTime reqdate;
                        if (fields.value == "")
                        {
                            reqdate = DateTime.Now;
                        }
                        else
                        {

                            string formattedDates = "";
                            string t = fields.value.ToString();
                            string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "MM-dd-yyyy", "dd-MM-yyyy", "M-dd-yyyy", "M/dd/yyyy" };
                            DateTime date;
                            if (DateTime.TryParseExact(t, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                                formattedDates = date.ToString("dd/MM/yyyy");
                            if (formattedDates != null && formattedDates != "")
                            {
                                string[] _list = formattedDates.Split('/');
                                var ttt = new DateTime(Convert.ToInt32(_list[2]), Convert.ToInt32(_list[1]), Convert.ToInt32(_list[0]));
                                var dt = ttt;
                                reqdate = dt;
                            }
                            else
                            {
                                reqdate = DateTime.Now;
                            }

                        }
                        Label label = new Label
                        {
                            StyleId = "Label",
                            FontAttributes = FontAttributes.None,
                            Text = fields.label_name,
                            TextColor = Color.Gray,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.Start,
                            FontSize = 13

                        };
                        DatePicker datePicker = new DatePicker
                        {
                            StyleId = "DatePicker",
                            Format = "MM-dd-yyyy",
                            Date = reqdate,
                            TextColor = Color.Blue,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.FillAndExpand,

                        };

                        _sg.Children.Add(label, 0, smgridrow);
                        _sg.Children.Add(datePicker, 0, smgridrow + 1);
                        GridCategoryInfo.Children.Add(_sg, 0, bggridrow);
                        CategoryFrame.Content = GridCategoryInfo;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void ImageSetting(Image p = null)
        {
            Application.Current.Properties.Remove("Image");
            Application.Current.Properties.Add("Image", p);
        }
        public async void buttonSubSearch_Tapped(object sender, EventArgs args)
        {
            try
            {


                var headerval = sender as Grid;
                var selecteditem = (Grid)sender;
                var item = selecteditem.BindingContext;
                var infoitems = item as Sub_category_Info;
                //var infoitem_data= GetFormTypeList(formdatas);

                FormTypeListDownLoadModel infoitem_data = new FormTypeListDownLoadModel();
                if (CrossConnectivity.Current.IsConnected)
                {
                    infoitem_data = GetFormTypeList(formdatas);
                }
                else
                {
                    var tempjoblist = _jobliststorage.SelectAllDatas_FormDisplay(formdatas);
                    FormTypeResponse rs = new FormTypeResponse();
                    if (tempjoblist.Count > 0)
                    {
                        foreach (var itn in tempjoblist)
                        {
                            rs.id = itn.id;
                            rs.job_id = itn.job_id;
                            rs.staff_id = itn.staff_id;
                            rs.site_id = itn.site_id;
                            rs.form_name = itn.form_name;
                            rs.form_type = itn.form_type;
                            rs.form_json = JsonConvert.DeserializeObject<FormJson>(itn.form_json);
                            rs.created_at = itn.created_at;
                            rs.updated_at = itn.updated_at;
                            rs.category_id = itn.category_id;
                            rs.form_id = itn.form_id;
                            rs.access_key = itn.access_key;
                            rs.form_status = itn.form_status;
                        }
                        infoitem_data.response = rs;
                    }

                }

                var t_data = (from m in infoitem_data.response.form_json.category_Info
                              where m.id == infoitems.parent_id
                              select m).ToList();
                var subdet = (from mf in t_data[0].sub_category
                              where mf.id == infoitems.id
                              select mf).FirstOrDefault();


                App.Current.MainPage = new Views.SubCategoryDetailsDescription(formdatas, subdet, _categorydata, Form_List_Model);
            }
            catch (Exception ex)
            {

            }
            // App.Current.MainPage = new Views.SubCategoryDetailsDescription(formdatas, infoitem, _categorydata, Form_List_Model);
        }
        public FormTypeListDownLoadModel GetFormTypeList(FormTypeList Formlistmodel)
        {
            var Formslistdata = dynamicFormListService.Dynamicformdatas(Formlistmodel);
            return Formslistdata;
        }

        void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            
        }

        private void OnDateSelected(object sender, DateChangedEventArgs e)
        {
            try
            {
                var _startDate = sender as DatePicker;
                var dateselectedm = _startDate.Date.ToString("MM-dd-yyyy");
                var tc = Application.Current.Properties["EntrySubDate"];
                Label ptk = tc as Label;
                ptk.TextColor = Color.Blue;
                ptk.Text = dateselectedm;
            }
            catch (Exception ex)
            {

            }
        }

    }
}