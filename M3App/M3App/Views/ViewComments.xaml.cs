﻿using Acr.UserDialogs;
using M3App.Cores.DataModel;
using M3App.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Globalization;
using M3App.Cores.APIService;
using Plugin.Connectivity;
using Newtonsoft.Json;

namespace M3App.Views
{
    public partial class ViewComments : ContentPage
    {
        List<DownloadComments> dwnlodcmt = new List<DownloadComments>();
        DynamicFormListService dynamicFormListService = new DynamicFormListService();
        FormTypeList form_type;
        RetrieveOfflineDatas _jobliststorage = new RetrieveOfflineDatas();
        FormTypeListDownLoadModel formtypelistmodel = new FormTypeListDownLoadModel();
        public ViewComments(List<DownloadComments> downloadComments, FormTypeList formtype)
        {
            try
            {
                InitializeComponent();

                var pagename = base.GetType().Name;
                Application.Current.Properties.Remove("PageName");
                Application.Current.Properties.Add("PageName", pagename);
				backPageTap.Tapped += BackPageTap_Tapped;
                dwnlodcmt = downloadComments;
                form_type = formtype;
                DownloadComments cmd = new DownloadComments();
				if (Device.RuntimePlatform.ToLower() == "ios")
                {
					bakicon.Margin = new Thickness(0, 12, 0, 0);
					TitleBasicinfo.Margin = new Thickness(0, 12, 0, 0);
                }
                Image image = new Image();
                if (downloadComments != null)
                {
                    for (int i = 0; i < downloadComments.Count; i++)
                    {
                        if (downloadComments[i] != null)
                        {
                            byte[] Base64Stream = Convert.FromBase64String(downloadComments[i].initials);
                            downloadComments[i].MyImage = Base64Stream;
                            downloadComments[i].date = downloadComments[i].date;
                            CommentsInfo.ItemsSource = downloadComments;
                       }
                    }
                }
                else
                {
                    //XFToast.ShortMessage("No Comments Here!");

                    if (Device.RuntimePlatform.ToLower() == "ios")
                    {
                        UserDialogs.Instance.Alert("No Comments Here!");
                    }
                    else
                    {
                        XFToast.LongMessage("No Comments Here!");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

       
        public string DateConversion(string dateval)
        {
            var dt = "";
            try
            {
                string tstdt = dateval;
                
                string formattedDates = "";
                var splcha = tstdt.Substring(0, 3);
                var splchb = splcha.Substring(2).ToCharArray();

                string[] _list1;
                _list1 = tstdt.Split(splchb);
                var ttt1 = new DateTime(Convert.ToInt32(_list1[2]), Convert.ToInt32(_list1[0]), Convert.ToInt32(_list1[1]));
                string mm = ttt1.ToString("dd/MM/yyyy");
                string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "MM-dd-yyyy", "dd-MM-yyyy", "M-dd-yyyy", "M/dd/yyyy" };
                DateTime date;

                if (DateTime.TryParseExact(mm, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    formattedDates = date.ToString("dd/MM/yyyy");
                if (formattedDates != null && formattedDates != "")
                {
                    var splch = formattedDates.Substring(0, 3);
                    var splch1 = splch.Substring(2).ToCharArray();
                    string[] _list = formattedDates.Split(splch1);
                    date = new DateTime(Convert.ToInt32(_list[2]), Convert.ToInt32(_list[1]), Convert.ToInt32(_list[0]));
                    dt = date.ToString("MM-dd-yyyy");
                }
                else
                {
                    dt = null;
                }
            }
            catch (Exception ex)
            {

            }
            return dt;
        }
        private async void deleteTapped(object sender, EventArgs e)
        {
            try
            {
                var singlecomments = sender as Grid;
                var valueinfo = singlecomments.BindingContext as DownloadComments;
                await PopupNavigation.Instance.PushAsync(new DisplayAlertMessage(valueinfo, dwnlodcmt));
            }
            catch (Exception ex)
            {

            }
        }

        private async void editTapped(object sender,EventArgs e)
        {
            try
            {
                var singlecomments = sender as Grid;
                var valueinfo = singlecomments.BindingContext as DownloadComments;
                await PopupNavigation.Instance.PushAsync(new EditSingleComment(valueinfo, form_type));
            }
            catch (Exception ex)
            {

            }
            //var commentId=singlecomments.
        }
        public FormTypeListDownLoadModel GetFormTypeList(FormTypeList Formlistmodel)
        {
            var Formslistdata = dynamicFormListService.Dynamicformdatas(Formlistmodel);
            return Formslistdata;
        }
        private async void BackPageTap_Tapped(object sender, EventArgs e)
		{
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...", MaskType.Gradient);
                await Task.Delay(100);
                //var formtypelistmodel = GetFormTypeList(form_type);
                DefaultvalueAssign(formtypelistmodel);
                App.Current.MainPage = new M3App.Views.CommentsPage(formtypelistmodel, form_type);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {

            }
        }
        public void DefaultvalueAssign(FormTypeListDownLoadModel formtypelistmodel)
        {
            try
            {
                var FormListtype = Application.Current.Properties["FormTypeListObject"] as FormTypeList;
                if (formtypelistmodel == null)
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        formtypelistmodel = new FormTypeListDownLoadModel();
                        formtypelistmodel = GetFormTypeList(FormListtype);
                    }
                    else
                    {
                        formtypelistmodel = new FormTypeListDownLoadModel();
                        var tempjoblist = _jobliststorage.SelectAllDatas_FormDisplay(FormListtype);
                        FormTypeResponse rs = new FormTypeResponse();
                        if (tempjoblist.Count > 0)
                        {
                            foreach (var itn in tempjoblist)
                            {
                                rs.id = itn.id;
                                rs.job_id = itn.job_id;
                                rs.staff_id = itn.staff_id;
                                rs.site_id = itn.site_id;
                                rs.form_name = itn.form_name;
                                rs.form_type = itn.form_type;
                                rs.form_json = JsonConvert.DeserializeObject<FormJson>(itn.form_json);
                                rs.created_at = itn.created_at;
                                rs.updated_at = itn.updated_at;
                                rs.category_id = itn.category_id;
                                rs.form_id = itn.form_id;
                                rs.access_key = itn.access_key;
                                rs.form_status = itn.form_status;
                            }
                            formtypelistmodel.response = rs;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}